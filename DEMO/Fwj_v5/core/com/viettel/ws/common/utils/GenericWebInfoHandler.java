package com.viettel.ws.common.utils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by LamNV5 on 5/26/2015.
 */
public class GenericWebInfoHandler implements SOAPHandler<SOAPMessageContext> {
//    private static final QName HEADER_TYPE_LOCATE =
//            new QName("http://service.ws.viettel.com/", "locate");
    private static final QName HEADER_TYPE_GENERIC_WEB_INFO =
            new QName("http://service.ws.viettel.com/", "genericWebInfo");

    private static final Set<QName> HEADER_TYPES =
            new HashSet<QName>(Arrays.asList(new QName[]{HEADER_TYPE_GENERIC_WEB_INFO}));

    private JAXBContext jaxbContext = null;

    public GenericWebInfoHandler() {
        try {
            jaxbContext = JAXBContext.newInstance(GenericWebInfo.class);
        } catch(JAXBException eJaxb) {
            eJaxb.printStackTrace();
        }
    }

    @Override
    public void close(MessageContext messageContext) {
    }

    @Override
    public boolean handleMessage(SOAPMessageContext messageContext) {
        Object[] matchingHeaders = messageContext.getHeaders(HEADER_TYPE_GENERIC_WEB_INFO,
                jaxbContext, true);
        if(matchingHeaders != null && matchingHeaders.length == 1) {
            messageContext.put(GenericWebInfo.class.getName(), matchingHeaders[0]);
        }


        return true;
    }

    @Override
    public Set<QName> getHeaders() {
        return HEADER_TYPES;
    }

    @Override
    public boolean handleFault(SOAPMessageContext messageContext) {
        return true;
    }
}
