package com.viettel.fw.common.util;

import com.viettel.fw.dto.UserDTO;
import com.viettel.ws.common.utils.GenericWebInfo;
import com.viettel.ws.common.utils.Locate;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.cxf.headers.Header;
import org.apache.cxf.jaxws.context.WebServiceContextImpl;
import org.apache.cxf.jaxws.context.WrappedMessageContext;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceContext;
import java.text.MessageFormat;
import java.util.List;

/**
 * Created by LamNV5 on 5/28/2015.
 */
public class GetTextFromBundleHelper {
    private static WebServiceContext wsContext = new WebServiceContextImpl();
    private static final String HEADER_QNAME = "{http://service.ws.viettel.com/}genericWebInfo";
    public static final Logger logger = Logger.getLogger(GetTextFromBundleHelper.class);

    public static Locate getLocate() {
        try {
            GenericWebInfo genericWebInfo = getGenericWebInfo();
            if (DataUtil.isNullOrEmpty(genericWebInfo.getLanguage())) {
                genericWebInfo.setLanguage("vi");
                genericWebInfo.setCountry("VN");
            }
            return new Locate(genericWebInfo.getLanguage(), genericWebInfo.getCountry());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new Locate("vi", "VN");
        }
    }

    public static GenericWebInfo getGenericWebInfo() {
        GenericWebInfo genericWebInfo;
        WrappedMessageContext wrappedMessageContext = null;
        if (BundleUtil.getRunEnvironment() == null) {
            genericWebInfo = new GenericWebInfo();
            genericWebInfo.setReqId(RandomStringUtils.randomNumeric(10));
            return genericWebInfo;
        }

        if ("service".equals(BundleUtil.getRunEnvironment())) {
            if (wsContext == null || wsContext.getMessageContext() == null) {
                return new GenericWebInfo();
            }


            wrappedMessageContext = (WrappedMessageContext) wsContext.getMessageContext();


            genericWebInfo = (GenericWebInfo) (wrappedMessageContext.getWrappedMessage().getContextualProperty(GenericWebInfo.class.getName()));

            if (genericWebInfo == null) {
                final List<Header> headers = (List<Header>) wsContext.getMessageContext().get(Header.HEADER_LIST);
                if (!DataUtil.isNullOrEmpty(headers)) {
                    for (final Header header : headers) {
                        final QName headerQName = header.getName();
                        if (HEADER_QNAME.equals(headerQName.toString())) {
                            final Unmarshaller marshaller;
                            try {
                                marshaller = JAXBContext.newInstance(GenericWebInfo.class).createUnmarshaller();
                                genericWebInfo = (GenericWebInfo) marshaller.unmarshal((Element) header.getObject());
                            } catch (JAXBException e) {
                                logger.error(e.getMessage(), e);
                            }
                        }
                    }
                }
            }
        } else {
            genericWebInfo = new GenericWebInfo();
            genericWebInfo.setLanguage(FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage());
            genericWebInfo.setCountry(FacesContext.getCurrentInstance().getViewRoot().getLocale().getCountry());

            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            genericWebInfo.setReqId(String.valueOf(request.getAttribute(Const.CALL_ID_TOKEN)));
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            genericWebInfo.setIpAddress(ipAddress);
            UserDTO userDTO = (UserDTO) request.getSession().getAttribute(com.viettel.fw.common.util.Const.USER_DTO_TOKEN);
            if (userDTO != null) {
                genericWebInfo.setStaffCode(userDTO.getStaffCode());
                genericWebInfo.setStaffId(userDTO.getStaffId());
                genericWebInfo.setShopCode(userDTO.getShopCode());
                genericWebInfo.setShopId(userDTO.getShopId());
                genericWebInfo.setUserId(userDTO.getUserId());
            }
            try {
                genericWebInfo.setServerPort(String.valueOf(request.getServerPort()));
                genericWebInfo.setServerAddress(String.valueOf(request.getServerName()));
            } catch (Exception e) {
                logger.error("Exception line 103 ", e);
            }
        }

        if (genericWebInfo == null) {
            genericWebInfo = new GenericWebInfo();
            genericWebInfo.setLanguage("vi");
            genericWebInfo.setCountry("VN");
        }
        try {
            if(null!=wrappedMessageContext) {
                String currentAddress = wrappedMessageContext.getWrappedMessage().get("http.base.path").toString().replace("http://", "").replace("https://", "");
                int index = currentAddress.indexOf("/");
                genericWebInfo.setCurrentAddress(currentAddress.substring(0, index));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
//            logger.error(e.getMessage());
        }

        if (DataUtil.isNullOrEmpty(genericWebInfo.getReqId())) {
            genericWebInfo.setReqId(RandomStringUtils.randomNumeric(10));
        }

        return genericWebInfo;
    }

    public static String getText(String key) {
        return BundleUtil.getText(getLocate(), key);
    }

    public static String getTextParam(String key, String... params) {
        return MessageFormat.format(getText(key), params);
    }

    public static String getReqId() {
        return getGenericWebInfo().getReqId();
    }
}
