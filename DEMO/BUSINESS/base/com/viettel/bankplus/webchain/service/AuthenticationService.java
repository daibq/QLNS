package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.model.ChainUser;
import com.viettel.common.DataLogin;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.dto.BaseMessage;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface AuthenticationService {

    @WebMethod
    public DataLogin login(String username, String password) throws Exception;

    //Truongxp
    @WebMethod
    public ChainUserDTO findOne(Long id) throws Exception;

}
