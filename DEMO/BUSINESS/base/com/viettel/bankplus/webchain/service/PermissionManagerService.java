package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.*;
import com.viettel.bankplus.webchain.input.InputLazyPermissionGroupFunctionSearch;
import com.viettel.bankplus.webchain.input.InputLazyUserPermissionGroup;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * @author hungnq on 9/5/2017
 */
@WebService
public interface PermissionManagerService {
    @WebMethod
    public int countLazyUserPermissionGroup(InputLazyUserPermissionGroup inputLazyUserPermissionGroup) throws Exception;

    @WebMethod
    public List<PermissionGroupUserDTO> findLazyUserPermissionGroup(InputLazyUserPermissionGroup inputLazyUserPermissionGroup, int pageSize, int pageNumber) throws Exception;

    @WebMethod
    void saveOrUpdateGroupFunction(List<PermissionGroupDTO> permissionGroupDTOList, List<FunctionObjectDTO> functionObjectDTOList, ActionLogDTO actionLogDTO, boolean isCreate) throws Exception;

    @WebMethod
    public PermissionGroupUserDTO findOne(Long id) throws Exception;

    @WebMethod
    public boolean checkchainUserId(Long chainUserId, Long permissionGroupId) throws Exception;

    @WebMethod
    public void updatePermissionGroupUser(PermissionGroupUserDTO dto, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public void savePermissionGroupUser(PermissionGroupUserDTO dto, List<PermissionGroupDTO> permissionGroupDTOList, List<ChainUserDTO> chainUserDTOList, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public void deletePermissionGroupUser(Long permissionGroupUserId, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public List<ChainUserDTO> getNameByChainUserIdByShopId(Long chainUserId) throws Exception;
    /*@WebMethod
    public List<PermissionGroupDTO> checkPermissionGroupTypOrStartus(Long permissionGroupId) throws Exception;*/

    @WebMethod
    public boolean checkIdPermissionGroupUser(Long id) throws Exception;

    @WebMethod
    public int countLazyGroupFunction(InputLazyPermissionGroupFunctionSearch inputSearch) throws Exception;

    @WebMethod
    public List<GroupPermissionFunctionDTO> findLazyGroupFunction(InputLazyPermissionGroupFunctionSearch inputSearch, int pageSize, int pageNumber) throws Exception;

    @WebMethod
    public List<PermissionGroupUserImportDTO> importPermissionGroupUserDTOTemplate(List<PermissionGroupUserImportDTO> DTOList, ActionLogDTO actionLogDTO) throws Exception;




}
