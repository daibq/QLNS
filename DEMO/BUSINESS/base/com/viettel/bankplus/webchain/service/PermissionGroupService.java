package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.dto.PermissionGroupDTO;
import com.viettel.bankplus.webchain.input.InputLazyFunctionGroupSearch;
import com.viettel.bankplus.webchain.model.PermissionGroup;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface PermissionGroupService {

    @WebMethod
    public PermissionGroupDTO findOne(Long id) throws Exception;

    @WebMethod
    public List<PermissionGroupDTO> findByStatus(boolean isCreate, Long shopId) throws Exception;

    @WebMethod
    public int countLazyFunctionGroup(InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch) throws Exception;

    @WebMethod
    public List<PermissionGroupDTO> findLazingPaging(InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch, int pageSize, int pageNumber) throws Exception;

    @WebMethod
    public void saveorUodate(PermissionGroupDTO permissionGroupDTO, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public void deleteList(PermissionGroupDTO dto, ActionLogDTO actionLogDTO,Long chainShopId) throws Exception;

    @WebMethod
    public boolean checkName(Long chainShopId, String name) throws Exception;
    /*@WebMethod
    public boolean checkPermissionGroupTypOrStartus(Long permissionGroupId) throws Exception;*/

    @WebMethod
    public boolean checkCodeOrId(String code, Long id) throws Exception;

    @WebMethod
    public int listDetailCount(PermissionGroupDTO dto ) throws  Exception;
    @WebMethod
    public List<FunctionObjectDTO> listDetail(PermissionGroupDTO dto, int pageSize, int pageNumber ) throws  Exception;

    //Đại
    @WebMethod
    public List<PermissionGroupDTO> getPermissionGroupDTOListByShopId(Long chainShopId) throws  Exception;
    //đại
    @WebMethod
    public List<PermissionGroupDTO> getPermissionCodeListByShopId(Long chainShopId) throws  Exception;

    //@DongNV
    @WebMethod
    public List<PermissionGroupDTO> importChainShopTemplate(List<PermissionGroupDTO> DTOList, ActionLogDTO actionLogDTO) throws Exception;

    //@DongNV
    @WebMethod
    boolean checkShopCodeOrNameExist(Long chainShopId, String code, String name) throws Exception;

}