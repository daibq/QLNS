package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ApDomainDTO;
import com.viettel.bankplus.webchain.input.InputApdomain;
import com.viettel.bankplus.webchain.model.ApDomain;
import com.viettel.fw.dto.BaseMessage;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ApDomainService {
    @WebMethod
    public ApDomainDTO findOne(Long id) throws Exception;

    @WebMethod
    public int countCtctProgramList(InputApdomain inputApdomainSearch) throws Exception;

    @WebMethod
    public List<ApDomainDTO> findLazingPaging(InputApdomain inputApdomainSearch, int pageSize, int pageNumber) throws Exception;

    @WebMethod
    public void saveorUodate (ApDomainDTO apDomainDTO,ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public  void deleteList(ApDomainDTO dto,ActionLogDTO actionLogDTO) throws  Exception;

    @WebMethod
    public boolean checkName(String type ,String name) throws  Exception;
    @WebMethod
    public boolean checkCodeOrId(String code, Long id) throws Exception;

    @WebMethod
    public List<ApDomainDTO> getValueApDomainByTypeOrderByCode(String type) throws Exception;

    @WebMethod
    public List<ApDomainDTO> getValueApDomainByType(String type) throws Exception;


}