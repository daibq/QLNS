package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.input.InputLazyActionLogSearch;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ActionLogService {

    @WebMethod
    ActionLogDTO findOne(Long id) throws Exception;

    @WebMethod
    Long count(List<FilterRequest> filters) throws Exception;

    @WebMethod
    List<ActionLogDTO> findByFilter(List<FilterRequest> filters) throws Exception;

    @WebMethod
    int countList(InputLazyActionLogSearch input) throws Exception;

    @WebMethod
    List<ActionLogDTO> findLazingPaging(InputLazyActionLogSearch input, int pageSize, int pageNumber) throws Exception;


}