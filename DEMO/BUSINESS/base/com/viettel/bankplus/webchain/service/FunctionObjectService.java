package com.viettel.bankplus.webchain.service;

import java.util.List;

import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.input.InputLazyFunctionSearch;
import com.viettel.fw.common.util.extjs.FilterRequest;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface FunctionObjectService {

    @WebMethod
    public FunctionObjectDTO findOne(Long id) throws Exception;

    @WebMethod
    public Long count(List<FilterRequest> filters) throws Exception;

    @WebMethod
    public int countLazyFunction(InputLazyFunctionSearch inputLazyFunctionSearch) throws Exception;

    @WebMethod
    public List<FunctionObjectDTO> findLazyFunction(InputLazyFunctionSearch inputLazyFunctionSearch, int pageSize, int pageNumber, boolean isLazySearch) throws Exception;

    @WebMethod
    public List<FunctionObjectDTO> findByFilter(List<FilterRequest> filters) throws Exception;

    @WebMethod
    public List<FunctionObjectDTO> findByParentId(Long parentId, Short status) throws Exception;

    @WebMethod
    public List<FunctionObjectDTO> findByParentIdFlowUserId(Long userId, Long parentId, Short status) throws Exception;

    @WebMethod
    public boolean isExistOrderNo(Long parentId, Short oderNo, Long functionObjectId) throws Exception;

    @WebMethod
    public void saveOrUpdate(FunctionObjectDTO functionObjectDTO, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public List<FunctionObjectDTO> getListChild(Long permissionId, boolean isChild) throws Exception;

    public void deleteFunction(Long functionId, ActionLogDTO actionLogDTO) throws Exception;

    //truongnx
    public boolean checkExistCode(String code) throws Exception;

    //truongnx
    public boolean checkExistName(String name, Long parentId) throws Exception;


}