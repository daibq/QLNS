package com.viettel.bankplus.webchain.service;

import java.util.Date;
import java.util.List;

import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.input.InputLazyChainUserSearch;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.fw.dto.BaseMessage;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ChainUserService {

    @WebMethod
    public ChainUserDTO findOne(Long id) throws Exception;

    @WebMethod
    public Long count(List<FilterRequest> filters) throws Exception;

    @WebMethod
    public List<ChainUserDTO> findByFilter(List<FilterRequest> filters) throws Exception;


    //Truongxp
    @WebMethod
    BaseMessage changePassword(String newPassword, String userName, String takeSalt, long id, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public boolean checkPassword(String oldPassword, String takeSalt) throws Exception;

    @WebMethod
    public String takeSaltWithUser(String userName) throws Exception;

    @WebMethod
    boolean checkDuplicatePassword(String newPassword, String takeSalt, String userName) throws Exception;

    @WebMethod
    long findId(String userName) throws Exception;

    @WebMethod
    ChainUserDTO getValueByUserName(String userName) throws Exception;

    @WebMethod
    Date getLastDateChangePass(String userName);

    @WebMethod
    Date getSysDate() throws Exception;

    @WebMethod
    List<ChainUserDTO> getUserByInput(String input) throws Exception;

    //dai
    @WebMethod
    public List<ChainUserDTO> getChainUserByParentShopId(Long chainShopId) throws Exception;

    @WebMethod
    public List<ChainUserDTO> getChainUserByChainShopId(Long chainShopId) throws Exception;

    //@HUNGNQ
    @WebMethod
    public int countLazyChainUser(InputLazyChainUserSearch inputLazyChainUserSearch) throws Exception;

    //@HUNGNQ
    @WebMethod
    public List<ChainUserDTO> findLazyChainUser(InputLazyChainUserSearch inputLazyChainUserSearch, int pageSize, int pageNumber, boolean isLazySearch) throws Exception;

    //@HUNGNQ
    @WebMethod
    public boolean checkUserNameExist(Long chainUserId, String userName) throws Exception;

    //@HUNGNQ
    @WebMethod
    public void saveOrUpdate(ChainUserDTO chainUserDTO, ActionLogDTO actionLogDTO) throws Exception;

    //@HUNGNQ
    @WebMethod
    public void deleteChainUser(Long chainUserId, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public List<ChainUserDTO> importChainUserTemplate(List<ChainUserDTO> chainUserDTOList, ActionLogDTO actionLogDTO) throws Exception;

    @WebMethod
    public ChainUserDTO getByUsername(String username) throws Exception;

    @WebMethod
    public boolean validateActive(Long userId, Long chainId, Long shopId, Long userType) throws Exception;

   /* //duyetdk
    @WebMethod
    public List<ChainUserDTO> getUsernameByChainShopID(Long chainShopId, Long viettelId) throws Exception;*/

    //dai
    public List<ChainUserDTO> getChainUserByShopId(Long chainShopId) throws Exception;

    //truongnx
    @WebMethod
    public List<ChainUserDTO> getChainUserByChainShopIdOrderByName(Long chainShopId) throws Exception;

   /* //checkTopup
    @WebMethod
    public boolean validateActiveTopup(Long userId, Long chainId,Long userType) throws Exception;

    //truongnx_get user topup
    public List<ChainUserDTO> getUserByChainTopupId(Long chainShopId) throws Exception;*/
}