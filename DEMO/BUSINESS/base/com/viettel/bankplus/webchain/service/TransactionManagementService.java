package com.viettel.bankplus.webchain.service;

import vn.com.viettel.bccs.api.CancelTransactionCustomerWebRequest;
import vn.com.viettel.bccs.api.CancelTransactionCustomerWebResponse;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * @author hungnq on 10/24/2017
 */
@WebService
public interface TransactionManagementService {
    @WebMethod
    public CancelTransactionCustomerWebResponse doCancelTransactionCustomer(CancelTransactionCustomerWebRequest request) throws Exception;
}
