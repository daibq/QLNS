/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bankplus.webchain.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author hungn
 */
@Entity
@Table(name = "FUNCTION_OBJECT", catalog = "")
@XmlRootElement
public class FunctionObject implements Serializable {
    public static enum COLUMNS {CREATEDATE, CREATEUSER, DESCRIPTION, FUNCTIONADMINTYPE, FUNCTIONCODE, FUNCTIONNAME, FUNCTIONOBJECTID, FUNCTIONPATH, FUNCTIONTYPE, ORDERNO, PARENTID, STATUS, UPDATEDATE, UPDATEUSER, EXCLUSE_ID_LIST}

    ;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "FUNCTION_OBJECT_ID")
    @SequenceGenerator(name = "FUNCTION_OBJECT_ID_GENERATOR", sequenceName = "FUNCTION_OBJECT_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FUNCTION_OBJECT_ID_GENERATOR")
    private Long functionObjectId;
    @Basic(optional = false)
    @Column(name = "FUNCTION_NAME")
    private String functionName;
    @Column(name = "FUNCTION_CODE")
    private String functionCode;
    @Column(name = "FUNCTION_PATH")
    private String functionPath;
    @Column(name = "FUNCTION_TYPE")
    private Short functionType;
    @Column(name = "FUNCTION_ADMIN_TYPE")
    private Short functionAdminType;
    @Column(name = "STATUS")
    private Short status;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ORDER_NO")
    private Short orderNo;
    @Column(name = "CREATE_USER")
    private String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UPDATE_USER")
    private String updateUser;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "PARENT_ID")
    private Long parentId;

    @Column(name = "PERMISSION_TYPE")
    private Short permissionType;

    public FunctionObject() {
    }

    public FunctionObject(Long functionObjectId) {
        this.functionObjectId = functionObjectId;
    }

    public FunctionObject(Long functionObjectId, String functionName) {
        this.functionObjectId = functionObjectId;
        this.functionName = functionName;
    }

    public Long getFunctionObjectId() {
        return functionObjectId;
    }

    public void setFunctionObjectId(Long functionObjectId) {
        this.functionObjectId = functionObjectId;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getFunctionPath() {
        return functionPath;
    }

    public void setFunctionPath(String functionPath) {
        this.functionPath = functionPath;
    }

    public Short getFunctionType() {
        return functionType;
    }

    public void setFunctionType(Short functionType) {
        this.functionType = functionType;
    }

    public Short getFunctionAdminType() {
        return functionAdminType;
    }

    public void setFunctionAdminType(Short functionAdminType) {
        this.functionAdminType = functionAdminType;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Short getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Short orderNo) {
        this.orderNo = orderNo;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Short getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(Short permissionType) {
        this.permissionType = permissionType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (functionObjectId != null ? functionObjectId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FunctionObject)) {
            return false;
        }
        FunctionObject other = (FunctionObject) object;
        if ((this.functionObjectId == null && other.functionObjectId != null) || (this.functionObjectId != null && !this.functionObjectId.equals(other.functionObjectId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.model.FunctionObject[ functionObjectId=" + functionObjectId + " ]";
    }

}
