package com.viettel.bankplus.webchain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QChainUser is a Querydsl query type for ChainUser
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QChainUser extends EntityPathBase<ChainUser> {

    private static final long serialVersionUID = 1507619836L;

    public static final QChainUser chainUser = new QChainUser("chainUser");

    public final NumberPath<Short> activeStatus = createNumber("activeStatus", Short.class);

    public final StringPath chainShopCode = createString("chainShopCode");

    public final NumberPath<Long> chainShopId = createNumber("chainShopId", Long.class);

    public final NumberPath<Long> chainUserId = createNumber("chainUserId", Long.class);

    public final NumberPath<Short> chainUserType = createNumber("chainUserType", Short.class);

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final StringPath createUser = createString("createUser");

    public final StringPath fullName = createString("fullName");

    public final DateTimePath<java.util.Date> lastChangePasswordDate = createDateTime("lastChangePasswordDate", java.util.Date.class);

    public final StringPath password = createString("password");

    public final StringPath phoneNumber = createString("phoneNumber");

    public final StringPath salt = createString("salt");

    public final NumberPath<Short> status = createNumber("status", Short.class);

    public final DateTimePath<java.util.Date> updateDate = createDateTime("updateDate", java.util.Date.class);

    public final StringPath updateUser = createString("updateUser");

    public final StringPath userName = createString("userName");

    public final DateTimePath<java.util.Date> lastLoginTime = createDateTime("lastLoginTime", java.util.Date.class);

    public final StringPath email = createString("email");

    public QChainUser(String variable) {
        super(ChainUser.class, forVariable(variable));
    }

    public QChainUser(Path<? extends ChainUser> path) {
        super(path.getType(), path.getMetadata());
    }

    public QChainUser(PathMetadata<?> metadata) {
        super(ChainUser.class, metadata);
    }

}

