package com.viettel.bankplus.webchain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QApDomain is a Querydsl query type for ApDomain
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QApDomain extends EntityPathBase<ApDomain> {

    private static final long serialVersionUID = 922667813L;

    public static final QApDomain apDomain = new QApDomain("apDomain");

    public final NumberPath<Long> apDomainId = createNumber("apDomainId", Long.class);

    public final StringPath code = createString("code");

    public final StringPath createBy = createString("createBy");

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final StringPath description = createString("description");

    public final StringPath name = createString("name");

    public final NumberPath<Long> status = createNumber("status", Long.class);

    public final StringPath type = createString("type");

    public final StringPath value = createString("value");

    public QApDomain(String variable) {
        super(ApDomain.class, forVariable(variable));
    }

    public QApDomain(Path<? extends ApDomain> path) {
        super(path.getType(), path.getMetadata());
    }

    public QApDomain(PathMetadata<?> metadata) {
        super(ApDomain.class, metadata);
    }

}

