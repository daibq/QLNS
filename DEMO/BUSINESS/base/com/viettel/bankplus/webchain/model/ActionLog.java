/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bankplus.webchain.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author HoangAnh
 */
@Entity
@Table(name = "ACTION_LOG")
@XmlRootElement
public class ActionLog implements Serializable {
    public static enum COLUMNS {ACTION, ACTIONID, ACTIONLOGID, ACTIONTYPE, CREATEDATE, DESCRIPTION, OBJECTID, OBJECTTYPE, USERNAME, EXCLUSE_ID_LIST};
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ACTION_LOG_ID")
    @SequenceGenerator(name="ACTION_LOG_ID_GENERATOR", sequenceName = "ACTION_LOG_SEQ", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACTION_LOG_ID_GENERATOR")
    private Long actionLogId;
    @Column(name = "USER_NAME")
    private String userName;
    @Column(name = "ACTION_TYPE")
    private String actionType;
    @Column(name = "OBJECT_ID")
    private Long objectId;
    @Column(name = "OBJECT_TYPE")
    private String objectType;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ACTION_ID")
    private Long actionId;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "ACTION")
    private String action;

    public ActionLog() {
    }

    public ActionLog(Long actionLogId) {
        this.actionLogId = actionLogId;
    }

    public Long getActionLogId() {
        return actionLogId;
    }

    public void setActionLogId(Long actionLogId) {
        this.actionLogId = actionLogId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getActionId() {
        return actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }


    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (actionLogId != null ? actionLogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActionLog)) {
            return false;
        }
        ActionLog other = (ActionLog) object;
        if ((this.actionLogId == null && other.actionLogId != null) || (this.actionLogId != null && !this.actionLogId.equals(other.actionLogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.viettel.bccs.product.model.ActionLog[ actionLogId=" + actionLogId + " ]";
    }
    
}
