/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bankplus.webchain.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author hungn
 */
@Entity
@Table(name = "PERMISSION_GROUP_USER", catalog = "")
@XmlRootElement
public class PermissionGroupUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PERMISSION_GROUP_USER_ID")
    @SequenceGenerator(name = "PERMISSION_GROUP_USER_GENERATOR", sequenceName = "PERMISSION_GROUP_USER_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERMISSION_GROUP_USER_GENERATOR")
    private Long permissionGroupUserId;
    @Basic(optional = false)
    @Column(name = "CHAIN_USER_ID")
    private long chainUserId;
    @Basic(optional = false)
    @Column(name = "PERMISSION_GROUP_ID")
    private long permissionGroupId;
    @Column(name = "CREATE_USER")
    private String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "STATUS")
    private Short status;

    public PermissionGroupUser() {
    }

    public PermissionGroupUser(Long permissionGroupUserId) {
        this.permissionGroupUserId = permissionGroupUserId;
    }

    public PermissionGroupUser(Long permissionGroupUserId, long chainUserId, long permissionGroupId) {
        this.permissionGroupUserId = permissionGroupUserId;
        this.chainUserId = chainUserId;
        this.permissionGroupId = permissionGroupId;
    }

    public Long getPermissionGroupUserId() {
        return permissionGroupUserId;
    }

    public void setPermissionGroupUserId(Long permissionGroupUserId) {
        this.permissionGroupUserId = permissionGroupUserId;
    }

    public long getChainUserId() {
        return chainUserId;
    }

    public void setChainUserId(long chainUserId) {
        this.chainUserId = chainUserId;
    }

    public long getPermissionGroupId() {
        return permissionGroupId;
    }

    public void setPermissionGroupId(long permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permissionGroupUserId != null ? permissionGroupUserId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermissionGroupUser)) {
            return false;
        }
        PermissionGroupUser other = (PermissionGroupUser) object;
        if ((this.permissionGroupUserId == null && other.permissionGroupUserId != null) || (this.permissionGroupUserId != null && !this.permissionGroupUserId.equals(other.permissionGroupUserId))) {
            return false;
        }
        /*dai
        * Check ngoaif diều kiên trong truong hop id = null thì check thêm căp(chainUserId,permissionGroupId) là duy nhất thì mới trả ve true
        */
        if(this.permissionGroupUserId == null && other.permissionGroupUserId == null){
            if(this.chainUserId != other.getChainUserId() || this.permissionGroupId != other.getPermissionGroupId())
                return false;
        }
        /*dai-end*/
        return true;
    }

    @Override
    public String toString() {
        return "com.model.PermissionGroupUser[ permissionGroupUserId=" + permissionGroupUserId + " ]";
    }

}
