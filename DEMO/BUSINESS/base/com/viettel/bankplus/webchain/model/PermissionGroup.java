/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bankplus.webchain.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author hungn
 */
@Entity
@Table(name = "PERMISSION_GROUP", catalog = "")
@XmlRootElement
public class PermissionGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PERMISSION_GROUP_ID")
    @SequenceGenerator(name = "PERMISSION_GROUP_ID_GENERATOR", sequenceName = "PERMISSION_GROUP_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERMISSION_GROUP_ID_GENERATOR")
    private Long permissionGroupId;
    @Basic(optional = false)
    @Column(name = "PERMISSION_GROUP_NAME")
    private String permissionGroupName;
    @Basic(optional = false)
    @Column(name = "PERMISSION_GROUP_CODE")
    private String permissionGroupCode;
    @Column(name = "STATUS")
    private Long status;
    @Column(name = "CREATE_USER")
    private String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UPDATE_USER")
    private String updateUser;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "PERMISSION_GROUP_TYPE")
    private Long permissionGroupType;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CHAIN_SHOP_ID")
    private Long chainShopId;
    @Column(name = "USE_OBJECT_TYPE")
    private Long userObjectType;

    public PermissionGroup() {
    }

    public PermissionGroup(Long permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

//    public PermissionGroup(String permissionGroupName, String permissionGroupCode, Long status, String createUser, Date createDate, String updateUser, Date updateDate, Long permissionGroupType, String description, Long chainShopId) {
//        this.permissionGroupName = permissionGroupName;
//        this.permissionGroupCode = permissionGroupCode;
//        this.status = status;
//        this.createUser = createUser;
//        this.createDate = createDate;
//        this.updateUser = updateUser;
//        this.updateDate = updateDate;
//        this.permissionGroupType = permissionGroupType;
//        this.description = description;
//        this.chainShopId = chainShopId;
//    }

    public Long getPermissionGroupId() {
        return permissionGroupId;
    }

    public void setPermissionGroupId(Long permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public String getPermissionGroupName() {
        return permissionGroupName;
    }

    public void setPermissionGroupName(String permissionGroupName) {
        this.permissionGroupName = permissionGroupName;
    }

    public String getPermissionGroupCode() {
        return permissionGroupCode;
    }

    public void setPermissionGroupCode(String permissionGroupCode) {
        this.permissionGroupCode = permissionGroupCode;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getPermissionGroupType() {
        return permissionGroupType;
    }

    public void setPermissionGroupType(Long permissionGroupType) {
        this.permissionGroupType = permissionGroupType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getChainShopId() {
        return chainShopId;
    }

    public void setChainShopId(Long chainShopId) {
        this.chainShopId = chainShopId;
    }

    public Long getUserObjectType() {
        return userObjectType;
    }

    public void setUserObjectType(Long userObjectType) {
        this.userObjectType = userObjectType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permissionGroupId != null ? permissionGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermissionGroup)) {
            return false;
        }
        PermissionGroup other = (PermissionGroup) object;
        if ((this.permissionGroupId == null && other.permissionGroupId != null) || (this.permissionGroupId != null && !this.permissionGroupId.equals(other.permissionGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.model.PermissionGroup[ permissionGroupId=" + permissionGroupId + " ]";
    }

}
