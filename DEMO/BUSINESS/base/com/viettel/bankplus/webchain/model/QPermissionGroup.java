package com.viettel.bankplus.webchain.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;

import javax.annotation.Generated;

import com.mysema.query.types.Path;


/**
 * QPermissionGroup is a Querydsl query type for PermissionGroup
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPermissionGroup extends EntityPathBase<PermissionGroup> {

    private static final long serialVersionUID = 1558868928L;

    public static final QPermissionGroup permissionGroup = new QPermissionGroup("permissionGroup");

    public final DateTimePath<java.util.Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final StringPath createUser = createString("createUser");

    public final StringPath permissionGroupCode = createString("permissionGroupCode");

    public final NumberPath<Long> permissionGroupId = createNumber("permissionGroupId", Long.class);

    public final StringPath description = createString("description");

    public final NumberPath<Long> permissionGroupType = createNumber("permissionGroupType", Long.class);

    public final NumberPath<Long> userObjectType = createNumber("userObjectType", Long.class);

    public final StringPath permissionGroupName = createString("permissionGroupName");

    public final NumberPath<Long> status = createNumber("status", Long.class);

    public final DateTimePath<java.util.Date> updateDate = createDateTime("updateDate", java.util.Date.class);

    public final StringPath updateUser = createString("updateUser");

    public final NumberPath<Long> chainShopId = createNumber("chainShopId", Long.class);

    public QPermissionGroup(String variable) {
        super(PermissionGroup.class, forVariable(variable));
    }

    public QPermissionGroup(Path<? extends PermissionGroup> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPermissionGroup(PathMetadata<?> metadata) {
        super(PermissionGroup.class, metadata);
    }

}

