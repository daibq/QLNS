package com.viettel.bankplus.webchain.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;


@Entity
@Table(name = "AP_DOMAIN")
@XmlRootElement
public class ApDomain {
    @Id
    @Column(name = "AP_DOMAIN_ID")
    @Basic(optional = false)
    @SequenceGenerator(name = "AP_DOMAIN_SEQ", sequenceName = "AP_DOMAIN_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AP_DOMAIN_SEQ")
    private Long apDomainId;
    @Basic
    @Column(name = "TYPE")
    private String type;
    @Basic
    @Column(name = "CODE")
    private String code;
    @Basic
    @Column(name = "NAME")
    private String name;
    @Basic
    @Column(name = "VALUE")
    private String value;

    @Basic
    @Column(name = "STATUS")
    private Long status;
    @Basic
    @Column(name = "CREATE_BY")
    private String createBy;
    @Basic
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic
    @Column(name = "DESCRIPTION")
    private String description;
    public static enum COLUMNS {APDOMAINID, CODE, CREATEBY, CREATEDATE, DESCRIPTION, NAME, STATUS, TYPE, VALUE, EXCLUSE_ID_LIST};


    public Long getApDomainId() {
        return apDomainId;
    }

    public void setApDomainId(Long apDomainId) {
        this.apDomainId = apDomainId;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }


    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }


    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApDomain apDomain = (ApDomain) o;

        if (apDomainId != apDomain.apDomainId) return false;
        if (type != null ? !type.equals(apDomain.type) : apDomain.type != null) return false;
        if (code != null ? !code.equals(apDomain.code) : apDomain.code != null) return false;
        if (name != null ? !name.equals(apDomain.name) : apDomain.name != null) return false;
        if (value != null ? !value.equals(apDomain.value) : apDomain.value != null) return false;
        if (status != null ? !status.equals(apDomain.status) : apDomain.status != null) return false;
        if (createBy != null ? !createBy.equals(apDomain.createBy) : apDomain.createBy != null) return false;
        if (createDate != null ? !createDate.equals(apDomain.createDate) : apDomain.createDate != null) return false;
        if (description != null ? !description.equals(apDomain.description) : apDomain.description != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        Long result = apDomainId;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (createBy != null ? createBy.hashCode() : 0);
        result = 31 * result + (createDate != null ? createDate.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result.intValue();
    }
}
