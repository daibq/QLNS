/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bankplus.webchain.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author hungn
 */
@Entity
@Table(name = "PERMISSION_GROUP_FUNCTION", catalog = "")
@XmlRootElement
public class PermissionGroupFunction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PERMISSION_GROUP_FUNCTION_ID")
    @SequenceGenerator(name = "PERMISSION_GROUP_FUNCTION_ID_GENERATOR", sequenceName = "PERMISSION_GROUP_FUNCTION_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERMISSION_GROUP_FUNCTION_ID_GENERATOR")
    private Long permissionGroupFunctionId;
    @Basic(optional = false)
    @Column(name = "PERMISSION_GROUP_ID")
    private Long permissionGroupId;
    @Basic(optional = false)
    @Column(name = "FUNCTION_OBJECT_ID")
    private Long functionObjectId;
    @Column(name = "CREATE_USER")
    private String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "STATUS")
    private Short status;

    public PermissionGroupFunction() {
    }

    public PermissionGroupFunction(Long permissionGroupFunctionId) {
        this.permissionGroupFunctionId = permissionGroupFunctionId;
    }

    public PermissionGroupFunction(Long permissionGroupFunctionId, long permissionGroupId, long functionObjectId) {
        this.permissionGroupFunctionId = permissionGroupFunctionId;
        this.permissionGroupId = permissionGroupId;
        this.functionObjectId = functionObjectId;
    }

    public Long getPermissionGroupFunctionId() {
        return permissionGroupFunctionId;
    }

    public void setPermissionGroupFunctionId(Long permissionGroupFunctionId) {
        this.permissionGroupFunctionId = permissionGroupFunctionId;
    }

    public Long getPermissionGroupId() {
        return permissionGroupId;
    }

    public void setPermissionGroupId(Long permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public Long getFunctionObjectId() {
        return functionObjectId;
    }

    public void setFunctionObjectId(Long functionObjectId) {
        this.functionObjectId = functionObjectId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permissionGroupFunctionId != null ? permissionGroupFunctionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermissionGroupFunction)) {
            return false;
        }
        PermissionGroupFunction other = (PermissionGroupFunction) object;
        if ((this.permissionGroupFunctionId == null && other.permissionGroupFunctionId != null) || (this.permissionGroupFunctionId != null && !this.permissionGroupFunctionId.equals(other.permissionGroupFunctionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.model.PermissionGroupFunction[ permissionGroupFunctionId=" + permissionGroupFunctionId + " ]";
    }

}
