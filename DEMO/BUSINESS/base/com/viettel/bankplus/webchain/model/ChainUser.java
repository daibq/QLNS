/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.bankplus.webchain.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author hungn
 */
@Entity
@Table(name = "CHAIN_USER", catalog = "")
@XmlRootElement
public class ChainUser implements Serializable {
    public static enum COLUMNS {ACTIVESTATUS, CHAINSHOPCODE, CHAINSHOPID, CHAINUSERID, CHAINUSERTYPE, CREATEDATE, CREATEUSER, FULLNAME, LASTCHANGEPASSWORDDATE, PASSWORD, PHONENUMBER, SALT, STATUS, UPDATEDATE, UPDATEUSER, USERNAME, EXCLUSE_ID_LIST}

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CHAIN_USER_ID")
    @SequenceGenerator(name = "CHAIN_USER_GENERATOR", sequenceName = "CHAIN_USER_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CHAIN_USER_GENERATOR")
    private Long chainUserId;
    @Basic(optional = false)
    @Column(name = "USER_NAME")
    private String userName;
    @Basic(optional = false)
    @Column(name = "FULL_NAME")
    private String fullName;
    @Basic(optional = false)
    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;
    @Column(name = "STATUS")
    private Short status;
    @Column(name = "CREATE_USER")
    private String createUser;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "UPDATE_USER")
    private String updateUser;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Basic(optional = false)
    @Column(name = "CHAIN_SHOP_ID")
    private Long chainShopId;
    @Basic(optional = false)
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "LAST_CHANGE_PASSWORD_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastChangePasswordDate;
    @Column(name = "ACTIVE_STATUS")
    private Short activeStatus;
    @Column(name = "CHAIN_SHOP_CODE")
    private String chainShopCode;
    @Column(name = "CHAIN_USER_TYPE")
    private Short chainUserType;
    @Column(name = "SALT")
    private String salt;
    @Column(name = "LAST_LOGIN_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLoginTime;
    @Column(name = "EMAIL")
    private String email;

    public ChainUser() {
    }

    public ChainUser(Long chainUserId) {
        this.chainUserId = chainUserId;
    }

    public ChainUser(Long chainUserId, String userName, String fullName, String phoneNumber, long chainShopId, String password) {
        this.chainUserId = chainUserId;
        this.userName = userName;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.chainShopId = chainShopId;
        this.password = password;
    }

    public Long getChainUserId() {
        return chainUserId;
    }

    public void setChainUserId(Long chainUserId) {
        this.chainUserId = chainUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Long getChainShopId() {
        return chainShopId;
    }

    public void setChainShopId(Long chainShopId) {
        this.chainShopId = chainShopId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastChangePasswordDate() {
        return lastChangePasswordDate;
    }

    public void setLastChangePasswordDate(Date lastChangePasswordDate) {
        this.lastChangePasswordDate = lastChangePasswordDate;
    }

    public Short getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(Short activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getChainShopCode() {
        return chainShopCode;
    }

    public void setChainShopCode(String chainShopCode) {
        this.chainShopCode = chainShopCode;
    }

    public Short getChainUserType() {
        return chainUserType;
    }

    public void setChainUserType(Short chainUserType) {
        this.chainUserType = chainUserType;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (chainUserId != null ? chainUserId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChainUser)) {
            return false;
        }
        ChainUser other = (ChainUser) object;
        if ((this.chainUserId == null && other.chainUserId != null) || (this.chainUserId != null && !this.chainUserId.equals(other.chainUserId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.model.ChainUser[ chainUserId=" + chainUserId + " ]";
    }

}
