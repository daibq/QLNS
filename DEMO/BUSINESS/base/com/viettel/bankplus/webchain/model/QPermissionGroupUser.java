package com.viettel.bankplus.webchain.model;

import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;

import javax.annotation.Generated;
import java.util.Date;

import static com.mysema.query.types.PathMetadataFactory.forVariable;

/**
 * @author hungnq on 9/7/2017
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPermissionGroupUser extends EntityPathBase<PermissionGroupUser> {
    private static final long serialVersionUID = 1558868928L;
    public static final QPermissionGroupUser permissionGroupUser = new QPermissionGroupUser("permissionGroupUser");

    public final DateTimePath<Date> createDate = createDateTime("createDate", java.util.Date.class);

    public final StringPath createUser = createString("createUser");

    public final NumberPath<Long> permissionGroupUserId = createNumber("permissionGroupUserId", Long.class);

    public final NumberPath<Long> chainUserId = createNumber("chainUserId", Long.class);

    public final NumberPath<Long> permissionGroupId = createNumber("permissionGroupId", Long.class);

    public final NumberPath<Long> status = createNumber("status", Long.class);

    public QPermissionGroupUser(String variable) {
        super(PermissionGroupUser.class, forVariable(variable));
    }

    public QPermissionGroupUser(Path<? extends PermissionGroupUser> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPermissionGroupUser(PathMetadata<?> metadata) {
        super(PermissionGroupUser.class, metadata);
    }
}
