package com.viettel.bankplus.webchain.model;

import com.mysema.query.types.Path;
import com.mysema.query.types.PathMetadata;
import com.mysema.query.types.path.DateTimePath;
import com.mysema.query.types.path.EntityPathBase;
import com.mysema.query.types.path.NumberPath;
import com.mysema.query.types.path.StringPath;

import javax.annotation.Generated;
import java.util.Date;

import static com.mysema.query.types.PathMetadataFactory.forVariable;


/**
 * QActionLog is a Querydsl query type for ActionLog
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QActionLog extends EntityPathBase<ActionLog> {

    private static final long serialVersionUID = 1132135964L;

    public static final QActionLog actionLog = new QActionLog("actionLog");

    public final StringPath action = createString("action");

    public final NumberPath<Long> actionId = createNumber("actionId", Long.class);

    public final NumberPath<Long> actionLogId = createNumber("actionLogId", Long.class);

    public final StringPath actionType = createString("actionType");

    public final DateTimePath<Date> createDate = createDateTime("createDate", Date.class);

    public final StringPath description = createString("description");

    public final NumberPath<Long> objectId = createNumber("objectId", Long.class);

    public final StringPath objectType = createString("objectType");

    public final StringPath userName = createString("userName");

    public QActionLog(String variable) {
        super(ActionLog.class, forVariable(variable));
    }

    public QActionLog(Path<? extends ActionLog> path) {
        super(path.getType(), path.getMetadata());
    }

    public QActionLog(PathMetadata<?> metadata) {
        super(ActionLog.class, metadata);
    }

}

