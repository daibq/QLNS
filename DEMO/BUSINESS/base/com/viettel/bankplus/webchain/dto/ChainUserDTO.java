package com.viettel.bankplus.webchain.dto;

import java.lang.Long;
import java.lang.Short;
import java.util.Date;

import com.viettel.fw.dto.BaseDTO;

import java.io.Serializable;

public class ChainUserDTO extends BaseDTO implements Serializable {

    public static enum COLUMNS {ACTIVESTATUS, CHAINSHOPCODE, CHAINSHOPID, CHAINUSERID, CHAINUSERTYPE, CREATEDATE, CREATEUSER, FULLNAME, LASTCHANGEPASSWORDDATE, PASSWORD, PHONENUMBER, SALT, STATUS, UPDATEDATE, UPDATEUSER, USERNAME, EXCLUSE_ID_LIST}

    ;

    public String getKeySet() {
        return keySet;
    }

    private Short activeStatus;
    private String chainShopCode;
    private Long chainShopId;
    private Long chainUserId;
    private Short chainUserType;
    private Date createDate;
    private String createUser;
    private String fullName;
    private Date lastChangePasswordDate;
    private String password;
    private String phoneNumber;
    private String salt;
    private Short status;
    private Date updateDate;
    private String updateUser;
    private String userName;
    private Date lastLoginTime;
    private String email;
    // add more
    private String chainShopName;// ten cua hang chuoi
    private Long parentShopId; // id  chuoi
    private String parentShopName;//ten chuoi
    private String parentShopCode;// ma chuoi
    private String repeatPassword;
    private String importExcelError;
    private String provinceCode;
    private String chainProvinceCode;
    private String prefixUsername;
    private String chainMobile;
    private String chainAddress;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Short getActiveStatus() {
        return this.activeStatus;
    }

    public void setActiveStatus(Short activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getChainShopCode() {
        return this.chainShopCode;
    }

    public void setChainShopCode(String chainShopCode) {
        this.chainShopCode = chainShopCode;
    }

    public Long getChainShopId() {
        return this.chainShopId;
    }

    public void setChainShopId(Long chainShopId) {
        this.chainShopId = chainShopId;
    }

    public Long getChainUserId() {
        return this.chainUserId;
    }

    public void setChainUserId(Long chainUserId) {
        this.chainUserId = chainUserId;
    }

    public Short getChainUserType() {
        return this.chainUserType;
    }

    public void setChainUserType(Short chainUserType) {
        this.chainUserType = chainUserType;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getLastChangePasswordDate() {
        return this.lastChangePasswordDate;
    }

    public void setLastChangePasswordDate(Date lastChangePasswordDate) {
        this.lastChangePasswordDate = lastChangePasswordDate;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSalt() {
        return this.salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Short getStatus() {
        return this.status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return this.updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Long getParentShopId() {
        return parentShopId;
    }

    public void setParentShopId(Long parentShopId) {
        this.parentShopId = parentShopId;
    }

    public String getParentShopName() {
        return parentShopName;
    }

    public void setParentShopName(String parentShopName) {
        this.parentShopName = parentShopName;
    }

    public String getParentShopCode() {
        return parentShopCode;
    }

    public void setParentShopCode(String parentShopCode) {
        this.parentShopCode = parentShopCode;
    }

    public String getChainShopName() {
        return chainShopName;
    }

    public void setChainShopName(String chainShopName) {
        this.chainShopName = chainShopName;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getImportExcelError() {
        return importExcelError;
    }

    public void setImportExcelError(String importExcelError) {
        this.importExcelError = importExcelError;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getChainProvinceCode() {
        return chainProvinceCode;
    }

    public void setChainProvinceCode(String chainProvinceCode) {
        this.chainProvinceCode = chainProvinceCode;
    }

    public String getPrefixUsername() {
        return prefixUsername;
    }

    public void setPrefixUsername(String prefixUsername) {
        this.prefixUsername = prefixUsername;
    }

    public String getChainMobile() {
        return chainMobile;
    }

    public void setChainMobile(String chainMobile) {
        this.chainMobile = chainMobile;
    }

    public String getChainAddress() {
        return chainAddress;
    }

    public void setChainAddress(String chainAddress) {
        this.chainAddress = chainAddress;
    }
}
