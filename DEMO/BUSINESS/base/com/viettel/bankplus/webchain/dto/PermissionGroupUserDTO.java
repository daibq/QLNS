package com.viettel.bankplus.webchain.dto;

import com.viettel.fw.dto.BaseDTO;

import java.io.Serializable;
import java.util.Date;

/**
 * @author hungnq on 9/7/2017
 */
public class PermissionGroupUserDTO extends BaseDTO implements Serializable {
    public String getKeySet() {
        return keySet;
    }

    private Long permissionGroupUserId;
    private Long chainUserId;
    private Long permissionGroupId;
    private String createUser;
    private Date createDate;
    private Short status;

    //them cac bien can thiet
    private String userName;
    private String permissionGroupName;
    private String fullName;
    /*them*/
    private String importExcelError;

    public Long getPermissionGroupUserId() {
        return permissionGroupUserId;
    }

    public void setPermissionGroupUserId(Long permissionGroupUserId) {
        this.permissionGroupUserId = permissionGroupUserId;
    }

    public Long getChainUserId() {
        return chainUserId;
    }

    public void setChainUserId(Long chainUserId) {
        this.chainUserId = chainUserId;
    }

    public Long getPermissionGroupId() {
        return permissionGroupId;
    }

    public void setPermissionGroupId(Long permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPermissionGroupName() {
        return permissionGroupName;
    }

    public void setPermissionGroupName(String permissionGroupName) {
        this.permissionGroupName = permissionGroupName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getImportExcelError() {
        return importExcelError;
    }

    public void setImportExcelError(String importExcelError) {
        this.importExcelError = importExcelError;
    }
}
