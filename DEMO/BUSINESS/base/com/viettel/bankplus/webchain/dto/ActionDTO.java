package com.viettel.bankplus.webchain.dto;

import java.lang.Long;
import java.lang.Short;

import com.viettel.fw.dto.BaseDTO;

import java.io.Serializable;

public class ActionDTO extends BaseDTO implements Serializable {
    public String getKeySet() {
        return keySet;
    }

    public static enum COLUMNS {ACTIONID, CODE, DESCRIPTION, NAME, STATUS, TYPE, EXCLUSE_ID_LIST}

    ;
    private Long actionId;
    private String code;
    private String description;
    private String name;
    private Short status;
    private String type;

    public Long getActionId() {
        return this.actionId;
    }

    public void setActionId(Long actionId) {
        this.actionId = actionId;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getStatus() {
        return this.status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
