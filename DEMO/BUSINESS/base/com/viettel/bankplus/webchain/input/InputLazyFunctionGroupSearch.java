package com.viettel.bankplus.webchain.input;

/**
 * Created by buiqu on 9/5/2017.
 */
public class InputLazyFunctionGroupSearch {
    private Long chainShopId;
    private String permissionGroupCode;
    private Long status;

    public Long getChainShopId() {
        return chainShopId;
    }

    public void setChainShopId(Long chainShopId) {
        this.chainShopId = chainShopId;
    }

    public String getPermissionGroupCode() {
        return permissionGroupCode;
    }

    public void setPermissionGroupCode(String permissionGroupCode) {
        this.permissionGroupCode = permissionGroupCode;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
