package com.viettel.bankplus.webchain.input;

/**
 * @author hungnq on 9/8/2017
 */
public class InputLazyChainUserSearch {
    private Long chainShopId;
    private String username;
    private Long parentShopId;
    private Short status;
    private String shopCode;

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Long getParentShopId() {
        return parentShopId;
    }

    public void setParentShopId(Long parentShopId) {
        this.parentShopId = parentShopId;
    }

    public Long getChainShopId() {
        return chainShopId;
    }

    public void setChainShopId(Long chainShopId) {
        this.chainShopId = chainShopId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }
}
