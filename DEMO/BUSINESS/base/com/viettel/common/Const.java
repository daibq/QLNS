package com.viettel.common;

/**
 * Created by namdx on 11/30/2015.
 */
public class Const {


    public static final Long DEFAULT_LONG_VALUE = -1L;
    public static final String DEFAULT_STRING_VALUE = "-1";


    public static final class AP_DOMAIN {
        public static final String APPROVAL_STATUS = "APPROVAL_STATUS";
        public static final String MANAGED_GROUP = "MANAGED_GROUP";
        public static final String MANAGED_GROUP_1 = "MANAGED_GROUP_1";
        public static final String PROJECT_SCALE = "PROJECT_SCALE";
        public static final String BUSINESS_TYPE = "BUSINESS_TYPE";
        public static final String BUSINESS_STATUS = "BUSINESS_STATUS";
        public static final String ROLE_TYPE_CUSTOMER = "ROLE_TYPE_CUSTOMER";
        public static final String BUSINESS_MAIN = "BUSINESS_MAIN";
        public static final String WC_FUNCTION_TYPE = "WC_FUNCTION_TYPE";
        public static final String WC_PAY_WITHDRAW_BCCS_TYPE = "WC_PAY_WITHDRAW_BCCS_TYPE";
        public static final String WC_ACTION_TYPE = "WC_ACTION_TYPE";
        public static final String WC_STATUS = "WC_STATUS";
        public static final String WC_TRANS_TYPE = "WC_TRANS_TYPE";
        public static final String TOPUP_TRANS_APP_TYPE = "TOPUP_TRANS_APP_TYPE";
        public static final String WC_TRANS_DEBT_TYPE = "WC_TRANS_DEBT_TYPE";
        public static final String WC_FUNCTION_PERMISSION_TYPE = "WC_FUNCTION_PERMISSION_TYPE";
        public static final String WC_CHAIN_USER = "WC_CHAIN_USER";
        public static final String WC_PAYMENT = "WEBCHAIN_USER_DEBT_DEDUCTION";
        public static final String WC_METHOD_RECEIVE = "WC_METHOD_RECEIVE";
        public static final String WC_FORM_TRANS = "WC_FORM_TRANS";
        public static final String WC_RECHARGE_FORM = "WC_RECHARGE_FORM";
        public static final String WC_PRODUCT_FEE_DISCOUNT_TYPE = "WC_PRODUCT_FEE_DISCOUNT_TYPE";
        public static final String WC_SERVICE_CODE_VTC_K = "WC_SERVICE_CODE_VTC_K";
        public static final String WC_TRANS_STATUS = "WC_TRANS_STATUS";
        public static final String WC_TRANS_REPORT_TYPE = "WC_TRANS_REPORT_TYPE";
        public static final String WC_DISCOUNT_REPORT_TYPE = "WC_DISCOUNT_REPORT_TYPE";
        public final static String TELCO_PINCODE = "TELCO_PINCODE";
        public final static String WEBCHAIN_APP = "WEBCHAIN_APP";
        public final static String WEBCHAIN_GAME = "WEBCHAIN_GAME";
        public final static String WC_VIETTEL_LIMIT_AMOUNT = "WC_VIETTEL_LIMIT_AMOUNT";

        public final static String TELCO_PINCODE_CARD_VIETTEL = "TELCO_PINCODE_CARD_VIETTEL";
        public final static String TELCO_PINCODE_CARD_BEELINE = "TELCO_PINCODE_CARD_BEELINE";
        public final static String TELCO_PINCODE_CARD_VINAPHONE = "TELCO_PINCODE_CARD_VINAPHONE";
        public final static String TELCO_PINCODE_CARD_MOBIFONE = "TELCO_PINCODE_CARD_MOBIFONE";
        public final static String TELCO_PINCODE_CARD_VIETNAMMOBILE = "TELCO_PINCODE_CARD_VIETNAMMOBILE";

        public static final String INTERNET_AMOUNT = "INTERNET_AMOUNT";
        public static final String HOMEPHONE_AMOUNT = "HOMEPHONE_AMOUNT";
        public static final String FIXEDTELECOM_AMOUNT = "FIXEDTELECOM_AMOUNT";
        public static final String VTCTHS_AMOUNT = "VTCTHS_AMOUNT";

        public final static String WEBCHAIN_GAME_VCOIN_CARD = "WEBCHAIN_GAME_VCOIN_CARD";
        public final static String WEBCHAIN_GAME_GARENA_CARD = "WEBCHAIN_GAME_GARENA_CARD";
        public final static String WEBCHAIN_GAME_MEGACARD_CARD = "WEBCHAIN_GAME_MEGACARD_CARD";
        public final static String WEBCHAIN_GAME_ZINGPCE_CARD = "WEBCHAIN_GAME_ZINGPCE_CARD";
        public final static String WEBCHAIN_GAME_GATE_CARD = "WEBCHAIN_GAME_GATE_CARD";
        public final static String WEBCHAIN_GAME_ONCASH_CARD = "WEBCHAIN_GAME_ONCASH_CARD";
        public final static String TRANS_DETAIL_SERVICE_TYPE = "TRANS_DETAIL_SERVICE_TYPE";
        public final static String TRANS_DETAIL_FORM_TYPE = "TRANS_DETAIL_FORM_TYPE";

        public final static String TRANSFER_VALIDATE_AMOUNT = "TRANSFER_VALIDATE_AMOUNT";

        public static final String TOPUP_REPORT_TYPE = "TOPUP_REPORT_TYPE";
        public static final String TOPUP_IMPART_TYPE = "TOPUP_IMPART_TYPE";
        public static final String TOPUP_REPORT = "TOPUP_REPORT";
        public static final String TOPUP_REPORT_DETAIL = "TOPUP_REPORT_DETAIL";
        public static final String TOPUPCONFIG_TYPE = "TOPUPCONFIG_TYPE";

        public static final String TOPUP_TRANSFER_TYPE = "TOPUP_TRANSFER_TYPE";
        public static final String TOPUP_TRANSFER_STATUS = "TOPUP_TRANSFER_STATUS";
        public static final String TOPUP_DETAIL_TRANSFER_STATUS="TOPUP_DETAIL_TRANSFER_STATUS";

        public final static String CHECK_MSISDN_LENGTH = "CHECK_MSISDN_LENGTH";

        public static final String TRANSACTION_NOT_SEARCH = "TRANSACTION_NOT_SEARCH";

        public final static String TOPUP_TRANSACTION_TYPE = "TOPUP_TRANSACTION_TYPE";

    }

    public static final class API {
        public static final String VIETTEL_BANK_CODE = "CHAIN";
        public static final String APP_ID = "WEB_CHAIN";
    }

    public static final class TELCO_CARD_SERVICE_CODE {
        public static final String VIETTEL = "PCE";
        public static final String VINAPHONE = "VNPPCE";
        public static final String MOBIFONE = "VMSPCE";
        public static final String BEELINE = "BEEPCE";
        public static final String VIETNAMMOBILE = "VNMPCE";
        public static final String OTHER_TELCO_CHANEL_INFO = "WEB";
        public static final String OTHER_TELCO_INTERNAL = "INTERNAL";
        public static final String OTHER_TELCO_REFERENCE = "1";
    }

    public static final class GAME_CARD_SERVICE_CODE {
        public static final String VCOIN = "VCOIN";
        public static final String GARENA = "GARENA";
        public static final String MEGACARD = "MEGACARD";
        public static final String ZINGPCE = "ZINGPCE";
        public static final String GATE = "GATE";
        public static final String ONCASH = "ONCASH";
    }

    public static final class CHAIN_SHOP {
        public static final int MAXLENGTH_CODE = 100;
        public static final int MAXLENGTH_NAME = 100;
        public static final int MAXLENGTH_ADDRESS = 500;
        public static final String FILE_NAME_RESULT = "CHAIN_SHOP_RESULT";
        public static final String FILE_NAME = "CHAIN_SHOP.xlsx";
    }

    public static final class TRANS_TOP_UP {
        //        public static final int
//        public static final int
        public static final String FILE_NAME_RESULT = "TEMPLATE_TRANS_TOPUP_RESULT";
        public static final String TEMP_FILE_NAME = "TEMPLATE_TRANS_TOPUP";
        public static final String FILE_NAME = "TRANS_TOPUP.xlsx";
    }

    public static final class CHAIN_USER {
        public static final int MAXLENGTH_CODE = 100;
        public static final int MAXLENGTH_USER_NAME = 100;
        public static final int MAXLENGTH_ADDRESS = 500;
        public static final int MAXLENGTH_FULL_NAME = 100;
        public static final int MAXLENGTH_PASSWORD = 15;
        public static final String FILE_NAME_RESULT = "CHAIN_USER_RESULT";
        public static final String FILE_NAME = "CHAIN_USER.xlsx";
    }

    public static final class PAY_WITHDRAW_BCCS {
        public static final String PAY_WITHDRAW_BCCS_SERVICE_CODE = "PAYMENT";
    }


    public static final class FUNCTION_GROUP {
        public static final int MAXLENGTH_CODE = 50;
        public static final int MAXLENGTH_NAME = 200;
        public static final int MAXLENGTH_DESCRIPTION = 1000;
        public static final String FILE_NAME_RESULT = "FUNCTION_GROUP_RESULT";
        public static final String FILE_NAME = "FUNCTION_GROUP.xlsx";
    }

    public static final class PERMISSION_GROUP_USER {
        public static final int MAXLENGTH_CODE = 50;
        public static final int MAXLENGTH_NAME = 100;
        public static final String FILE_NAME_RESULT = "PERMISSION_GROUP_USER_RESULT";
        public static final String FILE_NAME = "PERMISSION_GROUP_USER.xlsx";
    }


    public static final class RESOURCE_TEMPLATE {
        public final static String RESOURCE_TEMPLATE_PATH = "/resources/templates/";
    }

    public static final class ACTION_LOG {
        //GHI LOG CHO BANG AP_DOMAIN
        public static final String AP_DOMAIN_TYPE = "AP_DOMAIN";
        public static final String AP_DOMAIN_TYPE_ADD_NEW = "action.apdomain.add.new";
        public static final String AP_DOMAIN_TYPE_UPDATE = "action.apdomain.update";
        public static final String AP_DOMAIN_TYPE_DELETE = "action.apdomain.delete";

        //GHI LOG CHO BANG ACTION
        public static final String ACTION_TYPE = "ACTION";
        public static final String ACTION_TYPE_ADD_NEW = "action.action.add.new";
        public static final String ACTION_TYPE_UPDATE = "action.action.update";
        public static final String ACTION_TYPE_DELETE = "action.action.delete";

        //GHI LOG CHO BANG CHAN_SHOP - cua hang chuoi
        public static final String CHAIN_SHOP_TYPE = "CHAIN_SHOP";
        public static final String CHAIN_SHOP_TYPE_ADD_NEW = "action.chain.shop.add.new";
        public static final String CHAIN_SHOP_TYPE_UPDATE = "action.chain.shop.update";
        public static final String CHAIN_SHOP_TYPE_DELETE = "action.chain.shop.delete";
        public static final String CHAIN_SHOP_TYPE_IMPORT = "action.chain.shop.import";

        //GHI LOG CHO BANG CHAIN_USER - nguoi dung chuoi
        public static final String CHAIN_USER_TYPE = "CHAIN_USER";
        public static final String CHAIN_USER_TYPE_ADD_NEW = "action.chain.user.add.new";
        public static final String CHAIN_USER_TYPE_UPDATE = "action.chain.user.update";
        public static final String CHAIN_USER_TYPE_DELETE = "action.chain.user.delete";
        public static final String CHAIN_USER_TYPE_IMPORT = "action.chain.user.import";

        //GHI LOG CHO BANG FUNCTION_OBJECT - chuc nang
        public static final String FUNCTION_OBJECT_TYPE = "FUNCTION_OBJECT";
        public static final String FUNCTION_OBJECT_TYPE_ADD_NEW = "action.function.object.add.new";
        public static final String FUNCTION_OBJECT_TYPE_UPDATE = "action.function.object.update";
        public static final String FUNCTION_OBJECT_TYPE_DELETE = "action.function.object.delete";

        //GHI LOG CHO BANG PERMISSION_GROUP -nhom chuc nang
        public static final String PERMISSION_GROUP_TYPE = "PERMISSION_GROUP";
        public static final String PERMISSION_GROUP_TYPE_ADD_NEW = "action.permission.group.add.new";
        public static final String PERMISSION_GROUP_TYPE_UPDATE = "action.permission.group.update";
        public static final String PERMISSION_GROUP_TYPE_DELETE = "action.permission.group.delete";
        public static final String PERMISSION_GROUP_TYPE_IMPORT = "action.permission.group.import";

        //GHI LOG CHO BANG PERMISSION_GROUP_FUNCTION -nhom chuc nang-chuc nang
        public static final String PERMISSION_GROUP_FUNCTION_TYPE = "PERMISSION_GROUP_FUNCTION";
        public static final String PERMISSION_GROUP_FUNCTION_TYPE_UPDATE_NEW = "action.permission.group.function.update.new";
        public static final String PERMISSION_GROUP_FUNCTION_TYPE_NEW_ADD = "action.permission.group.function.new.add";
        public static final String PERMISSION_GROUP_FUNCTION_TYPE_UPDATE = "action.permission.group.function.update";
        public static final String PERMISSION_GROUP_FUNCTION_TYPE_DELETE = "action.permission.group.function.delete";


        //GHI LOG CHO BANG PERMISSION_GROUP_FUNCTION -nhom chuc nang-nguoi dung
        public static final String PERMISSION_GROUP_USER_TYPE = "PERMISSION_GROUP_USER";
        public static final String PERMISSION_GROUP_USER_TYPE_ADD_NEW = "action.permission.group.user.add.new";
        public static final String PERMISSION_GROUP_USER_TYPE_UPDATE = "action.permission.group.user.update";
        public static final String PERMISSION_GROUP_USER_TYPE_DELETE = "action.permission.group.user.delete";
        public static final String PERMISSION_GROUP_USER_IMPORT = "action.permission.group.user.import";
        //GHI LOG CHO BANG PRODUCT_PRICE - cac yeu to cau thanh cong no user chuoi
        public static final String PRODUCT_PRICE_TYPE = "PRODUCT_PRICE";
        public static final String PRODUCT_PRICE_TYPE_ADD_NEW = "action.product.price.add.new";
        public static final String PRODUCT_PRICE_TYPE_UPDATE = "action.product.price.add.update";
        public static final String PRODUCT_PRICE_TYPE_DELETE = "action.product.price.add.delete";
        //GHI LOG CHO CAU HINH HAN MUC
        public static final String TOPUP_CONFIG_TYPE = "TOPUP_CONFIG";
        public static final String TOPUP_CONFIG_TYPE_ADD_NEW = "action.topup.config.add.new";
        public static final String TOPUP_CONFIG_TYPE_UPDATE = "action.topup.config.edit";
        public static final String TOPUP_CONFIG_TYPE_DELETE = "action.topup.config.delete";

    }

    public final class WEB_CHAIN {
        public final static String BILL_CODE_PATTERN = "^[a-zA-Z0-9_-]*$";
        public final static String CODE_PATTERN = "^[a-zA-Z0-9]*$";
        public final static String ORDER_ID_PATTERN = "^(YC)[0-9]*$";
        public final static String NUMBER_PATTERN = "^[0-9]*$";
        public final static String VIETNAMESE_PATTERN = "^[A-Za-zẠ-ỹ0-9-/\\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý]*$";
        public final static String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        public final static String MONEY_PATTERN = "^[0-9]*$";
        public final static String PHONE_NO_PATTERN = "^[0-9]{9,11}$";
        public final static String PHONE_PATTERN = "^[0-9]*$";
        public final static String SERIAL_PATTERN = "^[0-9]{19,19}$";
        public final static String NOT_PHONE_DEVICE_SERIAL_PATTERN = "^[0-9]{1,20}$";
        public final static String DEVICE_SERIAL_PATTERN = "^[a-zA-Z0-9]{1,20}$";
        public final static String DEVICE_SERIAL_PATTERN_NUMBER = "^[0-9]{1,20}$";
        public final static String PHONE_NUMBER_PATTERN = "\\d+";
        public final static String REPRESENTATIVE_CONTACT_TYPE = "RCR";
        public final static String STATUS_ACTIVE = "1";
        public final static String STATUS_IN_ACTIVE = "0";
        public final static long MIN_PHONENO_LENGTH = 5L;
        public final static long MAX_PHONENO_LENGTH = 15L;
        public final static short FUNCTION_ACTIVE = 1;
        public final static short FUNCTION_IN_ACTIVE = 0;
        public final static short CHAIN_SHOP_ACTIVE = 1;
        public final static short CHAIN_SHOP_IN_ACTIVE = 0;
        public final static short CHAIN_SHOP_TYPE_VIETTEL = 1;// He thong admin thuoc TCDT
        public final static short CHAIN_SHOP_TYPE_CHAIN = 2;// He thong chuoi
        public final static short CHAIN_SHOP_TYPE_CHAIN_STORE = 3;// He thong cua hang chuoi
        public final static String FUNCTION_EXCEL_TEMPLATE_PATH = "resources/templates/WEB_CHAIN_FUNCTION.xlsx";
        public final static String FUNCTION_GROUP_EXCEL_TEMPLATE_PATH = "resources/templates/WEB_FUNCTION_GROUP.xlsx";
        public final static String AREA_EXCEL_TEMPLATE_PATH = "resources/templates/AREA.xlsx";
        public final static short PERMISSION_GROUP_FUNCTION_ACTIVE = 1;
        public final static short PERMISSION_GROUP_FUNCTION_INACTIVE = 0;
        public final static short PERMISSION_GROUP_USER_ACTIVE = 1;
        public final static short PERMISSION_GROUP_USER_INACTIVE = 0;
        public final static short CHAIN_USER_TYPE_VIETTEL = 1;// admin TCDT
        public final static short CHAIN_USER_TYPE_CHAIN = 2;// admin chuoi
        public final static short CHAIN_USER_TYPE_CHAIN_STORE = 3;// nguoi dung chuoi
        public final static String BL_CHANGE_PASSWORD_TRUE = "1";
        public final static String BL_CHANGE_PASSWORD_FALSE = "0";
        public final static String IMPORT_ERROR = "Lỗi";
        public final static int MAX_SIZE_10M = 10 * 1024 * 1024;
        public final static int MAX_SIZE_5M = 5 * 1024 * 1024;
        public final static String SMS_CREATE_USER = "CREATE_USER";
        public final static String SMS_OTP_RESET_PASSWORD = "RESET_PIN";
        public final static String ACCOUNT_TYPE_PRE = "pre";
        public final static String ACCOUNT_TYPE_POST = "post";
        public final static String REGEX_LOCATION = "^[A-Za-zẠ-ỹ0-9-/\\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý]{1,500}$";
        public final static String VALUE_TRANS_POINT = "1";
        public final static String VALUE_HOME = "2";
        public final static String CHANNEL_INFO = "WEB";
        public final static String PARTNER_TYPE = "INTERNAL";
        public final static String CANCEL = "CANCEL";
        public final static String INIT_TYPE_CASH_DELIVERY = "1";
        public final static String INIT_TYPE_CANCEL_TRANSFER = "2";
        public final static String SERVICE_CODE_FINANCE = "SERVICE_CODE_FINANCE";
        public final static String WC_SUPPLIER_APPLICATION = "WC_SUPPLIER_APPLICATION";
        public final static String WC_SUPPLIER_WATER = "WC_SUPPLIER_WATER";
        public final static String REFERENCE_CODE_APP = "1";
        public final static short API_TRANS_STATUS_IN_PROCESS = 0;
        public final static short API_TRANS_STATUS_SUCESS = 1;
        public final static short API_TRANS_STATUS_TIMEOUT = 2;
        public final static short API_TRANS_STATUS_ERROR = 3;
        public final static String TYPE_REPORT_TRANS = "1";
        public final static String TYPE_REPORT_TRANS_DETAIL = "2";
        public final static String REPORT_TRANS_EXCEL_TEMPLATE_PATH = "resources/templates/TEMPLATE_REPORT_TRANSACTION.xlsx";
        public final static String REPORT_TRANS_DETAIL_EXCEL_TEMPLATE_PATH = "resources/templates/TEMPLATE_REPORT_TRANSACTION_DETAIL.xlsx";
        public final static String CHAIN_DEBIT_EXCEL_TEMPLATE_PATH = "resources/templates/CHAIN_DEBIT.xlsx"; //cong no chuoi
        public final static String REPORT_DISCOUNT_EXCEL_TEMPLATE_PATH = "resources/templates/TEMPLATE_REPORT_DISCOUNT.xlsx"; // bao cao chiet khau
        public final static String REPORT_DISCOUNT_DETAIL_EXCEL_TEMPLATE_PATH = "resources/templates/TEMPLATE_REPORT_DISCOUNT_DETAIL.xlsx"; // bao cao chiet khau chi tiet
        public final static String DOWNLOAD_FILE_LIST_OF_LOCAL_PATH = "resources/templates/FILE_LIST_OF_LOCAL.xlsx"; // bao cao chiet khau chi tiet
        public final static String USER_DEBT_TEMP = "resources/templates/USER_DEBT_TEMP.xlsx"; // bao cao chiet khau
        public final static String TOPUP_DEBIT_EXCEL_TEMPLATE_PATH = "resources/templates/TOPUP_TRANSACTION_DETAIL.xlsx"; //cong no topup
        public final static short CHECK_TRANS_RESULT_VIETTEL = 1;
        public final static short CHECK_TRANS_RESULT_OTHER = 2;
        public final static short CHECK_TRANS_RESULT_TRANSFER = 3;
        public final static short RESEND_VIETTEL_PINCODE = 1;
        public final static short RESEND_OTHER_PINCODE = 2;
        public final static short CANCEL_TRANS_VIETTEL = 1;
        public final static short CANCEL_TRANS_OTHER = 2;
        public final static short CANCEL_TRANS_TRANSFER = 3;
        public final static short PRODUCT_TYPE_DISCOUNT = 1;
        public final static short PRODUCT_TYPE_FEE = 2;
        public final static String TIMEOUT_CALL_API = "TIMEOUT_CALL_API";
        public final static int TIME_SPACE_TRANSACTION_REPORT = 30;
        public final static int TIME_SPACE_TRANSACTION_MANAGEMENT = 30;
        public final static int TIME_SPACE_DISCOUNT_REPORT = 30;
        public final static int TIME_SPACE_USER_DEBT = 30;
        public final static int MAX_DAY_SEARCH = 30;
        public final static String PAY_INTO_BCCS = "1"; // nap tien
        public final static String WITHDRAW_BCCS = "2";// rut tien
        public final static String PAY_WITHDRAW_SERVICE_CODE = "PAYMENT";
        public final static String IS_NULL = "null";
        public final static int AMOUNT_VALIDATE = 5000000;
        public final static long MAX_ORERDER_12 = 100000000000L;
        public final static String YC_ORDER_ID = "YC";
        public final static String PAY_CARD_TYPE = "3";
        public final static double WC_VIETTEL_LIMIT_AMOUNT = 1000000000;
        public final static String WC_VIETTEL_MIN_AMOUNT = "10000";
        public final static String TRANSFER_MIN_HOME_CODE = "TRANSFER_MIN_HOME";
        public final static String TRANSFER_MAX_HOME_CODE = "TRANSFER_MAX_HOME";
        public final static String TRANSFER_MIN_POINT_CODE = "TRANSFER_MIN_POINT";
        public final static String TRANSFER_MAX_POINT_CODE = "TRANSFER_MAX_POINT";
        public final static long TRANSFER_MIN_HOME = 50000;
        public final static long TRANSFER_MAX_HOME = 20000000;
        public final static long TRANSFER_MIN_POINT = 50000;
        public final static long TRANSFER_MAX_POINT = 50000000;
        public final static short SERVICE_TYPE_2 = 2;
        public final static short SERVICE_TYPE_3 = 3;
        public final static String GIVE_MONEY_Y = "Y";
        public final static String GIVE_MONEY_N = "N";
    }

    public final static class TOPUP_TAXI {
        public final static int MAX_SIZE_5M = 5 * 1024 * 1024;
        public final static int MAX_RECORD_10M = 10000;
        public final static short CHAIN_USER_TYPE_ADMIN_FINTECH = 1; // Admin tại fintech = sys
        public final static short CHAIN_USER_TYPE_ADMIN_CHAIN = 4; // admin tại đại lý
        public final static short TRANSFER_TYPE_TOPUP = 1;
        public final static short TRANSFER_TYPE_GIFT = 2;
        public final static short TOPUP_PAYMENT_TYPE_DAY = 1; // Theo ngày
        public final static short TOPUP_PAYMENT_TYPE_MONTH = 2; // Theo tháng
        public final static short TOPUP_CONFIG_TYPE_TRANS_DAY = 1; // GD/nagy
        public final static short TOPUP_CONFIG_TYPE_TRANS_MONTH = 2; // GD/thang
        public final static short TOPUP_CONFIG_TYPE_AMOUNT_DAY = 3; // Tien/Ngay
        public final static short TOPUP_CONFIG_TYPE_AMOUNT_MONTH = 4; // Tien/Thang

        public final static short TRANS_STATUS_NOT_IMPORT_FILE = 0; // Chưa import file
        public final static short TRANS_STATUS_PROCESS_TOPUP = 1;//Đang xử lý
        public final static short TRANS_STATUS_FINISHED = 2;    // Hoàn thành
        public final static short TRANS_STATUS_ERROR = 3;       //Thất bại

        public final static short TRANS_DETAIL_STATUS_INVALID = 4;

        public final static short TRANS_DETAIL_STATUS_PROCESS_TOPUP = 0;//Đang xử lý
        public final static short TRANS_DETAIL_STATUS_FINISHED = 1;    // Thành công
        public final static short TRANS_DETAIL_STATUS_TIMEOUE = 2;     //Timeout
        public final static short TTRANS_DETAIL_STATUS_ERROR = 3;       //Thất bại

        public final static short CHAIN_SHOP_TYPE = 4;
        public final static short STATUS_ACTIVE = 1;
        public final static short STATUS_NO_ACTIVE = 0;
        public final static int TIME_SPACE_TRANSFER_TOPUP = 30;
        public final static String ERROR_CODE_PROCESS_TOPUP = "00";
        public final static long MIN_AMOUNT = 10000L;
        public final static long MAX_AMOUNT = 5000000L;
        public final static String SERVICE_CODE_TOPUP = "PCE";

        public final static String TRANSACTION_TYPE_TOPUP = "1";
        public final static String TRANSACTION_TYPE_PAY_WITHDRAW = "2";




    }

    public final static class TELCO {
        public final static String VIETTEL = "VIETTEL";
        public final static String VINAPHONE = "VINAPHONE";
        public final static String MOBIFONE = "MOBIFONE";
        public final static String VIETNAMMOBILE = "VIETNAMMOBILE";
        public final static String BEELINE = "BEELINE";
    }

    public final static class TELCO_AMOUNT {
        public final static String VIETTEL = "VIETTEL_AMOUNT";
        public final static String VINAPHONE = "VINAPHONE_AMOUNT";
        public final static String MOBIFONE = "MOBIFONE_AMOUNT";
        public final static String VIETNAMMOBILE = "VIETNAMMOBILE_AMOUNT";
        public final static String BEELINE = "BEELINE_AMOUNT";
    }

    public final static class PROCESS_CODE {
        public final static String PAY_BILL = "PAY_BILL";
        public final static String GET_BILL = "GET_BILL";
        public final static String MAKE_TRANSFER = "MAKE_TRANSFER";
        public final static String EDIT_TRANSFER = "EDIT_TRANSFER";
        public final static String CONFIRM_DELIVERY = "CONFIRM_DELIVERY";
        public final static String PAY_TELECHARGE_VT_WEB = "PAY_TELECHARGE_VT_WEB";
        public final static String PAY_PINCODE_VT_WEB = "PAY_PINCODE_VT_WEB";
        public final static String INIT_TRANSFER = "INIT_TRANSFER";
    }


    public final static class CALL_API {
        public final static String URL = "https://125.235.40.34:8088/CoreChain/CoreChainAPI/BusinessAPI";
        public final static int TIMEOUT_DEFAULT = 180000;
        public final static String CORE_API_CLIENT_ID = "alice";
        public final static String CORE_API_PASSWORD = "changeme";
        public final static String SIGN = "changeme";
    }

    public final static class SERVICE_CODE {
        public final static String VT_MOBILE = "100000";
        public final static String BEE_MOBILE = "BEEPRE";
        public final static String VINA_MOBILE = "VNPPRE";
        public final static String MOBI_MOBILE = "VMSPRE";
        public final static String VN_MOBILE = "VNMPRE";
        public final static String VTCTHS = "VTCTHS";
        public final static String VT_HOMEPHONE = "200000";
        public final static String VT_INTERNET_OR_CAB = "000003";
        public final static String VT_FIXED_PHONE = "000004";
        public final static String TRANSFER = "TRANSFER";
        public final static String EVN = "EVN";
        public final static String PAY_VIETTEL_PINCODE = "000004";
        public final static String EVN_VALUE = "1";
        public final static String NHCM_VALUE = "2";
        public final static String FINANCE_VALUE = "3";
    }

    public final static class TRANS_STATUS {
        public final static Short IN_PROCCESS = 0;
        public final static Short SUCCESS = 1;
        public final static Short TIME_OUT = 2;
        public final static Short FAIL = 3;
        public final static Short CANCEL = 4;
    }

    public final static class ERROR {
        public final static String SUCCESS_00 = "00";
        public final static String TIMEOUT_605 = "605";
        public final static String TIMEOUT_32 = "32";
        public final static String FAILT_02 = "02";
        public final static String REFERANCE_0 = "0";
    }

    public final static class PERSISTENT_UNIT {
        public final static String BANK_PLUS = "BANK_PLUS";

        private PERSISTENT_UNIT() {
        }
    }

    public final static class TPP_TYPE {
        public final static String TT = "TT";
        public final static String TS = "TS";
    }

    public final static class TPP_NAME {
        public final static String TT = "Trả trước";
        public final static String TS = "Trả sau";
    }

    public final static class TOPUP {
        public final static String REPORT_TEMPLATE_PATH = "resources/templates/TOPUP_REPORT_TEMPLATE.xlsx";
        public final static String REPORT_DETAIL_TEMPLATE_PATH = "resources/templates/TOPUP_REPORT_DETAIL_TEMPLATE.xlsx";
    }

    public final class SORT {
        public final static String ASC = "ASC";
        public final static String DESC = "DESC";
    }
}
