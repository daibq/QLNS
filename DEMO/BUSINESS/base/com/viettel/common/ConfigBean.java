package com.viettel.common;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author Admin
 * @since 09/06/2015
 */
public class ConfigBean {
    // Cau hinh tham so gui lenh tong dai.
    private String allowSendToProvisioning;

    @Value("${LOGGING_HELPER:0}")
    private String loggingHepper;

    public String getAllowSendToProvisioning() {
        return allowSendToProvisioning;
    }

    public void setAllowSendToProvisioning(String allowSendToProvisioning) {
        this.allowSendToProvisioning = allowSendToProvisioning;
    }

    public String getLoggingHepper() {
        return loggingHepper;
    }

    public void setLoggingHepper(String loggingHepper) {
        this.loggingHepper = loggingHepper;
    }
}
