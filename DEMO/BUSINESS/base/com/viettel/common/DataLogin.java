package com.viettel.common;

import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;

import java.util.List;

public class DataLogin {
    private String returnCode;
    private String blChangePassword;
    private List<FunctionObjectDTO> menu;
    private ChainUserDTO profile;


    public DataLogin() {

    }

    public DataLogin(ChainUserDTO profile) {

        this.profile = profile;
    }

    public DataLogin(List<FunctionObjectDTO> menu, ChainUserDTO profile) {
        this.menu = menu;
        this.profile = profile;
    }

    public List<FunctionObjectDTO> getMenu() {
        return menu;
    }

    public void setMenu(List<FunctionObjectDTO> menu) {
        this.menu = menu;
    }

    public ChainUserDTO getProfile() {
        return profile;
    }

    public void setProfile(ChainUserDTO profile) {
        this.profile = profile;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getBlChangePassword() {
        return blChangePassword;
    }

    public void setBlChangePassword(String blChangePassword) {
        this.blChangePassword = blChangePassword;
    }
}
