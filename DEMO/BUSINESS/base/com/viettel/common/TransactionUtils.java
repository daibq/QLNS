package com.viettel.common;

import com.viettel.bankplus.webchain.service.ActionLogService;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;

/**
 * Created by truong on 13-10-2017.
 */
public class TransactionUtils {
    public static final Logger logger = Logger.getLogger(TransactionUtils.class);

    public static short getTransStatus(String errorCode) {
        if (DataUtil.isNullOrEmpty(errorCode)) {
            return Const.TRANS_STATUS.IN_PROCCESS;
        } else if (DataUtil.safeEqual(errorCode, Const.ERROR.SUCCESS_00)) {
            return Const.TRANS_STATUS.SUCCESS;
        } else if (DataUtil.safeEqual(errorCode, Const.ERROR.TIMEOUT_605) || DataUtil.safeEqual(errorCode, Const.ERROR.TIMEOUT_32)) {
            return Const.TRANS_STATUS.TIME_OUT;
        } else {
            return Const.TRANS_STATUS.FAIL;
        }
    }

}