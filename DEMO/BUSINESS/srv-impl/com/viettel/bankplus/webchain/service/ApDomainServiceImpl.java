package com.viettel.bankplus.webchain.service;


import com.mysema.query.types.expr.BooleanExpression;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ApDomainDTO;
import com.viettel.bankplus.webchain.input.InputApdomain;
import com.viettel.bankplus.webchain.model.ActionLog;
import com.viettel.bankplus.webchain.model.ActionLogDetail;
import com.viettel.bankplus.webchain.model.ApDomain;
import com.viettel.bankplus.webchain.model.QApDomain;
import com.viettel.bankplus.webchain.repo.ActionLogDetailRepo;
import com.viettel.bankplus.webchain.repo.ActionLogRepo;
import com.viettel.bankplus.webchain.repo.ApDomainRepo;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.service.BaseServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ApDomainServiceImpl extends BaseServiceImpl implements ApDomainService {
    private final BaseMapper<ActionLog, ActionLogDTO> mapperActionLog = new BaseMapper<>(ActionLog.class, ActionLogDTO.class);
    private final BaseMapper<ApDomain, ApDomainDTO> mapper = new BaseMapper<>(ApDomain.class, ApDomainDTO.class);


    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    @Autowired
    private ApDomainRepo repository;
    @Autowired
    private ActionLogRepo actionLogRepo;
    @Autowired
    private ActionLogDetailRepo actionLogDetailRepo;
    @Autowired
    private ActionLogDetailService actionLogDetailService;

    public static final Logger logger = Logger.getLogger(ApDomainService.class);

    @Override
    public ApDomainDTO findOne(Long id) throws Exception {
        return mapper.toDtoBean(repository.findOne(id));
    }

    /*
    * tìm và dếm sô bảm ghi tìm được danh mục tham số
    */
    @Override
    public int countCtctProgramList(InputApdomain inputApdomainSearch) throws Exception {
        return repository.countCtctProgramList(inputApdomainSearch);
    }

    /*
    * tìm kiếm danh mục tham số
    */
    @Override
    public List<ApDomainDTO> findLazingPaging(InputApdomain inputApdomainSearch, int pageSize, int pageNumber) throws Exception {
        return repository.findLazingPaging(inputApdomainSearch, pageSize, pageNumber);
    }

    /*
    * thêm mới hoặc sửa tham số
    */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveorUodate(ApDomainDTO dto, ActionLogDTO actionLogDTO) throws Exception {

        Date currentDate = DbUtil.getSysDate(em);
        try {
            dto.setCode(dto.getCode().toUpperCase());
            boolean isCreate = DataUtil.isNullObject(dto.getApDomainId()) ? true : false;

            if (isCreate) {
                dto.setCreateDate(currentDate);
                ApDomain apDomain = mapper.toPersistenceBean(dto);
                repository.saveAndFlush(apDomain);

                actionLogDTO.setCreateDate(currentDate);
                actionLogDTO.setAction("CREATE AP_DOMAIN");
                actionLogDTO.setObjectId(apDomain.getApDomainId());
                /*actionLogDTO.setDescription(getText("webchain.catalog.actionLog.add") + " " + "[" + apDomain.getApDomainId() + "]");*/
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + "[" + apDomain.getApDomainId() + "]");
                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.save(actionLog);
            } else {
                ApDomain apDomainOld = new ApDomain();
                ApDomain apDomainl = repository.findOne(dto.getApDomainId());
                BeanUtils.copyProperties(apDomainl, apDomainOld);
                ApDomain apDomainNew = mapper.toPersistenceBean(dto);
                //  ApDomain apDomain = mapper.toPersistenceBean(dto);
                apDomainNew.setCreateDate(currentDate);
                apDomainNew.setCreateBy(actionLogDTO.getUserName());
                repository.saveAndFlush(apDomainNew);

                actionLogDTO.setAction("UPDATE AP_DOMAIN");
                actionLogDTO.setCreateDate(currentDate);
                /*actionLogDTO.setDescription(getText("webchain.catalog.actionLog.edit") + " " + "[" + apDomainNew.getApDomainId() + "]");*/
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + "[" + apDomainNew.getApDomainId() + "]");
                actionLogDTO.setObjectId(dto.getApDomainId());

                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.saveAndFlush(actionLog);

                List<ActionLogDetail> actionLogDetails = actionLogDetailService.getActionLogDetailList(actionLog, apDomainNew, apDomainOld, new ArrayList<>(), currentDate, true);
                actionLogDetailRepo.save(actionLogDetails);


            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }

    /*
    * xóa(chuyên trạng thái tham só về không hiệu lực) của danh mục tham số
    */

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteList(ApDomainDTO dto, ActionLogDTO actionLogDTO) throws Exception {

        Date currentDate = DbUtil.getSysDate(em);
        try {

            dto.setStatus(0l);
            ApDomain apDomain = mapper.toPersistenceBean(dto);
            mapper.toDtoBean(repository.saveAndFlush(apDomain));

            actionLogDTO.setCreateDate(currentDate);
            actionLogDTO.setAction("DELETE AP_DOMAIN");
            actionLogDTO.setObjectId(apDomain.getApDomainId());
            /*actionLogDTO.setDescription(getText("webchain.catalog.actionLog.delete") + " " + "[" + dto.getApDomainId() + "]");*/
            actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + "[" + dto.getApDomainId() + "]");
            ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
            actionLogRepo.save(actionLog);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    /*
    * check trùng tên theo loai của danh mục tham số
    */
    @Override
    public boolean checkName(String type, String name) throws Exception {
        BooleanExpression booleanExpression = QApDomain.apDomain.name.equalsIgnoreCase(name);
        booleanExpression = booleanExpression.and(QApDomain.apDomain.type.equalsIgnoreCase(type));
        List<ApDomain> list = (List<ApDomain>) repository.findAll(booleanExpression);
        return list.isEmpty();
    }

    /*
    * check trùng mã va id của danh mục tham số
    */
    @Override
    public boolean checkCodeOrId(String code, Long id) throws Exception {
        BooleanExpression booleanExpression = null;

        if (!DataUtil.isNullObject(code)) {
            booleanExpression = QApDomain.apDomain.code.equalsIgnoreCase(code);
        }
        if (!DataUtil.isNullObject(id)) {
            booleanExpression = QApDomain.apDomain.apDomainId.eq(id);
            booleanExpression = booleanExpression.and(QApDomain.apDomain.status.eq(1L));
        }

        List<ApDomain> list = (List<ApDomain>) repository.findAll(booleanExpression);
        return list.isEmpty();


    }

    //@HungNQ
    @Override
    public List<ApDomainDTO> getValueApDomainByTypeOrderByCode(String type) throws Exception {
        try {
            return repository.getValueApDomainByTypeOrderByCode(type);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
        }
    }

    //@HungNQ
    @Override
    public List<ApDomainDTO> getValueApDomainByType(String type) throws Exception {
        try {
            return repository.getValueApDomainByType(type);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
        }
    }


}
