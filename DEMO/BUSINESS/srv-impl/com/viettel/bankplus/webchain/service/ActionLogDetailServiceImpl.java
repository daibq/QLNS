package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDetailDTO;
import com.viettel.bankplus.webchain.model.ActionLog;
import com.viettel.bankplus.webchain.model.ActionLogDetail;
import com.viettel.bankplus.webchain.repo.ActionLogDetailRepo;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.service.BaseServiceImpl;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ActionLogDetailServiceImpl extends BaseServiceImpl implements ActionLogDetailService {

    private final BaseMapper<ActionLogDetail, ActionLogDetailDTO> mapper = new BaseMapper<>(ActionLogDetail.class, ActionLogDetailDTO.class);

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;
    @Autowired
    private ActionLogDetailRepo repo;
    public static final Logger logger = Logger.getLogger(ActionLogDetailService.class);

    @WebMethod
    public Long count(List<FilterRequest> filters) throws Exception {
        return repo.count(repo.toPredicate(filters));
    }

    @WebMethod
    public ActionLogDetailDTO findOne(Long id) throws Exception {
        return mapper.toDtoBean(repo.findOne(id));
    }

    @WebMethod
    public List<ActionLogDetailDTO> findByFilter(List<FilterRequest> filters) throws Exception {

        return mapper.toDtoBean(repo.findAll(repo.toPredicate(filters)));
    }

    public List<ActionLogDetail> getActionLogDetailList(ActionLog actionLog, Object newModel, Object oldModel, List<String> trackChangeColumns, Date sysDate, boolean trackAll) throws Exception {
        List<ActionLogDetail> actionLogDetailList = new ArrayList<ActionLogDetail>();

        Class modelClass = oldModel.getClass();
        String tableName = ((Table) modelClass.getAnnotation(Table.class)).name();
        Field[] fields = modelClass.getDeclaredFields();

        for (Field field : fields) {
            Column column = field.getAnnotation(Column.class);
            if (column != null) {
                String columnName = column.name();
                String fieldName = field.getName();
                if (trackAll || trackChangeColumns.contains(columnName)) {
                    try {
                        String oldValue = DataUtil.safeToString(BeanUtils.getProperty(oldModel, fieldName));
                        String newValue = DataUtil.safeToString(BeanUtils.getProperty(newModel, fieldName));

                        Object rawOldValue = FieldUtils.readDeclaredField(oldModel, fieldName, true);
                        Object rawNewValue = FieldUtils.readDeclaredField(newModel, fieldName, true);

                        if (!"".equals(oldValue) && rawOldValue instanceof Date) {
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                            oldValue = DataUtil.safeToString((simpleDateFormat.format((Date) rawOldValue)));
                        }

                        if (!"".equals(newValue) && rawNewValue instanceof Date) {
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                            newValue = DataUtil.safeToString((simpleDateFormat.format((Date) rawNewValue)));
                        }

                        if (!oldValue.equals(newValue)) {
                            ActionLogDetail actionDetail = new ActionLogDetail();
                            actionDetail.setTableName(tableName);
                            actionDetail.setActionDate(sysDate);
                            actionDetail.setActionLogId(actionLog.getActionLogId());
                            actionDetail.setColumnName(columnName);
                            /*if (rawOldValue instanceof Date) {
                                actionDetail.setNewValue(rawNewValue.toString());
                                actionDetail.setOldValue(rawOldValue.toString());
                            } else {*/
                            actionDetail.setNewValue(newValue);
                            actionDetail.setOldValue(oldValue);
                            //}
                            actionLogDetailList.add(actionDetail);
                        }
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            }
        }
        return actionLogDetailList;
    }

    @Override
    public int countList(ActionLogDTO dto) throws Exception {
        return repo.countList(dto);
    }

    @Override
    public List<ActionLogDetailDTO> findLazingPaging(ActionLogDTO actionLogDTO, int pageSize, int pageNumber) throws Exception {
        return repo.findLazingPaging(actionLogDTO, pageSize, pageNumber);
    }

}
