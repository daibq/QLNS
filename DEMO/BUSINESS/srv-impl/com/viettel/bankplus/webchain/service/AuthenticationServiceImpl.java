package com.viettel.bankplus.webchain.service;

import com.mysema.query.types.expr.BooleanExpression;
import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.model.ChainUser;
import com.viettel.bankplus.webchain.model.FunctionObject;
import com.viettel.bankplus.webchain.model.QChainUser;
import com.viettel.bankplus.webchain.repo.ChainUserRepo;
import com.viettel.bankplus.webchain.repo.FunctionObjectRepo;
import com.viettel.common.Const;
import com.viettel.common.DataLogin;
import com.viettel.fw.common.util.AESUtil;
import com.viettel.fw.common.util.ErrorCode;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.service.BaseServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

@Service
public class AuthenticationServiceImpl extends BaseServiceImpl implements AuthenticationService {
    private static final Logger LOGGER = Logger.getLogger(AuthenticationService.class);
    private final BaseMapper<ChainUser, ChainUserDTO> chainUserMapper = new BaseMapper<>(ChainUser.class, ChainUserDTO.class);
    private final BaseMapper<FunctionObject, FunctionObjectDTO> functionObjectMapper = new BaseMapper<>(FunctionObject.class, FunctionObjectDTO.class);
    @PersistenceContext(unitName = com.viettel.common.Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;
    @Autowired
    private ChainUserRepo chainUserRepo;
    @Autowired
    private FunctionObjectRepo functionObjectRepo;


    @WebMethod
    public ChainUserDTO findOne(Long id) throws Exception {
        return chainUserMapper.toDtoBean(chainUserRepo.findOne(id));
    }

    @Override
    public DataLogin login(String username, String password) throws Exception {
        Date currentDate = getSysDate(em);
        DataLogin dataLogin = new DataLogin();
        dataLogin.setReturnCode(ErrorCode.SUCCESSFUL);
        ChainUserDTO chainUserDTO;
        List<FunctionObjectDTO> menuDTO;
        try {
            BooleanExpression booleanExpression = QChainUser.chainUser.userName.equalsIgnoreCase(username);
            List<ChainUserDTO> chainUserDTOList = chainUserMapper.toDtoBean((List<ChainUser>) chainUserRepo.findAll(booleanExpression));
            // username not exist
            if (chainUserDTOList.isEmpty()) {
                dataLogin.setReturnCode(ErrorCode.ERROR_USER.ERROR_USER_NOT_EXIST);
            } else if (chainUserDTOList.size() > 1) { // trung du lieu
                dataLogin.setReturnCode(ErrorCode.ERROR_USER.ERROR_USER_DUPLICATE_OBJECT);
            } else {// loi khac
                chainUserDTO = chainUserDTOList.get(0);
                // trang thai nguoi dung
                if (chainUserDTO.getActiveStatus() == 0) {
                    dataLogin.setReturnCode(ErrorCode.ERROR_USER.ERROR_USER_IN_ACTIVE);
                } else
                    //validate password
                    if (!AESUtil.validatePassword(password, chainUserDTO.getSalt(), chainUserDTO.getPassword())) {
                        dataLogin.setReturnCode(ErrorCode.ERROR_USER.ERROR_USER_PASS_INVALID);
                    } else {
                        // luu log dang nhap
                        chainUserDTO.setLastLoginTime(currentDate);
                        chainUserRepo.save(chainUserMapper.toPersistenceBean(chainUserDTO));

                        // lay danh sach chuc nang
                        if (dataLogin.getReturnCode().equals(ErrorCode.SUCCESSFUL)) {
                            menuDTO = functionObjectMapper.toDtoBean(functionObjectRepo.getMenuByUserId(chainUserDTO.getChainUserId()));
                            if (null == menuDTO || menuDTO.isEmpty()) {
                                dataLogin.setReturnCode(ErrorCode.ERROR_USER.ERROR_USER_NO_PERMISSION);
                            } else {
                                dataLogin.setMenu(menuDTO);
                                chainUserDTO.setPassword(null);
                                chainUserDTO.setLastLoginTime(null);
                                chainUserDTO.setSalt(null);
                                dataLogin.setProfile(chainUserDTO);
                                //check expire password here
                                // lay config expire trong apdomain
                                if (null == chainUserDTO.getLastChangePasswordDate()) {
                                    dataLogin.setBlChangePassword(Const.WEB_CHAIN.BL_CHANGE_PASSWORD_TRUE);
                                } else if (null != chainUserDTO.getLastChangePasswordDate() && (currentDate.getTime() - chainUserDTO.getLastChangePasswordDate().getTime() > java.util.concurrent.TimeUnit.DAYS.toMillis(82))) {
                                    dataLogin.setBlChangePassword(Const.WEB_CHAIN.BL_CHANGE_PASSWORD_TRUE);
                                } else {
                                    dataLogin.setBlChangePassword(Const.WEB_CHAIN.BL_CHANGE_PASSWORD_FALSE);
                                }
                            }
                        }
                        //
                    }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
        return dataLogin;
    }



}
