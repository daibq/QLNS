package com.viettel.bankplus.webchain.service;

import com.mysema.query.types.expr.BooleanExpression;
import com.viettel.bankplus.webchain.dto.ActionDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.input.InputLazyActionSearch;
import com.viettel.bankplus.webchain.model.Action;
import com.viettel.bankplus.webchain.model.ActionLog;
import com.viettel.bankplus.webchain.model.ActionLogDetail;
import com.viettel.bankplus.webchain.model.QAction;
import com.viettel.bankplus.webchain.repo.ActionLogDetailRepo;
import com.viettel.bankplus.webchain.repo.ActionLogRepo;
import com.viettel.bankplus.webchain.repo.ActionRepo;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.service.BaseServiceImpl;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.common.util.extjs.FilterRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.jws.WebMethod;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

@Service
public class ActionServiceImpl extends BaseServiceImpl implements ActionService {

    private final BaseMapper<ActionLog, ActionLogDTO> mapperActionLog = new BaseMapper<>(ActionLog.class, ActionLogDTO.class);
    private final BaseMapper<Action,ActionDTO> mapper = new BaseMapper<>(Action.class,ActionDTO.class);

    @Autowired
    private ActionLogDetailService actionLogDetailService;
    @Autowired
    private ActionLogRepo actionLogRepo;
    @Autowired
    private ActionLogDetailRepo actionLogDetailRepo;
    @Autowired
    private ActionRepo repo;

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    public static final Logger logger = Logger.getLogger(ActionService.class);

    @WebMethod
    public Long count(List<FilterRequest> filters) throws Exception {
        return repo.count(repo.toPredicate(filters));
    }

    @WebMethod
    public ActionDTO findOne(Long id) throws Exception {
        return mapper.toDtoBean(repo.findOne(id));
    }

    @WebMethod
    public List<ActionDTO> findByFilter(List<FilterRequest> filters) throws Exception {
        return mapper.toDtoBean(repo.findAll(repo.toPredicate(filters)));
    }

    @Override
    public int countActionList(InputLazyActionSearch inputLazyActionSearch) throws Exception {
        return repo.countActionList(inputLazyActionSearch);
    }

    @Override
    public List<ActionDTO> findLazingPaging(InputLazyActionSearch inputLazyProgramSearch, int pageSize, int pageNumber) throws Exception {
        return repo.findLazingPaging(inputLazyProgramSearch, pageSize, pageNumber);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(ActionDTO actionDTO, ActionLogDTO actionLogDTO) throws Exception{
        try {
            Date sysdate = DbUtil.getSysDate(em);
            actionDTO.setCode(actionDTO.getCode().toLowerCase());
            actionDTO.setType(actionDTO.getType().toUpperCase());
            boolean isCreate= DataUtil.isNullObject(actionDTO.getActionId()) ? true : false;
            actionLogDTO.setCreateDate(sysdate);

            if (isCreate){
                Action action =mapper.toPersistenceBean(actionDTO);
                repo.saveAndFlush(action);

                actionLogDTO.setAction("CREATE ACTION");
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " [" + action.getActionId() +"]");
                actionLogDTO.setObjectId(action.getActionId());
                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.save(actionLog);
            }else {
                Action actionOld = new Action();
                Action action1 = repo.findOne(actionDTO.getActionId());
                BeanUtils.copyProperties(action1, actionOld);
                Action actionNew =mapper.toPersistenceBean(actionDTO);
                repo.saveAndFlush(actionNew);

                actionLogDTO.setAction("UPDATE ACTION");
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " [" + actionNew.getActionId() +"]");
                actionLogDTO.setObjectId(actionNew.getActionId());
                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.saveAndFlush(actionLog);

                List<ActionLogDetail> actionLogDetails = actionLogDetailService.getActionLogDetailList(actionLog, actionNew, actionOld, new ArrayList<>(), sysdate, true);
                actionLogDetailRepo.save(actionLogDetails);
            }
        }catch (Exception e){
            logger.error(e.getMessage(), e);
            throw e;
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delList(ActionDTO dto, ActionLogDTO actionLogDTO) throws Exception {
        try{
            Date sysdate = DbUtil.getSysDate(em);
            dto.setStatus(Short.valueOf("0"));
            mapper.toDtoBean(repo.saveAndFlush(mapper.toPersistenceBean(dto)));
            actionLogDTO.setCreateDate(sysdate);
            actionLogDTO.setAction("DELETE ACTION");
            actionLogDTO.setObjectId(dto.getActionId());
            actionLogDTO.setDescription(actionLogDTO.getDescription() + " [" + dto.getActionId() +"]");
            ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
            actionLogRepo.save(actionLog);
        }catch (Exception e){
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public boolean isCodeOrIdExist(String code, Long id) throws Exception {
        BooleanExpression booleanExpression= null;
        Short status= 1;
       if (!DataUtil.isNullObject(code)){
            booleanExpression = QAction.action.code.equalsIgnoreCase(code);
            booleanExpression = booleanExpression.and(QAction.action.status.eq(status));

       }

       if (!DataUtil.isNullObject(id)){
            booleanExpression = QAction.action.actionId.eq(id);
            booleanExpression = booleanExpression.and(QAction.action.status.eq(status));
       }

        List<Action> list =(List<Action>)repo.findAll(booleanExpression);
        return list.isEmpty();
    }

    @Override
    public boolean isDuplicateName(String name) throws Exception {
        BooleanExpression booleanExpression = QAction.action.name.equalsIgnoreCase(name);
        /*booleanExpression = booleanExpression.and(QAction.action.type.equalsIgnoreCase(type));*/
        /*booleanExpression = booleanExpression.and(QAction.action.status.eq(status));*/
        List<Action> list =(List<Action>)repo.findAll(booleanExpression);
       return list.isEmpty();
    }

    @Override
    public List<ActionDTO> getListAction() throws Exception{
        return repo.getActionOrderByName();
    }

    @Override
    public List<ActionDTO> getValueActionByTypeOrderByCode(String type) throws Exception{
        return repo.getValueActionByTypeOrderByCode(type);
    }

    @Override
    public List<ActionDTO> getValueActionByType(String type) throws Exception{
        return repo.getValueActionByType(type);
    }
    //@Hunngq
    @Override
    public List<ActionDTO> getListActionDTOByType(String type) throws Exception {
        return repo.getListActionDTOByType(type);
    }
}
