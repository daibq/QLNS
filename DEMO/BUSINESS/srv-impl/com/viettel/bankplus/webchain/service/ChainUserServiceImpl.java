package com.viettel.bankplus.webchain.service;

import com.mysema.query.types.expr.BooleanExpression;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.input.InputLazyChainUserSearch;
import com.viettel.bankplus.webchain.model.*;
import com.viettel.bankplus.webchain.repo.ActionLogDetailRepo;
import com.viettel.bankplus.webchain.repo.ActionLogRepo;
import com.viettel.bankplus.webchain.repo.ChainUserRepo;
import com.viettel.common.Const;
import com.viettel.fw.common.util.AESUtil;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.dto.BaseMessage;
import com.viettel.fw.service.BaseServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebMethod;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ChainUserServiceImpl extends BaseServiceImpl implements ChainUserService {
    private Date current;
    @PersistenceContext(unitName = com.viettel.common.Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;
    private final BaseMapper<ChainUser, ChainUserDTO> mapper = new BaseMapper<>(ChainUser.class, ChainUserDTO.class);
    private final BaseMapper<ActionLog, ActionLogDTO> mapperActionLog = new BaseMapper<>(ActionLog.class, ActionLogDTO.class);
    @Autowired
    private ChainUserRepo repo;

    public static final Logger LOGGER = Logger.getLogger(ChainUserService.class);
    @Autowired
    private ActionLogRepo actionLogRepo;
    @Autowired
    private ActionLogDetailService actionLogDetailService;
    @Autowired
    private ActionLogDetailRepo actionLogDetailRepo;

    @WebMethod
    public Long count(List<FilterRequest> filters) throws Exception {
        return repo.count(repo.toPredicate(filters));
    }

    @WebMethod
    public ChainUserDTO findOne(Long id) throws Exception {
        return mapper.toDtoBean(repo.findOne(id));
    }

    @WebMethod
    public List<ChainUserDTO> findByFilter(List<FilterRequest> filters) throws Exception {

        return mapper.toDtoBean(repo.findAll(repo.toPredicate(filters)));
    }


    @Override
    public BaseMessage changePassword(String newPassword, String userName, String takeSalt, long id, ActionLogDTO actionLogDTO) throws Exception {
        try {
            current = getSysDate(em);
            ChainUserDTO chainUserDTO;
            ChainUser chainUserOld = new ChainUser();
            String salt = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            chainUserDTO = findOne(id);
            BeanUtils.copyProperties(chainUserDTO, chainUserOld);
            String password = AESUtil.createHash(newPassword, salt);
            chainUserDTO.setPassword(password);
            chainUserDTO.setLastChangePasswordDate(current);
            chainUserDTO.setSalt(salt);
            chainUserDTO.setUpdateDate(current);
            chainUserDTO.setUpdateUser(userName);
            ChainUser chainUser = repo.saveAndFlush(mapper.toPersistenceBean(chainUserDTO));

            /* Write Log*/
            actionLogDTO.setUserName(userName);
            actionLogDTO.setCreateDate(current);
            actionLogDTO.setAction("UPDATE CHAIN_USER");
            actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + id);
            actionLogDTO.setObjectId(id);
            ActionLog actionLog = actionLogRepo.save(mapperActionLog.toPersistenceBean(actionLogDTO));

            List<ActionLogDetail> actionLogDetailList = actionLogDetailService.getActionLogDetailList(actionLog, chainUser, chainUserOld, null, current, true);
            actionLogDetailRepo.save(actionLogDetailList);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return new BaseMessage(true);
    }

    @Override
    public boolean checkPassword(String oldPassword, String takeSalt) throws Exception {
        return repo.checkPassword(oldPassword, takeSalt);
    }

    @Override
    public String takeSaltWithUser(String userName) throws Exception {
        return repo.takeSaltWithUser(userName);
    }


    @Override
    public boolean checkDuplicatePassword(String newPassword, String takeSalt, String userName) throws Exception {
        return repo.checkDuplicatePassword(newPassword, takeSalt, userName);
    }

    @Override
    public long findId(String userName) throws Exception {
        return repo.findId(userName);
    }

    @Override
    public ChainUserDTO getValueByUserName(String userName) throws Exception {
        return repo.getValueByUserName(userName);
    }

    @Override
    public Date getLastDateChangePass(String userName) {
        return repo.getLastDateChangePass(userName);
    }

    @Override
    public Date getSysDate() throws Exception {
        return DbUtil.getSysDate(em);
    }

    @Override
    public List<ChainUserDTO> getUserByInput(String input) throws Exception {
        return repo.getUserByInput(input);
    }

    //@HUNGNQ
    @Override
    public List<ChainUserDTO> getChainUserByParentShopId(Long parentShopId) throws Exception {
        return repo.getChainUserByParentShopId(parentShopId);
    }

    @Override
    public List<ChainUserDTO> getChainUserByChainShopId(Long chainShopId) throws Exception {
        BooleanExpression booleanExpression = QChainUser.chainUser.status.eq(Short.valueOf(Const.WEB_CHAIN.STATUS_ACTIVE));
        booleanExpression = booleanExpression.and(QChainUser.chainUser.chainShopId.eq(chainShopId));
        return mapper.toDtoBean(repo.findAll(booleanExpression));
    }

    @Override
    public List<ChainUserDTO> getChainUserByChainShopIdOrderByName(Long chainShopId) throws Exception {
        return mapper.toDtoBean(repo.getChainUserByChainShopIdOrderByName(chainShopId));
    }

    //dai lay người nguoi dung chuoi voi type = 3 theo shop
    @Override
    public List<ChainUserDTO> getChainUserByShopId(Long chainShopId) throws Exception {
        BooleanExpression booleanExpression = QChainUser.chainUser.status.eq(Short.valueOf(Const.WEB_CHAIN.STATUS_ACTIVE));
        booleanExpression = booleanExpression.and(QChainUser.chainUser.chainUserType.eq(Const.WEB_CHAIN.CHAIN_USER_TYPE_CHAIN_STORE));
        booleanExpression = booleanExpression.and(QChainUser.chainUser.chainShopId.eq(chainShopId));
        return mapper.toDtoBean(repo.findAll(booleanExpression));
    }

    @Override
    public int countLazyChainUser(InputLazyChainUserSearch inputLazyChainUserSearch) throws Exception {
        return repo.countLazyChainUser(inputLazyChainUserSearch);
    }

    @Override
    public List<ChainUserDTO> findLazyChainUser(InputLazyChainUserSearch inputLazyChainUserSearch, int pageSize, int pageNumber, boolean isLazySearch) throws Exception {
        return repo.findLazyChainUser(inputLazyChainUserSearch, pageSize, pageNumber, isLazySearch);
    }

    @Override
    public boolean checkUserNameExist(Long chainUserId, String userName) throws Exception {
        BooleanExpression booleanExpression = QChainUser.chainUser.userName.equalsIgnoreCase(userName);
        if (null != chainUserId) {
            booleanExpression = booleanExpression.and(QChainUser.chainUser.chainShopId.eq(chainUserId));
        }
        return repo.count(booleanExpression) > 0;
    }

    @Override
    public ChainUserDTO getByUsername(String username) throws Exception {
        return repo.getByUsername(username);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(ChainUserDTO chainUserDTO, ActionLogDTO actionLogDTO) throws Exception {
        try {
            Date sysdate = DbUtil.getSysDate(em);
            // cap nhat
            ChainUser chainUserOld = new ChainUser();
            ChainUser chainUser;
            if (!DataUtil.isNullObject(chainUserDTO.getChainUserId())) {
                chainUser = repo.findOne(chainUserDTO.getChainUserId());
                BeanUtils.copyProperties(chainUser, chainUserOld);
                chainUser.setUpdateDate(sysdate);
                chainUser.setUpdateUser(actionLogDTO.getUserName());
                chainUser.setUserName(chainUserDTO.getUserName());
                chainUser.setChainShopId(chainUserDTO.getChainShopId());
                chainUser.setPhoneNumber(chainUserDTO.getPhoneNumber());
                chainUser.setFullName(chainUserDTO.getFullName());
                repo.saveAndFlush(chainUser);
                //ghi log action detail
                actionLogDTO.setCreateDate(sysdate);
                actionLogDTO.setAction("UPDATE CHAIN_USER");
                actionLogDTO.setObjectId(chainUser.getChainUserId());
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + "[" + chainUserDTO.getChainUserId() + "]");
                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.saveAndFlush(actionLog);
                List<ActionLogDetail> actionLogDetails = actionLogDetailService.getActionLogDetailList(actionLog, chainUser, chainUserOld, new ArrayList<>(), sysdate, true);
                actionLogDetailRepo.save(actionLogDetails);
            } else {// them moi
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                String salt = simpleDateFormat.format(sysdate);
                String hashPassword = AESUtil.createHash(chainUserDTO.getPassword(), salt);
                chainUser = mapper.toPersistenceBean(chainUserDTO);
                chainUser.setCreateDate(sysdate);
                chainUser.setCreateUser(actionLogDTO.getUserName());
                chainUser.setChainUserType(Const.WEB_CHAIN.CHAIN_USER_TYPE_CHAIN_STORE);
                chainUser.setPhoneNumber(chainUserDTO.getPhoneNumber());
                chainUser.setChainShopId(chainUserDTO.getChainShopId());
                chainUser.setStatus(DataUtil.safeToShort(Const.WEB_CHAIN.STATUS_ACTIVE));
                chainUser.setActiveStatus(DataUtil.safeToShort(Const.WEB_CHAIN.STATUS_ACTIVE));
                chainUser.setPassword(hashPassword);
                chainUser.setSalt(salt);
                chainUser.setUserName(chainUserDTO.getPrefixUsername() + chainUser.getUserName());
                repo.saveAndFlush(chainUser);
                actionLogDTO.setCreateDate(sysdate);
                actionLogDTO.setAction("CREATE CHAIN_USER");
                actionLogDTO.setObjectId(chainUser.getChainUserId());
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " ID " + chainUser.getChainUserId());
                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.save(actionLog);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteChainUser(Long chainUserId, ActionLogDTO actionLogDTO) throws Exception {
        Date currentDate = getSysDate(em);
        ChainUser chainUser = repo.findOne(chainUserId);
        chainUser.setStatus(DataUtil.safeToShort(Const.WEB_CHAIN.STATUS_IN_ACTIVE));
        chainUser.setUpdateUser(actionLogDTO.getUserName());
        chainUser.setUpdateDate(currentDate);
        repo.saveAndFlush(chainUser);
        actionLogDTO.setAction("DELETE CHAIN_USER");
        actionLogDTO.setObjectId(chainUser.getChainUserId());
        actionLogDTO.setCreateDate(currentDate);
        actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + chainUserId);
        ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
        actionLogRepo.save(actionLog);
    }

    @Override
    public List<ChainUserDTO> importChainUserTemplate(List<ChainUserDTO> chainUserDTOList, ActionLogDTO actionLogDTO) throws Exception {
        Date createdDate = getSysDate(em);
        ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
        ActionLog actionLogTemp;
        actionLog.setCreateDate(createdDate);
        for (ChainUserDTO chainUserDTO : chainUserDTOList) {
            if (null == chainUserDTO.getImportExcelError()) {
                ChainUser chainUser = mapper.toPersistenceBean(chainUserDTO);
                actionLogTemp = new ActionLog();

                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                    String salt = simpleDateFormat.format(new Date());
                    String hashPassword = AESUtil.createHash(chainUserDTO.getPassword(), salt);
                    chainUser.setCreateDate(createdDate);
                    chainUser.setCreateUser(actionLog.getUserName());
                    chainUser.setActiveStatus(DataUtil.safeToShort(Const.WEB_CHAIN.STATUS_ACTIVE));
                    chainUser.setChainUserType(Const.WEB_CHAIN.CHAIN_USER_TYPE_CHAIN_STORE);
                    chainUser.setChainShopCode(null);
                    chainUser.setPassword(hashPassword);
                    chainUser.setSalt(salt);
                    repo.saveAndFlush(chainUser);
                    actionLogTemp.setActionId(actionLog.getActionId());
                    actionLogTemp.setAction("IMPORT CHAIN_USER");
                    actionLogTemp.setObjectId(chainUser.getChainUserId());
                    actionLogTemp.setCreateDate(actionLog.getCreateDate());
                    actionLogTemp.setUserName(actionLog.getUserName());
                    actionLogTemp.setDescription(actionLog.getDescription() + "[" + chainUser.getChainUserId() + "]");
                    actionLogRepo.saveAndFlush(actionLogTemp);
                    chainUserDTO.setImportExcelError("1");
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    chainUserDTO.setImportExcelError("0");
                }
            }
        }
        return chainUserDTOList;
    }

    @Override
    public boolean validateActive(Long userId, Long chainId, Long shopId, Long userType) throws Exception {
        BooleanExpression booleanExpression = QChainUser.chainUser.status.eq(Short.valueOf(Const.WEB_CHAIN.STATUS_ACTIVE));
        booleanExpression = booleanExpression.and(QChainUser.chainUser.chainUserId.eq(userId));
        Long countUser = repo.count(booleanExpression);
    /*    BooleanExpression booleanChain = QChainShop.chainShop.status.eq(Short.valueOf(Const.WEB_CHAIN.STATUS_ACTIVE));
        booleanChain = booleanChain.and(QChainShop.chainShop.chainShopId.eq(chainId));*/
       /* Long countChain = chainShopRepo.count(booleanChain);*/
      /*  Long countShop = 1L;
        if (userType == 3) {
            BooleanExpression booleanShop = QChainShop.chainShop.status.eq(Short.valueOf(Const.WEB_CHAIN.STATUS_ACTIVE));
            booleanShop = booleanShop.and(QChainShop.chainShop.chainShopId.eq(shopId));
            booleanShop = booleanShop.and(QChainShop.chainShop.parentId.eq(chainId));
            countShop = chainShopRepo.count(booleanShop);
        }*/
        if (countUser == 1) return true;
        else return false;
    }
/*
    //duyetdk
    @Override
    public List<ChainUserDTO> getUsernameByChainShopID(Long chainShopId, Long viettelId) throws Exception {
        return repo.getUsernameByChainShopID(chainShopId, viettelId);
    }*/

   /* //check userTopup
    @Override
    public boolean validateActiveTopup(Long userId, Long chainId,Long userType) throws Exception {
        if(DataUtil.safeEqual(userType,Const.TOPUP_TAXI.CHAIN_USER_TYPE_ADMIN_FINTECH)) return true;
        BooleanExpression booleanExpression = QChainUser.chainUser.status.eq(Short.valueOf(Const.WEB_CHAIN.STATUS_ACTIVE));
        booleanExpression = booleanExpression.and(QChainUser.chainUser.chainUserId.eq(userId));
        Long countUser = repo.count(booleanExpression);
        BooleanExpression booleanChain = QChainShop.chainShop.status.eq(Short.valueOf(Const.WEB_CHAIN.STATUS_ACTIVE));
        booleanChain = booleanChain.and(QChainShop.chainShop.chainShopId.eq(chainId));
        booleanChain = booleanChain.and(QChainShop.chainShop.chainShopType.eq(Const.TOPUP_TAXI.CHAIN_SHOP_TYPE));
        Long countChain = chainShopRepo.count(booleanChain);
        if (countUser == 1 && countChain == 1) return true;
        else return false;
    }*/

  /*  @Override
    public List<ChainUserDTO> getUserByChainTopupId(Long chainShopId) throws Exception {
        BooleanExpression booleanExpression = QChainUser.chainUser.status.eq(Short.valueOf(Const.WEB_CHAIN.STATUS_ACTIVE));
        booleanExpression = booleanExpression.and(QChainUser.chainUser.chainUserType.eq(Const.TOPUP_TAXI.CHAIN_USER_TYPE_ADMIN_CHAIN));
        booleanExpression = booleanExpression.and(QChainUser.chainUser.chainShopId.eq(chainShopId));
        return mapper.toDtoBean(repo.findAll(booleanExpression));
    }*/
}
