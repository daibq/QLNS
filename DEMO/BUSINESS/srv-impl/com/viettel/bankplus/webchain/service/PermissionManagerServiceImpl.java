package com.viettel.bankplus.webchain.service;

import com.mysema.query.types.expr.BooleanExpression;
import com.viettel.bankplus.webchain.dto.*;
import com.viettel.bankplus.webchain.input.InputLazyPermissionGroupFunctionSearch;
import com.viettel.bankplus.webchain.input.InputLazyUserPermissionGroup;
import com.viettel.bankplus.webchain.model.*;
import com.viettel.bankplus.webchain.repo.ActionLogDetailRepo;
import com.viettel.bankplus.webchain.repo.ActionLogRepo;
import com.viettel.bankplus.webchain.repo.GroupPermissionFunctionRepo;
import com.viettel.bankplus.webchain.repo.PermissionGroupUserRepo;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.service.BaseServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author hungnq on 9/5/2017
 */
@Service
public class PermissionManagerServiceImpl extends BaseServiceImpl implements PermissionManagerService {

    @Autowired
    private ActionLogRepo actionLogRepo;
    @Autowired
    private ActionLogDetailRepo actionLogDetailRepo;
    @Autowired
    private ActionLogDetailService actionLogDetailService;
    @Autowired
    private PermissionGroupUserRepo permissionGroupUserRepo;
    public static final Logger logger = Logger.getLogger(FunctionObjectService.class);
    @Autowired
    private GroupPermissionFunctionRepo groupPermissionFunctionRepo;
    @Autowired
    private ActionService actionService;
    private final BaseMapper<ActionLog, ActionLogDTO> mapperActionLog = new BaseMapper<>(ActionLog.class, ActionLogDTO.class);
    private final BaseMapper<PermissionGroupUser, PermissionGroupUserDTO> mapper = new BaseMapper<>(PermissionGroupUser.class, PermissionGroupUserDTO.class);
    private final BaseMapper<PermissionGroupUser, PermissionGroupUserImportDTO> mapper1 = new BaseMapper<>(PermissionGroupUser.class, PermissionGroupUserImportDTO.class);
    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    //truongnx
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdateGroupFunction(List<PermissionGroupDTO> permissionGroupDTOList, List<FunctionObjectDTO> functionObjectDTOList, ActionLogDTO actionLogDTO, boolean isCreate) throws Exception {

      /*  List<ActionDTO> actionDTOList = actionService.getListActionDTOByType(Const.ACTION_LOG.PERMISSION_GROUP_FUNCTION_TYPE);*/
        List<PermissionGroupFunction> groupFunctionList = new ArrayList<>();
        Date sysdate = DbUtil.getSysDate(em);
        for (PermissionGroupDTO permissionGroupDTO : permissionGroupDTOList) {
            for (FunctionObjectDTO functionObjectDTO : functionObjectDTOList) {
                PermissionGroupFunction permissionGroupFunction = new PermissionGroupFunction();
                permissionGroupFunction.setFunctionObjectId(functionObjectDTO.getFunctionObjectId());
                permissionGroupFunction.setPermissionGroupId(permissionGroupDTO.getPermissionGroupId());
                permissionGroupFunction.setCreateUser(actionLogDTO.getUserName());
                permissionGroupFunction.setCreateDate(sysdate);
                permissionGroupFunction.setStatus(Const.WEB_CHAIN.PERMISSION_GROUP_FUNCTION_ACTIVE);
                groupFunctionList.add(permissionGroupFunction);
            }
        }
        if (isCreate) {
            List<PermissionGroupFunction> list = groupPermissionFunctionRepo.save(groupFunctionList);
            List<ActionLogDTO> actionLogDTOList = new ArrayList<>();
            for (PermissionGroupFunction permissionGroupFunction : list) {
                ActionLogDTO actionLogDTO1 = new ActionLogDTO();
                actionLogDTO1.setActionId(actionLogDTO.getActionId());
                actionLogDTO1.setCreateDate(sysdate);
                actionLogDTO1.setAction("CREATE PERMISSION_GROUP_FUNCTION");
                actionLogDTO1.setActionType("1");
                actionLogDTO1.setUserName(actionLogDTO.getUserName());
                actionLogDTO1.setObjectId(permissionGroupFunction.getPermissionGroupFunctionId());
                actionLogDTO1.setDescription(getTextParam(Const.ACTION_LOG.PERMISSION_GROUP_FUNCTION_TYPE_NEW_ADD, permissionGroupFunction.getFunctionObjectId() + "", permissionGroupFunction.getPermissionGroupId() + ""));
                actionLogDTOList.add(actionLogDTO1);
            }
            actionLogRepo.save(mapperActionLog.toPersistenceBean(actionLogDTOList));
            actionLogRepo.flush();

        } else {
            List<PermissionGroupFunction> listSave = getListSave(groupFunctionList, permissionGroupDTOList.get(0).getPermissionGroupId(), actionLogDTO);
            groupPermissionFunctionRepo.save(listSave);
        }
        groupPermissionFunctionRepo.flush();
    }


    //truongnx
    private List<PermissionGroupFunction> getListSave(List<PermissionGroupFunction> groupFunctionList, Long permissionGroupId, ActionLogDTO actionLog) throws Exception {
        Date sysdate = DbUtil.getSysDate(em);
        List<PermissionGroupFunction> listSave = new ArrayList<>();
        List<PermissionGroupFunction> listActive = groupPermissionFunctionRepo.findByPermissionGroupIdAndStatus(permissionGroupId, Const.WEB_CHAIN.PERMISSION_GROUP_FUNCTION_ACTIVE);
        List<PermissionGroupFunction> listInactive = groupPermissionFunctionRepo.findByPermissionGroupIdAndStatus(permissionGroupId, Const.WEB_CHAIN.PERMISSION_GROUP_FUNCTION_INACTIVE);
        List<ActionLogDTO> listLog = new ArrayList<>();
        for (PermissionGroupFunction permissionGroupFunction : groupFunctionList) {
            boolean check = true;
            for (PermissionGroupFunction groupFunction : listActive) {
                if (DataUtil.safeEqual(permissionGroupFunction.getFunctionObjectId(),groupFunction.getFunctionObjectId())) {
                    check = false;
                    listActive.remove(groupFunction);
                    break;
                }
            }
            if (check) {
                for (PermissionGroupFunction groupFunction : listInactive) {
                    if (DataUtil.safeEqual(permissionGroupFunction.getFunctionObjectId(),groupFunction.getFunctionObjectId())) {

                        PermissionGroupFunction groupFunctionOld = new PermissionGroupFunction();
                        BeanUtils.copyProperties(groupFunction, groupFunctionOld);
                        groupFunction.setStatus(Const.WEB_CHAIN.PERMISSION_GROUP_FUNCTION_ACTIVE);

                        //write log update
                        ActionLogDTO actionLogDTO1 = new ActionLogDTO();
                        actionLogDTO1.setActionId(actionLog.getActionId());
                        actionLogDTO1.setCreateDate(sysdate);
                        actionLogDTO1.setAction("UPDATE PERMISSION_GROUP_FUNCTION");
                        actionLogDTO1.setActionType("2");
                        actionLogDTO1.setUserName(actionLog.getUserName());
                        actionLogDTO1.setObjectId(groupFunction.getPermissionGroupFunctionId());
                        actionLogDTO1.setDescription(getTextParam(Const.ACTION_LOG.PERMISSION_GROUP_FUNCTION_TYPE_UPDATE, permissionGroupFunction.getFunctionObjectId() + "", permissionGroupFunction.getPermissionGroupId() + ""));
                        ActionLog actionLog1 = actionLogRepo.save(mapperActionLog.toPersistenceBean(actionLogDTO1));

                        //write logDetail uupdate
                        List<ActionLogDetail> actionLogDetails = actionLogDetailService.getActionLogDetailList(actionLog1, groupFunction, groupFunctionOld, new ArrayList<>(), sysdate, true);

                        actionLogDetailRepo.save(actionLogDetails);

                        listSave.add(groupFunction);
                        check = false;
                        break;
                    }
                }
                if (check) {
                    PermissionGroupFunction permissionGroupFunction1 = groupPermissionFunctionRepo.save(permissionGroupFunction);
                    ActionLogDTO actionLogDTO1 = new ActionLogDTO();
                    actionLogDTO1.setActionId(actionLog.getActionId());
                    actionLogDTO1.setCreateDate(sysdate);
                    actionLogDTO1.setAction("UPDATE PERMISSION_GROUP_FUNCTION");
                    actionLogDTO1.setActionType("2");
                    actionLogDTO1.setUserName(actionLog.getUserName());
                    actionLogDTO1.setObjectId(permissionGroupFunction1.getPermissionGroupFunctionId());
                    actionLogDTO1.setDescription(getTextParam(Const.ACTION_LOG.PERMISSION_GROUP_FUNCTION_TYPE_UPDATE_NEW, permissionGroupFunction.getFunctionObjectId() + "", permissionGroupFunction.getPermissionGroupId() + ""));
                    listLog.add(actionLogDTO1);

                }
            }
        }
        if (!DataUtil.isNullOrEmpty(listActive)) {
            for (PermissionGroupFunction permissionGroupFunction : listActive) {
                permissionGroupFunction.setStatus(Const.WEB_CHAIN.PERMISSION_GROUP_FUNCTION_INACTIVE);
                ActionLogDTO actionLogDTO1 = new ActionLogDTO();
                actionLogDTO1.setActionId(actionLog.getActionId());
                actionLogDTO1.setCreateDate(sysdate);
                actionLogDTO1.setAction("UPDATE PERMISSION_GROUP_FUNCTION");
                actionLogDTO1.setActionType("2");
                actionLogDTO1.setUserName(actionLog.getUserName());
                actionLogDTO1.setObjectId(permissionGroupFunction.getPermissionGroupFunctionId());
                actionLogDTO1.setDescription(getTextParam(Const.ACTION_LOG.PERMISSION_GROUP_FUNCTION_TYPE_DELETE, permissionGroupFunction.getFunctionObjectId() + "", permissionGroupFunction.getPermissionGroupId() + ""));
                listLog.add(actionLogDTO1);
                listSave.add(permissionGroupFunction);
            }
        }
        actionLogRepo.save(mapperActionLog.toPersistenceBean(listLog));
        actionLogRepo.flush();
        actionLogDetailRepo.flush();
        return listSave;
    }

    /**
     * Đại-Start
     */
    /*dai - them moi*/
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void savePermissionGroupUser(PermissionGroupUserDTO dto, List<PermissionGroupDTO> permissionGroupDTOList, List<ChainUserDTO> chainUserDTOList, ActionLogDTO actionLogDTO) throws Exception {
        boolean isCreate = DataUtil.isNullObject(dto.getPermissionGroupUserId()) ? true : false;
        List<PermissionGroupUser> permissionGroupUsers = new ArrayList<>();
        List<PermissionGroupUser> permissionGroupUserList = permissionGroupUserRepo.findAll();

        Date sysdate = DbUtil.getSysDate(em);
        for (PermissionGroupDTO permissionGroupDTO : permissionGroupDTOList) {
            for (ChainUserDTO chainUserDTO : chainUserDTOList) {
                dto.setPermissionGroupId(permissionGroupDTO.getPermissionGroupId());
                dto.setChainUserId(chainUserDTO.getChainUserId());
                dto.setCreateDate(sysdate);
                dto.setStatus(Const.WEB_CHAIN.PERMISSION_GROUP_USER_ACTIVE);
                PermissionGroupUser permissionGroupUser = mapper.toPersistenceBean(dto);
                permissionGroupUsers.add(permissionGroupUser);
            }
        }

        if (isCreate) {
            try {

              /* Tự đông update status len 1 neeuban ghicos status= 0
               for (PermissionGroupUser permissionGroupUserCheck : permissionGroupUserList) {
                    for (PermissionGroupUser permissionGroupUser : permissionGroupUsers) {
                        if (permissionGroupUserCheck.getPermissionGroupId() == permissionGroupUser.getPermissionGroupId() && permissionGroupUserCheck.getChainUserId() == permissionGroupUser.getChainUserId() && permissionGroupUserCheck.getStatus() == Const.WEB_CHAIN.PERMISSION_GROUP_USER_INACTIVE) {
                            permissionGroupUserCheck.setStatus(Const.WEB_CHAIN.PERMISSION_GROUP_USER_ACTIVE);
                            break;
                        }
                    }
                }*/

                for (PermissionGroupUser permissionGroupUserCheck : permissionGroupUserList) {
                    for (PermissionGroupUser permissionGroupUser : permissionGroupUsers) {
                            if (permissionGroupUserCheck.getPermissionGroupId() == permissionGroupUser.getPermissionGroupId() && permissionGroupUserCheck.getChainUserId() == permissionGroupUser.getChainUserId()) {
                                permissionGroupUsers.remove(permissionGroupUser);
                                break;
                            }
                    }
                }

                permissionGroupUserRepo.save(permissionGroupUsers);
                List<ActionLogDTO> actionLogDTOList = new ArrayList<>();
                for (PermissionGroupUser permissionGroupUser : permissionGroupUsers) {
                    ActionLogDTO actionLogDTO1 = new ActionLogDTO();
                    actionLogDTO1.setActionId(actionLogDTO.getActionId());
                    actionLogDTO1.setCreateDate(sysdate);
                    actionLogDTO1.setAction("CREATE PERMISSION_GROUP_USER");
                    actionLogDTO1.setUserName(actionLogDTO.getUserName());
                    actionLogDTO1.setObjectId(permissionGroupUser.getPermissionGroupId());
                    actionLogDTO1.setDescription(getTextParam(Const.ACTION_LOG.PERMISSION_GROUP_USER_TYPE_ADD_NEW, DataUtil.safeToString(permissionGroupUser.getChainUserId()), DataUtil.safeToString(permissionGroupUser.getPermissionGroupId())));
                    actionLogDTOList.add(actionLogDTO1);
                }
                actionLogRepo.save(mapperActionLog.toPersistenceBean(actionLogDTOList));
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    /*dai - sua*/
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updatePermissionGroupUser(PermissionGroupUserDTO dto, ActionLogDTO actionLogDTO) throws Exception {
        boolean isCreate = DataUtil.isNullObject(dto.getPermissionGroupUserId()) ? true : false;
        Date sysdate = DbUtil.getSysDate(em);
        try {
            if (!isCreate) {
                PermissionGroupUser permissionGroupUserOld = new PermissionGroupUser();
                PermissionGroupUser permissionGroupUserl = permissionGroupUserRepo.findOne(dto.getPermissionGroupUserId());
                BeanUtils.copyProperties(permissionGroupUserl, permissionGroupUserOld);
                PermissionGroupUser permissionGroupUserNew = mapper.toPersistenceBean(dto);

                permissionGroupUserNew.setCreateDate(sysdate);
                permissionGroupUserNew.setCreateUser(actionLogDTO.getUserName());
                permissionGroupUserRepo.saveAndFlush(permissionGroupUserNew);

                actionLogDTO.setAction("UPDATE PERMISSION_GROUP_USER");
                actionLogDTO.setCreateDate(sysdate);
                actionLogDTO.setDescription(getTextParam(Const.ACTION_LOG.PERMISSION_GROUP_USER_TYPE_UPDATE, DataUtil.safeToString(permissionGroupUserNew.getChainUserId()), DataUtil.safeToString(permissionGroupUserNew.getPermissionGroupId())));
                actionLogDTO.setObjectId(dto.getPermissionGroupId());

                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.saveAndFlush(actionLog);

                List<ActionLogDetail> actionLogDetails = actionLogDetailService.getActionLogDetailList(actionLog, permissionGroupUserNew, permissionGroupUserOld, new ArrayList<>(), sysdate, true);
                actionLogDetailRepo.save(actionLogDetails);

            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }

    /*dai xoa*/
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deletePermissionGroupUser(Long permissionGroupUserId, ActionLogDTO actionLogDTO) throws Exception {
        try {
            Date sysdate = DbUtil.getSysDate(em);
            PermissionGroupUser permissionGroupUser = permissionGroupUserRepo.findOne(permissionGroupUserId);
            permissionGroupUser.setStatus(Const.WEB_CHAIN.PERMISSION_GROUP_USER_INACTIVE);
            permissionGroupUserRepo.saveAndFlush(permissionGroupUser);

            actionLogDTO.setCreateDate(sysdate);
            actionLogDTO.setAction("DELETE PERMISSION_GROUP_USER");
            actionLogDTO.setObjectId(permissionGroupUser.getPermissionGroupId());
            /*actionLogDTO.setDescription(getText("webchain.functionGroup.actionLog.delete") + " " + "[" + dto.getPermissionGroupId() + "]");*/
            actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + "[" + permissionGroupUser.getPermissionGroupUserId() + "]");
            ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
            actionLogRepo.save(actionLog);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }

    /*dai*/
    @Override
    public List<ChainUserDTO> getNameByChainUserIdByShopId(Long chainUserId) throws Exception {
        return permissionGroupUserRepo.getNameByChainUserIdByShopId(chainUserId) ;
    }
    /*dai*/
    /*@Override
    public List<PermissionGroupDTO> checkPermissionGroupTypOrStartus(Long permissionGroupId) throws Exception {
        return permissionGroupUserRepo.checkPermissionGroupTypOrStartus(permissionGroupId);

    }*/

    /*dai check Id theo status*/
    @Override
    public boolean checkIdPermissionGroupUser(Long id) throws Exception {

        BooleanExpression booleanExpression = null;
        if (!DataUtil.isNullObject(id)) {
            booleanExpression = QPermissionGroupUser.permissionGroupUser.permissionGroupUserId.eq(id);
            booleanExpression = booleanExpression.and(QPermissionGroupUser.permissionGroupUser.status.eq(Long.valueOf(Const.WEB_CHAIN.PERMISSION_GROUP_USER_ACTIVE)));
        }

        List<PermissionGroupUser> list = (List<PermissionGroupUser>) permissionGroupUserRepo.findAll(booleanExpression);
        return list.isEmpty();
    }

    /*dai-findone*/
    @Override
    public PermissionGroupUserDTO findOne(Long id) throws Exception {
        return mapper.toDtoBean(permissionGroupUserRepo.findOne(id));
    }

    /*dai check chainUserId theo permissionGroupId*/
    @Override
    public boolean checkchainUserId(Long chainUserId, Long permissionGroupId) throws Exception {
        BooleanExpression booleanExpression = QPermissionGroupUser.permissionGroupUser.chainUserId.eq(chainUserId);
        booleanExpression = booleanExpression.and(QPermissionGroupUser.permissionGroupUser.permissionGroupId.eq(permissionGroupId));
        /*booleanExpression = booleanExpression.and(QPermissionGroupUser.permissionGroupUser.status.eq(Long.valueOf(Const.WEB_CHAIN.PERMISSION_GROUP_USER_ACTIVE)));*/
        List<PermissionGroupUser> list = (List<PermissionGroupUser>) permissionGroupUserRepo.findAll(booleanExpression);
        return list.isEmpty();
    }

    /**
     * Đại-end
     */

    //truongnx
    @Override
    public int countLazyGroupFunction(InputLazyPermissionGroupFunctionSearch inputSearch) throws Exception {
        return groupPermissionFunctionRepo.countLazyGroupFunction(inputSearch);
    }

    //truongnx
    @Override
    public List<GroupPermissionFunctionDTO> findLazyGroupFunction(InputLazyPermissionGroupFunctionSearch inputSearch, int pageSize, int pageNumber) throws Exception {
        return groupPermissionFunctionRepo.findLazyGroupFunction(inputSearch, pageSize, pageNumber);
    }
    //dai import
    @Override
    public List<PermissionGroupUserImportDTO> importPermissionGroupUserDTOTemplate(List<PermissionGroupUserImportDTO> DTOList, ActionLogDTO actionLogDTO) throws Exception {
        {
            Date createdDate = getSysDate(em);
            ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
            ActionLog actionLogTemp;
            actionLog.setCreateDate(createdDate);
            for (PermissionGroupUserImportDTO permissionGroupUserImportDTO : DTOList) {
                if (null == permissionGroupUserImportDTO.getImportExcelError()) {
                    try {
                        PermissionGroupUser obj = mapper1.toPersistenceBean(permissionGroupUserImportDTO);
                        obj.setStatus(Const.WEB_CHAIN.PERMISSION_GROUP_USER_ACTIVE);
                        obj.setCreateDate(createdDate);
                        obj.setCreateUser(actionLog.getUserName());
                        permissionGroupUserRepo.saveAndFlush(obj);

                        actionLogTemp = new ActionLog();
                        actionLogTemp.setActionId(actionLog.getActionId());
                        actionLogTemp.setObjectId(obj.getPermissionGroupUserId());
                        actionLogTemp.setAction("IMPORT PERMISSION_GROUP_USER");
                        actionLogTemp.setCreateDate(actionLog.getCreateDate());
                        actionLogTemp.setUserName(actionLog.getUserName());
                        actionLogTemp.setDescription(getTextParam(Const.ACTION_LOG.PERMISSION_GROUP_USER_IMPORT, DataUtil.safeToString(obj.getChainUserId()), DataUtil.safeToString(obj.getPermissionGroupId())));
                        actionLogRepo.saveAndFlush(actionLogTemp);

                        permissionGroupUserImportDTO.setImportExcelError("1");
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                        permissionGroupUserImportDTO.setImportExcelError("0");
                    }
                }
            }
            return DTOList;
        }
    }


    @Override
    public int countLazyUserPermissionGroup(InputLazyUserPermissionGroup inputLazyUserPermissionGroup) throws Exception {
        return permissionGroupUserRepo.countUserPermissionGroup(inputLazyUserPermissionGroup);
    }

    @Override
    public List<PermissionGroupUserDTO> findLazyUserPermissionGroup(InputLazyUserPermissionGroup inputLazyUserPermissionGroup, int pageSize, int pageNumber) throws Exception {
        return permissionGroupUserRepo.findUserPermissionGroup(inputLazyUserPermissionGroup, pageSize, pageNumber);
    }

}
