package com.viettel.bankplus.webchain.service;

import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.expr.BooleanExpression;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.input.InputLazyFunctionSearch;
import com.viettel.bankplus.webchain.model.ActionLog;
import com.viettel.bankplus.webchain.model.ActionLogDetail;
import com.viettel.bankplus.webchain.model.FunctionObject;
import com.viettel.bankplus.webchain.model.QFunctionObject;
import com.viettel.bankplus.webchain.repo.ActionLogDetailRepo;
import com.viettel.bankplus.webchain.repo.ActionLogRepo;
import com.viettel.bankplus.webchain.repo.FunctionObjectRepo;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.service.BaseServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebMethod;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FunctionObjectServiceImpl extends BaseServiceImpl implements FunctionObjectService {
    private final BaseMapper<ActionLog, ActionLogDTO> mapperActionLog = new BaseMapper<>(ActionLog.class, ActionLogDTO.class);
    private final BaseMapper<FunctionObject, FunctionObjectDTO> mapper = new BaseMapper<>(FunctionObject.class, FunctionObjectDTO.class);
    @Autowired
    private ActionLogRepo actionLogRepo;
    @Autowired
    private ActionLogDetailRepo actionLogDetailRepo;
    @Autowired
    private ActionLogDetailService actionLogDetailService;
    @Autowired
    private FunctionObjectRepo repo;
    public static final Logger logger = Logger.getLogger(FunctionObjectService.class);
    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    @WebMethod
    public Long count(List<FilterRequest> filters) throws Exception {
        return repo.count(repo.toPredicate(filters));
    }

    @Override
    public int countLazyFunction(InputLazyFunctionSearch inputLazyFunctionSearch) throws Exception {
        return repo.countLazyFunction(inputLazyFunctionSearch);
    }

    @Override
    public List<FunctionObjectDTO> findLazyFunction(InputLazyFunctionSearch inputLazyFunctionSearch, int pageSize, int pageNumber, boolean isLazySearch) throws Exception {
        return repo.findLazyFunction(inputLazyFunctionSearch, pageSize, pageNumber, isLazySearch);
    }

    @WebMethod
    public FunctionObjectDTO findOne(Long id) throws Exception {
        return mapper.toDtoBean(repo.findOne(id));
    }

    @WebMethod
    public List<FunctionObjectDTO> findByFilter(List<FilterRequest> filters) throws Exception {

        return mapper.toDtoBean(repo.findAll(repo.toPredicate(filters)));
    }

    /**
     * @param parentId Ma chuc nang cha. Neu ma chuc nang cha null, lay tat ca chuc nang cap 1. Neu ma chuc nang cha khac null, lay tat cac chuc nang con cua chuc nang do
     * @param status   Trang thai chuc nang. Neu trang trai chuc nang null, lay ta ca ma chuc nang, khong phan biet trang thai
     *
     * @return Danh sach chuc nang
     *
     * @throws Exception
     */

    @Override
    public List<FunctionObjectDTO> findByParentId(Long parentId, Short status) throws Exception {
        BooleanExpression booleanExpression = QFunctionObject.functionObject.status.eq(Short.valueOf("1"));
        if (null == parentId) {
            booleanExpression = booleanExpression.and(QFunctionObject.functionObject.parentId.isNull());
        } else {
            booleanExpression = booleanExpression.and(QFunctionObject.functionObject.parentId.eq(parentId));
        }
        if (null != status) {
            booleanExpression = booleanExpression.and(QFunctionObject.functionObject.status.eq(Short.valueOf(status)));
        }
        OrderSpecifier<Short> sortOrder = QFunctionObject.functionObject.orderNo.asc();
        return mapper.toDtoBean((List<FunctionObject>) repo.findAll(booleanExpression, sortOrder));
    }

    @Override
    public List<FunctionObjectDTO> findByParentIdFlowUserId(Long userId, Long parentId, Short status) throws Exception {
        return mapper.toDtoBean(repo.findByParentIdFlowUserId(userId, parentId, status));
    }

    @Override
    public boolean isExistOrderNo(Long parentId, Short oderNo, Long functionObjectId) throws Exception {
        BooleanExpression booleanExpression = QFunctionObject.functionObject.orderNo.eq(oderNo);
        if (null == parentId) {
            booleanExpression = booleanExpression.and(QFunctionObject.functionObject.parentId.isNull());
        } else {
            booleanExpression = booleanExpression.and(QFunctionObject.functionObject.parentId.eq(parentId));
        }
        if (null != functionObjectId) {
            booleanExpression = booleanExpression.and(QFunctionObject.functionObject.functionObjectId.ne(functionObjectId));
        }
        List<FunctionObject> list = (List<FunctionObject>) repo.findAll(booleanExpression);
        return !list.isEmpty();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdate(FunctionObjectDTO functionObjectDTO, ActionLogDTO actionLogDTO) throws Exception {
        try {
            Date sysdate = DbUtil.getSysDate(em);
            // cap nhat
            if (!DataUtil.isNullObject(functionObjectDTO.getFunctionObjectId())) {
                FunctionObject functionObjectOld = new FunctionObject();
                FunctionObject functionObject = repo.findOne(functionObjectDTO.getFunctionObjectId());
                BeanUtils.copyProperties(functionObject, functionObjectOld);
                FunctionObject functionObjectNew = mapper.toPersistenceBean(functionObjectDTO);
                functionObjectNew.setUpdateDate(sysdate);
                functionObjectNew.setUpdateUser(actionLogDTO.getUserName());
                repo.saveAndFlush(functionObjectNew);
                //ghi log action detail
                actionLogDTO.setCreateDate(sysdate);
                actionLogDTO.setAction("UPDATE FUNCTION_OBJECT");
                actionLogDTO.setObjectId(functionObjectDTO.getFunctionObjectId());
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + functionObjectDTO.getFunctionObjectId());
                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.saveAndFlush(actionLog);
                List<ActionLogDetail> actionLogDetails = actionLogDetailService.getActionLogDetailList(actionLog, functionObjectNew, functionObjectOld, new ArrayList<>(), sysdate, true);
                actionLogDetailRepo.save(actionLogDetails);
            } else {// them moi
                functionObjectDTO.setCreateDate(sysdate);
                functionObjectDTO.setCreateUser(actionLogDTO.getUserName());
                FunctionObject functionObject = mapper.toPersistenceBean(functionObjectDTO);
                repo.saveAndFlush(functionObject);
                actionLogDTO.setCreateDate(sysdate);
                actionLogDTO.setAction("CREATE FUNCTION_OBJECT");
                actionLogDTO.setObjectId(functionObject.getFunctionObjectId());
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + functionObject.getFunctionObjectId());
                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.save(actionLog);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }

    @Override
    public List<FunctionObjectDTO> getListChild(Long permissionId, boolean isChild) throws Exception {
        return mapper.toDtoBean(repo.getListChild(permissionId, isChild));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteFunction(Long functionId, ActionLogDTO actionLogDTO) throws Exception {
        Date currentDate = getSysDate(em);
        FunctionObject functionObject = repo.findOne(functionId);
        functionObject.setStatus(DataUtil.safeToShort(Const.WEB_CHAIN.STATUS_IN_ACTIVE));
        functionObject.setUpdateDate(currentDate);
        functionObject.setUpdateUser(actionLogDTO.getUserName());
        repo.saveAndFlush(functionObject);
        actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + functionId);
        actionLogDTO.setAction("DELETE FUNCTION_OBJECT");
        actionLogDTO.setObjectId(functionObject.getFunctionObjectId());
        actionLogDTO.setCreateDate(currentDate);
        ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
        actionLogRepo.save(actionLog);
    }

    //truongnx
    @Override
    public boolean checkExistCode(String code) throws Exception {
        BooleanExpression booleanExpression = QFunctionObject.functionObject.functionCode.equalsIgnoreCase(code);
        List<FunctionObject> list = (List<FunctionObject>) repo.findAll(booleanExpression);
        return !list.isEmpty();
    }
    //truongnx
    @Override
    public boolean checkExistName(String name, Long parentId) throws Exception {
        BooleanExpression booleanExpression = QFunctionObject.functionObject.functionName.equalsIgnoreCase(name);
        if (null == parentId) {
            booleanExpression = booleanExpression.and(QFunctionObject.functionObject.parentId.isNull());
        } else {
            booleanExpression = booleanExpression.and(QFunctionObject.functionObject.parentId.eq(parentId));
        }
        List<FunctionObject> list = (List<FunctionObject>) repo.findAll(booleanExpression);
        return !list.isEmpty();
    }
}
