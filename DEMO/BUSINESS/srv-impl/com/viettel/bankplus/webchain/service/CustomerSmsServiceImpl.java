/*
package com.viettel.bankplus.webchain.service;

import com.viettel.bankplus.webchain.dto.TransAppDTO;
import com.viettel.bankplus.webchain.model.TransApp;
import com.viettel.bankplus.webchain.repo.TransAppRepo;
import com.viettel.common.Const;
import com.viettel.common.TransactionUtils;
import com.viettel.fw.SystemConfig;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.dto.BaseMessage;
import com.viettel.fw.service.BaseServiceImpl;
import com.viettel.web.common.controller.BaseController;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.com.viettel.bccs.api.*;
import vn.com.viettel.bccs.client.Client;

import java.util.Date;

import static vn.com.viettel.bccs.api.APIUtil.JACKSON_MAPPER_API;

*/
/**
 * @author hungnq on 9/21/2017
 *//*


@Service
public class CustomerSmsServiceImpl extends BaseServiceImpl implements CustomerSmsService {
    public static final Logger logger = Logger.getLogger(CustomerSmsServiceImpl.class);
    private final BaseMapper<TransApp, TransAppDTO> transAppMapper = new BaseMapper<>(TransApp.class, TransAppDTO.class);
    @Autowired
    private TransAppRepo transAppRepo;
    @Autowired
    private TransAppService transAppService;
    @Autowired
    private SystemConfig systemConfig;

    @Override
    public BaseMessage sendSms(SendSmsWebRequest request) throws Exception {
        BaseMessage baseMessage = new BaseMessage();

        request.setOrderId(transAppService.getTransactionOrderId());
        String clientId = DataUtil.isNullOrEmpty(systemConfig.CORE_API_CLIENT_ID) ? Const.CALL_API.CORE_API_CLIENT_ID : systemConfig.CORE_API_CLIENT_ID;
        request.setClientId(clientId);
        String data = JACKSON_MAPPER_API.writeValueAsString(request);
        TransAppDTO transAppDTO = new TransAppDTO();
        transAppDTO.setOrderId(request.getOrderId());
        transAppDTO.setRequestContent(data);
        transAppDTO.setStaffUsername(request.getStaffUsername());
        transAppDTO.setRequestDate(new Date());
        transAppDTO.setChainCode(request.getPartnerCode());
        transAppDTO.setShopCode(request.getShopCode());
        transAppDTO.setChainName(request.getChainName());
        transAppDTO.setShopName(request.getShopName());
        transAppDTO.setTransStatus(Const.TRANS_STATUS.IN_PROCCESS);
        transAppDTO.setViettelBankCode(Const.API.VIETTEL_BANK_CODE);
        transAppDTO.setAppId(Const.API.APP_ID);
        transAppDTO.setServerPath(systemConfig.CORE_API_URL);
        transAppDTO.setProcessCode(Constant.ApiCmd.SEND_SMS_WEB.toString());
        transAppDTO.setProcessName(Constant.ApiCmd.SEND_SMS_WEB.getFullName());
        transAppRepo.saveAndFlush(transAppMapper.toPersistenceBean(transAppDTO));
        SendSmsWebResponse customerSmsWebResponse;
        try {
            String response = Client.callAPIWithConfig(Constant.ApiCmd.SEND_SMS_WEB.toString(), data, systemConfig);
            customerSmsWebResponse = JACKSON_MAPPER_API.readValue(response, SendSmsWebResponse.class);
            if (DataUtil.isNullObject(customerSmsWebResponse)) {
                baseMessage.setSuccess(false);
                baseMessage.setDescription(getText("common.viettel.api.sms.error"));
                transAppDTO.setErrorMsg(getText("common.error.happened.againAction"));
                transAppDTO.setCorrectMsg(transAppDTO.getErrorMsg());
                transAppDTO.setTransStatus(Const.TRANS_STATUS.TIME_OUT);
                transAppRepo.saveAndFlush(transAppMapper.toPersistenceBean(transAppDTO));
            } else {
                transAppDTO.setRequestId(DataUtil.safeToLong(customerSmsWebResponse.getTransId()));
                transAppDTO.setResponseDate(new Date());
                transAppDTO.setResponseContent(response);
                transAppDTO.setErrorCode(customerSmsWebResponse.getErrorCode());
                transAppDTO.setErrorMsg(customerSmsWebResponse.getErrorMsg());
                transAppDTO.setCorrectCode(customerSmsWebResponse.getErrorCode());
                transAppDTO.setCorrectMsg(customerSmsWebResponse.getErrorMsg());
                transAppDTO.setTransStatus(TransactionUtils.getTransStatus(customerSmsWebResponse.getErrorCode()));
                transAppRepo.saveAndFlush(transAppMapper.toPersistenceBean(transAppDTO));
                if (Const.ERROR.SUCCESS_00.equals(customerSmsWebResponse.getErrorCode())) {
                    baseMessage.setSuccess(true);
                } else {
                    baseMessage.setSuccess(false);
                    if (DataUtil.isNullOrEmpty(customerSmsWebResponse.getErrorCode())) {
                        baseMessage.setDescription(getText("common.viettel.api.sms.error"));
                    } else {
                        if (!DataUtil.isNullOrEmpty(customerSmsWebResponse.getErrorMsg())) {
                            baseMessage.setDescription(customerSmsWebResponse.getErrorMsg());
                        } else
                            baseMessage.setDescription(getText("common.viettel.api.sms.error"));
                    }
                }
            }
        } catch (Exception ex) {
            baseMessage.setSuccess(false);
            baseMessage.setDescription(getText("common.viettel.api.sms.error"));
            logger.error(ex.getMessage(), ex);
            transAppDTO.setErrorMsg(getText("common.error.happened.againAction"));
            transAppDTO.setCorrectMsg(transAppDTO.getErrorMsg());
            transAppDTO.setTransStatus(Const.TRANS_STATUS.TIME_OUT);
            transAppRepo.saveAndFlush(transAppMapper.toPersistenceBean(transAppDTO));
        }
        return baseMessage;
    }
}
*/
