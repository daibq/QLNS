package com.viettel.bankplus.webchain.service;

import com.mysema.query.types.expr.BooleanExpression;
import com.viettel.bankplus.webchain.dto.AreaDTO;
import com.viettel.bankplus.webchain.model.QArea;
import com.viettel.bankplus.webchain.repo.AreaRepo;
import com.viettel.common.Const;
import com.viettel.fw.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hungnq on 9/6/2017
 */
@Service
public class AreaServiceImpl extends BaseServiceImpl implements AreaService {
    @Autowired
    private AreaRepo areaRepo;

    @Override
    public List<AreaDTO> getProvinceList() throws Exception {
        return areaRepo.getProvinceList();
    }

    @Override
    public List<AreaDTO> getDistrictListByProvinceCode(String provinceCode) throws Exception {

        return areaRepo.getListDistrictByProvinceCode(provinceCode);
    }

    @Override
    public List<AreaDTO> getPrecinctListByDistrictCode(String districtCode) throws Exception {
        return areaRepo.getPrecinctListByDistrictCode(districtCode);
    }

    @Override
    public List<AreaDTO> exportArea() throws Exception {
        return areaRepo.exportArea();
    }

    @Override
    public boolean isExistArea(String areaCode) throws Exception {
        BooleanExpression booleanExpression = QArea.area.status.equalsIgnoreCase(Const.WEB_CHAIN.STATUS_ACTIVE);
        booleanExpression = booleanExpression.and(QArea.area.areaCode.equalsIgnoreCase(areaCode));
        return areaRepo.count(booleanExpression) > 0;
    }
}
