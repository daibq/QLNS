package com.viettel.bankplus.webchain.service;

import com.mysema.query.types.expr.BooleanExpression;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;

import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.dto.PermissionGroupDTO;
import com.viettel.bankplus.webchain.input.InputLazyFunctionGroupSearch;
import com.viettel.bankplus.webchain.model.ActionLog;
import com.viettel.bankplus.webchain.model.ActionLogDetail;
import com.viettel.bankplus.webchain.model.PermissionGroup;
import com.viettel.bankplus.webchain.model.QPermissionGroup;
import com.viettel.bankplus.webchain.repo.ActionLogDetailRepo;
import com.viettel.bankplus.webchain.repo.ActionLogRepo;
import com.viettel.bankplus.webchain.repo.PermissionGroupRepo;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.mapper.BaseMapper;
import com.viettel.fw.service.BaseServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebMethod;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PermissionGroupServiceImpl extends BaseServiceImpl implements PermissionGroupService {
    private final BaseMapper<ActionLog, ActionLogDTO> mapperActionLog = new BaseMapper<>(ActionLog.class, ActionLogDTO.class);
    private final BaseMapper<PermissionGroup, PermissionGroupDTO> mapper = new BaseMapper<>(PermissionGroup.class, PermissionGroupDTO.class);

    public static final Logger logger = Logger.getLogger(PermissionGroupService.class);

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    @Autowired
    private PermissionGroupRepo repo;
    @Autowired
    private ActionLogRepo actionLogRepo;
    @Autowired
    private ActionLogDetailRepo actionLogDetailRepo;
    @Autowired
    private ActionLogDetailService actionLogDetailService;



    /*@WebMethod
    public Long count(List<FilterRequest> filters) throws Exception {
        return repo.count(repo.toPredicate(filters));
    }*/

    @WebMethod
    public PermissionGroupDTO findOne(Long id) throws Exception {
        return mapper.toDtoBean(repo.findOne(id));
    }

    @Override
    public int countLazyFunctionGroup(InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch) throws Exception {
        return repo.countLazyFunctionGroup(inputLazyFunctionGroupSearch);
    }

    @Override
    public List<PermissionGroupDTO> findLazingPaging(InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch, int pageSize, int pageNumber) throws Exception {
        return repo.findLazingPaging(inputLazyFunctionGroupSearch, pageSize, pageNumber);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveorUodate(PermissionGroupDTO permissionGroupDTO, ActionLogDTO actionLogDTO) throws Exception {
        try {
            Date currentDate = DbUtil.getSysDate(em);
            permissionGroupDTO.setPermissionGroupCode(permissionGroupDTO.getPermissionGroupCode().toUpperCase());
            boolean isCreate = DataUtil.isNullObject(permissionGroupDTO.getPermissionGroupId()) ? true : false;

            if (isCreate) {
                permissionGroupDTO.setCreateDate(currentDate);
                PermissionGroup permissionGroup = mapper.toPersistenceBean(permissionGroupDTO);
                repo.saveAndFlush(permissionGroup);

                actionLogDTO.setCreateDate(currentDate);
                actionLogDTO.setAction("CREATE PERMISSION_GROUP");
                actionLogDTO.setObjectId(permissionGroup.getPermissionGroupId());
                /*actionLogDTO.setDescription(getText("webchain.functionGroup.actionLog.add") + " " + "[" + permissionGroup.getPermissionGroupId() + "]");*/
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + "[" + permissionGroup.getPermissionGroupId() + "]");
                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.save(actionLog);
            } else {
                PermissionGroup permissionGroupOld = new PermissionGroup();
                PermissionGroup permissionGroupl = repo.findOne(permissionGroupDTO.getPermissionGroupId());
                BeanUtils.copyProperties(permissionGroupl, permissionGroupOld);
                PermissionGroup permissionGroupNew = mapper.toPersistenceBean(permissionGroupDTO);
                //  ApDomain apDomain = mapper.toPersistenceBean(dto);
                permissionGroupNew.setUpdateDate(currentDate);
                permissionGroupNew.setUpdateUser(actionLogDTO.getUserName());
                repo.saveAndFlush(permissionGroupNew);

                actionLogDTO.setAction("UPDATE PERMISSION_GROUP");
                actionLogDTO.setCreateDate(currentDate);
                /*actionLogDTO.setDescription(getText("webchain.functionGroup.actionLog.edit") + " " + "[" + permissionGroupNew.getPermissionGroupId() + "]");*/
                actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + "[" + permissionGroupNew.getPermissionGroupId() + "]");
                actionLogDTO.setObjectId(permissionGroupDTO.getPermissionGroupId());

                ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
                actionLogRepo.saveAndFlush(actionLog);

                List<ActionLogDetail> actionLogDetails = actionLogDetailService.getActionLogDetailList(actionLog, permissionGroupNew, permissionGroupOld, new ArrayList<>(), currentDate, true);
                actionLogDetailRepo.save(actionLogDetails);


            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteList(PermissionGroupDTO dto, ActionLogDTO actionLogDTO, Long chainShopId) throws Exception {
        Date currentDate = DbUtil.getSysDate(em);
        try {
            dto.setStatus(0l);
            dto.setChainShopId(chainShopId);
            PermissionGroup permissionGroup = mapper.toPersistenceBean(dto);
            mapper.toDtoBean(repo.saveAndFlush(permissionGroup));

            actionLogDTO.setCreateDate(currentDate);
            actionLogDTO.setAction("DELETE PERMISSION_GROUP");
            actionLogDTO.setObjectId(permissionGroup.getPermissionGroupId());
            /*actionLogDTO.setDescription(getText("webchain.functionGroup.actionLog.delete") + " " + "[" + dto.getPermissionGroupId() + "]");*/
            actionLogDTO.setDescription(actionLogDTO.getDescription() + " " + "[" + dto.getPermissionGroupId() + "]");
            ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
            actionLogRepo.save(actionLog);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }

    }

    @Override
    public boolean checkName(Long chainShopId, String name) throws Exception {
        BooleanExpression booleanExpression = QPermissionGroup.permissionGroup.permissionGroupName.equalsIgnoreCase(name);
        booleanExpression = booleanExpression.and(QPermissionGroup.permissionGroup.chainShopId.eq(chainShopId));
        List<PermissionGroup> list = (List<PermissionGroup>) repo.findAll(booleanExpression);
        return list.isEmpty();
    }

  /*  @Override
    public boolean checkPermissionGroupTypOrStartus(Long permissionGroupId) throws Exception {

        BooleanExpression booleanExpression = QPermissionGroup.permissionGroup.permissionGroupType.eq(1l);
        booleanExpression = booleanExpression.and(QPermissionGroup.permissionGroup.permissionGroupType.eq(2l));
        booleanExpression = booleanExpression.and(QPermissionGroup.permissionGroup.permissionGroupId.eq(permissionGroupId));
        booleanExpression= booleanExpression.and(QPermissionGroup.permissionGroup.status.eq(1l));
        List<PermissionGroup> list = (List<PermissionGroup>) repo.findAll(booleanExpression);
        return list.isEmpty();
    }*/

    @Override
    public boolean checkCodeOrId(String code, Long id) throws Exception {
        BooleanExpression booleanExpression = null;

        if (!DataUtil.isNullObject(code)) {
            booleanExpression = QPermissionGroup.permissionGroup.permissionGroupCode.equalsIgnoreCase(code);
        }
        if (!DataUtil.isNullObject(id)) {
            booleanExpression = QPermissionGroup.permissionGroup.permissionGroupId.eq(id);
            booleanExpression = booleanExpression.and(QPermissionGroup.permissionGroup.status.eq(1L));
        }

        List<PermissionGroup> list = (List<PermissionGroup>) repo.findAll(booleanExpression);
        return list.isEmpty();
    }

    @Override
    public boolean checkShopCodeOrNameExist(Long chainShopId, String code, String name) throws Exception {
        BooleanExpression booleanExpression = null;
        if (!DataUtil.isNullObject(code)) {
            booleanExpression = QPermissionGroup.permissionGroup.permissionGroupCode.equalsIgnoreCase(code);
            booleanExpression = booleanExpression.and(QPermissionGroup.permissionGroup.chainShopId.eq(chainShopId));
        }

        if (!DataUtil.isNullObject(name)) {
            booleanExpression = QPermissionGroup.permissionGroup.permissionGroupName.equalsIgnoreCase(name);
            booleanExpression = booleanExpression.and(QPermissionGroup.permissionGroup.chainShopId.eq(chainShopId));
        }
        return repo.count(booleanExpression) > 0;
    }

    @Override
    public int listDetailCount(PermissionGroupDTO dto) throws Exception {
        return repo.listDetailCount(dto);
    }

    @Override
    public List<FunctionObjectDTO> listDetail(PermissionGroupDTO dto, int pageSize, int pageNumber) throws Exception {
        return repo.listDetail(dto, pageSize, pageNumber);
    }

    /**
     * @param chainShopId
     * @return
     */

    @Override
    public List<PermissionGroupDTO> getPermissionGroupDTOListByShopId(Long chainShopId) throws Exception {
        /*BooleanExpression booleanExpression = QPermissionGroup.permissionGroup.status.eq(Long.valueOf(Const.WEB_CHAIN.STATUS_ACTIVE));
        booleanExpression = booleanExpression.and(QPermissionGroup.permissionGroup.chainShopId.eq(chainShopId));
        booleanExpression = booleanExpression.and(QPermissionGroup.permissionGroup.permissionGroupType.eq(Long.valueOf(Const.WEB_CHAIN.CHAIN_SHOP_TYPE_CHAIN_STORE)));
        return mapper.toDtoBean(repo.findAll(booleanExpression));*/
        return repo.getPermissionGroupDTOListByShopId(chainShopId);
    }

    @Override
    public List<PermissionGroupDTO> getPermissionCodeListByShopId(Long chainShopId) throws Exception {
        BooleanExpression booleanExpression = QPermissionGroup.permissionGroup.chainShopId.eq(chainShopId);
        return mapper.toDtoBean(repo.findAll(booleanExpression, QPermissionGroup.permissionGroup.permissionGroupCode.asc()));
    }

    @Override
    public List<PermissionGroupDTO> importChainShopTemplate(List<PermissionGroupDTO> DTOList, ActionLogDTO actionLogDTO) throws Exception {
        Date createdDate = getSysDate(em);
        ActionLog actionLog = mapperActionLog.toPersistenceBean(actionLogDTO);
        ActionLog actionLogTemp;
        actionLog.setCreateDate(createdDate);
        for (PermissionGroupDTO permissionGroupDTO : DTOList) {
            if (null == permissionGroupDTO.getImportExcelError()) {
                try {
                    PermissionGroup obj = mapper.toPersistenceBean(permissionGroupDTO);
                    obj.setCreateDate(createdDate);
                    obj.setCreateUser(actionLog.getUserName());
                    repo.saveAndFlush(obj);

                    actionLogTemp = new ActionLog();
                    actionLogTemp.setActionId(actionLog.getActionId());
                    actionLogTemp.setObjectId(obj.getPermissionGroupId());
                    actionLogTemp.setAction("IMPORT PERMISSION_GROUP");
                    actionLogTemp.setCreateDate(actionLog.getCreateDate());
                    actionLogTemp.setUserName(actionLog.getUserName());
                    actionLogTemp.setDescription(actionLog.getDescription() + "[" + obj.getPermissionGroupId() + "]");
                    actionLogRepo.saveAndFlush(actionLogTemp);

                    permissionGroupDTO.setImportExcelError("1");
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    permissionGroupDTO.setImportExcelError("0");
                }
            }
        }
        return DTOList;
    }

    /* @WebMethod
     public List<PermissionGroupDTO> findByFilter(List<FilterRequest> filters) throws Exception {

         return mapper.toDtoBean(repo.findAll(repo.toPredicate(filters)));
     }*/
    @Override
    public List<PermissionGroupDTO> findByStatus(boolean isCreate, Long shopId) throws Exception {
        return mapper.toDtoBean(repo.findByStatus(isCreate, shopId));
    }

}
