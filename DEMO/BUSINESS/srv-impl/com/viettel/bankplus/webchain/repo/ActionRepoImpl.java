package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.ActionDTO;
import com.viettel.bankplus.webchain.input.InputLazyActionSearch;
import com.viettel.bankplus.webchain.model.Action;
import com.viettel.bankplus.webchain.model.QAction;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.template.BooleanTemplate;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.mapper.BaseMapper;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class ActionRepoImpl implements ActionRepoCustom {

    public static final Logger logger = Logger.getLogger(ActionRepoCustom.class);
    public final transient BaseMapper<Action, ActionDTO> mapper = new BaseMapper<>(Action.class, ActionDTO.class);

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    @Override
    public Predicate toPredicate(List<FilterRequest> filters) {
        logger.info("Entering predicates :: " + filters);
        QAction action = QAction.action;
        BooleanExpression result = BooleanTemplate.create("1 = 1");
        try {
            for (FilterRequest filter : filters) {
                ActionDTO.COLUMNS column = ActionDTO.COLUMNS.valueOf(filter.getProperty().toUpperCase());
                BooleanExpression expression = null;
                switch (column) {
                    case ACTIONID:
                        expression = DbUtil.createLongExpression(action.actionId, filter);
                        break;
                    case CODE:
                        expression = DbUtil.createStringExpression(action.code, filter);
                        break;
                    case DESCRIPTION:
                        expression = DbUtil.createStringExpression(action.description, filter);
                        break;
                    case NAME:
                        expression = DbUtil.createStringExpression(action.name, filter);
                        break;
                    case STATUS:
                        expression = DbUtil.createShortExpression(action.status, filter);
                        break;
                    case TYPE:
                        expression = DbUtil.createStringExpression(action.type, filter);
                        break;
                    default:break;
                }
                if (expression != null) {
                    result = result.and(expression);
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        logger.info("Result Predicate :: " + (result != null ? result.toString() : ""));
        logger.info("Exiting predicates");
        return result;
    }

    @Override
    public int countActionList(InputLazyActionSearch input) {
        List pramList = new ArrayList();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT count(*)");
        sql.append("FROM ACTION P WHERE 1=1 ");

        if (!DataUtil.isNullOrEmpty(input.getCode())) {
            sql.append(" AND LOWER(P.CODE) LIKE? ESCAPE '\\' ");
            String code = "%" + DataUtil.formatQueryString(input.getCode()) + "%";
            pramList.add(code);
        }
        if (!DataUtil.isNullOrEmpty(input.getName())) {
            sql.append(" AND LOWER(P.NAME) LIKE? ESCAPE '\\' ");
            String name = "%" + DataUtil.formatQueryString(input.getName()) + "%";
            pramList.add(name);
        }
        if (!DataUtil.isNullOrEmpty(input.getType())) {
            sql.append(" AND P.TYPE =? ");
            pramList.add(input.getType());
        }
        if (!DataUtil.isNullObject(input.getStatus())) {
            sql.append(" AND P.STATUS = ? ");
            pramList.add(input.getStatus());
        }
        /*sql.append("AND P.STATUS != 0 ");*/
        sql.append("ORDER BY P.TYPE DESC, P.STATUS DESC , P.ACTION_ID DESC");

        Query query = em.createNativeQuery(sql.toString());
        if (!pramList.isEmpty()) {
            for (int i = 0; i < pramList.size(); i++) {
                query.setParameter(i + 1, pramList.get(i));
            }
        }
        int total = DataUtil.safeToInt(query.getSingleResult());
        return total;
    }

    @Override
    public List<ActionDTO> findLazingPaging(InputLazyActionSearch input, int pageSize, int pageNumber) throws ParseException, Exception {
        List paramList = new ArrayList();
        StringBuilder sql = new StringBuilder();
        List<ActionDTO> actionDTOList = new ArrayList<>();
        sql.append("SELECT CODE, NAME, TYPE, DESCRIPTION, STATUS, ACTION_ID ");
        sql.append(" FROM ACTION P where 1=1 ");

        if (!DataUtil.isNullOrEmpty(input.getCode())) {
            sql.append(" AND LOWER(P.CODE) LIKE? ESCAPE '\\' ");
            String code = "%" + DataUtil.formatQueryString(input.getCode()) + "%";
            paramList.add(code);
        }
        if (!DataUtil.isNullOrEmpty(input.getName())) {
            sql.append(" AND LOWER(P.NAME) LIKE? ESCAPE '\\' ");
            String name = "%" +DataUtil.formatQueryString( input.getName()) + "%";
            paramList.add(name);
        }
        if (!DataUtil.isNullOrEmpty(input.getType())) {
            sql.append(" AND P.TYPE =? ");
            paramList.add(input.getType());
        }
        if (!DataUtil.isNullObject(input.getStatus())) {
            sql.append(" AND P.STATUS = ? ");
            paramList.add(input.getStatus());
        }
        /*sql.append("AND P.STATUS != 0 ");*/
        sql.append("ORDER BY P.TYPE DESC, P.STATUS DESC , P.ACTION_ID DESC");

        Query query = em.createNativeQuery(sql.toString());
        if (!paramList.isEmpty()) {
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i + 1, paramList.get(i));
            }
        }
        query.setMaxResults(pageSize);
        query.setFirstResult(pageNumber * pageSize);
        List lstTemp = query.getResultList();
        if (lstTemp != null && !lstTemp.isEmpty()) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                ActionDTO dto = new ActionDTO();

                dto.setCode(DataUtil.safeToString(obj[0]));
                dto.setName(DataUtil.safeToString(obj[1]));
                dto.setType(DataUtil.safeToString(obj[2]));
                dto.setDescription(DataUtil.safeToString(obj[3]));
                dto.setStatus(DataUtil.safeToShort(obj[4]));
                dto.setActionId(DataUtil.safeToLong(obj[5]));
                actionDTOList.add(dto);
            }
        }

        return actionDTOList;
    }

    @Override
    public List<ActionDTO> getValueActionByTypeOrderByCode(String type) {
        StringBuilder sql = new StringBuilder(200);
        sql.append("select * from ACTION where type =#type and status = 1 order by NLSSORT(code,'NLS_SORT=vietnamese')  ");
        Query query = em.createNativeQuery(sql.toString(), Action.class);
        query.setParameter("type", type);
        List<Action> listValue = query.getResultList();
        return mapper.toDtoBean(listValue);
    }

    @Override
    public List<ActionDTO> getActionOrderByName() {
        StringBuilder sql = new StringBuilder(200);
        sql.append("select * from ACTION where status = 1 order by NLSSORT(name,'NLS_SORT=vietnamese')");
        Query query = em.createNativeQuery(sql.toString(), Action.class);
        List<Action> listValue = query.getResultList();
        return mapper.toDtoBean(listValue);
    }

    @Override
    public List<ActionDTO> getValueActionByType(String type) {
        StringBuilder sql = new StringBuilder(200);
        sql.append("select DISTINCT *  from ACTION where type =#type and status = 1 order by NLSSORT(name,'NLS_SORT=vietnamese')  ");
        Query query = em.createNativeQuery(sql.toString(), Action.class);
        query.setParameter("type", type);
        List<Action> listValue = query.getResultList();
        return mapper.toDtoBean(listValue);
    }


    @Override
    public List<ActionDTO> getListActionDTOByType(String type) {
        StringBuilder sql = new StringBuilder(200);
        sql.append("SELECT ACTION.ACTION_ID  id, ACTION.NAME name, ACTION.STATUS status, ACTION.TYPE type,ACTION.CODE code FROM ACTION WHERE ACTION.STATUS = 1 and ACTION.TYPE = #type");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("type", type);
        List<Object[]> listValue = query.getResultList();
        em.close();
        List<ActionDTO> actionDTOList = new ArrayList<>();
        for (Object[] obj : listValue) {
            ActionDTO action = new ActionDTO();
            action.setActionId(DataUtil.safeToLong(obj[0]));
            action.setName(DataUtil.safeToString(obj[1]));
            action.setStatus(DataUtil.safeToShort(obj[2]));
            action.setType(DataUtil.safeToString(obj[3]));
            action.setCode(DataUtil.safeToString(obj[4]));
            actionDTOList.add(action);
        }
        return actionDTOList;
    }
}
