package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.model.ChainUser;
import com.viettel.fw.persistence.BaseRepository;


public interface ChainUserRepo extends BaseRepository<ChainUser, Long>, ChainUserRepoCustom {

}