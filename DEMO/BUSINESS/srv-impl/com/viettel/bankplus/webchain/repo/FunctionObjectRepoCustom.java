package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.input.InputLazyFunctionSearch;
import com.viettel.bankplus.webchain.model.FunctionObject;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.mysema.query.types.Predicate;

import java.util.List;

public interface FunctionObjectRepoCustom {
    public Predicate toPredicate(List<FilterRequest> filters);

    public List<FunctionObject> getMenuByUserId(Long chainUserId);

    public int countLazyFunction(InputLazyFunctionSearch inputLazyFunctionSearch);

    public List<FunctionObjectDTO> findLazyFunction(InputLazyFunctionSearch inputLazyFunctionSearch, int pageSize, int pageNumber, boolean isLazySearch) throws Exception;

    public List<FunctionObject> getListChild(Long permissionId, boolean isChild);

    public List<FunctionObject> findByParentIdFlowUserId(Long userId, Long parentId, Short status) throws Exception;
}