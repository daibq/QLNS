package com.viettel.bankplus.webchain.repo;

import com.viettel.fw.persistence.BaseRepository;
import com.viettel.bankplus.webchain.model.ApDomain;

public interface ApDomainRepo extends BaseRepository<ApDomain, Long>, ApDomainRepoCustom {

}