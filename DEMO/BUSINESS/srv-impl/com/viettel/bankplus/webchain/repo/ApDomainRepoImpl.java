package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.ApDomainDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.input.InputApdomain;
import com.viettel.bankplus.webchain.model.ApDomain;
import com.viettel.bankplus.webchain.model.FunctionObject;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DateUtil;
import com.viettel.fw.common.util.mapper.BaseMapper;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class ApDomainRepoImpl implements ApDomainRepoCustom {

    public static final Logger logger = Logger.getLogger(ApDomainRepoCustom.class);
    private final BaseMapper<ApDomain, ApDomainDTO> mapper = new BaseMapper<>(ApDomain.class, ApDomainDTO.class);

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;


    @Override
    public int countCtctProgramList(InputApdomain inputApdomainSearch) {
        List paramList = new ArrayList();
        java.lang.StringBuilder sql = new java.lang.StringBuilder();
        sql.append("SELECT count(*)");
        sql.append(" FROM AP_DOMAIN P WHERE 1=1 ");
        if (!DataUtil.isNullOrEmpty(inputApdomainSearch.getInputName())) {
            sql.append(" AND LOWER(P.NAME) LIKE? ESCAPE '\\' ");
            String inputName = "%" + DataUtil.formatQueryString(inputApdomainSearch.getInputName()) + "%";
            paramList.add(inputName);
        }
        if (!DataUtil.isNullOrEmpty(inputApdomainSearch.getInputType())) {
            sql.append(" AND LOWER(P.TYPE) LIKE? ESCAPE '\\' ");
            String inputType = "%" + DataUtil.formatQueryString(inputApdomainSearch.getInputType()) + "%";
            paramList.add(inputType);
        }

        if (!DataUtil.isNullOrEmpty(inputApdomainSearch.getInputCode())) {
            sql.append(" AND LOWER(P.CODE) LIKE? ESCAPE '\\' ");
            String inputCode = "%" + inputApdomainSearch.getInputCode().trim().toLowerCase().replace("\\", "\\\\").replaceAll("%", "\\\\%").replaceAll("_", "\\\\_").replaceAll("!", "\\\\") + "%";
            paramList.add(inputCode);
        }
        if (!DataUtil.isNullOrEmpty(inputApdomainSearch.getInputValue())) {
            sql.append(" AND LOWER(P.VALUE) LIKE? ESCAPE '\\' ");
            String inputType = "%" + DataUtil.formatQueryString(inputApdomainSearch.getInputValue()) + "%";
            paramList.add(inputType);
        }

        if (!DataUtil.isNullObject(inputApdomainSearch.getStatus())) {
            sql.append(" AND P.STATUS = ? ");
            paramList.add(inputApdomainSearch.getStatus());
        }

        sql.append(" ORDER BY P.AP_DOMAIN_ID");

        Query query = em.createNativeQuery(sql.toString());
        if (!paramList.isEmpty()) {
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i + 1, paramList.get(i));
            }
        }
        return DataUtil.safeToInt(query.getSingleResult());
    }

    @Override
    public List<ApDomainDTO> findLazingPaging(InputApdomain inputApdomainSearch, int pageSize, int pageNumber) throws ParseException, Exception {
        List paramList = new ArrayList();
        java.lang.StringBuilder sql = new java.lang.StringBuilder();
        List<ApDomainDTO> apDomainDTOList = new ArrayList<>();
        sql.append("SELECT AP_DOMAIN_ID,TYPE,NAME,CODE,VALUE,STATUS,CREATE_BY,CREATE_DATE,DESCRIPTION");
        sql.append(" FROM AP_DOMAIN P where 1=1 ");
        if (!DataUtil.isNullOrEmpty(inputApdomainSearch.getInputName())) {
            sql.append(" AND LOWER(P.NAME) LIKE? ESCAPE '\\' ");
            /*String inputName = "%" + inputApdomainSearch.getInputName().trim().toLowerCase().replace("\\", "\\\\").replaceAll("%", "\\\\%").replaceAll("_", "\\\\_").replaceAll("!", "\\\\") + "%";*/
            String inputName = "%" + DataUtil.formatQueryString(inputApdomainSearch.getInputName()) + "%";
            paramList.add(inputName);
        }
        if (!DataUtil.isNullOrEmpty(inputApdomainSearch.getInputType())) {
            sql.append(" AND LOWER(P.TYPE) LIKE ? ESCAPE '\\' ");
            /*String inputType = "%" + inputApdomainSearch.getInputType().trim().toLowerCase().replace("\\", "\\\\").replaceAll("%", "\\\\%").replaceAll("_", "\\\\_").replaceAll("!", "\\\\") + "%";*/
            String inputType = "%" + DataUtil.formatQueryString(inputApdomainSearch.getInputType()) + "%";
            paramList.add(inputType);
        }

        if (!DataUtil.isNullOrEmpty(inputApdomainSearch.getInputCode())) {
            sql.append(" AND LOWER(P.CODE) LIKE? ESCAPE '\\' ");
            String inputCode = "%" + inputApdomainSearch.getInputCode().trim().toLowerCase().replace("\\", "\\\\").replaceAll("%", "\\\\%").replaceAll("_", "\\\\_").replaceAll("!", "\\\\") + "%";
            paramList.add(inputCode);
        }

        if (!DataUtil.isNullOrEmpty(inputApdomainSearch.getInputValue())) {
            sql.append(" AND LOWER(P.VALUE) LIKE? ESCAPE '\\' ");
            /*String inputValue = "%" + inputApdomainSearch.getInputValue().trim().toLowerCase().replace("\\", "\\\\").replaceAll("%", "\\\\%").replaceAll("_", "\\\\_").replaceAll("!", "\\\\") + "%";*/
            String inputValue = "%" + DataUtil.formatQueryString(inputApdomainSearch.getInputValue()) + "%";
            paramList.add(inputValue);
        }

        if (!DataUtil.isNullObject(inputApdomainSearch.getStatus())) {
            sql.append(" AND P.STATUS = ? ");
            paramList.add(inputApdomainSearch.getStatus());
        }

        sql.append(" ORDER BY P.AP_DOMAIN_ID desc");
        Query query = em.createNativeQuery(sql.toString());
        if (!paramList.isEmpty()) {
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i + 1, paramList.get(i));
            }
        }
        query.setMaxResults(pageSize);
        query.setFirstResult(pageNumber * pageSize);
        List lstTemp = query.getResultList();
        if (lstTemp != null && !lstTemp.isEmpty()) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                ApDomainDTO adomainDTO = new ApDomainDTO();
                adomainDTO.setApDomainId(DataUtil.safeToLong(obj[0]));
                adomainDTO.setType(DataUtil.safeToString(obj[1]));
                adomainDTO.setName(DataUtil.safeToString(obj[2]));
                adomainDTO.setCode(DataUtil.safeToString(obj[3]));
                adomainDTO.setValue(DataUtil.safeToString(obj[4]));
                adomainDTO.setStatus(DataUtil.safeToLong(obj[5]));
                adomainDTO.setCreateBy(DataUtil.safeToString(obj[6]));
                adomainDTO.setCreateDate(DateUtil.convertStringToTime(DataUtil.safeToString(obj[7]), "yyyy-MM-dd HH:mm:ss"));
                adomainDTO.setDescription(DataUtil.safeToString(obj[8]));
                apDomainDTOList.add(adomainDTO);
            }
        }
        return apDomainDTOList;
    }

    @Override
    public List<ApDomainDTO> getValueApDomainByTypeOrderByCode(String type) {
        StringBuilder sql = new StringBuilder(200);
        sql.append("select * from AP_DOMAIN where type =#type and status = 1 order by NLSSORT(code,'NLS_SORT=vietnamese')  ");
        Query query = em.createNativeQuery(sql.toString(), ApDomain.class);
        query.setParameter("type", type);
        List<ApDomain> listValue = query.getResultList();
        return mapper.toDtoBean(listValue);
    }

    @Override
    public List<ApDomainDTO> getValueApDomainByType(String type) {
        StringBuilder sql = new StringBuilder(200);
        sql.append("select DISTINCT * from AP_DOMAIN where type =#type and status = 1 order by NLSSORT(name,'NLS_SORT=vietnamese')  ");
        Query query = em.createNativeQuery(sql.toString(), ApDomain.class);
        query.setParameter("type", type);
        List<ApDomain> listValue = query.getResultList();
        return mapper.toDtoBean(listValue);
    }

}
