package com.viettel.bankplus.webchain.repo;

import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.template.BooleanTemplate;
import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.input.InputLazyChainUserSearch;
import com.viettel.bankplus.webchain.model.ChainUser;
import com.viettel.bankplus.webchain.model.QChainUser;
import com.viettel.fw.common.util.AESUtil;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChainUserRepoImpl implements ChainUserRepoCustom {

    public static final Logger logger = Logger.getLogger(ChainUserRepoCustom.class);
    @PersistenceContext(unitName = com.viettel.common.Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    @Override
    public Predicate toPredicate(List<FilterRequest> filters) {
        logger.info("Entering predicates :: " + filters);
        QChainUser chainUser = QChainUser.chainUser;
        BooleanExpression result = BooleanTemplate.create("1 = 1");
        try {
            for (FilterRequest filter : filters) {
                ChainUser.COLUMNS column = ChainUser.COLUMNS.valueOf(filter.getProperty().toUpperCase());
                BooleanExpression expression = null;
                switch (column) {
                    case ACTIVESTATUS:
                        expression = DbUtil.createShortExpression(chainUser.activeStatus, filter);
                        break;
                    case CHAINSHOPCODE:
                        expression = DbUtil.createStringExpression(chainUser.chainShopCode, filter);
                        break;
                    case CHAINSHOPID:
                        expression = DbUtil.createLongExpression(chainUser.chainShopId, filter);
                        break;
                    case CHAINUSERID:
                        expression = DbUtil.createLongExpression(chainUser.chainUserId, filter);
                        break;
                    case CHAINUSERTYPE:
                        expression = DbUtil.createShortExpression(chainUser.chainUserType, filter);
                        break;
                    case CREATEDATE:
                        expression = DbUtil.createDateExpression(chainUser.createDate, filter);
                        break;
                    case CREATEUSER:
                        expression = DbUtil.createStringExpression(chainUser.createUser, filter);
                        break;
                    case FULLNAME:
                        expression = DbUtil.createStringExpression(chainUser.fullName, filter);
                        break;
                    case LASTCHANGEPASSWORDDATE:
                        expression = DbUtil.createDateExpression(chainUser.lastChangePasswordDate, filter);
                        break;
                    case PASSWORD:
                        expression = DbUtil.createStringExpression(chainUser.password, filter);
                        break;
                    case PHONENUMBER:
                        expression = DbUtil.createStringExpression(chainUser.phoneNumber, filter);
                        break;
                    case SALT:
                        expression = DbUtil.createStringExpression(chainUser.salt, filter);
                        break;
                    case STATUS:
                        expression = DbUtil.createShortExpression(chainUser.status, filter);
                        break;
                    case UPDATEDATE:
                        expression = DbUtil.createDateExpression(chainUser.updateDate, filter);
                        break;
                    case UPDATEUSER:
                        expression = DbUtil.createStringExpression(chainUser.updateUser, filter);
                        break;
                    case USERNAME:
                        expression = DbUtil.createStringExpression(chainUser.userName, filter);
                        break;
                    default:break;
                }
                if (expression != null) {
                    result = result.and(expression);
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        logger.info("Result Predicate :: " + (result != null ? result.toString() : ""));
        logger.info("Exiting predicates");
        return result;
    }

    //Truongxp
    @Override
    public boolean checkPassword(String oldPassword, String takeSalt) throws Exception {
        boolean cPass;
        //String salt = new SimpleDateFormat("yyyyMMddHHmmss").format(getSysDate(em));
       /* String salt = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());*/
        String password = AESUtil.createHash(oldPassword, takeSalt);
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT count(*) FROM CHAIN_USER WHERE 1=1 AND PASSWORD = #password");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("password", password);
        int total = DataUtil.safeToInt(query.getSingleResult());
        if (total > 0) {
            cPass = true;
        } else
            cPass = false;
        return cPass;
    }

    @Override
    public String takeSaltWithUser(String userName) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT SALT FROM CHAIN_USER WHERE 1=1 AND USER_NAME = #userName ");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("userName", userName);
        String result = DataUtil.safeToString(query.getSingleResult());
        return result;
    }


    @Override
    public boolean checkDuplicatePassword(String newPassword, String takeSalt, String userName) throws Exception {
        boolean dupliPass;
        String password = AESUtil.createHash(newPassword, takeSalt);
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT COUNT(*) FROM CHAIN_USER WHERE 1=1 AND PASSWORD = #password AND USER_NAME = #userName");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("password", password);
        query.setParameter("userName", userName);
        int total = DataUtil.safeToInt(query.getSingleResult());
        if (total > 0) {
            dupliPass = true;
        } else
            dupliPass = false;
        return dupliPass;
    }

    @Override
    public long findId(String userName) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT CHAIN_USER_ID FROM CHAIN_USER WHERE 1=1 AND USER_NAME = #userName ");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("userName", userName);
        long total = DataUtil.safeToLong(query.getSingleResult());
        return total;
    }

    @Override
    public ChainUserDTO getValueByUserName(String userName) throws Exception {
        try {
            if (DataUtil.isNullObject(userName)) {
                return null;
            }
            ChainUserDTO chainUserDTO = null;
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT CU.PHONE_NUMBER,\n" +
                        " CS.CHAIN_SHOP_NAME,\n" +
                        " CSP.CHAIN_SHOP_NAME,\n" +
                        " CS.CHAIN_SHOP_CODE\n" +
                        "  FROM CHAIN_USER CU\n" +
                            " LEFT JOIN CHAIN_SHOP CS\n" +
                                " ON CU.CHAIN_SHOP_ID = CS.CHAIN_SHOP_ID\n" +
                            " LEFT JOIN CHAIN_SHOP CSP\n" +
                                " ON CS.parent_id = CSP.CHAIN_SHOP_ID\n" +
                            "  WHERE 1=1 AND LOWER(CU.user_name)= #userName");
            Query query = em.createNativeQuery(sql.toString());
            query.setParameter("userName",userName.toLowerCase());
            List lstTemp = query.getResultList();
            if (lstTemp != null && !DataUtil.isNullOrEmpty(lstTemp)) {
                Object[] obj = (Object[]) lstTemp.get(0);
                chainUserDTO = new ChainUserDTO();
                chainUserDTO.setPhoneNumber(DataUtil.safeToString(obj[0]));
                chainUserDTO.setChainShopName(DataUtil.safeToString(obj[1]));
                chainUserDTO.setParentShopName(DataUtil.safeToString(obj[2]));
                chainUserDTO.setChainShopCode(DataUtil.safeToString(obj[3]));
            }
            return chainUserDTO;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public Date getLastDateChangePass(String userName) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT LAST_CHANGE_PASSWORD_DATE FROM CHAIN_USER WHERE USER_NAME = #userName ");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("userName", userName);
        return (Date) query.getSingleResult();
    }

    @Override
    public List<ChainUserDTO> getUserByInput(String input) {
        StringBuilder sql = new StringBuilder();
        List<ChainUserDTO> userList = new ArrayList<>();
        sql.append(" SELECT CHAIN_USER_ID, USER_NAME, FULL_NAME FROM CHAIN_USER WHERE LOWER(USER_NAME) LIKE #input ");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("input", "%" + input + "%");
        List lstTemp = query.getResultList();
        if (null != lstTemp && !DataUtil.isNullOrEmpty(lstTemp)) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                ChainUserDTO chainUserDTO = new ChainUserDTO();
                chainUserDTO.setChainUserId(DataUtil.safeToLong(obj[0]));
                chainUserDTO.setUserName(DataUtil.safeToString(obj[1]));
                chainUserDTO.setFullName(DataUtil.safeToString(obj[2]));
                userList.add(chainUserDTO);
            }
        }
        return userList;
    }

    /**
     * @param parentShopId chain_shop_id cua chuoi cua hang chi lay nguoi dung la nguoi dung chuoi va dang hoat dong
     *
     * @return danh sach nguoi dung cua tat ca cac cua hang chuoi thuoc chuoi
     *
     * @throws Exception
     * @HungNQ
     */
    @Override
    public List<ChainUserDTO> getChainUserByParentShopId(Long parentShopId) throws Exception {
        List<ChainUserDTO> list = new ArrayList<>();
        StringBuilder sql = new StringBuilder(500);
        sql.append(" SELECT ");
        sql.append("  CU.CHAIN_USER_ID,   ");
        sql.append("  CU.USER_NAME,   ");
        sql.append("  CU.FULL_NAME   ");
        sql.append("  FROM   ");
        sql.append("  CHAIN_USER cu   ");
        sql.append("  WHERE   ");
        sql.append("  CU.STATUS = 1  AND CU.CHAIN_USER_TYPE =3 ");
        sql.append("  AND CU.CHAIN_SHOP_ID IN (   ");
        sql.append("                           SELECT   ");
        sql.append("                               CHAIN_SHOP_ID   ");
        sql.append("                           FROM   ");
        sql.append("                               CHAIN_SHOP cs   ");
        sql.append("                           WHERE   ");
        sql.append("                               cs.PARENT_ID = #parentShopId   ");
        sql.append("                           AND cs.STATUS = 1   ");
        sql.append("                           )");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("parentShopId", parentShopId);
        List lstTemp = query.getResultList();
        if (lstTemp != null && !DataUtil.isNullOrEmpty(lstTemp)) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                ChainUserDTO chainUserDTO = new ChainUserDTO();
                chainUserDTO.setChainUserId(DataUtil.safeToLong(obj[0].toString()));
                chainUserDTO.setUserName(DataUtil.safeToString(obj[1].toString()));
                chainUserDTO.setFullName(DataUtil.safeToString(obj[2].toString()));
                list.add(chainUserDTO);
            }
        }
        return list;
    }

    /**
     * hungnq
     *
     * @param inputLazyChainUserSearch
     *
     * @return
     */

    @Override
    public int countLazyChainUser(InputLazyChainUserSearch inputLazyChainUserSearch) {
        Long chainShopId = inputLazyChainUserSearch.getChainShopId();
        Long parentShopId = inputLazyChainUserSearch.getParentShopId();
        if (parentShopId == null && DataUtil.isNullObject(chainShopId)) {
            return 0;
        }
        String username = DataUtil.formatQueryString(inputLazyChainUserSearch.getUsername());
        Short status = inputLazyChainUserSearch.getStatus();
        StringBuilder sql = new StringBuilder(500);
        sql.append(" SELECT COUNT(*)");
        sql.append(" FROM ");
        sql.append(" CHAIN_USER cu, ");
        sql.append(" chain_shop cs , ");
        sql.append(" chain_shop pcs ");
        sql.append(" WHERE ");
        sql.append(" 1 = 1  ");
        sql.append(" AND cu.CHAIN_SHOP_ID = cs.CHAIN_SHOP_ID and cs.PARENT_ID = pcs.CHAIN_SHOP_ID ");

        if (!DataUtil.isNullOrEmpty(username)) {
            sql.append("   AND LOWER(cu.USER_NAME) LIKE #username ESCAPE '\\' ");
        }
        if (DataUtil.isNullObject(chainShopId)) {
            sql.append(" AND cs.PARENT_ID = #parentShopId ");
        } else {
            sql.append(" AND cs.CHAIN_SHOP_ID =#chainShopId");
        }
        if (null != status) {
            sql.append(" AND cu.STATUS =#status");
        }
        Query query = em.createNativeQuery(sql.toString());
        if (!DataUtil.isNullOrEmpty(username)) {
            query.setParameter("username", "%" + username + "%");
        }
        if (DataUtil.isNullObject(chainShopId)) {
            query.setParameter("parentShopId", parentShopId);
        } else {
            query.setParameter("chainShopId", chainShopId);
        }
        if (null != status) {
            query.setParameter("status", status);
        }
        int total = DataUtil.safeToInt(query.getSingleResult());
        return total;
    }

    /**
     * hungnq
     *
     * @param inputLazyChainUserSearch
     * @param pageSize
     * @param pageNumber
     * @param isLazySearch
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public List<ChainUserDTO> findLazyChainUser(InputLazyChainUserSearch inputLazyChainUserSearch, int pageSize, int pageNumber, boolean isLazySearch) throws Exception {
        try {

            Long chainShopId = inputLazyChainUserSearch.getChainShopId();
            Long parentShopId = inputLazyChainUserSearch.getParentShopId();
            if (parentShopId == null && DataUtil.isNullObject(chainShopId)) {
                return new ArrayList<>();
            }
            Short status = inputLazyChainUserSearch.getStatus();
            List<ChainUserDTO> chainUserDTOList = new ArrayList<>();
            String username = DataUtil.formatQueryString(inputLazyChainUserSearch.getUsername());
            StringBuilder sql = new StringBuilder(500);
            sql.append(" SELECT ");
            sql.append(" pcs.CHAIN_SHOP_CODE parentShopCode, ");
            sql.append(" cs.CHAIN_SHOP_CODE chainShopCode, ");
            sql.append(" cs.CHAIN_SHOP_NAME chainShopName, ");
            sql.append(" cu.CHAIN_USER_ID chainUserId, ");
            sql.append(" cu.USER_NAME userName, ");
            sql.append(" cu.FULL_NAME fullName, ");
            sql.append(" cu.PHONE_NUMBER phoneNumber, ");
            sql.append(" cu.STATUS status, ");
            sql.append(" cu.CHAIN_SHOP_ID chainShopId ");
            sql.append(" FROM ");
            sql.append(" CHAIN_USER cu, ");
            sql.append(" chain_shop cs , ");
            sql.append(" chain_shop pcs ");
            sql.append(" WHERE ");
            sql.append(" 1 = 1  ");
            sql.append(" AND cu.CHAIN_SHOP_ID = cs.CHAIN_SHOP_ID and cs.PARENT_ID = pcs.CHAIN_SHOP_ID");
            if (!DataUtil.isNullOrEmpty(username)) {
                sql.append("   AND LOWER(cu.USER_NAME) LIKE #username ESCAPE '\\' ");
            }
            if (DataUtil.isNullObject(chainShopId)) {
                sql.append(" AND cs.PARENT_ID = #parentShopId ");
            } else {
                sql.append(" AND cs.CHAIN_SHOP_ID =#chainShopId");
            }
            if (null != status) {
                sql.append(" AND cu.STATUS =#status");
            }
            sql.append(" ORDER BY  NLSSORT(cu.USER_NAME,'NLS_SORT=vietnamese')");
            Query query = em.createNativeQuery(sql.toString());
            if (null != status) {
                query.setParameter("status", status);
            }
            if (!DataUtil.isNullOrEmpty(username)) {
                query.setParameter("username", "%" + username + "%");
            }
            if (DataUtil.isNullObject(chainShopId)) {
                query.setParameter("parentShopId", parentShopId);
            } else {
                query.setParameter("chainShopId", chainShopId);
            }

            if (isLazySearch) {
                query.setMaxResults(pageSize);
                query.setFirstResult(pageNumber * pageSize);
            }
            List lstTemp = query.getResultList();
            if (lstTemp != null && !DataUtil.isNullOrEmpty(lstTemp)) {
                for (int i = 0; i < lstTemp.size(); i++) {
                    Object[] obj = (Object[]) lstTemp.get(i);
                    ChainUserDTO chainUserDTO = new ChainUserDTO();
                    //id
                    chainUserDTO.setParentShopCode(DataUtil.safeToString(obj[0]));
                    chainUserDTO.setChainShopCode(DataUtil.safeToString(obj[1]));
                    chainUserDTO.setChainShopName(DataUtil.safeToString(obj[2]));
                    chainUserDTO.setChainUserId(DataUtil.safeToLong(obj[3]));
                    chainUserDTO.setUserName(DataUtil.safeToString(obj[4]));
                    chainUserDTO.setFullName(DataUtil.safeToString(obj[5]));
                    chainUserDTO.setPhoneNumber(DataUtil.safeToString(obj[6]));
                    chainUserDTO.setStatus(DataUtil.safeToShort(obj[7]));
                    chainUserDTO.setChainShopId(DataUtil.safeToLong(obj[8]));
                    chainUserDTOList.add(chainUserDTO);
                }
            }
            return chainUserDTOList;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    //edit by duyetdk
    @Override
    public ChainUserDTO getByUsername(String username) throws Exception {
        try {
            if (DataUtil.isNullObject(username)) {
                return null;
            }
            ChainUserDTO chainUserDTO = null;
            StringBuilder sql = new StringBuilder(500);
            sql.append(" SELECT shop.chain_shop_code,\n" +
                    "       shop.chain_shop_name,\n" +
                    "       chain.chain_shop_code,\n" +
                    "       chain.chain_shop_name,\n" +
                    "       u.full_name,\n" +
                    "       u.user_name,\n" +
                    "       shop.province_code,\n" +
                    "       u.chain_user_type\n" +
                    "  FROM chain_user u\n" +
                    "       LEFT JOIN chain_shop shop\n" +
                    "           ON shop.chain_shop_id = u.chain_shop_id\n" +
                    "       LEFT JOIN chain_shop chain\n" +
                    "           ON shop.parent_id = chain.chain_shop_id" +
                    " WHERE lower(u.user_name) = #userName ");
            Query query = em.createNativeQuery(sql.toString());
            query.setParameter("userName", DataUtil.formatQueryString(username.toLowerCase()).replace("\\","").trim());
            List lstTemp = query.getResultList();
            if (lstTemp != null && !DataUtil.isNullOrEmpty(lstTemp)) {
                Object[] obj = (Object[]) lstTemp.get(0);
                chainUserDTO = new ChainUserDTO();
                chainUserDTO.setChainShopCode(DataUtil.safeToString(obj[0]));
                chainUserDTO.setChainShopName(DataUtil.safeToString(obj[1]));
                chainUserDTO.setParentShopCode(DataUtil.safeToString(obj[2]));
                chainUserDTO.setParentShopName(DataUtil.safeToString(obj[3]));
                chainUserDTO.setFullName(DataUtil.safeToString(obj[4]));
                chainUserDTO.setUserName(DataUtil.safeToString(obj[5]));
                chainUserDTO.setProvinceCode(DataUtil.safeToString(obj[6]));
                chainUserDTO.setChainUserType(DataUtil.safeToShort(obj[7]));
            }
            return chainUserDTO;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    //DUYETDK
    @Override
    public List<ChainUserDTO> getUsernameByChainShopID(Long chainShopId, Long viettelId) throws Exception {
        StringBuilder sql = new StringBuilder(500);
        List<ChainUserDTO> listDTO = new ArrayList<>();
        sql.append("SELECT ");
        sql.append(" CU.USER_NAME USERNAME, ");
        sql.append(" CU.FULL_NAME FULLNAME ");
        sql.append(" FROM  ");
        sql.append(" CHAIN_USER CU ");
        sql.append(" WHERE 1=1 ");
        sql.append(" AND CU.STATUS = 1 ");
        sql.append(" AND CU.ACTIVE_STATUS = 1 ");
        if (chainShopId != null) {
            sql.append(" AND CU.CHAIN_SHOP_ID IN (SELECT CS.CHAIN_SHOP_ID FROM CHAIN_SHOP CS " +
                    "WHERE CS.STATUS = 1 AND CS.PARENT_ID IN (SELECT CHAIN_SHOP_ID FROM CHAIN_SHOP " +
                    "WHERE CHAIN_SHOP_ID = #chainShopId AND STATUS = 1)) ");
            sql.append(" OR (CU.CHAIN_SHOP_ID IN (SELECT CHAIN_SHOP_ID FROM CHAIN_SHOP " +
                    "WHERE CHAIN_SHOP_ID = #chainShopId AND STATUS = 1) AND CU.STATUS = 1) ");
        }
        if(viettelId != null){
            sql.append(" OR (CU.CHAIN_SHOP_ID IN (SELECT CHAIN_SHOP_ID FROM CHAIN_SHOP " +
                    "WHERE CHAIN_SHOP_ID = #viettelId AND STATUS = 1) AND CU.STATUS = 1 ) ");
        }
        sql.append(" ORDER BY CU.USER_NAME ASC ");

        Query query = em.createNativeQuery(sql.toString());

        if (chainShopId != null) {
            query.setParameter("chainShopId", chainShopId);
        }
        if (viettelId != null) {
            query.setParameter("viettelId", viettelId);
        }
        List lstTemp = query.getResultList();
        if (!lstTemp.isEmpty()) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                ChainUserDTO dto = new ChainUserDTO();
                dto.setUserName(DataUtil.safeToString(obj[0]));
                dto.setFullName(DataUtil.safeToString(obj[1]));
                listDTO.add(dto);
            }
        }
        return listDTO;
    }

    @Override
    public List<ChainUser> getChainUserByChainShopIdOrderByName(Long chainShopId) throws Exception {
        StringBuilder sql = new StringBuilder(100);
        sql.append(" SELECT * FROM CHAIN_USER WHERE STATUS = 1 AND 1=1");
        sql.append(" AND CHAIN_SHOP_ID =#chainShopId ");
        sql.append(" ORDER BY NLSSORT(FULL_NAME,'NLS_SORT=vietnamese')");

        Query query = em.createNativeQuery(sql.toString(), ChainUser.class);
        query.setParameter("chainShopId", chainShopId);
        return query.getResultList();
    }
}