package com.viettel.bankplus.webchain.repo;

import com.mysema.query.types.Predicate;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.input.InputLazyActionLogSearch;
import com.viettel.fw.common.util.extjs.FilterRequest;

import java.util.List;

public interface ActionLogRepoCustom {

  public Predicate toPredicate(List<FilterRequest> filters);

  int countList(InputLazyActionLogSearch input);
  
  List<ActionLogDTO> findLazingPaging(InputLazyActionLogSearch input, int pageSize, int pageNumber);

}