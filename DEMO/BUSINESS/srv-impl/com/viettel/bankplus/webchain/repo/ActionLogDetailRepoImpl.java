package com.viettel.bankplus.webchain.repo;

import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.template.BooleanTemplate;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDetailDTO;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DateUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.bankplus.webchain.model.ActionLogDetail;
import com.viettel.bankplus.webchain.model.QActionLogDetail;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class ActionLogDetailRepoImpl implements ActionLogDetailRepoCustom {

    public static final Logger logger = Logger.getLogger(ActionLogDetailRepoCustom.class);

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    @Override
    public Predicate toPredicate(List<FilterRequest> filters) {
        logger.info("Entering predicates :: " + filters);
        QActionLogDetail actionLogDetail = QActionLogDetail.actionLogDetail;
        BooleanExpression result = BooleanTemplate.create("1 = 1");
        try {
            for (FilterRequest filter : filters) {
                ActionLogDetail.COLUMNS column = ActionLogDetail.COLUMNS.valueOf(filter.getProperty().toUpperCase());
                BooleanExpression expression = null;
                switch (column) {
                    case ACTIONDATE:
                        expression = DbUtil.createDateExpression(actionLogDetail.actionDate, filter);
                        break;
                    case ACTIONDETAILID:
                        expression = DbUtil.createLongExpression(actionLogDetail.actionDetailId, filter);
                        break;
                    case ACTIONLOGID:
                        expression = DbUtil.createLongExpression(actionLogDetail.actionLogId, filter);
                        break;
                    case COLUMNNAME:
                        expression = DbUtil.createStringExpression(actionLogDetail.columnName, filter);
                        break;
                    case DESCRIPTION:
                        expression = DbUtil.createStringExpression(actionLogDetail.description, filter);
                        break;
                    case NEWVALUE:
                        expression = DbUtil.createStringExpression(actionLogDetail.newValue, filter);
                        break;
                    case OLDVALUE:
                        expression = DbUtil.createStringExpression(actionLogDetail.oldValue, filter);
                        break;
                    case TABLENAME:
                        expression = DbUtil.createStringExpression(actionLogDetail.tableName, filter);
                        break;
                    default: break;
                }
                if (expression != null) {
                    result = result.and(expression);
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        logger.info("Result Predicate :: " + (result != null ? result.toString() : ""));
        logger.info("Exiting predicates");
        return result;
    }

    @Override
    public int countList(ActionLogDTO dto) throws Exception {
        List paramList = new ArrayList();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT count(*)");
        sql.append("FROM ACTION_LOG_DETAIL P WHERE 1=1 ");

        if (!DataUtil.isNullObject(dto.getActionLogId())) {
            sql.append(" AND P.ACTION_LOG_ID = ? ");
            paramList.add(dto.getActionLogId());
        }

        sql.append("ORDER BY P.ACTION_DATE DESC");

        Query query = em.createNativeQuery(sql.toString());
        if (!paramList.isEmpty()) {
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i + 1, paramList.get(i));
            }
        }
        int total = DataUtil.safeToInt(query.getSingleResult());
        return total;
    }

    @Override
    public List<ActionLogDetailDTO> findLazingPaging(ActionLogDTO actionLogDTO, int pageSize, int pageNumber) throws Exception {
        List paramList = new ArrayList();
        StringBuilder sql = new StringBuilder();
        List<ActionLogDetailDTO> listDTO = new ArrayList<>();
        sql.append("SELECT ACTION_DETAIL_ID, ACTION_LOG_ID, TABLE_NAME, COLUMN_NAME, OLD_VALUE, NEW_VALUE, DESCRIPTION, TO_CHAR (ACTION_DATE,'dd/MM/yyyy hh24:mi:ss')  ");
        sql.append(" FROM ACTION_LOG_DETAIL P WHERE 1=1 ");

        if (!DataUtil.isNullObject(actionLogDTO.getActionLogId())) {
            sql.append(" AND P.ACTION_LOG_ID = ? ");
            paramList.add(actionLogDTO.getActionLogId());
        }
        sql.append("ORDER BY P.ACTION_DATE DESC");


        Query query = em.createNativeQuery(sql.toString());
        if (!paramList.isEmpty()) {
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i + 1, paramList.get(i));
            }
        }

        query.setMaxResults(pageSize);
        query.setFirstResult(pageNumber * pageSize);
        List lstTemp = query.getResultList();
        if (lstTemp != null && !lstTemp.isEmpty()) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                ActionLogDetailDTO dto = new ActionLogDetailDTO();
                dto.setActionDetailId(DataUtil.safeToLong(obj[0]));
                dto.setActionLogId(DataUtil.safeToLong(obj[1]));
                dto.setTableName(DataUtil.safeToString(obj[2]));
                dto.setColumnName(DataUtil.safeToString(obj[3]));
                dto.setOldValue(DataUtil.safeToString(obj[4]));
                dto.setNewValue(DataUtil.safeToString(obj[5]));
                dto.setDescription(DataUtil.safeToString(obj[6]));
                dto.setActionDate(DateUtil.convertStringToTime(DataUtil.safeToString(obj[7]), "dd/MM/yyyy HH:mm:ss"));
                listDTO.add(dto);
            }
        }
        return listDTO;
    }

}