package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.dto.PermissionGroupUserDTO;
import com.viettel.bankplus.webchain.input.InputLazyUserPermissionGroup;
import com.viettel.bankplus.webchain.model.ChainUser;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.mapper.BaseMapper;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dai on 9/7/2017
 */
public class PermissionGroupUserRepoImpl implements PermissionGroupUserRepoCustom {
    public static final Logger logger = Logger.getLogger(PermissionGroupUserRepoCustom.class);
    private final BaseMapper<ChainUser, ChainUserDTO> mapper = new BaseMapper<>(ChainUser.class, ChainUserDTO.class);
    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;


    @Override
    public int countUserPermissionGroup(InputLazyUserPermissionGroup input) {
        StringBuilder sql = new StringBuilder(500);
        sql.append(" SELECT COUNT(*) ");
        sql.append(" FROM  ");
        sql.append("  chain_user cu,  ");
        sql.append("  permission_group pg,  ");
        sql.append("  permission_group_user pgu  ");
        sql.append(" WHERE  ");
        sql.append("  CU.chain_user_id = pgu.chain_user_id  ");
        sql.append(" AND PG.permission_group_id = PGU.permission_group_id  ");
        sql.append(" AND cu.status = 1  ");
        /*sql.append(" AND PGU.status = 1  ");*/
        sql.append(" AND CU.CHAIN_SHOP_ID IN ( SELECT CHAIN_SHOP_ID FROM CHAIN_SHOP WHERE STATUS=1 AND (PARENT_ID= #chainId OR CHAIN_SHOP_ID=#chainId ) ) ");
        sql.append(" AND pg.status = 1");
        if (null != input.getChainUserIdList() && !input.getChainUserIdList().isEmpty()) {
            sql.append(" AND CU.chain_user_id " + DbUtil.createInQuery("chain_user_id", input.getChainUserIdList()));
        }
        if (null != input.getPermissionGroupIdList() && !input.getPermissionGroupIdList().isEmpty()) {
            sql.append(" AND PGU.permission_group_id " + DbUtil.createInQuery("permission_group_id", input.getPermissionGroupIdList()));
        }
        if (!DataUtil.isNullObject(input.getStatus())){
            sql.append(" AND PGU.status =" + input.getStatus());
        }
        sql.append(" ORDER BY PGU.permission_group_id");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("chainId", input.getChainId());
        if (null != input.getChainUserIdList() && !input.getChainUserIdList().isEmpty()) {
            DbUtil.setParamInQuery(query, "chain_user_id", input.getChainUserIdList());
        }
        if (null != input.getPermissionGroupIdList() && !input.getPermissionGroupIdList().isEmpty()) {
            DbUtil.setParamInQuery(query, "permission_group_id", input.getPermissionGroupIdList());
        }

        int total = DataUtil.safeToInt(query.getSingleResult());
        return total;
    }

    @Override
    public List<PermissionGroupUserDTO> findUserPermissionGroup(InputLazyUserPermissionGroup input, int pageSize, int pageNumber) {
        List<PermissionGroupUserDTO> permissionGroupUserDTOList = new ArrayList<>();
        StringBuilder sql = new StringBuilder(500);
        sql.append(" SELECT  ");
        sql.append("  PGU.permission_group_user_id permissionGroupUserId,  ");
        sql.append("  PGU.chain_user_id chainUserId,  ");
        sql.append("  PGU.permission_group_id permissionGroupId,  ");
        sql.append("  CU.user_name chainUserName,  ");
        sql.append("  CU.full_name fullName,  ");
        sql.append("  PG.permission_group_name permissionGroupName,  ");
        sql.append("  PGU.status status_PGU  ");
        sql.append(" FROM  ");
        sql.append("  chain_user cu,  ");
        sql.append("  permission_group pg,  ");
        sql.append("  permission_group_user pgu  ");
        sql.append(" WHERE  ");
        sql.append("  CU.chain_user_id = pgu.chain_user_id  ");
        sql.append(" AND PG.permission_group_id = PGU.permission_group_id  ");
        sql.append(" AND cu.status = 1  ");
        sql.append(" AND CU.CHAIN_SHOP_ID IN ( SELECT CHAIN_SHOP_ID FROM CHAIN_SHOP WHERE STATUS=1 AND (PARENT_ID= #chainId OR CHAIN_SHOP_ID=#chainId ) ) ");
        /*sql.append(" AND PGU.status = 1  ");*/
        sql.append(" AND pg.status = 1");

        if (null != input.getChainUserIdList() && !input.getChainUserIdList().isEmpty()) {
            sql.append(" AND CU.chain_user_id " + DbUtil.createInQuery("chain_user_id", input.getChainUserIdList()));
        }
        if (null != input.getPermissionGroupIdList() && !input.getPermissionGroupIdList().isEmpty()) {
            sql.append(" AND PGU.permission_group_id " + DbUtil.createInQuery("permission_group_id", input.getPermissionGroupIdList()));
        }
        if (!DataUtil.isNullObject(input.getStatus())){
            sql.append(" AND PGU.status =" + input.getStatus());
        }
        sql.append(" ORDER BY PGU.permission_group_id desc");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("chainId", input.getChainId());
        if (null != input.getChainUserIdList() && !input.getChainUserIdList().isEmpty()) {
            DbUtil.setParamInQuery(query, "chain_user_id", input.getChainUserIdList());
        }
        if (null != input.getPermissionGroupIdList() && !input.getPermissionGroupIdList().isEmpty()) {
            DbUtil.setParamInQuery(query, "permission_group_id", input.getPermissionGroupIdList());
        }

        query.setMaxResults(pageSize);
        query.setFirstResult(pageNumber * pageSize);
        List lstTemp = query.getResultList();
        if (lstTemp != null && !DataUtil.isNullOrEmpty(lstTemp)) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                PermissionGroupUserDTO permissionGroupUserDTO = new PermissionGroupUserDTO();
                permissionGroupUserDTO.setPermissionGroupUserId(DataUtil.safeToLong(obj[0].toString()));
                permissionGroupUserDTO.setChainUserId(DataUtil.safeToLong(obj[1].toString()));
                permissionGroupUserDTO.setPermissionGroupId(DataUtil.safeToLong(obj[2].toString()));
                permissionGroupUserDTO.setUserName(DataUtil.safeToString(obj[3].toString()));
                permissionGroupUserDTO.setFullName(DataUtil.safeToString(obj[4].toString()));
                permissionGroupUserDTO.setPermissionGroupName(DataUtil.safeToString(obj[5].toString()));
                permissionGroupUserDTO.setStatus(DataUtil.safeToShort(obj[6].toString()));
                permissionGroupUserDTOList.add(permissionGroupUserDTO);
            }
        }
        return permissionGroupUserDTOList;
    }

    @Override
    public List<ChainUserDTO> getNameByChainUserIdByShopId(Long chainUserId) {
        StringBuilder sql = new StringBuilder(200);
        sql.append("Select * FROM CHAIN_USER  WHERE CHAIN_USER_ID =#id ");
        Query query = em.createNativeQuery(sql.toString(), ChainUser.class);
        query.setParameter("id", chainUserId);
        List<ChainUser> listValue = query.getResultList();
        List<ChainUserDTO> list =mapper.toDtoBean(listValue);
        return list;
    }

  /*  @Override
    public List<PermissionGroupDTO> checkPermissionGroupTypOrStartus(Long permissionGroupId) {
        StringBuilder sql = new StringBuilder(200);
        sql.append("Select * FROM PERMISSION_GROUP  WHERE PERMISSION_GROUP_ID=#id  AND( PERMISSION_GROUP_TYPE !=3 OR STATUS !=1)");
        Query query = em.createNativeQuery(sql.toString(), PermissionGroup.class);
        query.setParameter("id", permissionGroupId);
        List<PermissionGroup> listValue = query.getResultList();
        List<PermissionGroupDTO> list =mapperPermiss.toDtoBean(listValue);
            return list;
    }*/
}
