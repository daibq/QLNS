package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.input.InputLazyFunctionSearch;
import com.viettel.bankplus.webchain.model.FunctionObject;
import com.viettel.bankplus.webchain.model.QFunctionObject;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DateUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.template.BooleanTemplate;

import java.util.ArrayList;
import java.util.List;

import com.viettel.fw.common.util.DbUtil;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class FunctionObjectRepoImpl implements FunctionObjectRepoCustom {

    public static final Logger logger = Logger.getLogger(FunctionObjectRepoCustom.class);

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;

    @Override
    public Predicate toPredicate(List<FilterRequest> filters) {
        logger.info("Entering predicates :: " + filters);
        QFunctionObject functionObject = QFunctionObject.functionObject;
        BooleanExpression result = BooleanTemplate.create("1 = 1");
        try {
            for (FilterRequest filter : filters) {
                FunctionObject.COLUMNS column = FunctionObject.COLUMNS.valueOf(filter.getProperty().toUpperCase());
                BooleanExpression expression = null;
                switch (column) {
                    case CREATEDATE:
                        expression = DbUtil.createDateExpression(functionObject.createDate, filter);
                        break;
                    case CREATEUSER:
                        expression = DbUtil.createStringExpression(functionObject.createUser, filter);
                        break;
                    case DESCRIPTION:
                        expression = DbUtil.createStringExpression(functionObject.description, filter);
                        break;
                    case FUNCTIONADMINTYPE:
                        expression = DbUtil.createShortExpression(functionObject.functionAdminType, filter);
                        break;
                    case FUNCTIONCODE:
                        expression = DbUtil.createStringExpression(functionObject.functionCode, filter);
                        break;
                    case FUNCTIONNAME:
                        expression = DbUtil.createStringExpression(functionObject.functionName, filter);
                        break;
                    case FUNCTIONOBJECTID:
                        expression = DbUtil.createLongExpression(functionObject.functionObjectId, filter);
                        break;
                    case FUNCTIONPATH:
                        expression = DbUtil.createStringExpression(functionObject.functionPath, filter);
                        break;
                    case FUNCTIONTYPE:
                        expression = DbUtil.createShortExpression(functionObject.functionType, filter);
                        break;
                    case ORDERNO:
                        expression = DbUtil.createShortExpression(functionObject.orderNo, filter);
                        break;
                    case PARENTID:
                        expression = DbUtil.createLongExpression(functionObject.parentId, filter);
                        break;
                    case STATUS:
                        expression = DbUtil.createShortExpression(functionObject.status, filter);
                        break;
                    case UPDATEDATE:
                        expression = DbUtil.createDateExpression(functionObject.updateDate, filter);
                        break;
                    case UPDATEUSER:
                        expression = DbUtil.createStringExpression(functionObject.updateUser, filter);
                        break;
                    default:break;
                }
                if (expression != null) {
                    result = result.and(expression);
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        logger.info("Result Predicate :: " + (result != null ? result.toString() : ""));
        logger.info("Exiting predicates");
        return result;
    }

    @Override
    public List<FunctionObject> getMenuByUserId(Long chainUserId) {

        StringBuilder sql = new StringBuilder(500);
        sql.append("SELECT DISTINCT fo.FUNCTION_OBJECT_ID ,");
        sql.append("            fo.FUNCTION_NAME,  ");
        sql.append("            fo.FUNCTION_CODE, ");
        sql.append("                fo.ORDER_NO, ");
        sql.append("                fo.PARENT_ID, ");
        sql.append("                fo.FUNCTION_PATH, ");
        sql.append("                fo.FUNCTION_TYPE ");
        sql.append("  FROM function_object            fo, ");
        sql.append("       chain_user                 cu, ");
        sql.append("       permission_group           pg, ");
        sql.append("       permission_group_user      pgu, ");
        sql.append("       permission_group_function  pfg  ");
        sql.append(" WHERE     ");
        sql.append("       fo.FUNCTION_OBJECT_ID = pfg.FUNCTION_OBJECT_ID ");
        sql.append("       AND pfg.PERMISSION_GROUP_ID = pgu.PERMISSION_GROUP_ID ");
        sql.append("       AND pgu.CHAIN_USER_ID = cu.CHAIN_USER_ID ");
        sql.append("       AND fo.STATUS = 1 ");
        sql.append("       AND pfg.STATUS = 1 ");
        sql.append("       AND pgu.STATUS = 1 ");
        sql.append("       AND pg.STATUS = 1 ");
        sql.append("       AND cu.STATUS = 1 ");
        sql.append("       AND cu.CHAIN_USER_ID =#chainUserId");
        Query query = em.createNativeQuery(sql.toString(), FunctionObject.class);
        query.setParameter("chainUserId", chainUserId);
        List<FunctionObject> lstTemp = query.getResultList();
        return lstTemp;
//        if (lstTemp != null && lstTemp.size() > 0) {
//            for (int i = 0; i < lstTemp.size(); i++) {
//                Object[] obj = (Object[]) lstTemp.get(i);
//                FunctionObjectDTO dto = new FunctionObjectDTO();
//                //name
//                if (obj[0] != null) {
//                }
//
//            }
//        }
//        return null;
    }

    /**
     * count record when search
     *
     * @param inputLazyFunctionSearch
     *
     * @return
     */
    @Override
    public int countLazyFunction(InputLazyFunctionSearch inputLazyFunctionSearch) {
        String functionCode = DataUtil.formatQueryString(inputLazyFunctionSearch.getFunctionCode());
        Short status = inputLazyFunctionSearch.getStatus();
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT COUNT(*) FROM FUNCTION_OBJECT FO WHERE 1=1 ");
        if (!DataUtil.isNullOrEmpty(functionCode)) {
            sql.append("  AND LOWER(FO.FUNCTION_CODE) LIKE #functionCode ESCAPE '\\' ");
        }
        if (!DataUtil.isNullObject(status)) {
            sql.append("  AND FO.STATUS = #status ");
        }
        Query query = em.createNativeQuery(sql.toString());
        if (!DataUtil.isNullOrEmpty(functionCode)) {
            query.setParameter("functionCode", "%" + functionCode + "%");
        }
        if (!DataUtil.isNullObject(status)) {
            query.setParameter("status", status);
        }
        int total = DataUtil.safeToInt(query.getSingleResult());
        return total;
    }

    @Override
    public List<FunctionObjectDTO> findLazyFunction(InputLazyFunctionSearch inputLazyFunctionSearch, int pageSize, int pageNumber, boolean isLazySearch) throws Exception {
        List<FunctionObjectDTO> functionObjectDTOList = new ArrayList<>();
        String functionCode = DataUtil.formatQueryString(inputLazyFunctionSearch.getFunctionCode());
        Short status = inputLazyFunctionSearch.getStatus();
        StringBuilder sql = new StringBuilder(300);
        sql.append(" SELECT FO.FUNCTION_OBJECT_ID FUNCTIONOBJECTID, ");
        sql.append("       FO.FUNCTION_NAME FUNCTIONNAME, ");
        sql.append("       FO.FUNCTION_CODE FUNCTIONCODE, ");
        sql.append("       FO.FUNCTION_PATH FUNCTIONPATH, ");
        sql.append("       FO.FUNCTION_TYPE FUNCTIONTYPE, ");
        sql.append("       FO.STATUS STATUS, ");
        sql.append("       FO.DESCRIPTION DESCRIPTION, ");
        sql.append("       FO.ORDER_NO ORDERNO, ");
        sql.append("       FO.PARENT_ID PARENTID, ");
        sql.append("       (SELECT FUNCTION_NAME FROM FUNCTION_OBJECT WHERE FUNCTION_OBJECT.FUNCTION_OBJECT_ID = FO.PARENT_ID) PARENTNAME ,");
        sql.append("   FO.CREATE_USER CREATEUSER, ");
        sql.append("   TO_CHAR(FO.CREATE_DATE ,'dd/MM/yyyy hh24:mi:ss') CREATEDATE, ");
        sql.append("   FO.PERMISSION_TYPE PERMISSIONTYPE ");
        sql.append(" FROM FUNCTION_OBJECT FO ");
        sql.append(" WHERE 1 = 1 ");
        if (!DataUtil.isNullOrEmpty(functionCode)) {
            sql.append("  AND LOWER(FO.FUNCTION_CODE) LIKE #functionCode ESCAPE '\\' ");
        }
        if (!DataUtil.isNullObject(status)) {
            sql.append("  AND FO.STATUS = #status ");
        }
        sql.append(" ORDER BY FO.PARENT_ID DESC, ORDER_NO ASC ");
        Query query = em.createNativeQuery(sql.toString());
        if (!DataUtil.isNullOrEmpty(functionCode)) {
            query.setParameter("functionCode", "%" + functionCode + "%");
        }
        if (!DataUtil.isNullObject(status)) {
            query.setParameter("status", status);
        }
        if (isLazySearch) {
            query.setMaxResults(pageSize);
            query.setFirstResult(pageNumber * pageSize);
        }
        List lstTemp = query.getResultList();
        if (lstTemp != null && !DataUtil.isNullOrEmpty(lstTemp)) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                FunctionObjectDTO functionObjectDTO = new FunctionObjectDTO();
                //id
                functionObjectDTO.setFunctionObjectId(DataUtil.safeToLong(obj[0]));
                // code
                functionObjectDTO.setFunctionName(DataUtil.safeToString(obj[1]));
                // name
                functionObjectDTO.setFunctionCode(DataUtil.safeToString(obj[2]));
                // path
                if (null != obj[3])
                    functionObjectDTO.setFunctionPath(DataUtil.safeToString(obj[3]));
                else {
                    functionObjectDTO.setFunctionPath("");
                }
                // type
                functionObjectDTO.setFunctionType(DataUtil.safeToShort(obj[4]));

                // status
                functionObjectDTO.setStatus(DataUtil.safeToShort(obj[5]));
                // description
                functionObjectDTO.setDescription(DataUtil.safeToString(obj[6]));
                // order no
                functionObjectDTO.setOrderNo(DataUtil.safeToShort(obj[7]));
                // parent_id
                functionObjectDTO.setParentId(DataUtil.safeToLong(obj[8]));
                // parent_name
                functionObjectDTO.setParentFunctionName(DataUtil.safeToString(obj[9]));
                // create_user
                functionObjectDTO.setCreateUser(DataUtil.safeToString(obj[10]));
                // create_date
                functionObjectDTO.setCreateDate(DateUtil.convertStringToTime(DataUtil.safeToString(obj[11]), "dd/MM/yyyy HH:mm:ss"));
                // loai chuc nang
                functionObjectDTO.setPermissionType(DataUtil.safeToShort(obj[12]));
                functionObjectDTOList.add(functionObjectDTO);
            }
        }
        return functionObjectDTOList;
    }

    @Override
    public List<FunctionObject> getListChild(Long permissionId, boolean isChild) {
        StringBuilder sql = new StringBuilder("SELECT fo.* FROM FUNCTION_OBJECT fo, PERMISSION_GROUP_FUNCTION pgf WHERE 1=1 ");
        sql.append(" AND fo.STATUS=1 AND pgf.STATUS=1 AND pgf.FUNCTION_OBJECT_ID=fo.FUNCTION_OBJECT_ID ");
        sql.append(" AND pgf.PERMISSION_GROUP_ID = #permissionId");
        if (isChild){
            sql.append(" AND fo.FUNCTION_OBJECT_ID NOT IN ");
            sql.append(" (SELECT fo.PARENT_ID FROM FUNCTION_OBJECT fo, PERMISSION_GROUP_FUNCTION pgf WHERE 1=1 ");
            sql.append(" AND fo.PARENT_ID is not null AND fo.STATUS=1 AND pgf.STATUS=1 ");
            sql.append(" AND pgf.FUNCTION_OBJECT_ID=fo.FUNCTION_OBJECT_ID  AND pgf.PERMISSION_GROUP_ID = #permissionId )");
        }

        Query query = em.createNativeQuery(sql.toString(), FunctionObject.class);
        query.setParameter("permissionId", permissionId);
        return query.getResultList();
    }

    @Override
    public List<FunctionObject> findByParentIdFlowUserId(Long userId, Long parentId, Short status) {
        StringBuilder sql = new StringBuilder("SELECT * FROM FUNCTION_OBJECT WHERE STATUS=1 AND PARENT_ID ");
        if (parentId == null) sql.append(" is NULL ");
        else sql.append(" = #parentId");

        sql.append(" AND FUNCTION_OBJECT_ID IN ");
        sql.append(" ( SELECT DISTINCT pgf.FUNCTION_OBJECT_ID FROM PERMISSION_GROUP_FUNCTION pgf, PERMISSION_GROUP_USER pgu ");
        sql.append(" WHERE pgf.STATUS=1 AND pgu.STATUS=1 AND pgf.PERMISSION_GROUP_ID = pgu.PERMISSION_GROUP_ID AND pgu.CHAIN_USER_ID= #userId )");

        Query query = em.createNativeQuery(sql.toString(), FunctionObject.class);
        if (parentId != null) query.setParameter("parentId", parentId);
        query.setParameter("userId", userId);
        return query.getResultList();
    }
}