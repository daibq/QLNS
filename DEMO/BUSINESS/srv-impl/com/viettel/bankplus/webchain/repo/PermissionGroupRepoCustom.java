package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.dto.PermissionGroupDTO;
import com.viettel.bankplus.webchain.input.InputLazyFunctionGroupSearch;
import com.viettel.bankplus.webchain.model.ChainUser;
import com.viettel.bankplus.webchain.model.PermissionGroup;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.mysema.query.types.Predicate;

import java.text.ParseException;
import java.util.List;

public interface PermissionGroupRepoCustom {
    public int countLazyFunctionGroup(InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch);

    public List<PermissionGroupDTO> findLazingPaging(InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch, int pageSize, int pageNumber) throws ParseException, Exception;

    public List<FunctionObjectDTO> listDetail(PermissionGroupDTO dto, int pageSize, int pageNumber) throws Exception;

    public int listDetailCount(PermissionGroupDTO dto);

    public List<PermissionGroupDTO> getPermissionGroupDTOListByShopId(long parentShopId) throws Exception;

    public List<PermissionGroup> findByStatus(boolean isCreate, Long shopId) throws Exception;


}