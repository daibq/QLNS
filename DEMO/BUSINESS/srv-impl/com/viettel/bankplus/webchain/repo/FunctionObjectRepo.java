package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.model.FunctionObject;
import com.viettel.fw.persistence.BaseRepository;

public interface FunctionObjectRepo extends BaseRepository<FunctionObject, Long>, FunctionObjectRepoCustom {

}