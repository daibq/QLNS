package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.ApDomainDTO;
import com.viettel.bankplus.webchain.input.InputApdomain;

import java.text.ParseException;
import java.util.List;

public interface ApDomainRepoCustom {
    public int countCtctProgramList(InputApdomain inputApdomainSearch);

    public List<ApDomainDTO> findLazingPaging(InputApdomain inputApdomainSearch, int pageSize, int pageNumber) throws ParseException, Exception;

    public List<ApDomainDTO> getValueApDomainByTypeOrderByCode(String type);

    public List<ApDomainDTO> getValueApDomainByType(String type);
}