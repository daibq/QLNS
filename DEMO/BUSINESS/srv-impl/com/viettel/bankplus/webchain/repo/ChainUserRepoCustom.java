package com.viettel.bankplus.webchain.repo;

import com.mysema.query.types.Predicate;
import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.input.InputLazyChainUserSearch;
import com.viettel.bankplus.webchain.model.ChainUser;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.viettel.fw.dto.BaseMessage;

import java.util.Date;
import java.util.List;

public interface ChainUserRepoCustom {
    public Predicate toPredicate(List<FilterRequest> filters);

    //Truongxp
    public boolean checkPassword(String oldPassword, String takeSalt) throws Exception;

    public String takeSaltWithUser(String userName) throws Exception;

    boolean checkDuplicatePassword(String newPassword, String takeSalt, String userName) throws Exception;

    long findId(String userName) throws Exception;
    ChainUserDTO getValueByUserName(String userName) throws Exception;

    Date getLastDateChangePass(String userName);

    List<ChainUserDTO> getUserByInput(String input);

    //dai
    public List<ChainUserDTO> getChainUserByParentShopId(Long parentShopId) throws Exception;

    //@HUNGNQ
    public int countLazyChainUser(InputLazyChainUserSearch inputLazyChainUserSearch);

    //@HUNGNQ
    public List<ChainUserDTO> findLazyChainUser(InputLazyChainUserSearch inputLazyChainUserSearch, int pageSize, int pageNumber, boolean isLazySearch) throws Exception;

    //sondt
    public ChainUserDTO getByUsername(String username) throws Exception;

    //duyetdk
    public List<ChainUserDTO> getUsernameByChainShopID(Long chainShopId, Long viettelId) throws Exception;

    public List<ChainUser> getChainUserByChainShopIdOrderByName(Long chainShopId) throws Exception;

}