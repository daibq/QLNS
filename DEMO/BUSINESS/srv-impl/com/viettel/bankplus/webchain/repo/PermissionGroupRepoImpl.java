package com.viettel.bankplus.webchain.repo;

import com.viettel.bankplus.webchain.dto.ApDomainDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.dto.PermissionGroupDTO;
import com.viettel.bankplus.webchain.input.InputApdomain;
import com.viettel.bankplus.webchain.input.InputLazyFunctionGroupSearch;
import com.viettel.bankplus.webchain.model.ApDomain;
import com.viettel.bankplus.webchain.model.ChainUser;
import com.viettel.bankplus.webchain.model.PermissionGroup;
import com.viettel.bankplus.webchain.model.QPermissionGroup;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.DateUtil;
import com.viettel.fw.common.util.extjs.FilterRequest;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;
import com.mysema.query.types.template.BooleanTemplate;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.viettel.fw.common.util.DbUtil;
import com.viettel.fw.common.util.mapper.BaseMapper;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class PermissionGroupRepoImpl implements PermissionGroupRepoCustom {

    public static final Logger logger = Logger.getLogger(PermissionGroupRepoCustom.class);
    private final BaseMapper<PermissionGroup, PermissionGroupDTO> mapper = new BaseMapper<>(PermissionGroup.class, PermissionGroupDTO.class);

    @PersistenceContext(unitName = Const.PERSISTENT_UNIT.BANK_PLUS)
    private EntityManager em;


    @Override
    public int countLazyFunctionGroup(InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch) {
        List paramList = new ArrayList();
        StringBuilder sql = new StringBuilder(2000);
        sql.append("SELECT count(*)");
        sql.append(" FROM PERMISSION_GROUP P WHERE 1=1 ");
        if (!DataUtil.isNullOrEmpty(inputLazyFunctionGroupSearch.getPermissionGroupCode())) {
            sql.append(" AND P.PERMISSION_GROUP_CODE = ? ");
            String inputPermissionGroupCode = "%" + inputLazyFunctionGroupSearch.getPermissionGroupCode().trim().toLowerCase().replace("\\", "\\\\").replaceAll("%", "\\\\%").replaceAll("_", "\\\\_").replaceAll("!", "\\\\") + "%";
            paramList.add(inputPermissionGroupCode);
        }

        if (!DataUtil.isNullObject(inputLazyFunctionGroupSearch.getStatus())) {
            sql.append(" AND P.STATUS = ? ");
            paramList.add(inputLazyFunctionGroupSearch.getStatus());
        }
        if (!DataUtil.isNullObject(inputLazyFunctionGroupSearch.getChainShopId())) {
            sql.append(" AND P.CHAIN_SHOP_ID = ? ");
            paramList.add(inputLazyFunctionGroupSearch.getChainShopId());
        }
        sql.append(" ORDER BY P.PERMISSION_GROUP_ID");
        Query query = em.createNativeQuery(sql.toString());
        if (!DataUtil.isNullOrEmpty(paramList)) {
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i + 1, paramList.get(i));
            }
        }
        return DataUtil.safeToInt(query.getSingleResult());
    }

    @Override
    public List<PermissionGroupDTO> findLazingPaging(InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch, int pageSize, int pageNumber) throws ParseException, Exception {
        List paramList = new ArrayList();
        java.lang.StringBuilder sql = new java.lang.StringBuilder();
        List<PermissionGroupDTO> permissionGroupDTOList = new ArrayList<>();
        sql.append("SELECT PERMISSION_GROUP_ID,PERMISSION_GROUP_CODE,PERMISSION_GROUP_NAME,USE_OBJECT_TYPE,DESCRIPTION,STATUS,CREATE_DATE,CREATE_USER,UPDATE_DATE,UPDATE_USER,CHAIN_SHOP_ID");
        sql.append(" FROM PERMISSION_GROUP P where 1=1 ");
            if (!DataUtil.isNullOrEmpty(inputLazyFunctionGroupSearch.getPermissionGroupCode())) {
            sql.append(" AND P.PERMISSION_GROUP_CODE = ? ");
            paramList.add(inputLazyFunctionGroupSearch.getPermissionGroupCode());
        }

        if (!DataUtil.isNullObject(inputLazyFunctionGroupSearch.getStatus())) {
            sql.append(" AND P.STATUS = ? ");
            paramList.add(inputLazyFunctionGroupSearch.getStatus());
        }
        if (!DataUtil.isNullObject(inputLazyFunctionGroupSearch.getChainShopId())) {
            sql.append(" AND P.CHAIN_SHOP_ID = ? ");
            paramList.add(inputLazyFunctionGroupSearch.getChainShopId());
        }

        sql.append(" ORDER BY P.PERMISSION_GROUP_ID desc");
        Query query = em.createNativeQuery(sql.toString());
        if (!DataUtil.isNullOrEmpty(paramList)) {
            for (int i = 0; i < paramList.size(); i++) {
                query.setParameter(i + 1, paramList.get(i));
            }
        }
        query.setMaxResults(pageSize);
        query.setFirstResult(pageNumber * pageSize);
        List lstTemp = query.getResultList();
        if (lstTemp != null && !DataUtil.isNullOrEmpty(paramList)) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                PermissionGroupDTO permissionGroupDTO = new PermissionGroupDTO();
                permissionGroupDTO.setPermissionGroupId(DataUtil.safeToLong(obj[0]));
                permissionGroupDTO.setPermissionGroupCode(DataUtil.safeToString(obj[1]));
                permissionGroupDTO.setPermissionGroupName(DataUtil.safeToString(obj[2]));
                permissionGroupDTO.setUserObjectType(DataUtil.safeToLong(obj[3]));
                permissionGroupDTO.setDescription(DataUtil.safeToString(obj[4]));
                permissionGroupDTO.setStatus(DataUtil.safeToLong(obj[5]));
                permissionGroupDTO.setCreateDate(DateUtil.convertStringToTime(DataUtil.safeToString(obj[6]), "yyyy-MM-dd HH:mm:ss"));
                permissionGroupDTO.setCreateUser(DataUtil.safeToString(obj[7]));
                permissionGroupDTO.setUpdateDate(DateUtil.convertStringToTime(DataUtil.safeToString(obj[8]), "yyyy-MM-dd HH:mm:ss"));
                permissionGroupDTO.setUpdateUser(DataUtil.safeToString(obj[9]));
                permissionGroupDTOList.add(permissionGroupDTO);
            }
        }

        return permissionGroupDTOList;
    }


    @Override
    public List<FunctionObjectDTO> listDetail(PermissionGroupDTO dto, int pageSize, int pageNumber) throws Exception {
       /* List paramList = new ArrayList();*/
        List<FunctionObjectDTO> functionObjectDTOArrayList = new ArrayList<>();
        StringBuilder sql = new StringBuilder(200);
        /*sql.append("SELECT  " +
                "(SELECT FUNCTION_NAME FROM FUNCTION_OBJECT WHERE FUNCTION_OBJECT.FUNCTION_OBJECT_ID = B.PARENT_ID), " +
                "B.FUNCTION_CODE,B.FUNCTION_NAME,B.FUNCTION_TYPE,B.FUNCTION_PATH,B.DESCRIPTION,B.PERMISSION_TYPE," +
                "B.STATUS,B.UPDATE_USER,B.UPDATE_DATE " +
                "FROM PERMISSION_GROUP A, " +
                "FUNCTION_OBJECT B, " +
                "PERMISSION_GROUP_FUNCTION C  " +
                "WHERE A.PERMISSION_GROUP_ID = C.PERMISSION_GROUP_ID  " +
                "AND C.FUNCTION_OBJECT_ID = B.FUNCTION_OBJECT_ID " +
                "AND C.STATUS = 1 " +
                "AND B.STATUS = 1 " +
                "AND A.PERMISSION_GROUP_ID =#id");*/
        sql.append("SELECT \n" +
                "(SELECT FUNCTION_NAME FROM FUNCTION_OBJECT WHERE FUNCTION_OBJECT.FUNCTION_OBJECT_ID = B.PARENT_ID), \n" +
                "B.FUNCTION_CODE,\n" +
                "B.FUNCTION_NAME,\n" +
                "B.FUNCTION_TYPE,\n" +
                "B.FUNCTION_PATH,\n" +
                "B.DESCRIPTION,\n" +
                "B.PERMISSION_TYPE,\n" +
                "B.STATUS,\n" +
                "B.UPDATE_USER,\n" +
                "B.UPDATE_DATE\n" +
                "FROM PERMISSION_GROUP A,\n" +
                "FUNCTION_OBJECT B,\n" +
                "PERMISSION_GROUP_FUNCTION C \n" +
                "WHERE A.PERMISSION_GROUP_ID = C.PERMISSION_GROUP_ID \n" +
                "AND C.FUNCTION_OBJECT_ID = B.FUNCTION_OBJECT_ID\n" +
                "AND C.STATUS = 1 AND B.STATUS = 1 \n" +
                "AND A.PERMISSION_GROUP_ID =#id ");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("id", dto.getPermissionGroupId());
        query.setMaxResults(pageSize);
        query.setFirstResult(pageNumber * pageSize);
        List lstTemp = query.getResultList();
        if (lstTemp != null && !DataUtil.isNullOrEmpty(lstTemp)) {
            for (int i = 0; i < lstTemp.size(); i++) {
                Object[] obj = (Object[]) lstTemp.get(i);
                FunctionObjectDTO functionObjectDTO = new FunctionObjectDTO();
                functionObjectDTO.setParentFunctionName(DataUtil.safeToString(obj[0]));
                functionObjectDTO.setFunctionCode(DataUtil.safeToString(obj[1]));
                functionObjectDTO.setFunctionName(DataUtil.safeToString(obj[2]));
                functionObjectDTO.setFunctionType(DataUtil.safeToShort(obj[3]));
                functionObjectDTO.setFunctionPath(DataUtil.safeToString(obj[4]));
                functionObjectDTO.setDescription(DataUtil.safeToString(obj[5]));
                functionObjectDTO.setPermissionType(DataUtil.safeToShort(obj[6]));
                functionObjectDTO.setStatus(DataUtil.safeToShort(obj[7]));
                functionObjectDTO.setUpdateUser(DataUtil.safeToString(obj[8]));
                functionObjectDTO.setUpdateDate(DateUtil.convertStringToTime(DataUtil.safeToString(obj[9]), "yyyy-MM-dd HH:mm:ss"));
                functionObjectDTOArrayList.add(functionObjectDTO);
            }
        }
        return functionObjectDTOArrayList;
    }

    @Override
    public int listDetailCount(PermissionGroupDTO dto) {
        StringBuilder sql = new StringBuilder(200);
        sql.append("SELECT count(*) " +
                "FROM PERMISSION_GROUP A, " +
                "FUNCTION_OBJECT B, " +
                "PERMISSION_GROUP_FUNCTION C  " +
                "WHERE A.PERMISSION_GROUP_ID = C.PERMISSION_GROUP_ID  " +
                "AND C.FUNCTION_OBJECT_ID = B.FUNCTION_OBJECT_ID " +
                "AND C.STATUS = 1 " +
                "AND B.STATUS = 1 " +
                "AND A.PERMISSION_GROUP_ID =#id");
        Query query = em.createNativeQuery(sql.toString());
        query.setParameter("id", dto.getPermissionGroupId());
        return DataUtil.safeToInt(query.getSingleResult());
    }
    //đại
    @Override
    public List<PermissionGroupDTO> getPermissionGroupDTOListByShopId(long parentShopId) throws Exception {
        StringBuilder sql = new StringBuilder(200);
        sql.append("select * from PERMISSION_GROUP where CHAIN_SHOP_ID=#id AND PERMISSION_GROUP_TYPE =3 AND STATUS = 1 order by NLSSORT(PERMISSION_GROUP_NAME,'NLS_SORT=vietnamese')  ");
        Query query = em.createNativeQuery(sql.toString(), PermissionGroup.class);
        query.setParameter("id", parentShopId);
        List<PermissionGroup> listValue = query.getResultList();
        return mapper.toDtoBean(listValue);
    }

    @Override
    public List<PermissionGroup> findByStatus(boolean isCreate, Long shopId) throws Exception {
        StringBuilder sql = new StringBuilder(" SELECT * FROM PERMISSION_GROUP pg WHERE pg.STATUS=1");
        if (shopId != 1) {
            sql.append(" AND pg.CHAIN_SHOP_ID=#shopId ");
        }
        if (isCreate) sql.append(" AND pg.PERMISSION_GROUP_ID NOT IN ");
        else sql.append(" AND pg.PERMISSION_GROUP_ID IN ");
        sql.append(" ( SELECT pgf.PERMISSION_GROUP_ID FROM PERMISSION_GROUP_FUNCTION pgf, FUNCTION_OBJECT fo WHERE fo.STATUS=1 AND fo.FUNCTION_OBJECT_ID= pgf.FUNCTION_OBJECT_ID ");
        if (shopId != 1) {
            sql.append(" AND pg.CHAIN_SHOP_ID=#shopId ");
        }
        sql.append(" ) ");
        sql.append(" ORDER BY pg.PERMISSION_GROUP_NAME ASC");
        Query query = em.createNativeQuery(sql.toString(), PermissionGroup.class);
        if (shopId != 1) {
            query.setParameter("shopId", shopId);
        }
        return query.getResultList();
    }

}