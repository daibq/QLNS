package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * Created by truon on 21-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayBillNHCMWebResponse")
public class PayBillNHCMWebResponse extends PayBillNHCMWebRequest {
    @JsonProperty("trans_id")
    private String transId;

    @JsonProperty("errorCode")
    private String errorCode;

    @JsonProperty("error_msg")
    private String errorMsg;

    @JsonProperty("original_trans_id")
    private String originalTransId;

    @JsonProperty("command")
    private String command;

    @JsonProperty("trans_fee")
    private String transFee;

    @JsonProperty("real_billing_code")
    private String realBillingCode;

    @JsonProperty("real_service_code")
    private String realServiceCode;

    @JsonProperty("trans_date")
    private Date transDate;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getOriginalTransId() {
        return originalTransId;
    }

    public void setOriginalTransId(String originalTransId) {
        this.originalTransId = originalTransId;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getTransFee() {
        return transFee;
    }

    public void setTransFee(String transFee) {
        this.transFee = transFee;
    }

    public String getRealBillingCode() {
        return realBillingCode;
    }

    public void setRealBillingCode(String realBillingCode) {
        this.realBillingCode = realBillingCode;
    }

    public String getRealServiceCode() {
        return realServiceCode;
    }

    public void setRealServiceCode(String realServiceCode) {
        this.realServiceCode = realServiceCode;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }
}
