package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * Created by truon on 21-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetBillFINANCEWebResponse")
public class GetBillFINANCEWebResponse extends GetBillFINANCEWebRequest {
    @JsonProperty("trans_id")
    private String transId;

    @JsonProperty("errorCode")
    private String errorCode;

    @JsonProperty("error_msg")
    private String errorMsg;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("trans_date")
    private Date transDate;

    @JsonProperty("billing_detail")
    private CollectHCDEBTBillDetail[] billingDetail;

    @JsonProperty("reference_code")
    private String referenceCode;

    @JsonProperty("cust_name")
    private String custName;

    @JsonProperty("trans_fee")
    private String transFee;

    @JsonProperty("cust_name_unicode")
    private String custNameUnicode;

    @JsonProperty("real_service_code")
    private String realServiceCode;

    @JsonProperty("real_billing_code")
    private String realBillingCode;

    @JsonProperty("command")
    private String command;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public CollectHCDEBTBillDetail[] getBillingDetail() {
        return billingDetail;
    }

    public void setBillingDetail(CollectHCDEBTBillDetail[] billingDetail) {
        this.billingDetail = billingDetail;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getTransFee() {
        return transFee;
    }

    public void setTransFee(String transFee) {
        this.transFee = transFee;
    }

    public String getCustNameUnicode() {
        return custNameUnicode;
    }

    public void setCustNameUnicode(String custNameUnicode) {
        this.custNameUnicode = custNameUnicode;
    }

    public String getRealServiceCode() {
        return realServiceCode;
    }

    public void setRealServiceCode(String realServiceCode) {
        this.realServiceCode = realServiceCode;
    }

    public String getRealBillingCode() {
        return realBillingCode;
    }

    public void setRealBillingCode(String realBillingCode) {
        this.realBillingCode = realBillingCode;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }
}
