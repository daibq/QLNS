package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * TungHuynh
 * Created by sondt18 on 9/19/2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TeleChargeBillingDetail")
public class TeleChargeBillingDetail {
    @JsonProperty("contract_no")
    private String contractNo;

    @JsonProperty("cust_name")
    private String custName;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("status")
    private String status;

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
