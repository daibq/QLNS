package vn.com.viettel.bccs.api.topup;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 05-01-2018.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChannelInfoDTO")
public class ChannelInfoDTO {
    @JsonProperty("channel_type")
    private String channelType;

    @JsonProperty("shop_name")
    private String shopName;

    @JsonProperty("shop_address")
    private String shopAddress;

    @JsonProperty("staff_id")
    private String staffId;

    @JsonProperty("source")
    private String source;

    @JsonProperty("bank_code")
    private String bankCode;

    @JsonProperty("acc_no")
    private String accNo;

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }
}
