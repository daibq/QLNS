package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 06-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeeRequest")
public class FeeRequest {

    @JsonProperty("order_id")
    protected String orderId;

    @JsonProperty("clientId")
    protected String clientId;

    @JsonProperty("service_code")
    protected String serviceCode;

    @JsonProperty("shop_code")
    protected String shopCode;

    @JsonProperty("partner_code")
    protected String partnerCode;

    @JsonProperty("receiver")
    protected PersonDTO receiver;

    @JsonProperty("address")
    protected AddressDTO address;

    @JsonProperty("transfer_type")
    protected String transferType;

    @JsonProperty("transfer_form")
    protected String transferForm;

    @JsonProperty("amount")
    protected String amount;

    @JsonProperty("channel_info")
    protected String channelInfo;

    @JsonProperty("partner_type")
    protected String partnerType;

    @JsonProperty("staff_username")
    protected String staffUsername;

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public PersonDTO getReceiver() {
        return receiver;
    }

    public void setReceiver(PersonDTO receiver) {
        this.receiver = receiver;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getTransferForm() {
        return transferForm;
    }

    public void setTransferForm(String transferForm) {
        this.transferForm = transferForm;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public String getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(String channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }
}
