package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 14-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InitTransferWebRequest")
public class InitTransferWebRequest {

    @JsonProperty("order_id")
    protected String orderId;

    @JsonProperty("clientId")
    protected String clientId;

    @JsonProperty("shop_code")
    protected String shopCode;

    @JsonProperty("partner_code")
    protected String partnerCode;

    @JsonProperty("service_code")
    protected String serviceCode;

    @JsonProperty("sender")
    protected PersonDTO sender;

    @JsonProperty("receiver")
    protected PersonDTO receiver;

    @JsonProperty("receipt_code")
    protected String receiptCode;

    @JsonProperty("init_type")
    protected String initType;

    @JsonProperty("amount")
    protected String amount;

    @JsonProperty("channel_info")
    protected String channelInfo;

    @JsonProperty("staff_username")
    protected String staffUsername;

    @JsonProperty("partner_type")
    protected String partnerType;

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public PersonDTO getSender() {
        return sender;
    }

    public void setSender(PersonDTO sender) {
        this.sender = sender;
    }

    public PersonDTO getReceiver() {
        return receiver;
    }

    public void setReceiver(PersonDTO receiver) {
        this.receiver = receiver;
    }

    public String getReceiptCode() {
        return receiptCode;
    }

    public void setReceiptCode(String receiptCode) {
        this.receiptCode = receiptCode;
    }

    public String getInitType() {
        return initType;
    }

    public void setInitType(String initType) {
        this.initType = initType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(String channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }
}
