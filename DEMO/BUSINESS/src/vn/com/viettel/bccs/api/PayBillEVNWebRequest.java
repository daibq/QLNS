package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 20-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayBillEVNWebRequest")
public class PayBillEVNWebRequest {

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("reference_code")
    private String referenceCode;

    @JsonProperty("payer_name")
    private String payerName;

    @JsonProperty("shop_code")
    protected String shopCode;

    @JsonProperty("payer_msisdn")
    private String payerMsisdn;

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("clientId")
    private String clientId;

    @JsonProperty("partner_code")
    private String partnerCode;

    @JsonProperty("staff_username")
    private String staffUsername;

    @JsonProperty("service_code")
    private String serviceCode;

    @JsonProperty("channel_info")
    private String channelInfo;

    @JsonProperty("partner_type")
    private String partnerType;

    @JsonProperty("billing_code")
    private String billingCode;

    @JsonProperty("payer_address")
    protected String payerAddress;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayerMsisdn() {
        return payerMsisdn;
    }

    public void setPayerMsisdn(String payerMsisdn) {
        this.payerMsisdn = payerMsisdn;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(String channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getPayerAddress() {
        return payerAddress;
    }

    public void setPayerAddress(String payerAddress) {
        this.payerAddress = payerAddress;
    }
}
