package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 10-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EditTransactionWebRequest")
public class EditTransactionWebRequest extends  CashBankingTransactionRequest {
    @JsonProperty("original_trans_id")
    protected String originalTransId;

    public String getOriginalTransId() {
        return originalTransId;
    }

    public void setOriginalTransId(String originalTransId) {
        this.originalTransId = originalTransId;
    }
}
