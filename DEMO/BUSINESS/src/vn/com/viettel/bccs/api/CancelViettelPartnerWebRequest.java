package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * @author hungnq on 11/21/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancelViettelPartnerWebRequest")
public class CancelViettelPartnerWebRequest {
    @JsonProperty("order_id")
    protected String orderId;
    @JsonProperty("clientId")
    protected String clientId;
    @JsonProperty("service_code")
    protected String serviceCode;
    @JsonProperty("staff_username")
    protected String staffUsername;
    @JsonProperty("partner_code")
    protected String partnerCode;
    @JsonProperty("original_trans_id")
    protected String originalTransId;
    @JsonProperty("original_order_id")
    protected String originalOrderId;
    @JsonProperty("channel_info")
    protected String channelInfo;
    @JsonProperty("partner_type")
    protected String partnerType;
    @JsonProperty("shop_code")
    protected String shopCode;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getOriginalTransId() {
        return originalTransId;
    }

    public void setOriginalTransId(String originalTransId) {
        this.originalTransId = originalTransId;
    }

    public String getOriginalOrderId() {
        return originalOrderId;
    }

    public void setOriginalOrderId(String originalOrderId) {
        this.originalOrderId = originalOrderId;
    }

    public String getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(String channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }
}
