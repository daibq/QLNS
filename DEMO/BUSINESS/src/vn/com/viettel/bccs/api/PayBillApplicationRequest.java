package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by truon on 23-10-2017.
 */
public class PayBillApplicationRequest {
    @JsonProperty("order_id")
    protected String orderId;

    @JsonProperty("clientId")
    protected String clientId;

    @JsonProperty("partner_code")
    protected String partnerCode;

    @JsonProperty("shop_code")
    protected String shopCode;

    @JsonProperty("channel_info")
    protected String channelInfo;

    @JsonProperty("payer_msisdn")
    protected String payerMsisdn;

    @JsonProperty("payer_name")
    protected String payerName;

    @JsonProperty("payer_address")
    protected String payerAddress;

    @JsonProperty("service_code")
    protected String serviceCode;

    @JsonProperty("partner_type")
    protected String partnerType;

    @JsonProperty("amount")
    protected String amount;

    @JsonProperty("billing_code")
    protected String billingCode;

    @JsonProperty("reference_code")
    protected String referenceCode;

    @JsonProperty("staff_username")
    protected String staffUsername;


    public String getChannelInfo() {
        return channelInfo;
    }

    public void setChannelInfo(String channelInfo) {
        this.channelInfo = channelInfo;
    }

    public String getPayerMsisdn() {
        return payerMsisdn;
    }

    public void setPayerMsisdn(String payerMsisdn) {
        this.payerMsisdn = payerMsisdn;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public String getBillingCode() {
        return billingCode;
    }

    public void setBillingCode(String billingCode) {
        this.billingCode = billingCode;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayerAddress() {
        return payerAddress;
    }

    public void setPayerAddress(String payerAddress) {
        this.payerAddress = payerAddress;
    }
}
