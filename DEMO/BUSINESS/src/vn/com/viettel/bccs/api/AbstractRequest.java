package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractRequest")
public class AbstractRequest {
    @JsonUnwrapped
    protected CommonRequest commonRequest;

    public AbstractRequest() {
        commonRequest = new CommonRequest();
    }

    public CommonRequest getCommonRequest() {
        if (commonRequest==null){
            commonRequest = new CommonRequest();
        }
        return commonRequest;
    }

    public void setCommonRequest(CommonRequest commonRequest) {
        this.commonRequest = commonRequest;
    }
}
