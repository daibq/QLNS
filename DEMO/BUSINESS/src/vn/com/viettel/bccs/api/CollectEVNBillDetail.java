package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by truon on 20-10-2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectEVNBillDetail")
public class CollectEVNBillDetail {

    @JsonProperty("mahd")
    private String mahd;

    @JsonProperty("idhoadon")
    private String idhoadon;

    @JsonProperty("makh")
    private String makh;

    @JsonProperty("sotien")
    private String sotien;

    @JsonProperty("ngayhd")
    private String ngayhd;

    @JsonProperty("tungay")
    private String tungay;

    @JsonProperty("denngay")
    private String denngay;

    @JsonProperty("dntt")
    private String dntt;

    @JsonProperty("soho")
    private String soho;

    @JsonProperty("kyhieu")
    private String kyhieu;

    @JsonProperty("seri")
    private String seri;

    @JsonProperty("diachi")
    private String diachi;

    @JsonProperty("dienthoai")
    private String dienthoai;

    @JsonProperty("nam")
    private String nam;

    @JsonProperty("thang")
    private String thang;

    @JsonProperty("ky")
    private String ky;

    @JsonProperty("sosocongto")
    private String sosocongto;

    @JsonProperty("csdaukt")
    private String csdaukt;

    @JsonProperty("cscuoikt")
    private String cscuoikt;

    @JsonProperty("sogcs")
    private String sogcs;

    @JsonProperty("detailsinfo")
    private CollectEVNBillDetailsInfo[] detailsinfo;

    public String getMahd() {
        return mahd;
    }

    public void setMahd(String mahd) {
        this.mahd = mahd;
    }

    public String getIdhoadon() {
        return idhoadon;
    }

    public void setIdhoadon(String idhoadon) {
        this.idhoadon = idhoadon;
    }

    public String getMakh() {
        return makh;
    }

    public void setMakh(String makh) {
        this.makh = makh;
    }

    public String getSotien() {
        return sotien;
    }

    public void setSotien(String sotien) {
        this.sotien = sotien;
    }

    public String getNgayhd() {
        return ngayhd;
    }

    public void setNgayhd(String ngayhd) {
        this.ngayhd = ngayhd;
    }

    public String getTungay() {
        return tungay;
    }

    public void setTungay(String tungay) {
        this.tungay = tungay;
    }

    public String getDenngay() {
        return denngay;
    }

    public void setDenngay(String denngay) {
        this.denngay = denngay;
    }

    public String getDntt() {
        return dntt;
    }

    public void setDntt(String dntt) {
        this.dntt = dntt;
    }

    public String getSoho() {
        return soho;
    }

    public void setSoho(String soho) {
        this.soho = soho;
    }

    public String getKyhieu() {
        return kyhieu;
    }

    public void setKyhieu(String kyhieu) {
        this.kyhieu = kyhieu;
    }

    public String getSeri() {
        return seri;
    }

    public void setSeri(String seri) {
        this.seri = seri;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public String getDienthoai() {
        return dienthoai;
    }

    public void setDienthoai(String dienthoai) {
        this.dienthoai = dienthoai;
    }

    public String getNam() {
        return nam;
    }

    public void setNam(String nam) {
        this.nam = nam;
    }

    public String getThang() {
        return thang;
    }

    public void setThang(String thang) {
        this.thang = thang;
    }

    public String getKy() {
        return ky;
    }

    public void setKy(String ky) {
        this.ky = ky;
    }

    public String getSosocongto() {
        return sosocongto;
    }

    public void setSosocongto(String sosocongto) {
        this.sosocongto = sosocongto;
    }

    public String getCsdaukt() {
        return csdaukt;
    }

    public void setCsdaukt(String csdaukt) {
        this.csdaukt = csdaukt;
    }

    public String getCscuoikt() {
        return cscuoikt;
    }

    public void setCscuoikt(String cscuoikt) {
        this.cscuoikt = cscuoikt;
    }

    public String getSogcs() {
        return sogcs;
    }

    public void setSogcs(String sogcs) {
        this.sogcs = sogcs;
    }

    public CollectEVNBillDetailsInfo[] getDetailsinfo() {
        return detailsinfo;
    }

    public void setDetailsinfo(CollectEVNBillDetailsInfo[] detailsinfo) {
        this.detailsinfo = detailsinfo;
    }
}
