package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author hungnq on 11/22/2017
 */
public class MappingBankplusBccsWebResponse extends MappingBankplusBccsWebRequest {
    @JsonProperty("trans_id")
    private String transId;
    @JsonProperty("errorCode")
    private String errorCode;
    @JsonProperty("error_msg")
    private String errorMsg;
    @JsonProperty("acc_detail")
    private List<BankplusBccsAcountMapping> accDetailList;
    @JsonProperty("account_type")
    private String accountType;
    @JsonProperty("balance")
    private String balance;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<BankplusBccsAcountMapping> getAccDetailList() {
        return accDetailList;
    }

    public void setAccDetailList(List<BankplusBccsAcountMapping> accDetailList) {
        this.accDetailList = accDetailList;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
