package vn.com.viettel.bccs.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author hungnq on 12/13/2017
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendSmsWebRequest")
public class SendSmsWebRequest {
    @JsonProperty("order_id")
    protected String orderId;

    @JsonProperty("clientId")
    protected String clientId;

    @JsonProperty("request_date")
    protected Date requestDate;

    @JsonProperty("shop_code")
    protected String shopCode;

    @JsonProperty("partner_code")
    protected String partnerCode;
    //so dien thoai can gui tin nhan
    @JsonProperty("msisdn")
    protected String msisdn;
    // dich vu
    @JsonProperty("processCode")
    protected String processCode;
    // ten dang nhap cua acount thuc hien
    @JsonProperty("staff_username")
    protected String staffUsername;

    // ten chuoi cua nguoi dung thuc hien
    @JsonProperty("chain_name")
    protected String chainName;
    // ten cua hang chuoi cua nguoi dung thuc hien
    @JsonProperty("shop_name")
    protected String shopName;

    // ten dang nhap cua nguoi dung can gui tin nhan
    @JsonProperty("object_staff_username")
    protected String objectStaffUsername;
    //ma chuoi cua nguoi dung can gui tin nhan
    @JsonProperty("object_chain_code")
    protected String objectChainCode;
    // chuoi cua nguoi dung can gui tin nhan
    @JsonProperty("cbject_chain_name")
    protected String objectChainName;
    // ten cua hang chuoi cua nguoi dung can gui tin nhan
    @JsonProperty("object_shop_name")
    protected String objectShopName;
    // ma cua hang chuoi cua nguoi dung can gui tin nhan
    @JsonProperty("object_shop_code")
    protected String objectShopCode;
    // otp
    @JsonProperty("new_pin")
    protected String newPIN;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getStaffUsername() {
        return staffUsername;
    }

    public void setStaffUsername(String staffUsername) {
        this.staffUsername = staffUsername;
    }

    public String getChainName() {
        return chainName;
    }

    public void setChainName(String chainName) {
        this.chainName = chainName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getObjectStaffUsername() {
        return objectStaffUsername;
    }

    public void setObjectStaffUsername(String objectStaffUsername) {
        this.objectStaffUsername = objectStaffUsername;
    }

    public String getObjectChainCode() {
        return objectChainCode;
    }

    public void setObjectChainCode(String objectChainCode) {
        this.objectChainCode = objectChainCode;
    }

    public String getObjectChainName() {
        return objectChainName;
    }

    public void setObjectChainName(String objectChainName) {
        this.objectChainName = objectChainName;
    }

    public String getObjectShopName() {
        return objectShopName;
    }

    public void setObjectShopName(String objectShopName) {
        this.objectShopName = objectShopName;
    }

    public String getObjectShopCode() {
        return objectShopCode;
    }

    public void setObjectShopCode(String objectShopCode) {
        this.objectShopCode = objectShopCode;
    }

    public String getNewPIN() {
        return newPIN;
    }

    public void setNewPIN(String newPIN) {
        this.newPIN = newPIN;
    }
}
