package vn.com.viettel.bccs.client;


import com.viettel.common.Const;
import com.viettel.fw.SystemConfig;
import com.viettel.fw.common.util.DataUtil;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.net.ssl.HostnameVerifier;
import java.io.IOException;
import java.security.cert.X509Certificate;

/**
 * Created by dtn on 3/13/2017.
 */
public class Client {
    public static final Logger logger = Logger.getLogger(Client.class);
    // cau hinh
//    private static final String URL = "https://125.235.40.34:8088/CoreChain/CoreChainAPI/BusinessAPI";
//    private static final String SIGN = "changeme";
    private static CloseableHttpClient instance;
    private static final boolean USE_PROXY = false;
    private static final String PROXY_HOST = "";
    private static final int PROXY_PORT = 0;
//    private static int defaulAPITimout = 180000;


    private static final String TEMPLATE = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://webservices.corechain.bankplus.viettel.com/\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <web:process>\n" +
            "         <cmd>%CMD%</cmd>\n" +
            "         <data>%DATA%</data>\n" +
            "         <signature>%SIGN%</signature>\n" +
            "      </web:process>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>";

    static SSLContext getSslContextAll() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    X509Certificate[] noCer = {};
                    return noCer;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    // deo lam gi
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    // deo lam gi
                }
            }};
            SSLContext sc = SSLContext.getInstance("SSL");
            // no keystore
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            return sc;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    /*public static String getURL() {
        return URL;
    }*/

    private static HostnameVerifier getHostNameVerifierAll() {
        return new HostnameVerifier() {
            public boolean verify(String arg0, SSLSession arg1) {
                return true;
            }
        };
    }

    public static CloseableHttpClient getHttpClient() {
        if (instance == null) {
            try {
                // host
                Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("https", new SSLConnectionSocketFactory(getSslContextAll(), getHostNameVerifierAll()))
                        .register("http", new PlainConnectionSocketFactory())
                        .build();

                HttpClientBuilder builder = HttpClients.custom().setConnectionManager(getCm(socketFactoryRegistry));
                if (USE_PROXY) {
                    builder.setProxy(new HttpHost(PROXY_HOST, PROXY_PORT));
                }
                return builder.build();
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                return null;
            }
        } else {
            return instance;
        }
    }

    /*public static String callAPI(String cmd, String data) throws IOException {
        String response = callAPI(cmd, data, SIGN);
        if (response == null || "".equals(response))
            return null;

        if (response.indexOf("<return>") >= 0) {
            return response.substring(response.indexOf("<return>") + 8, response.indexOf("</return>"));
        } else {
            return "";
        }
    }*/

    /*public static String callAPI(String cmd, String data, String sign) throws IOException {
        HttpPost httpPost = new HttpPost(URL);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(defaulAPITimout).build();
        httpPost.setConfig(requestConfig);
        httpPost.addHeader("Content-Type", "text/xml;charset=UTF-8");
        String body = TEMPLATE.replaceAll("%CMD%", cmd).replaceAll("%DATA%", data).replaceAll("%SIGN%", sign);
        StringEntity requestEntity = new StringEntity(body, "UTF-8");
        httpPost.setEntity(requestEntity);
        CloseableHttpClient closeableHttpClient = getHttpClient();
        if (closeableHttpClient != null) {
            CloseableHttpResponse response = closeableHttpClient.execute(httpPost);
            return EntityUtils.toString(response.getEntity());
        } else {
            return null;
        }
    }*/

    public static String callAPIWithConfig(String cmd, String data, SystemConfig systemConfig) throws IOException {
        String response = callAPIWithConfigSystem(cmd, data, systemConfig);
        if (response == null || "".equals(response))
            return null;

        if (response.indexOf("<return>") >= 0) {
            return response.substring(response.indexOf("<return>") + 8, response.indexOf("</return>"));
        } else {
            return "";
        }
    }

    private static boolean stringIsNull(String st) {
        if (DataUtil.isNullOrEmpty(st)) return true;
        else if (st.equalsIgnoreCase(Const.WEB_CHAIN.IS_NULL)) return true;
        else return false;
    }

    private static boolean intIsNullOrZero(int value) {
        if (DataUtil.isNullOrZero(DataUtil.safeToLong(value))) return true;
        else if (DataUtil.safeToString(value).equalsIgnoreCase(Const.WEB_CHAIN.IS_NULL)) return true;
        else return false;
    }


    public static String callAPIWithConfigSystem(String cmd, String data, SystemConfig systemConfig) throws IOException {
        String url = stringIsNull(systemConfig.CORE_API_URL) ? Const.CALL_API.URL : systemConfig.CORE_API_URL;
        String sign = stringIsNull(systemConfig.CORE_API_SIGN) ? Const.CALL_API.SIGN : systemConfig.CORE_API_SIGN;
        int timeout = intIsNullOrZero(systemConfig.CORE_API_TIMEOUT) ? Const.CALL_API.TIMEOUT_DEFAULT : systemConfig.CORE_API_TIMEOUT;

        HttpPost httpPost = new HttpPost(url);
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(timeout).build();
        httpPost.setConfig(requestConfig);
        httpPost.addHeader("Content-Type", "text/xml;charset=UTF-8");
        String body = TEMPLATE.replaceAll("%CMD%", cmd).replaceAll("%DATA%", data).replaceAll("%SIGN%", sign);
        StringEntity requestEntity = new StringEntity(body, "UTF-8");
        httpPost.setEntity(requestEntity);
        CloseableHttpClient closeableHttpClient = getHttpClient();
        if (closeableHttpClient != null) {
            CloseableHttpResponse response = closeableHttpClient.execute(httpPost);
            return EntityUtils.toString(response.getEntity());
        } else {
            return null;
        }
    }

    public static CloseableHttpClient getInstance() {
        return instance;
    }

    public static void setInstance(CloseableHttpClient instance) {
        Client.instance = instance;
    }

    public static synchronized PoolingHttpClientConnectionManager getCm(Registry<ConnectionSocketFactory> socketFactoryRegistry) {
        PoolingHttpClientConnectionManager cm1 = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        cm1.setMaxTotal(200);
        cm1.setDefaultMaxPerRoute(20);
        return cm1;
    }
}
