package com.viettel.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Created by Admin on 6/18/2015.
 */
@FacesValidator("lengthValidator")
public class LengthValidator implements Validator {
    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
        if (value == null) {
            return; // Let required="true" handle.
        }
        Object minimumComponent = uiComponent.getAttributes().get("minimum");
        if (minimumComponent == null) {
            return;
        }
        Object maximumComponent = uiComponent.getAttributes().get("maximum");
        if (maximumComponent == null) {
            return;
        }
        String strValue = value.toString();
        if (strValue.length() < Long.valueOf(minimumComponent.toString())
                || strValue.length() > Long.valueOf(maximumComponent.toString())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
}
