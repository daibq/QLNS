package com.viettel.bccs.fw.custom;

import org.primefaces.component.fileupload.FileUpload;
import org.primefaces.component.fileupload.FileUploadRenderer;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;

/**
 * Created by thanhnt77 on 25/11/2015.
 */
public class CbsFileUploadRenderer extends FileUploadRenderer {

    protected void encodeAdvancedMarkup(FacesContext context, FileUpload fileUpload) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String clientId = fileUpload.getClientId(context);
        String style = fileUpload.getStyle();
        String styleClass = fileUpload.getStyleClass();
        styleClass = styleClass == null?"ui-fileupload ui-widget":"ui-fileupload ui-widget " + styleClass;
        boolean disabled = fileUpload.isDisabled();
        writer.startElement("div", fileUpload);
        writer.writeAttribute("id", clientId, "id");
        writer.writeAttribute("class", styleClass, styleClass);
        if(style != null) {
            writer.writeAttribute("style", style, "style");
        }

        writer.startElement("div", fileUpload);
        writer.writeAttribute("class", "ui-fileupload-buttonbar ui-widget-header ui-corner-top", (String)null);
        this.encodeChooseButton(context, fileUpload, disabled);
        if(!fileUpload.isAuto()) {
            this.encodeButton(context, fileUpload.getUploadLabel(), "ui-fileupload-upload", "ui-icon-arrowreturnthick-1-n");
            this.encodeButton(context, fileUpload.getCancelLabel(), "ui-fileupload-cancel", "ui-icon-cancel");
        }

        writer.endElement("div");
        this.renderChildren(context, fileUpload);
        writer.startElement("div", (UIComponent)null);
        writer.writeAttribute("class", "ui-fileupload-content ui-widget-content ui-corner-bottom", (String)null);
        writer.startElement("table", (UIComponent)null);
        writer.writeAttribute("class", "ui-fileupload-files", (String)null);
        writer.startElement("tbody", (UIComponent)null);
        writer.endElement("tbody");
        writer.endElement("table");
        writer.endElement("div");
        writer.endElement("div");
    }

    protected void encodeSimpleMarkup(FacesContext context, FileUpload fileUpload) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String clientId = fileUpload.getClientId(context);
        String style = fileUpload.getStyle();
        String styleClass = fileUpload.getStyleClass();
        if(fileUpload.isSkinSimple()) {
            styleClass = styleClass == null?"ui-fileupload-simple ui-widget":"ui-fileupload-simple ui-widget " + styleClass;
            String buttonClass = "ui-button ui-widget ui-state-default ui-corner-all";
//            String buttonClass = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-left";
            if(fileUpload.isDisabled()) {
                buttonClass = buttonClass + " ui-state-disabled";
            }

            writer.startElement("span", fileUpload);
            writer.writeAttribute("id", clientId, "id");
            writer.writeAttribute("class", styleClass, "styleClass");
            if(style != null) {
                writer.writeAttribute("style", style, "style");
            }

            writer.startElement("span", (UIComponent)null);
            writer.writeAttribute("class", buttonClass, (String)null);
//            writer.startElement("span", (UIComponent)null);
//            writer.writeAttribute("class", "ui-button-icon-left ui-icon ui-c ui-icon-plusthick", (String)null);
//            writer.endElement("span");
            writer.startElement("span", (UIComponent)null);
            writer.writeAttribute("class", "ui-button-text ui-c", (String)null);
            writer.writeText(fileUpload.getLabel(), "value");
            writer.endElement("span");
            this.encodeInputField(context, fileUpload, fileUpload.getClientId(context) + "_input");
            writer.endElement("span");
            writer.startElement("span", fileUpload);
            writer.writeAttribute("class", "ui-fileupload-filename", (String)null);
            writer.endElement("span");
            writer.endElement("span");
        } else {
            this.encodeSimpleInputField(context, fileUpload, fileUpload.getClientId(context), style, styleClass);
        }

    }

    protected void encodeChooseButton(FacesContext context, FileUpload fileUpload, boolean disabled) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String clientId = fileUpload.getClientId(context);
        String cssClass = "ui-button ui-widget ui-state-default ui-corner-all ui-fileupload-choose";
//        String cssClass = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-left ui-fileupload-choose";
        if(disabled) {
            cssClass = cssClass + " ui-state-disabled";
        }

        writer.startElement("span", (UIComponent)null);
        writer.writeAttribute("class", cssClass, (String)null);
//        writer.startElement("span", (UIComponent)null);
//        writer.writeAttribute("class", "ui-button-icon-left ui-icon ui-c ui-icon-plusthick", (String)null);
//        writer.endElement("span");
        writer.startElement("span", (UIComponent)null);
        writer.writeAttribute("class", "ui-button-text ui-c", (String)null);
        writer.writeText(fileUpload.getLabel(), "value");
        writer.endElement("span");
        if(!disabled) {
            this.encodeInputField(context, fileUpload, clientId + "_input");
        }

        writer.endElement("span");
    }

    protected void encodeButton(FacesContext context, String label, String styleClass, String icon) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String cssClass = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-left ui-state-disabled " + styleClass;
        writer.startElement("button", (UIComponent)null);
        writer.writeAttribute("type", "button", (String)null);
        writer.writeAttribute("class", cssClass, (String)null);
        writer.writeAttribute("disabled", "disabled", (String)null);
//        String iconClass = "ui-button-icon-left ui-icon ui-c";
//        writer.startElement("span", (UIComponent)null);
//        writer.writeAttribute("class", iconClass + " " + icon, (String)null);
//        writer.endElement("span");
        writer.startElement("span", (UIComponent)null);
        writer.writeAttribute("class", "ui-button-text ui-c", (String)null);
        writer.writeText(label, "value");
        writer.endElement("span");
        writer.endElement("button");
    }
}
