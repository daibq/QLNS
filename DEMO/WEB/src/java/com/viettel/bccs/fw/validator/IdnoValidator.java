package com.viettel.bccs.fw.validator;

import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.GetTextFromBundleHelper;
import org.apache.commons.lang.StringUtils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * @author nhannt34
 * @since 19/01/2016
 */
@FacesValidator("validator.idno")
public class IdnoValidator implements Validator {
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String idno = DataUtil.safeToString(value);
        if (StringUtils.length(idno) > 20) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, GetTextFromBundleHelper.getText("common.validate.idno.sum"),
                    GetTextFromBundleHelper.getText("common.validate.idno.detail.length"));
            throw new ValidatorException(msg);
        }
    }
}
