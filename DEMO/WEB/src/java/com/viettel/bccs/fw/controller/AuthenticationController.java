package com.viettel.bccs.fw.controller;

import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.service.AuthenticationService;
import com.viettel.common.DataLogin;
import com.viettel.fw.SystemConfig;
import com.viettel.fw.common.util.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.ErrorCode;
import com.viettel.fw.passport.CustomConnector;
import com.viettel.web.common.LoginBean;
import com.viettel.web.common.MenuBean;
import com.viettel.web.common.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import viettel.passport.client.ObjectToken;
import viettel.passport.client.UserToken;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.*;


@ManagedBean
@Component
@Scope("view")
public class AuthenticationController extends BaseController {
    private static final long serialVersionUID = 1;
    private String msgAlert;
    private String username;
    private String password;
    private String msgError;
    private String captcha = "";
    private String imageUrl;
    private boolean checkLogin;
    @Autowired
    private transient SystemConfig systemConfig;
    @Autowired
    private transient ApplicationContext applicationContext;
    @Autowired
    private transient AuthenticationService authenticationService;

    @PostConstruct
    public void init() throws IOException {
        //@HungNQ bo commit. Chay kiem tra lai xem co van de gi khong
        Boolean isAuthenticated = (Boolean) getSession().getAttribute(Const.AUTHENTICATED);
        if (isAuthenticated != null && isAuthenticated)
            getContext().getExternalContext().redirect(getRequest().getContextPath());
        imageUrl = "captcha.jpg";
    }

    /**
     * @return
     */
    private boolean validateLogin() {
        if (DataUtil.isNullOrEmpty(username)) {
            msgError = getText("common.error.username.empty");
            this.captcha = null;
            return false;
        }
        if (DataUtil.isNullOrEmpty(password)) {
            msgError = getText("common.error.password.empty");
            this.captcha = null;
            return false;
        }
        if (DataUtil.isNullOrEmpty(captcha)) {
            msgError = getText("reset.pass.code.confirm.not.blank");
            return false;
        }

        String captchaStr = DataUtil.safeToString(getSession().getAttribute("captcha"));

        if (!captcha.equals(captchaStr)) {
            msgError = getText("reset.pass.not.right");
            this.captcha = null;
            return false;
        }
        return true;
    }

    /**
     * do login action
     */
    public void login() {
        try {
            Date loginDate = new Date();
            if (validateLogin()) {
                String captchaStr = DataUtil.safeToString(getSession().getAttribute("captcha"));
                msgError = "";
                DataLogin dataLogin = authenticationService.login(username, password);
                if (dataLogin.getReturnCode().equals(ErrorCode.SUCCESSFUL) && captcha.equals(captchaStr)) {
                    UserToken userToken = new UserToken();
                    userToken.setStatus(Long.valueOf(Const.STATUS_ACTIVE));
                    userToken.setUserName(dataLogin.getProfile().getUserName());
                    userToken.setEmail(dataLogin.getProfile().getEmail());
                    userToken.setUserID(dataLogin.getProfile().getChainUserId());
                    userToken.setFullName(dataLogin.getProfile().getFullName());
                    // gan user_type vao gender
                    userToken.setGender(dataLogin.getProfile().getChainUserType());

                    ArrayList<FunctionObjectDTO> menuComponents = new ArrayList<>();
                    dataLogin.getMenu().forEach(m -> {
                        if (m.getFunctionType().shortValue() == Const.MENU.COMPONENT.shortValue())
                            menuComponents.add(m);
                    });
                    userToken.setComponentList(getComponentList(menuComponents));
                    List<FunctionObjectDTO> menuModules = new ArrayList<>();
                    dataLogin.getMenu().forEach(m -> {
                        if (m.getFunctionType().shortValue() == Const.MENU.MODULE.shortValue())
                            menuModules.add(m);
                    });

                    ObjectToken treeMenu = findChildMenuByParentId(menuModules, null); //build treemenu from root
                    treeMenu.setObjectCode(systemConfig.DEFAULT_SYS);
                    if (!DataUtil.isNullOrEmpty(treeMenu.getChildObjects())) { // order by stt (seq)
                        Collections.sort(treeMenu.getChildObjects(), (m1, m2) -> m1.getOrd().compareTo(m2.getOrd()));
                    }

                    ArrayList<ObjectToken> temp = new ArrayList<>();
                    temp.add(treeMenu);
                    userToken.setParentMenu(temp);

                    List<String> vsaAllowedURL = new ArrayList<>();
                    for (FunctionObjectDTO ot : menuModules) {
                        String servletPath = ot.getFunctionPath();
                        if (!DataUtil.isNullOrEmpty(servletPath) && !("#".equals(servletPath))) {
                            vsaAllowedURL.add(servletPath.split("\\?")[0]);
                        }
                    }
                    getSession().setAttribute(Const.VSA_ALLOW_URL, vsaAllowedURL);
                    getSession().setAttribute(Const.AUTHENTICATED, true);
                    getSession().setAttribute(Const.DEFAULT_SYS, systemConfig.DEFAULT_SYS);
                    getSession().setAttribute("userName", userToken.getUserName());
                    getSession().setAttribute(CustomConnector.LOGIN_DATE, new Date(System.currentTimeMillis()));
                    getSession().setAttribute(CustomConnector.VSA_USER_TOKEN, userToken);
                    getSession().setAttribute("isFirstLogin", dataLogin.getBlChangePassword());

                    LoginBean loginBean = applicationContext.getBean(LoginBean.class);
                    loginBean.setUserToken(userToken);
                    loginBean.setLoginDate(loginDate);
                    loginBean.createAuthoCache();

                    MenuBean menuBean = applicationContext.getBean(MenuBean.class);
                    menuBean.createMenuModel();
                    menuBean.createMapBreadCrumb();
                    //check pass here.
                    if (com.viettel.common.Const.WEB_CHAIN.BL_CHANGE_PASSWORD_TRUE.equalsIgnoreCase(dataLogin.getBlChangePassword())) {
                        // forward sang trang change pass
                        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
                        RequestDispatcher dispatcher = ((ServletRequest) context.getRequest()).getRequestDispatcher("j_spring_security_check");
                        dispatcher.forward((ServletRequest) context.getRequest(), (ServletResponse) context.getResponse());
                        FacesContext.getCurrentInstance().responseComplete();
                        //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,null,getText("pw.change.first.login.change.pass")));
                        //RequestContext.getCurrentInstance().execute("redirectChangePassword()");
                        //context.redirect(context.getRequestContextPath() + "/changePassword.jsf");
                    } else {
                        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
                        RequestDispatcher dispatcher = ((ServletRequest) context.getRequest()).getRequestDispatcher("j_spring_security_check");
                        dispatcher.forward((ServletRequest) context.getRequest(), (ServletResponse) context.getResponse());
                        FacesContext.getCurrentInstance().responseComplete();
                    }
                } else {
                    this.captcha = null;
                    switch (dataLogin.getReturnCode()) {
                        case ErrorCode.ERROR_USER.ERROR_USER_PASS_INVALID:
                            msgError = getText("ERROR_USER_PASS_INVALID");
                            break;
                        case ErrorCode.ERROR_USER.ERROR_USER_NOT_EXIST:
                            msgError = getText("ERROR_USER_NOT_EXIST");
                            break;
                        case ErrorCode.ERROR_USER.ERROR_USER_DUPLICATE_OBJECT:
                            msgError = getText("ERROR_USER_DUPLICATE_OBJECT");
                            break;
                        case ErrorCode.ERROR_USER.ERROR_USER_IN_ACTIVE:
                            msgError = getText("ERROR_USER_IN_ACTIVE");
                            break;
                        case ErrorCode.ERROR_USER.ERROR_USER_NO_PERMISSION:
                            msgError = getText("ERROR_USER_NO_PERMISSION");
                            break;
                        default:
                            msgError = getText("common.error.happened.againAction");
                            break;
                    }
                }
            }
        } catch (Exception e) {
            msgError = getText("common.error.happened.againAction");
            this.captcha = null;
            logger.error(e.getMessage(), e);
        }
    }


    /**
     * @param components
     *
     * @return
     */
    public ArrayList<ObjectToken> getComponentList(List<FunctionObjectDTO> components) {
        ArrayList<ObjectToken> objectTokens = new ArrayList<>();
        for (FunctionObjectDTO menuDTO : components) {
            ObjectToken objectToken = new ObjectToken();
            objectToken.setStatus(1L);
            objectToken.setObjectName(menuDTO.getFunctionName());
            objectToken.setObjectId(menuDTO.getFunctionObjectId());
            objectToken.setObjectCode(menuDTO.getFunctionCode());
            objectToken.setObjectUrl(menuDTO.getFunctionPath());
            objectToken.setParentId(menuDTO.getParentId());
            objectToken.setObjectType(1L);
            objectTokens.add(objectToken);
        }
        return objectTokens;
    }
    public static void redirect(String url) throws IOException {
        ExternalContext ctx = FacesContext.getCurrentInstance()
                .getExternalContext();
        if (url.startsWith("http://") || url.startsWith("https://")
                || url.startsWith("/")) {
            ctx.redirect(url);
        } else {
            ctx.redirect(ctx.getRequestContextPath() + "/" + url);
        }
    }

    public String getMsgError() {
        return msgError;
    }

    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    /**
     * dung cay thu muc nhe
     *
     * @param listMenu
     * @param parentId
     *
     * @return
     */
    private ObjectToken findChildMenuByParentId(List<FunctionObjectDTO> listMenu, Long parentId) {
        ObjectToken treeMenu = new ObjectToken();
        treeMenu.setChildObjects(new ArrayList<>());
        for (FunctionObjectDTO menuDTO : listMenu) {
            //if is children of node
            if ((parentId == null && menuDTO.getParentId() == null) || (menuDTO.getParentId() != null && parentId != null && menuDTO.getParentId().equals(parentId))) {
                ObjectToken temp = findChildMenuByParentId(listMenu, menuDTO.getFunctionObjectId()); //find children of child
                if (!DataUtil.isNullOrEmpty(temp.getObjectCode()) || !DataUtil.isNullOrEmpty(temp.getChildObjects())) {
                    //is a menu
                    ObjectToken treeMenuTemp = new ObjectToken();
                    treeMenuTemp.setChildObjects(new ArrayList<>());
                    treeMenuTemp.setObjectName(menuDTO.getFunctionName());
                    treeMenuTemp.setObjectCode(menuDTO.getFunctionCode());
                    treeMenuTemp.setParentId(DataUtil.safeToLong(menuDTO.getParentId()));
                    treeMenuTemp.setObjectId(menuDTO.getFunctionObjectId());
                    treeMenuTemp.setObjectUrl(menuDTO.getFunctionPath() == null ? "" : menuDTO.getFunctionPath());
                    treeMenuTemp.setOrd(DataUtil.safeToLong(menuDTO.getOrderNo()));
                    if (!DataUtil.isNullOrEmpty(temp.getChildObjects())) // if menu has child
                        treeMenuTemp.getChildObjects().addAll(temp.getChildObjects());
                    treeMenu.getChildObjects().add(treeMenuTemp);
                    Collections.sort(treeMenu.getChildObjects(), (m1, m2) -> m1.getOrd().compareTo(m2.getOrd()));
                } else { // if has nothing children then gather all into a list
                    ObjectToken treeMenuTemp = new ObjectToken();
                    treeMenuTemp.setObjectName(menuDTO.getFunctionName());
                    treeMenuTemp.setObjectCode(menuDTO.getFunctionCode());
                    treeMenuTemp.setParentId(DataUtil.safeToLong(menuDTO.getParentId()));
                    treeMenuTemp.setObjectId(menuDTO.getFunctionObjectId());
                    treeMenuTemp.setObjectUrl(menuDTO.getFunctionPath() == null ? "" : menuDTO.getFunctionPath());
                    treeMenuTemp.setOrd(DataUtil.safeToLong(menuDTO.getOrderNo()));
                    treeMenu.getChildObjects().add(treeMenuTemp);
                    Collections.sort(treeMenu.getChildObjects(), (m1, m2) -> m1.getOrd().compareTo(m2.getOrd()));
                }
            }
        }
        return treeMenu; // nothing mapping menu or loop all list
    }

    /**
     * @throws Exception
     */
    public void logout() {
        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            SecurityContextHolder.clearContext();
            /**
             * Delete Cookies
             */
            HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
            HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
            Cookie cookie = new Cookie("SPRING_SECURITY_REMEMBER_ME_COOKIE", null);
            cookie.setSecure(true);
            Cookie jsessionId = new Cookie("JSESSIONID", null);
            jsessionId.setSecure(true);
            cookie.setMaxAge(0);
            cookie.setPath(request.getContextPath().length() > 0 ? request
                    .getContextPath() : "/");
            jsessionId.setMaxAge(0);
            jsessionId.setPath(request.getContextPath().length() > 0 ? request
                    .getContextPath() : "/");
            response.addCookie(cookie);
            response.addCookie(jsessionId);

            ResourceBundle rb1;
            Locale locale = new Locale("en", "US");
            rb1 = ResourceBundle.getBundle("cas", locale);

            StringBuilder redirectUrl = new StringBuilder();
            redirectUrl.append(rb1.getString("login"));
            externalContext.invalidateSession();
            externalContext.redirect(redirectUrl.toString());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    public void handleExpiredSession() throws IOException {

        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Locale locale = new Locale("en", "US");
        ResourceBundle rb1 = ResourceBundle.getBundle("cas", locale);
        StringBuilder redirectUrl = new StringBuilder();
        redirectUrl.append(rb1.getString("login"));

        try {
            externalContext.redirect(redirectUrl.toString());
        } catch (IOException e) {
            logger.error(e);
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public String getImageUrl() {
        SecureRandom ranGen = new SecureRandom();
        return imageUrl + "?a=" + ranGen.nextDouble();
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMsgAlert() {
        return msgAlert;
    }

    public void setMsgAlert(String msgAlert) {
        this.msgAlert = msgAlert;
    }

    public boolean isCheckLogin() {
        return checkLogin;
    }

    public void setCheckLogin(boolean checkLogin) {
        this.checkLogin = checkLogin;
    }
}
