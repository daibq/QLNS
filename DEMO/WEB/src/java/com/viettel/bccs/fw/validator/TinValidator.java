package com.viettel.bccs.fw.validator;

import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.GetTextFromBundleHelper;
import org.apache.commons.lang3.StringUtils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * @author nhannt34
 *         Validate ma so thue
 * @since 19/01/2016
 */
@FacesValidator("validator.tin")
public class TinValidator implements Validator {
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String tin = DataUtil.safeToString(value);
        if (DataUtil.isNullOrEmpty(tin)) {
            return;
        }
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, GetTextFromBundleHelper.getText("common.validate.tin.sum"), "");
        //20160317: nhannt34 bo validate ma so thue chi co toan so
//        if (!StringUtils.isNumeric(tin)) {
//            msg.setDetail(GetTextFromBundleHelper.getText("common.validate.tin.detail.number"));
//        }

        if (StringUtils.length(tin) > 20) {
            msg.setDetail(GetTextFromBundleHelper.getText("common.validate.tin.detail.length"));
        }

        if (!DataUtil.isNullOrEmpty(msg.getDetail())) {
            throw new ValidatorException(msg);
        }
    }
}
