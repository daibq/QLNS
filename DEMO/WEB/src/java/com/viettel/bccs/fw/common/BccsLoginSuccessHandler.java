package com.viettel.bccs.fw.common;

import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.passport.CustomConnector;
import com.viettel.web.common.security.CustomPrincipal;
import com.viettel.web.common.security.URLBean;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import viettel.passport.client.UserToken;

import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by vtsoft on 4/15/2015.
 */
public class BccsLoginSuccessHandler implements AuthenticationSuccessHandler {

    public static final Logger logger = Logger.getLogger(BccsLoginSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication auth)
            throws IOException, ServletException {
        URLBean urlBean = getURLBean(request, response, auth);
        if (urlBean != null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect(urlBean.getTargetUrl());
            return;
        }
        String isFirstLogin = (String) request.getSession().getAttribute("isFirstLogin");
        if (com.viettel.common.Const.WEB_CHAIN.BL_CHANGE_PASSWORD_TRUE.equalsIgnoreCase(isFirstLogin)) {
            // forward sang trang change pass

            //RequestContext.getCurrentInstance().execute("redirectChangePassword()");
            FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/changePassword.jsf");
        } else
            FacesContext.getCurrentInstance().getExternalContext().redirect(request.getContextPath() + "/home");

    }

    private URLBean getURLBean(HttpServletRequest request,
                               HttpServletResponse response, Authentication auth) {
        URLBean urlBean = (URLBean) request.getSession().getAttribute(com.viettel.fw.common.util.Const.URL_BEAN);
        if (urlBean == null) {
            CustomPrincipal principal = (CustomPrincipal) auth.getPrincipal();
            if (principal != null) {
                urlBean = (URLBean) principal.getPrincipals().get(1);
            }
        }

        return urlBean;

    }


    public static String getUserName() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken.getUserName();
    }

    public static String getChainProvinceCode() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken.getPassportNumber();
    }

    public static final String IP_PORT_WEB = DataUtil.getEndPoint();

    public static String getFullName() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken == null ? null : userToken.getFullName();
    }

    public static Long getUserId() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken == null ? null : userToken.getUserID();
    }

    public static Long getChainId() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken == null ? null : userToken.getUserRight();
    }

    public static Long getShopId() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken == null ? null : userToken.getDeptId();
    }

    /**
     * @return
     */
    public static Long getChainUserType() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken == null ? null : userToken.getGender();
    }

    public static String getChainCode() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken == null ? null : userToken.getStaffCode();
    }

    public static String getChainName() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken == null ? null : userToken.getDescription();
    }

    public static String getShopCode() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken == null ? null : userToken.getAliasName();
    }

    public static String getShopName() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken == null ? null : userToken.getDeptName();
    }


    public static Long getParentShopId() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken == null ? null : userToken.getUserRight();
    }

    public static String getIpAddress() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        return ipAddress;
    }

    public static Long getUserCode() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken.getUserID();
    }

    public static String getChainMobible() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken.getCellphone();
    }

    public static String getChainAddress() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserToken userToken = (UserToken) request.getSession().getAttribute(CustomConnector.VSA_USER_TOKEN);
        return userToken.getBirthPlace();
    }
}
