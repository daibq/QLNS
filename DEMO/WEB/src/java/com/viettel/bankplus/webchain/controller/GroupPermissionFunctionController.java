package com.viettel.bankplus.webchain.controller;

import com.google.common.collect.Lists;
import com.viettel.bankplus.webchain.dto.*;
import com.viettel.bankplus.webchain.input.InputLazyPermissionGroupFunctionSearch;
import com.viettel.bankplus.webchain.service.*;
import com.viettel.bankplus.webchain.utils.ActionUtil;
import com.viettel.bccs.fw.common.BccsLoginSuccessHandler;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.web.common.controller.BaseController;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.util.*;

/**
 * @author hungnq on 9/5/2017
 */
@ManagedBean
@Component
@Scope("view")
public class GroupPermissionFunctionController extends BaseController {
    private static final long serialVersionUID = 1;
    // tree menu
    private transient TreeNode rootTreeMenu;
    private transient List<PermissionGroupDTO> permissionGroupDTOList;
    private List<PermissionGroupDTO> selectPermissionGroupDTOList;
    private List<String> selectList;
    private Long selectOne;
    private transient TreeNode[] selectedNode;

    //search
    private transient InputLazyPermissionGroupFunctionSearch inputSearch;
    private LazyDataModel<GroupPermissionFunctionDTO> lazyLoad;
    private boolean viewSearch;

    private String header;
    private List<FunctionObjectDTO> functionObjectDTOList;
    private boolean create;
    private List<ApDomainDTO> statusList = new ArrayList<>();
    private Map<Long, String> mapFunction = new HashMap<>();
    private ChainUserDTO chainUserDTO;
    private List<ActionDTO> actionDTOList;
    private String label;

    @Autowired
    private transient FunctionObjectService functionObjectService;

    @Autowired
    private transient PermissionGroupService permissionGroupService;

    @Autowired
    private transient PermissionManagerService permissionManagerService;

    @Autowired
    private transient ApDomainService apDomainService;

    @Autowired
    private transient ChainUserService chainUserService;

    @Autowired
    private transient ActionService actionService;

    @PostConstruct
    public void init() {
        try {
            label = getText("value.selected");
            actionDTOList = actionService.getListActionDTOByType(Const.ACTION_LOG.PERMISSION_GROUP_FUNCTION_TYPE);
            chainUserDTO = chainUserService.findOne(BccsLoginSuccessHandler.getUserCode());
            header = getText("permission.function.group.add");
            viewSearch = true;
            inputSearch = new InputLazyPermissionGroupFunctionSearch();
            inputSearch.setShopId(BccsLoginSuccessHandler.getShopId());
            inputSearch.setUserId(chainUserService.findId(BccsLoginSuccessHandler.getUserName()));
            create = true;
            selectList = new ArrayList<>();
            permissionGroupDTOList = new ArrayList<>();
            selectPermissionGroupDTOList = new ArrayList<>();
            statusList = apDomainService.getValueApDomainByTypeOrderByCode(Const.AP_DOMAIN.WC_STATUS);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private List<FunctionObjectDTO> getListFunction(Long parentId) throws Exception {
        if (chainUserDTO.getChainUserType() == 1) {
            return functionObjectService.findByParentId(parentId, Const.WEB_CHAIN.FUNCTION_ACTIVE);
        } else {
            return functionObjectService.findByParentIdFlowUserId(chainUserDTO.getChainUserId(), parentId, Const.WEB_CHAIN.FUNCTION_ACTIVE);
        }
    }

    public void updateList() throws Exception {
        updateHeader();
        label = getText("value.selected");
        selectOne = null;
        selectList = new ArrayList<>();
        selectPermissionGroupDTOList = new ArrayList<>();
        permissionGroupDTOList = permissionGroupService.findByStatus(create, BccsLoginSuccessHandler.getShopId());
        functionObjectDTOList = new ArrayList<>();
        mapFunction.clear();
        List<FunctionObjectDTO> functionObjectTree = getListFunction(null);
        rootTreeMenu = new DefaultTreeNode();
        rootTreeMenu = buildTreeNodeFromList(functionObjectTree, rootTreeMenu);
    }

    public void preToAddOrUpdate() throws Exception {
        create = true;
        permissionGroupDTOList = permissionGroupService.findByStatus(create, BccsLoginSuccessHandler.getShopId());
        selectList = new ArrayList<>();
        label = getText("value.selected");
        selectOne = null;
        create = true;
        mapFunction.clear();
        selectPermissionGroupDTOList = new ArrayList<>();
        functionObjectDTOList = new ArrayList<>();
        List<FunctionObjectDTO> functionObjectTree = getListFunction(null);
        rootTreeMenu = new DefaultTreeNode();
        rootTreeMenu = buildTreeNodeFromList(functionObjectTree, rootTreeMenu);
    }

    public void updateHeader() {
        if (create) header = getText("permission.function.group.add");
        else header = getText("permission.function.group.update");
    }

    private TreeNode buildTreeNodeFromList(List<FunctionObjectDTO> listRoot, TreeNode root) {
        for (FunctionObjectDTO baseDTO : listRoot) {
            FunctionObjectDTO functionObjectDTORoot = new FunctionObjectDTO();
            BeanUtils.copyProperties(baseDTO, functionObjectDTORoot);
            TreeNode parent = new DefaultTreeNode(functionObjectDTORoot, root);
            parent.setExpanded(true);
            parent.setSelectable(true);
            if (!create && !DataUtil.isNullOrEmpty(mapFunction.get(functionObjectDTORoot.getFunctionObjectId()))) {
                parent.setSelected(true);
            }
            try {
                // lay all
                List<FunctionObjectDTO> listChildZone = getListFunction(baseDTO.getFunctionObjectId());
                if (!DataUtil.isNullOrEmpty(listChildZone)) {
                    buildTreeNodeFromList(listChildZone, parent);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        if (!create) {
            List<TreeNode> children = DataUtil.defaultIfNull(root.getChildren(), new ArrayList<TreeNode>());
            root.setSelected(children.stream().allMatch(s -> s.isSelected()));
        }

        return root;
    }

    private void getListChildFlowGroupId(Long groupId) throws Exception {
        mapFunction.clear();
        List<FunctionObjectDTO> list = functionObjectService.getListChild(groupId, true);
        if (!DataUtil.isNullOrEmpty(list)) {
            for (FunctionObjectDTO functionObjectDTO : list) {
                mapFunction.put(functionObjectDTO.getFunctionObjectId(), functionObjectDTO.getFunctionCode());
            }
        }
        functionObjectDTOList = functionObjectService.getListChild(groupId, false);
    }

    public void setList() throws Exception {
        if (!create) {
            functionObjectDTOList = new ArrayList<>();
            getListChildFlowGroupId(selectOne);
            List<FunctionObjectDTO> functionObjectTree = getListFunction(null);
            rootTreeMenu = new DefaultTreeNode();
            rootTreeMenu = buildTreeNodeFromList(functionObjectTree, rootTreeMenu);
            if (!DataUtil.isNullObject(selectOne)) {
                selectList.clear();
                selectList.add(selectOne.toString());
            }else{
                selectList.clear();
            }
        }
        selectPermissionGroupDTOList = new ArrayList<>();
        label = !selectList.isEmpty() ? (selectList.size() + " " + getText("common.value.selected")) : getText("value.selected");
        if (!DataUtil.isNullOrEmpty(selectList)) {
            for (String id : selectList) {
                for (PermissionGroupDTO permissionGroupDTO : permissionGroupDTOList) {
                    if (id.equals(permissionGroupDTO.getPermissionGroupId().toString())) {
                        selectPermissionGroupDTOList.add(permissionGroupDTO);
                        break;
                    }
                }
            }
        }
        return;
    }

    public void onSelect() {
        functionObjectDTOList = new ArrayList<>();
        for (TreeNode node : selectedNode) {
            List<FunctionObjectDTO> listParent = new ArrayList<>();
            getListParent(node, listParent);
            if (!DataUtil.isNullOrEmpty(listParent)) {
                for (FunctionObjectDTO functionObjectDTO : listParent) {
                    if (checkList(functionObjectDTO, functionObjectDTOList)) functionObjectDTOList.add(functionObjectDTO);
                }
            }
            FunctionObjectDTO functionObjectDTO = (FunctionObjectDTO) node.getData();
            if (checkList(functionObjectDTO, functionObjectDTOList)) {
                functionObjectDTOList.add((FunctionObjectDTO) node.getData());
            }
        }

    }

    private void getListParent(TreeNode node, List<FunctionObjectDTO> list) {
        if (!DataUtil.isNullObject(node.getParent().getData())) {
            list.add((FunctionObjectDTO) node.getParent().getData());
            getListParent(node.getParent(), list);
        }
    }

    private boolean checkList(FunctionObjectDTO functionObjectDTO, List<FunctionObjectDTO> list) {
        for (FunctionObjectDTO functionObjectDTO1 : list) {
            if (functionObjectDTO1.getFunctionObjectId().equals(functionObjectDTO.getFunctionObjectId())) return false;
        }
        return true;
    }


    private boolean validateCreateOrUpdate() throws Exception {
        if (DataUtil.isNullOrEmpty(selectPermissionGroupDTOList)) {
            FacesContext.getCurrentInstance().addMessage("msgForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("permission.function.group.empty")));
            return false;
        } else {
            for (PermissionGroupDTO permissionGroupDTO : selectPermissionGroupDTOList) {
                PermissionGroupDTO pgDTO = permissionGroupService.findOne(permissionGroupDTO.getPermissionGroupId());
                if (DataUtil.isNullObject(pgDTO)) {
                    FacesContext.getCurrentInstance().addMessage("msgForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("permission.function.group.notExist")));
                    return false;
                } else if (!pgDTO.getStatus().toString().equalsIgnoreCase(Const.WEB_CHAIN.STATUS_ACTIVE)) {
                    FacesContext.getCurrentInstance().addMessage("msgForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("permission.function.group.inActive")));
                    return false;
                }
            }
        }
        if (DataUtil.isNullOrEmpty(functionObjectDTOList)) {
            FacesContext.getCurrentInstance().addMessage("msgForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("permission.function.function.empty")));
            return false;
        } else {
            for (FunctionObjectDTO functionObjectDTO : functionObjectDTOList) {
                FunctionObjectDTO foDTO = functionObjectService.findOne(functionObjectDTO.getFunctionObjectId());
                if (DataUtil.isNullObject(foDTO)) {
                    FacesContext.getCurrentInstance().addMessage("msgForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("permission.function.function.notExist")));
                    return false;
                } else if (!foDTO.getStatus().toString().equalsIgnoreCase(Const.WEB_CHAIN.STATUS_ACTIVE)) {
                    FacesContext.getCurrentInstance().addMessage("msgForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("permission.function.function.inActive")));
                    return false;
                }
            }
        }

        return true;
    }

    public void showDlg() throws Exception {
        RequestContext context = RequestContext.getCurrentInstance();
        if (validateCreateOrUpdate()) {
            context.execute("PF('test').show()");
        }
    }

    public void doCreateOrUpdate() {
        try {
            if (validateCreateOrUpdate()) {
                ActionLogDTO actionLogDTO = new ActionLogDTO();
                if (isCreate()) {
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.PERMISSION_GROUP_FUNCTION_TYPE_NEW_ADD));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_FUNCTION_TYPE_NEW_ADD));
                } else {
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.PERMISSION_GROUP_FUNCTION_TYPE_UPDATE));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_FUNCTION_TYPE_UPDATE));
                }
                actionLogDTO.setUserName(chainUserDTO.getUserName());
                permissionManagerService.saveOrUpdateGroupFunction(selectPermissionGroupDTOList, functionObjectDTOList, actionLogDTO, create);
                if (create) {
                    viewSearch = true;
                    RequestContext.getCurrentInstance().execute("PF('dlgGroupPermissionFunction').hide()");
                    FacesContext.getCurrentInstance().addMessage("msgSearch", new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("permission.function.group.add.success")));
                } else {
                    viewSearch = true;
                    RequestContext.getCurrentInstance().execute("PF('dlgGroupPermissionFunction').hide()");
                    FacesContext.getCurrentInstance().addMessage("msgSearch", new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("permission.function.group.update.success")));
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage("msgForm", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.msg.error")));
        }

    }

    public void doSearch() {
        try {
            lazyLoad = findLazyPermissionGroupFunction();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public LazyDataModel<GroupPermissionFunctionDTO> findLazyPermissionGroupFunction() {
        return new LazyDataModel<GroupPermissionFunctionDTO>() {
            @Override
            public List<GroupPermissionFunctionDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(permissionManagerService.countLazyGroupFunction(inputSearch));
                    return permissionManagerService.findLazyGroupFunction(inputSearch, page.getPageSize(), page.getPageNumber());
                } catch (Exception e) {
                    logger.error("findAllLazy", e);
                    return Lists.newArrayList();
                }
            }

            @Override
            public GroupPermissionFunctionDTO getRowData(String rowKey) {
                List<GroupPermissionFunctionDTO> tmp = (List<GroupPermissionFunctionDTO>) getWrappedData();
                Optional<GroupPermissionFunctionDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getPermissionFunctionId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(GroupPermissionFunctionDTO object) {
                return object != null ? object.getPermissionFunctionId() : null;
            }
        };
    }

    public TreeNode getRootTreeMenu() {
        return rootTreeMenu;
    }

    public void setRootTreeMenu(TreeNode rootTreeMenu) {
        this.rootTreeMenu = rootTreeMenu;
    }

    public List<PermissionGroupDTO> getPermissionGroupDTOList() {
        return permissionGroupDTOList;
    }

    public void setPermissionGroupDTOList(List<PermissionGroupDTO> permissionGroupDTOList) {
        this.permissionGroupDTOList = permissionGroupDTOList;
    }

    public List<PermissionGroupDTO> getSelectPermissionGroupDTOList() {
        return selectPermissionGroupDTOList;
    }

    public void setSelectPermissionGroupDTOList(List<PermissionGroupDTO> selectPermissionGroupDTOList) {
        this.selectPermissionGroupDTOList = selectPermissionGroupDTOList;
    }

    public TreeNode[] getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode[] selectedNode) {
        this.selectedNode = selectedNode;
    }

    public List<FunctionObjectDTO> getFunctionObjectDTOList() {
        return functionObjectDTOList;
    }

    public void setFunctionObjectDTOList(List<FunctionObjectDTO> functionObjectDTOList) {
        this.functionObjectDTOList = functionObjectDTOList;
    }

    public List<String> getSelectList() {
        return selectList;
    }

    public void setSelectList(List<String> selectList) {
        this.selectList = selectList;
    }

    public boolean isCreate() {
        return create;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public Long getSelectOne() {
        return selectOne;
    }

    public void setSelectOne(Long selectOne) {
        this.selectOne = selectOne;
    }

    public InputLazyPermissionGroupFunctionSearch getInputSearch() {
        return inputSearch;
    }

    public void setInputSearch(InputLazyPermissionGroupFunctionSearch inputSearch) {
        this.inputSearch = inputSearch;
    }

    public List<ApDomainDTO> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<ApDomainDTO> statusList) {
        this.statusList = statusList;
    }

    public LazyDataModel<GroupPermissionFunctionDTO> getLazyLoad() {
        return lazyLoad;
    }

    public void setLazyLoad(LazyDataModel<GroupPermissionFunctionDTO> lazyLoad) {
        this.lazyLoad = lazyLoad;
    }

    public boolean isViewSearch() {
        return viewSearch;
    }

    public void setViewSearch(boolean viewSearch) {
        this.viewSearch = viewSearch;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
