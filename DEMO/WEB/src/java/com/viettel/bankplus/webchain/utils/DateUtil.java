package com.viettel.bankplus.webchain.utils;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateUtil {
    public static final String FORMAT_TIME = "HH:MM:SS";
    public static final String FORMAT_DATE2 = "dd/MM/yyyy";
    public static final String FORMAT_DATE3 = "yyyy/MM/dd";
    public static final String FORMAT_DATE = "DD/MM/YYYY";
    public static final String FORMAT_DATE_TIME = "DD/MM/YYYY HH:MM:SS";
    public static final String FORMAT_DATE_TIME2 = "dd/MM/yyyy HH:mm:ss";
    public static final String FORMAT_DATE_TIME3 = "yyyyMMddHHmmss";
    public static final String FORMAT_DATE_TIME4 = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DB_TIME = "HH24:MI:SS";
    public static final String FORMAT_DB_DATE = "YYYY-MM-DD";
    public static final String FORMAT_DB_DATE_TIME = "YYYY-MM-DD HH24:MI:SS";
    public static final String FORMAT_DB_YDATE_TIME = "SYYYY-MM-DD HH24:MI:SS";
    //    public static final SimpleDateFormat FM_DATE_TIME = new SimpleDateFormat(FORMAT_DATE_TIME);
//    public static final SimpleDateFormat FM_DATE = new SimpleDateFormat(FORMAT_DATE);
    public static final Logger logger = Logger.getLogger(DateUtil.class);



    public static int convertDateAndHourToHour(int day, int hour) {
        int rs = day * 24 + hour;

        return rs;
    }

    public static String formatStringToHHmmss(int strHMS) {
        int h = strHMS / (3600);
        int m = (strHMS % (3600)) / 60;
        int s = (strHMS % (3600)) % 60;
        String strH = String.format("%02d", h);
        String strM = String.format("%02d", m);
        String strS = String.format("%02d", s);
        return strH + ":" + strM + ":" + strS;
    }

    public static Date timestampToDate(Timestamp stamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(stamp.getTime());
        return cal.getTime();
    }

    public static String dateToString(Date date, String strFormat) {
        if (date != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strFormat);
            return simpleDateFormat.format(date);
        } else
            return null;
    }

    public static Date StringToDate(String dateString, String strFormat) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strFormat);
            return simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    // //////////////////////////////////////////////////////

    /**
     * Check string format with current format locale
     *
     * @param strSource String to check
     *
     * @return boolean true if strSource represent a date, otherwise false
     */
    // //////////////////////////////////////////////////////
    public static boolean isDate(String strSource) {
        return isDate(strSource, DateFormat.getDateInstance());
    }

    // //////////////////////////////////////////////////////

    /**
     * Check string format
     *
     * @param strSource String
     * @param strFormat Format to check
     *
     * @return boolean true if strSource represent a date, otherwise false
     */
    // //////////////////////////////////////////////////////
    public static boolean isDate(String strSource, String strFormat) {
        SimpleDateFormat fmt = new SimpleDateFormat(strFormat);
        fmt.setLenient(false);
        return isDate(strSource, fmt);
    }

    // //////////////////////////////////////////////////////

    /**
     * Check string format
     *
     * @param strSource String
     * @param fmt       Format to check
     *
     * @return boolean true if strSource represent a date, otherwise false
     */
    // //////////////////////////////////////////////////////
    public static boolean isDate(String strSource, DateFormat fmt) {
        try {
            if (fmt.parse(strSource) == null)
                return false;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    // //////////////////////////////////////////////////////

    /**
     * Convert string to date using current format locale
     *
     * @param strSource String to convert
     *
     * @return Date converted, null if conversion failure
     */
    // //////////////////////////////////////////////////////
    public static Date toDate(String strSource) {
        return toDate(strSource, DateFormat.getDateInstance());
    }

    // //////////////////////////////////////////////////////

    /**
     * Convert string to date
     *
     * @param strSource String to convert
     * @param strFormat Format to convert
     *
     * @return Date converted, null if conversion failure
     */
    // //////////////////////////////////////////////////////
    public static Date toDate(String strSource, String strFormat) {
        SimpleDateFormat fmt = new SimpleDateFormat(strFormat);
        fmt.setLenient(false);
        return toDate(strSource, fmt);
    }

    // //////////////////////////////////////////////////////

    /**
     * Convert string to date
     *
     * @param strSource String to convert
     * @param fmt       Format to convert
     *
     * @return Date converted, null if conversion failure
     */
    // //////////////////////////////////////////////////////
    public static Date toDate(String strSource, DateFormat fmt) {
        try {
            return fmt.parse(strSource);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    // //////////////////////////////////////////////////////

    /**
     * Add date value by second
     *
     * @param dt     Date Date to add
     * @param iValue int value to add
     *
     * @return Date after add
     */
    // //////////////////////////////////////////////////////
    public static Date addSecond(Date dt, int iValue) {
        return add(dt, iValue, Calendar.SECOND);
    }

    // //////////////////////////////////////////////////////

    /**
     * Add date value by minute
     *
     * @param dt     Date Date to add
     * @param iValue int value to add
     *
     * @return Date after add
     */
    // //////////////////////////////////////////////////////
    public static Date addMinute(Date dt, int iValue) {
        return add(dt, iValue, Calendar.MINUTE);
    }

    // //////////////////////////////////////////////////////

    /**
     * Add date value by hour
     *
     * @param dt     Date Date to add
     * @param iValue int value to add
     *
     * @return Date after add
     */
    // //////////////////////////////////////////////////////
    public static Date addHour(Date dt, int iValue) {
        return add(dt, iValue, Calendar.HOUR);
    }

    // //////////////////////////////////////////////////////

    /**
     * Add date value by day
     *
     * @param dt     Date Date to add
     * @param iValue int value to add
     *
     * @return Date after add
     */
    // //////////////////////////////////////////////////////
    public static Date addDay(Date dt, int iValue) {
        return add(dt, iValue, Calendar.DATE);
    }

    // //////////////////////////////////////////////////////

    /**
     * Add date value by month
     *
     * @param dt     Date Date to add
     * @param iValue int value to add
     *
     * @return Date after add
     */
    // //////////////////////////////////////////////////////
    public static Date addMonth(Date dt, int iValue) {
        return add(dt, iValue, Calendar.MONTH);
    }

    // //////////////////////////////////////////////////////

    /**
     * Add date value by year
     *
     * @param dt     Date Date to add
     * @param iValue int value to add
     *
     * @return Date after add
     */
    // //////////////////////////////////////////////////////
    public static Date addYear(Date dt, int iValue) {
        return add(dt, iValue, Calendar.YEAR);
    }

    // //////////////////////////////////////////////////////

    /**
     * Add date value
     *
     * @param dt     Date Date to add
     * @param iValue int value to add
     * @param iType  type of unit
     *
     * @return Date after add
     */
    // //////////////////////////////////////////////////////
    public static Date add(Date dt, int iValue, int iType) {
        Calendar cld = Calendar.getInstance();
        cld.setTime(dt);
        cld.add(iType, iValue);
        return cld.getTime();
    }

    public static int getHoursFromDate(Date date) {
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        return cld.get(Calendar.HOUR_OF_DAY);
    }

    public static int getMinuteFromDate(Date date) {
        Calendar cld = Calendar.getInstance();
        cld.setTime(date);
        return cld.get(Calendar.MINUTE);
    }

    public static Date stringToDate(String str) {
        if (StringUtils.stringIsNullOrEmty(str))
            return null;
        DateFormat df = new SimpleDateFormat(FORMAT_DATE_TIME);
        try {
            return df.parse(str);
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public static String dateToString(Date date) {
        if (date == null)
            return null;
        DateFormat df = new SimpleDateFormat(FORMAT_DATE_TIME);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return df.format(cal.getTime());
    }

    // //////////////////////////////////////////////////////

    /**
     * Format date object
     *
     * @param dtImput    date to format
     * @param strPattern format pattern
     *
     * @return formatted string
     *
     * @author Thai Hoang Hiep
     */
    // //////////////////////////////////////////////////////
    public static String format(Date dtImput, String strPattern) {
        if (dtImput == null)
            return null;
        SimpleDateFormat fmt = new SimpleDateFormat(strPattern);
        return fmt.format(dtImput);
    }

    public static int compareWithoutTime(Date d1, Date d2) {
        return DateUtils.truncatedCompareTo(d1, d2, Calendar.DAY_OF_MONTH);
    }

    public static Integer compare(Date d1, Date d2, String pattern) {
        if (d1 == null || d2 == null) {
            return null;
        }
        String str1;
        String str2;
        if (pattern.startsWith("yyyy-Q")) {
            Calendar c1 = Calendar.getInstance();
            c1.setTime(d1);
            int y1 = c1.get(1);
            int m1 = c1.get(2);
            int q1 = (m1 / 3) + 1;
            str1 = y1 + "-" + q1;
            Calendar c2 = Calendar.getInstance();
            c2.setTime(d2);
            int y2 = c2.get(1);
            int m2 = c2.get(2);
            int q2 = (m2 / 3) + 1;
            str2 = y2 + "-" + q2;
        } else {
            str1 = dateToString(d1, pattern);
            str2 = dateToString(d2, pattern);
        }
        return str1.compareTo(str2);
    }

    public static Date getFirstOfCurrentMonth() {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.DAY_OF_MONTH, 1);
        return date.getTime();
    }

    public static Integer dateDiffDays(Date d1, Date d2) {
        if (d1 == null || d2 == null) {
            return null;
        }
        long diff = d1.getTime() - d2.getTime();
//		long diffSeconds = diff / 1000 % 60;
//		long diffMinutes = diff / (60 * 1000) % 60;
//		long diffHours = diff / (60 * 60 * 1000) % 24;
        return (int) (diff / (24 * 60 * 60 * 1000));
    }
}
