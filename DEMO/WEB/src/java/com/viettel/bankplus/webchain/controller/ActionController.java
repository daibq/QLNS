package com.viettel.bankplus.webchain.controller;

import com.google.common.collect.Lists;
import com.viettel.bankplus.webchain.dto.ActionDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ApDomainDTO;
import com.viettel.bankplus.webchain.input.InputLazyActionSearch;
import com.viettel.bankplus.webchain.service.ActionService;
import com.viettel.bankplus.webchain.service.ApDomainService;
import com.viettel.bankplus.webchain.utils.ActionUtil;
import com.viettel.bccs.fw.common.BccsLoginSuccessHandler;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.web.common.controller.BaseController;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;


/**
 * Created by Dong on 8/14/2017.
 */
@Component
@ManagedBean
@Scope("view")
public class ActionController extends BaseController {
    private static final long serialVersionUID = 1;
    @Autowired
    private transient ActionService actionService;

    @Autowired
    private transient ApDomainService apDomainService;

    private transient InputLazyActionSearch inputLazyActionSearch;
    private LazyDataModel<ActionDTO> actionDTOS;
    private List<ApDomainDTO> listStatus = new ArrayList<>();
    private List<ApDomainDTO> listActionType = new ArrayList<>();
    private List<ActionDTO> actionDTOList;
    private ActionDTO actionDTO;
    private boolean isCreateState;
    private final String ERROR = "ERROR";
    private final String SUCCESS = "SUCCESS";
    private final String WARNING = "WARNING";
    private ActionDTO actionOld;

    @PostConstruct
    public void init() {
        inputLazyActionSearch = new InputLazyActionSearch();
        actionDTO = new ActionDTO();
        /*actionDTOS=findLazy();*/
        try {
            listStatus = apDomainService.getValueApDomainByType(Const.AP_DOMAIN.WC_STATUS);
            listActionType = apDomainService.getValueApDomainByTypeOrderByCode(Const.AP_DOMAIN.WC_ACTION_TYPE);
            actionDTOList = actionService.getListActionDTOByType(Const.ACTION_LOG.ACTION_TYPE);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    public void prepareToShowAdd() {
        actionDTO = new ActionDTO();
    }

    public void prepareToShowEdit(ActionDTO currentActionDTO) {
        actionDTO = new ActionDTO();
        actionOld = new ActionDTO();
        BeanUtils.copyProperties(currentActionDTO, actionDTO);
        BeanUtils.copyProperties(currentActionDTO, actionOld);
    }

    /**
     * Display report type Error, Success , Warning.
     */
    public void showMessage(String displayArea, String type, String key) {
        if (type.equals(SUCCESS))
            FacesContext.getCurrentInstance().addMessage(displayArea, new FacesMessage(FacesMessage.SEVERITY_INFO, getText(key), null));
        if (type.equals(ERROR))
            FacesContext.getCurrentInstance().addMessage(displayArea, new FacesMessage(FacesMessage.SEVERITY_ERROR, getText(key), null));
        if (type.equals(WARNING))
            FacesContext.getCurrentInstance().addMessage(displayArea, new FacesMessage(FacesMessage.SEVERITY_WARN, getText(key), null));

    }

    /**
     * Validate form msgFormCU
     */
    public boolean validateFrom() {
        if (DataUtil.isNullObject(actionDTO.getCode())) {
            showMessage("msgFormCU", ERROR, "webchain.action.messValidate.code");
            return false;
        }
        Pattern docCode = Pattern.compile("[\\d\\w/_\\.]+");
        if (!docCode.matcher(actionDTO.getCode()).matches()) {
            showMessage("msgFormCU", ERROR, "webchain.action.messValidate.NotImportWord.code");
            return false;
        }
        if (actionDTO.getCode().length() > 50) {
            showMessage("msgFormCU", ERROR, "webchain.action.messValidate.lengt.code");
            return false;
        }
        if (actionDTO.getDescription().length() > 400) {
            showMessage("msgFormCU", ERROR, "webchain.action.messValidate.lengt.description");
            return false;
        }
        if (DataUtil.isNullObject(actionDTO.getType())) {
            showMessage("msgFormCU", ERROR, "webchain.action.messValidate.type");
            return false;
        }
        if (actionDTO.getType().length() > 30) {
            showMessage("msgFormCU", ERROR, "webchain.action.messValidate.lengt.type");
            return false;
        }
        if (DataUtil.isNullObject(actionDTO.getName())) {
            showMessage("msgFormCU", ERROR, "webchain.action.messValidate.name");
            return false;
        }
        if (actionDTO.getName().length() > 200) {
            showMessage("msgFormCU", ERROR, "webchain.action.messValidate.lengt.name");
            return false;
        }
        if (DataUtil.isNullObject(actionDTO.getStatus())) {
            showMessage("msgFormCU", ERROR, "webchain.action.messValidate.status");
            return false;
        }

        return true;
    }

    /**
     * Validate for add new action
     */
    public boolean validateBeforeSave() {
        try {
            boolean nameNotExist;
            boolean isNotCodeDuplicate = actionService.isCodeOrIdExist(actionDTO.getCode(), null);
            nameNotExist = actionService.isDuplicateName(actionDTO.getName());
            if (!isNotCodeDuplicate) {
                showMessage("msgFormCU", ERROR, "webchain.action.report.duplicate.code");
                return false;
            }
            if (!nameNotExist) {
                showMessage("msgFormCU", ERROR, "webchain.action.report.duplicate.name");
                return false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return true;
    }

    /**
     * Kiểm tra bản ghi tồn tại hay không
     */
    public boolean checkRecordExist(ActionDTO dto) {
        boolean check = true;
        try {
            boolean isRecordExist = actionService.isCodeOrIdExist(null, dto.getActionId());
            if (isRecordExist) {
                showMessage("msg", ERROR, "webchain.action.report.exist");
                check = false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return check;
    }

    /**
     * Validate for Edit
     */
    public boolean validateBeforeUpdate(ActionDTO dto) {
        try {
            boolean nameNotExist;
            boolean isNotCodeDupicate = actionService.isCodeOrIdExist(actionDTO.getCode(), null);
            nameNotExist = actionService.isDuplicateName(actionDTO.getName());
            if (!dto.getCode().equals(actionOld.getCode()) && !isNotCodeDupicate) {
                showMessage("msgFormCU", ERROR, "webchain.action.report.duplicate.code");
                return false;
            }

            if (dto.getName().equals(actionOld.getName()) && dto.getType().equals(actionOld.getType())) {
                return true;
            } else {
                if (!nameNotExist) {
                    showMessage("msgFormCU", ERROR, "webchain.action.report.duplicate.name");
                    return false;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return true;
    }


    /**
     * Add new and update record
     */
    public void saveorupdate() {
        try {
            if (!validateFrom()) {
                return;
            }
            Short statusValue;
            statusValue = actionDTO.getStatus() == null ? 1 : actionDTO.getStatus();
            isCreateState = actionDTO.getActionId() == null ? true : false;

            ActionLogDTO actionLogDTO = new ActionLogDTO();
            actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
            actionDTO.setStatus(statusValue);

            if (isCreateState) { // thêm mới
                if (validateBeforeSave()) {
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.ACTION_TYPE_ADD_NEW));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.ACTION_TYPE_ADD_NEW));
                    actionService.saveOrUpdate(actionDTO, actionLogDTO);
                    showMessage("msg", SUCCESS, "webchain.action.report.add");
                    RequestContext.getCurrentInstance().execute("PF('dlgActions').hide()");
                } else return;

            } else { //sửa
                /*if (checkRecordExist(actionDTO)){*/
                if (validateBeforeUpdate(actionDTO)) {
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.ACTION_TYPE_UPDATE));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.ACTION_TYPE_UPDATE));
                    actionService.saveOrUpdate(actionDTO, actionLogDTO);
                    showMessage("msg", SUCCESS, "webchain.action.report.edit");
                    RequestContext.getCurrentInstance().execute("PF('dlgActions').hide()");
                }
                /*}*/
            }
        } catch (Exception e) {
            showMessage("msg", ERROR, "webchain.action.report.err");
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Delete record
     */
    public void doDelete(ActionDTO dto) {
        try {
            if (checkRecordExist(dto)) {
                ActionLogDTO actionLogDTO = new ActionLogDTO();
                actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.ACTION_TYPE_DELETE));
                actionLogDTO.setDescription(getText(Const.ACTION_LOG.ACTION_TYPE_DELETE));
                actionService.delList(dto, actionLogDTO);
                showMessage("msg", SUCCESS, "webchain.action.report.delete");
                doSearch();
            }
        } catch (Exception e) {
            showMessage("msg", ERROR, "webchain.action.report.err");
            logger.error(e.getMessage(), e);
        }
    }

    @Secured("@")
    public void doSearch() {
        try {
            actionDTOS = findLazy();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Loading datatable
     */
    private LazyDataModel<ActionDTO> findLazy() {
        return new LazyDataModel<ActionDTO>() {
            @Override
            public List<ActionDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    if (sortField != null && sortOrder != null) {
                        if (SortOrder.ASCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.ASC, sortField);
                        } else if (SortOrder.DESCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.DESC, sortField);
                        }
                    }
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(actionService.countActionList(inputLazyActionSearch));
                    return actionService.findLazingPaging(inputLazyActionSearch, page.getPageSize(), page.getPageNumber());
                } catch (Exception e) {
                    logger.error("findAllLazy", e);
                    return Lists.newArrayList();
                }
            }

            @Override
            public ActionDTO getRowData(String rowKey) {
                List<ActionDTO> tmp = (List<ActionDTO>) getWrappedData();
                Optional<ActionDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getActionId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(ActionDTO object) {
                return object != null ? object.getActionId() : null;
            }
        };
    }



    /* =============================SETTER GETTER======================================*/

    public LazyDataModel<ActionDTO> getActionDTOS() {
        return actionDTOS;
    }

    public void setActionDTOS(LazyDataModel<ActionDTO> actionDTOS) {
        this.actionDTOS = actionDTOS;
    }

    public InputLazyActionSearch getInputLazyActionSearch() {
        return inputLazyActionSearch;
    }

    public void setInputLazyActionSearch(InputLazyActionSearch inputLazyActionSearch) {
        this.inputLazyActionSearch = inputLazyActionSearch;
    }

    public ActionDTO getActionDTO() {
        return actionDTO;
    }

    public void setActionDTO(ActionDTO actionDTO) {
        this.actionDTO = actionDTO;
    }

    public List<ApDomainDTO> getListStatus() {
        return listStatus;
    }

    public void setListStatus(List<ApDomainDTO> listStatus) {
        this.listStatus = listStatus;
    }

    public List<ActionDTO> getActionDTOList() {
        return actionDTOList;
    }

    public void setActionDTOList(List<ActionDTO> actionDTOList) {
        this.actionDTOList = actionDTOList;
    }

    public List<ApDomainDTO> getListActionType() {
        return listActionType;
    }

    public void setListActionType(List<ApDomainDTO> listActionType) {
        this.listActionType = listActionType;
    }
}
