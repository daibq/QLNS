package com.viettel.bankplus.webchain.common;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * TungHuynh Created by sondt18 on 9/12/2017.
 */

@ManagedBean(name = "bankplusMessageController")
@ViewScoped
public class BankplusMessageController {
    public static final Logger logger = Logger.getLogger(BankplusMessageController.class);
    private String severity;
    private String summary;
    private String content;
    private String focusField;

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFocusField() {
        return focusField;
    }

    public void setFocusField(String focusField) {
        this.focusField = focusField;
    }

    public void close() {
        RequestContext.getCurrentInstance().closeDialog(null);
    }


}