package com.viettel.bankplus.webchain.controller;

import com.viettel.bankplus.webchain.dto.ActionDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.service.ActionService;
import com.viettel.bankplus.webchain.service.ChainUserService;
import com.viettel.bankplus.webchain.utils.ActionUtil;
import com.viettel.bccs.fw.common.BccsLoginSuccessHandler;
import com.viettel.common.Const;
import com.viettel.fw.Exception.LogicException;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.common.util.ErrorCode;
import com.viettel.fw.dto.BaseMessage;
import com.viettel.web.common.controller.BaseController;
import nl.captcha.Captcha;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Truongxp on 08/30/17.
 */
@Component
@Scope("view")
@ManagedBean
public class ChainUserController extends BaseController {
    private static final long serialVersionUID = 1;
    private String userName;
    private String oldPassword;
    private String newPassword;
    private String typeAgainPassword;
    private String captcha = "";
    private String imageUrl;
    private String newCapcha;

    @Autowired
    transient private ChainUserService chainUserService;
    @Autowired
    transient private ActionService actionService;
    transient private List<ActionDTO> actionDTOList;

    public void messCheckChangePass() throws Exception {
        Date datePass = chainUserService.getLastDateChangePass(getUserToken().getUserName());
        Date dateCurrent = chainUserService.getSysDate();
        if (!DataUtil.isNullObject(datePass) && (dateCurrent.getTime() - datePass.getTime() > java.util.concurrent.TimeUnit.DAYS.toMillis(82))) {
            Date tomorrow = new Date(datePass.getTime() + java.util.concurrent.TimeUnit.DAYS.toMillis(90));
            if (tomorrow.getTime() - dateCurrent.getTime() <= java.util.concurrent.TimeUnit.DAYS.toMillis(7)) {
                long showDate = tomorrow.getTime() - dateCurrent.getTime();
                String cvtDate = DataUtil.safeToString(showDate / (1000 * 60 * 60 * 24));
                if (DataUtil.safeToLong(cvtDate) < 1) {
                    //redirectPageChangePass();
                    ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
                    context.redirect(context.getRequestContextPath() + "/changePassword.jsf");
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, null, getTextParam("management.debt.desc.alert", cvtDate)));

            }
        }
    }

    @PostConstruct
    public void init() {
        userName = BccsLoginSuccessHandler.getUserName();
        String tempO = oldPassword;
        oldPassword = tempO;
        imageUrl = "captcha.jpg";
        try {
            actionDTOList = actionService.getListActionDTOByType(Const.ACTION_LOG.CHAIN_USER_TYPE);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void showMessage() {
        String isFirstLogin = (String) getRequest().getSession().getAttribute("isFirstLogin");
        if (com.viettel.common.Const.WEB_CHAIN.BL_CHANGE_PASSWORD_TRUE.equalsIgnoreCase(isFirstLogin)) {
            FacesContext.getCurrentInstance().addMessage("frmChangePw:msgForm", new FacesMessage(FacesMessage.SEVERITY_WARN, null, getText("pw.change.first.login.change.pass")));
        }
//        else if(checkChangePass) {
//            checkChangePass = false;
//        }
    }

   /* public boolean checkUser() {
        if (!DataUtil.isNullOrEmpty(userName)) {
            return false;
        }
        return true;
    }*/

    public boolean validateLogin() {
    /*    if (DataUtil.isNullOrEmpty(userName)) {
            userName = "Bạn chưa nhập tên người dùng";
             isOk = false;
        }*/
        if (DataUtil.isNullOrEmpty(oldPassword)) {
            /*  msgError=getText("Mật khẩu cũ không được để trống");*/
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.old.not.blank")));
            this.captcha = null;
            return false;
        }
        if (DataUtil.isNullOrEmpty(newPassword)) {
            /*      msgError=getText("Mật khẩu mới không được để trống");*/
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.new.not.blank")));
            this.captcha = null;
            return false;
        }
        if (DataUtil.isNullOrEmpty(typeAgainPassword)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.repeat.not.blank")));
            this.captcha = null;
            return false;
        }
        if (DataUtil.isNullOrEmpty(typeAgainPassword)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.repeat.not.blank")));
            this.captcha = null;
            return false;
        }

        Pattern checkChar = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})");
        if (!DataUtil.isNullOrEmpty(newPassword) && !checkChar.matcher(newPassword).matches()) {
            FacesContext.getCurrentInstance().addMessage("frmChangePw:pwNew", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.char.special.and.MaxLengh")));
            this.captcha = null;
            return false;
        }
        if (!DataUtil.isNullOrEmpty(typeAgainPassword) && !checkChar.matcher(typeAgainPassword).matches()) {
            FacesContext.getCurrentInstance().addMessage("frmChangePw:pwRepeat", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.char.special.and.MaxLengh")));
            this.captcha = null;
            return false;
        }

        if (DataUtil.isNullOrEmpty(captcha)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.code.confirm.not.blank")));
//            checkConfirm = false;
            return false;
        }
        String captchaStr = DataUtil.safeToString(getSession().getAttribute("captcha"));
        if (!captcha.equals(captchaStr)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.not.right")));
            this.captcha = null;
            return false;
        }
        return true;
    }

    public void submit() {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correct", "Correct");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void changePassword() {

        if (!validateLogin()) {
            return;
        }
        ActionLogDTO actionLogDTO = new ActionLogDTO();
        actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.CHAIN_USER_TYPE_UPDATE));
        actionLogDTO.setDescription(getText(Const.ACTION_LOG.ACTION_TYPE_UPDATE));
        userName = (userName == null ? "" : userName.trim());
        try {
            boolean cPass, dupliPass;
            String takeSalt = chainUserService.takeSaltWithUser(userName);
            takeSalt = takeSalt == null ? "" : takeSalt.trim();
            cPass = chainUserService.checkPassword(oldPassword, takeSalt);
            if (!cPass) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.old.not.correct")));
                this.captcha = null;
            } else {
                if (!DataUtil.isNullOrEmpty(newPassword) && !DataUtil.isNullOrEmpty(typeAgainPassword) && newPassword.equals(typeAgainPassword)) {
                    String captchaStr = DataUtil.safeToString(getSession().getAttribute("captcha"));
                    if (captcha.equals(captchaStr)) {
                        dupliPass = chainUserService.checkDuplicatePassword(newPassword, takeSalt, userName);
                        if (dupliPass) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.old.duplicate")));
                            this.captcha = null;
                            return;
                        }
                        // Get id to userName
                        long id = chainUserService.findId(userName);
                        BaseMessage baseMessage = chainUserService.changePassword(newPassword, userName, takeSalt, id, actionLogDTO);
                        if (baseMessage.isSuccess()) {
                            getRequest().getSession().setAttribute("isFirstLogin", "0");
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("pw.change.successful")));
                            this.captcha = null;
//                            checkChangePass = true;
                        }
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.change.not.math")));
                    this.captcha = null;
                }
            }
        } catch (Exception ex) {
            reportError("", "common.error.happened.againAction", ex);
            logger.error(ex.getMessage(), ex);
        }
    }

    /**
     * reset lai khi F5
     *
     */
    /*public void executeCommand(String command) {
        StringBuilder bCommand = new StringBuilder(command);
        if (!DataUtil.isNullOrEmpty(command)) {
            if (!command.endsWith(")") && !command.endsWith(");")) {
                bCommand.append("();");
            }
            RequestContext.getCurrentInstance().execute(bCommand.toString());
        }
    }*/

    /*public void validateCapCha() throws Exception {
        if (DataUtil.isNullOrEmpty(this.newCapcha)) {
            executeCommand("resetCapcha()");
            throw new LogicException(ErrorCode.ERROR_USER.ERROR_USER_INVALID_FORMAT, "validate.capcha.require");
        }
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        Captcha secretcaptcha = (Captcha) session.getAttribute(Captcha.NAME);
        if (secretcaptcha != null && !secretcaptcha.isCorrect(this.newCapcha)) {
            executeCommand("resetCapcha()");
            throw new LogicException(ErrorCode.ERROR_USER.ERROR_USER_INVALID_FORMAT, "validate.capcha.validatorMessage");
        }
        this.newCapcha = "";
        executeCommand("resetCapcha()");
    }*/
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getTypeAgainPassword() {
        return typeAgainPassword;
    }

    public void setTypeAgainPassword(String typeAgainPassword) {
        this.typeAgainPassword = typeAgainPassword;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public String getImageUrl() {
        return imageUrl + "?b=" + new SecureRandom().nextInt();
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getNewCapcha() {
        return newCapcha;
    }

    public void setNewCapcha(String newCapcha) {
        this.newCapcha = newCapcha;
    }
}
