package com.viettel.bankplus.webchain.common;

import com.fss.util.StringUtil;
import org.apache.log4j.Logger;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.component.calendar.Calendar;
import org.primefaces.context.RequestContext;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Iterator;
import java.util.Map;

/**
 * TungHuynh
 * Created by sondt on 9/9/2017.
 */
public class ComponentUtils {
    public static final Logger logger = Logger.getLogger(ComponentUtils.class);
    public static void focusComponentOnReg(String strFrmStrId) {
        if (strFrmStrId == null) {
            return;
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("$(function(){PrimeFaces.focus('" + strFrmStrId + "');});");
    }

    public static void showDialog(String dialogVar) {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("try{PF('" + dialogVar + "').show();}catch(e){console.log(e);}");
    }

    public static void hideDialog(String dialogVar) {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("try{PF('" + dialogVar + "').hide();}catch(e){console.log(e);}");
    }

    public static void updateUI(String id) {
        RequestContext context = RequestContext.getCurrentInstance();
        //vinhndq modified : kiem tra clientId co trong list cho update chua. Neu co roi thi khong thuc hien update nua
        String validClientId = StringUtil.nvl(getClientId(id), id);
        if(!alreadyUpdate(validClientId))
        {
            context.update(validClientId);
        }
    }

    public static boolean alreadyUpdate(String clientId)
    {
        try
        {
            return FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().contains(clientId);
        }
        catch(Exception e)
        {
            logger.error(e.getMessage(),e);
            return false;
        }
    }

    public static Object getBeanByName(String beanName) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getApplication().createValueBinding("#{" + beanName + "}").getValue(facesContext);
    }

    public static void removeBeanByName(String beanName){
        FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove(beanName);
    }

    /**
     * @param componentId
     * @return
     * get clientId
     */
    public static String getClientId(String componentId) {
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot root = context.getViewRoot();

        UIComponent c = findComponent(root, componentId);
        if (c != null)
            return c.getClientId(context);
        else
            return componentId;
    }

    /**
     * Finds component with the given id
     */
    private static UIComponent findComponent(UIComponent c, String id) {
        if (c.getClientId().endsWith(id)) {
            return c;
        }
        Iterator<UIComponent> kids = c.getFacetsAndChildren();
        while (kids.hasNext()) {
            UIComponent found = findComponent(kids.next(), id);
            if (found != null) {
                return found;
            }
        }
        return null;
    }

    /**
     * get uiComponent
     *
     * @param componentId
     * @return
     */
    public static UIComponent findUiComponent(String componentId) {
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot root = context.getViewRoot();

        UIComponent uiComponent = findComponent(root, componentId);
        return uiComponent;
    }

    public static boolean checkActionRequest(String componentId){
        Map<String, String> requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        for(String key : requestMap.keySet()){
            if(key.equalsIgnoreCase(getClientId(componentId))) return true;
        }
        return false;
    }

    public static void excuteJS(String jsContent){
        RequestContext.getCurrentInstance().execute("try{"
                + jsContent
                + " }catch(e){"
                + "  console.log('excuteJS:' + e);"
                + " }");
    }

}
