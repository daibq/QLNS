package com.viettel.bankplus.webchain.controller;

import com.google.common.collect.Lists;
import com.viettel.bankplus.webchain.dto.ActionDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ApDomainDTO;
import com.viettel.bankplus.webchain.dto.FunctionObjectDTO;
import com.viettel.bankplus.webchain.input.InputLazyFunctionSearch;
import com.viettel.bankplus.webchain.service.ActionService;
import com.viettel.bankplus.webchain.service.ApDomainService;
import com.viettel.bankplus.webchain.service.FunctionObjectService;
import com.viettel.bankplus.webchain.utils.ActionUtil;
import com.viettel.bccs.fw.common.BccsLoginSuccessHandler;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.web.common.controller.BaseController;
import org.apache.poi.ss.usermodel.*;
import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.io.*;
import java.util.*;

@ManagedBean
@Component
@Scope("view")
public class FunctionController extends BaseController {
    private static final long serialVersionUID = 1;
    @Autowired
    private transient FunctionObjectService functionObjectService;
    @Autowired
    private transient ApDomainService apDomainService;
    @Autowired
    private transient ActionService actionService;

    private transient InputLazyFunctionSearch inputLazyFunctionSearch;

    private LazyDataModel<FunctionObjectDTO> functionObjectDTOList;
    private List<FunctionObjectDTO> selectedFunctionObjectDTOList;
    private FunctionObjectDTO functionObjectDTOSelected;
    private FunctionObjectDTO functionObjectDTO;
    private List<ApDomainDTO> statusList = new ArrayList<>();
    private List<ApDomainDTO> permissionTypeList = new ArrayList<>();
    private List<ApDomainDTO> functionTypeList = new ArrayList<>();
    private String dlgHeader;
    private String dlgConfirmHeader;
    private String dlgConfirmContent;
    // tree menu
    private List<FunctionObjectDTO> functionObjectDTOForTreeList;
    private transient TreeNode rootTreeMenu;
    private transient TreeNode selectedNode;
    private String searchFunctionObject;
    private Boolean adminOnly;
    private List<ActionDTO> actionDTOList;
    private Map<String, String> functionStatusMap;
    private Map<String, String> permissionTypeMap;
    private Map<String, String> functionTypeMap;
    // export excel
    private File fileExport;
    private List<FunctionObjectDTO> functionObjectDTOExportList;
    private String suffix;
    private String templatePath;

    @PostConstruct
    public void init() {
        try {
            inputLazyFunctionSearch = new InputLazyFunctionSearch();
            statusList = apDomainService.getValueApDomainByTypeOrderByCode(Const.AP_DOMAIN.WC_STATUS);
            permissionTypeList = apDomainService.getValueApDomainByTypeOrderByCode(Const.AP_DOMAIN.WC_FUNCTION_PERMISSION_TYPE);
            functionTypeList = apDomainService.getValueApDomainByTypeOrderByCode(Const.AP_DOMAIN.WC_FUNCTION_TYPE);
            actionDTOList = actionService.getListActionDTOByType(Const.ACTION_LOG.FUNCTION_OBJECT_TYPE);
            functionObjectDTO = new FunctionObjectDTO();
            templatePath = Const.WEB_CHAIN.FUNCTION_EXCEL_TEMPLATE_PATH;
            functionStatusMap = new HashMap<>();
            permissionTypeMap = new HashMap<>();
            functionTypeMap = new HashMap<>();
            if (!statusList.isEmpty()) {
                for (ApDomainDTO apDomainDTO : statusList) {
                    functionStatusMap.put(apDomainDTO.getValue().trim().toLowerCase(), apDomainDTO.getName());
                }
            }
            if (!permissionTypeList.isEmpty()) {
                for (ApDomainDTO apDomainDTO : permissionTypeList) {
                    permissionTypeMap.put(apDomainDTO.getValue().trim().toLowerCase(), apDomainDTO.getName());
                }
            }
            if (!functionTypeList.isEmpty()) {
                for (ApDomainDTO apDomainDTO : functionTypeList) {
                    functionTypeMap.put(apDomainDTO.getValue().trim().toLowerCase(), apDomainDTO.getName());
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    /**
     *
     */
    @Secured("@")
    public void doSearch() {
        try {
            functionObjectDTOList = findLazyFunction();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * chuan bi du lieu cho form them moi
     */

    public void preAddNewFunction() {
        try {
            dlgHeader = getText("dlg.function.add.new.header");
            functionObjectDTO = new FunctionObjectDTO();
            functionObjectDTOSelected = null;
            searchFunctionObject = "";
            // lay danh sach cap 1
            List<FunctionObjectDTO> functionObjectDTOList = functionObjectService.findByParentId(null, Const.WEB_CHAIN.FUNCTION_ACTIVE);
            rootTreeMenu = new DefaultTreeNode();
            if (DataUtil.isNullOrEmpty(functionObjectDTOList)) {
                functionObjectDTOList = new ArrayList<>();
            }
            rootTreeMenu = buildTreeNodeFromList(functionObjectDTOList, rootTreeMenu);
        } catch (Exception e) {
            FacesContext.getCurrentInstance().validationFailed();
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    public void delNodeParentOnDlg() {
        functionObjectDTOSelected = null;
        searchFunctionObject = "";
    }

    /**
     * xay dung cay chuc nang. chi lay nhung chuc nang dang active, chi lay chuc nang cap 1 va cap 2
     *
     * @param listRoot
     * @param root
     *
     * @return
     */
    private TreeNode buildTreeNodeFromList(List<FunctionObjectDTO> listRoot, TreeNode root) {
        for (FunctionObjectDTO baseDTO : listRoot) {
            FunctionObjectDTO functionObjectDTORoot = new FunctionObjectDTO();
            BeanUtils.copyProperties(baseDTO, functionObjectDTORoot);
            TreeNode parent = new DefaultTreeNode(functionObjectDTORoot, root);
            parent.setExpanded(true);
            parent.setSelectable(true);
            if (!DataUtil.isNullObject(functionObjectDTOSelected) && functionObjectDTOSelected.getFunctionObjectId().equals(baseDTO.getFunctionObjectId())) {
                if (!DataUtil.isNullObject(root.getParent())) {
                    root.getParent().setExpanded(true);
                    root.getParent().setSelected(true);
                }
                root.setSelectable(true);
                root.setExpanded(true);
            }
            try {
                // lay all
                if (baseDTO.getParentId() == null) {
                    List<FunctionObjectDTO> listChildZone = functionObjectService.findByParentId(baseDTO.getFunctionObjectId(), Const.WEB_CHAIN.FUNCTION_ACTIVE);
                    if (!DataUtil.isNullOrEmpty(listChildZone)) {
                        buildTreeNodeFromList(listChildZone, parent);
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return root;
    }

    /**
     * chuan bi du lieu cho cap nhat chuc nang
     *
     * @param functionObjectDTOEdit
     */
    public void preEditFunction(FunctionObjectDTO functionObjectDTOEdit) {
        try {
            functionObjectDTO = new FunctionObjectDTO();
            BeanUtils.copyProperties(functionObjectDTOEdit, functionObjectDTO);
            functionObjectDTO.setOldCode(functionObjectDTO.getFunctionCode());
            functionObjectDTO.setOldName(functionObjectDTO.getFunctionName());
            dlgHeader = getText("dlg.function.update.header");
            searchFunctionObject = "";
            if (!DataUtil.isNullObject(functionObjectDTO.getParentId())) {
                functionObjectDTOSelected = functionObjectService.findOne(functionObjectDTO.getParentId());
                if (!DataUtil.isNullObject(functionObjectDTOSelected)) {
                    searchFunctionObject = functionObjectDTOSelected.getFunctionName();
                }
            }
            List<FunctionObjectDTO> functionObjectDTOList = functionObjectService.findByParentId(null, Const.WEB_CHAIN.FUNCTION_ACTIVE);
            rootTreeMenu = new DefaultTreeNode();
            rootTreeMenu = buildTreeNodeFromList(functionObjectDTOList, rootTreeMenu);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
            FacesContext.getCurrentInstance().validationFailed();

        }
    }


    /**
     * @param functionObjectDTO
     */
    public void deleteFunction(FunctionObjectDTO functionObjectDTO) {
        try {
            ActionLogDTO actionLogDTO = new ActionLogDTO();
            actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
            actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.FUNCTION_OBJECT_TYPE_DELETE));
            actionLogDTO.setDescription(getText(Const.ACTION_LOG.FUNCTION_OBJECT_TYPE_DELETE));
            functionObjectService.deleteFunction(functionObjectDTO.getFunctionObjectId(), actionLogDTO);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.action.report.delete")));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    /**
     *
     */
    public void doCreateOrUpdate() {
        try {
            if (validateCreateOrUpdate()) {
                ActionLogDTO actionLogDTO = new ActionLogDTO();
                if (functionObjectDTO.getFunctionObjectId() == null) {
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.FUNCTION_OBJECT_TYPE_ADD_NEW));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.FUNCTION_OBJECT_TYPE_ADD_NEW));
                } else {
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.FUNCTION_OBJECT_TYPE_UPDATE));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.FUNCTION_OBJECT_TYPE_UPDATE));
                }
                actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                functionObjectService.saveOrUpdate(functionObjectDTO, actionLogDTO);
                if (functionObjectDTO.getFunctionObjectId() == null) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("function.create.success")));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("function.update.success")));
                }
                functionObjectDTO = new FunctionObjectDTO();
                RequestContext.getCurrentInstance().update("function:msgSearch");
                RequestContext.getCurrentInstance().execute("PF('dlgFunctionForm').hide()");
                doSearch();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    /**
     * validate du lieu them moi Ten khong trung khi cung thuoc 1 chuc nang cha Ten khong qua 300 ky tu Code khong dc chua ky tu dac biet Code khong dc trung Code khong dc qua 100 ky tu
     *
     * @return
     */
    private boolean validateCreateOrUpdate() throws Exception {
        // code_else(truongnx)
        if (DataUtil.isNullObject(functionObjectDTO.getFunctionCode())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.code.empty")));
            return false;
        } else {
            if ((DataUtil.isNullOrEmpty(functionObjectDTO.getOldCode()) || !DataUtil.safeEqual(functionObjectDTO.getFunctionCode(), functionObjectDTO.getOldCode())) && (functionObjectService.checkExistCode
                    (functionObjectDTO.getFunctionCode()))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.code.exist")));
                return false;
            }
        }
        //name
        if (DataUtil.isNullObject(functionObjectDTO.getFunctionName())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.name.empty")));
            return false;
        }
        // chuc nang cha
        if (!DataUtil.isNullObject(functionObjectDTOSelected)) {
            FunctionObjectDTO temp = functionObjectService.findOne(functionObjectDTOSelected.getFunctionObjectId());
            if (temp == null || temp.getStatus().shortValue() == Const.WEB_CHAIN.FUNCTION_IN_ACTIVE) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.parent.in.active")));
                return false;
            } else {
                functionObjectDTO.setParentId(temp.getFunctionObjectId());
            }

        } else {
            functionObjectDTO.setParentId(null);
        }
        //name_truongnx_ check trung trong db
        if (!DataUtil.isNullObject(functionObjectDTO.getFunctionName()) && ((DataUtil.isNullOrEmpty(functionObjectDTO.getOldName()) || !DataUtil.safeEqual(functionObjectDTO.getFunctionName(),
                functionObjectDTO.getOldName()))) && functionObjectService.checkExistName(functionObjectDTO.getFunctionName(), functionObjectDTO.getParentId())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.name.exist")));
            return false;
        }
        // loai chuc nang
        if (DataUtil.isNullObject(functionObjectDTO.getFunctionType())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.type.empty")));
            return false;
        }
        //loai quyen_truongnx
        if (DataUtil.isNullObject(functionObjectDTO.getPermissionType())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.permission.type.empty")));
            return false;
        }
        // thu tu
        if (DataUtil.isNullObject(functionObjectDTO.getOrderNo())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.order.no.empty")));
            return false;
        } else {
            if (functionObjectService.isExistOrderNo(functionObjectDTO.getParentId(), functionObjectDTO.getOrderNo(), functionObjectDTO.getFunctionObjectId())) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.order.no.exist")));
                return false;
            }
        }
        // trang thai
        if (DataUtil.isNullObject(functionObjectDTO.getStatus())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.status.empty")));
            return false;
        }
        // duong dan
//        if (DataUtil.isNullObject(functionObjectDTO.getFunctionPath())) {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.path.empty")));
//            return false;
//        }
        if (functionObjectDTO.getFunctionCode().length() > 100) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.code.len.100")));
            return false;
        }
        if (functionObjectDTO.getFunctionName().length() > 300) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.name.len.300")));
            return false;
        }
        if (functionObjectDTO.getFunctionPath().length() > 250) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.path.len.250")));
            return false;
        }
        if (functionObjectDTO.getOrderNo().toString().length() > 3) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.orderno.len.3")));
            return false;
        }
        if (functionObjectDTO.getOrderNo() < 0) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.orderno.gl.0")));
        }
        if (functionObjectDTO.getDescription().length() > 500) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("menu.function.descrip.len.500")));
            return false;
        }

        return true;
    }

    public void updateNodeSelected() {
        try {
            if (!DataUtil.isNullObject(functionObjectDTOSelected)) {
                searchFunctionObject = functionObjectDTOSelected.getFunctionName();
            } else {
                searchFunctionObject = "";
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void onNodeSelect(NodeSelectEvent event) {
        try {
            DefaultTreeNode node = (DefaultTreeNode) event.getTreeNode();
            FunctionObjectDTO functionObjectDTO = (FunctionObjectDTO) node.getData();
            functionObjectDTOSelected = new FunctionObjectDTO();
            BeanUtils.copyProperties(functionObjectDTO, functionObjectDTOSelected);
            functionObjectDTOSelected.setLoadChildren(true);
            functionObjectDTOForTreeList = functionObjectService.findByParentId(functionObjectDTO.getFunctionObjectId(), Const.WEB_CHAIN.FUNCTION_ACTIVE);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Tim kiem phan trang trong chuc nang tim kiem Tìm theo 1. Ma chuc nang 2. Trang thai cua chuc nang
     *
     * @return
     *
     * @throws Exception
     */

    public LazyDataModel<FunctionObjectDTO> findLazyFunction() {
        return new LazyDataModel<FunctionObjectDTO>() {
            @Override
            public List<FunctionObjectDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(functionObjectService.countLazyFunction(inputLazyFunctionSearch));
                    return functionObjectService.findLazyFunction(inputLazyFunctionSearch, page.getPageSize(), page.getPageNumber(), true);
                } catch (Exception e) {
                    logger.error("findAllLazy", e);
                    return Lists.newArrayList();
                }
            }

            @Override
            public FunctionObjectDTO getRowData(String rowKey) {
                List<FunctionObjectDTO> tmp = (List<FunctionObjectDTO>) getWrappedData();
                Optional<FunctionObjectDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getFunctionObjectId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(FunctionObjectDTO object) {
                return object != null ? object.getFunctionObjectId() : null;
            }
        };
    }


    /**
     * @throws Exception
     */
    public void prepareDownloadFileExport() throws Exception {
        functionObjectDTOExportList = functionObjectService.findLazyFunction(inputLazyFunctionSearch, 0, 0, false);
    }

    /**
     * download excel file
     *
     * @return
     */
    public StreamedContent downloadFile() {
        createFile();
        InputStream stream;
        suffix = templatePath.substring(templatePath.lastIndexOf(".") + 1, templatePath.length());
        try {
            stream = new FileInputStream(fileExport);
            return new DefaultStreamedContent(stream, "application/excel", getText("function.export.name") + "." + suffix);
        } catch (FileNotFoundException ex) {
            logger.error(ex.toString(), ex);
            reportError("", "", "common.error.happened.againAction");
        }
        return null;
    }

    /**
     * create excel file
     */
    public void createFile() {
        try {
            Workbook workbook = WorkbookFactory.create(Faces.getResourceAsStream(templatePath));
            Sheet sheet = workbook.getSheetAt(0);
            CellStyle cs = workbook.createCellStyle();
            cs.setBorderRight(CellStyle.BORDER_THIN);
            cs.setRightBorderColor(IndexedColors.BLACK.getIndex());
            cs.setBorderBottom(CellStyle.BORDER_THIN);
            cs.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            cs.setBorderLeft(CellStyle.BORDER_THIN);
            cs.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            cs.setBorderTop(CellStyle.BORDER_THIN);
            cs.setTopBorderColor(IndexedColors.BLACK.getIndex());
            cs.setWrapText(true);

            Font font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            CellStyle csHeader = workbook.createCellStyle();
            csHeader.setBorderRight(CellStyle.BORDER_THIN);
            csHeader.setRightBorderColor(IndexedColors.BLACK.getIndex());
            csHeader.setBorderBottom(CellStyle.BORDER_THIN);
            csHeader.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            csHeader.setBorderLeft(CellStyle.BORDER_THIN);
            csHeader.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            csHeader.setBorderTop(CellStyle.BORDER_THIN);
            csHeader.setTopBorderColor(IndexedColors.BLACK.getIndex());
            csHeader.setFont(font);
            csHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            csHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
            int row = 2;
            if (!DataUtil.isNullOrEmpty(functionObjectDTOExportList)) {
                for (FunctionObjectDTO functionObjectDTOExport : functionObjectDTOExportList) {
                    ++row;
                    Row xRow = sheet.createRow((row));
                    Cell cell0 = xRow.createCell(0);
                    cell0.setCellValue((row - 2));
                    cell0.setCellStyle(cs);
                    // chuc nang cha
                    Cell cell1 = xRow.createCell(1);
                    cell1.setCellValue(functionObjectDTOExport.getParentFunctionName());
                    cell1.setCellStyle(cs);
                    // ma chuc nang
                    Cell cell2 = xRow.createCell(2);
                    cell2.setCellValue(functionObjectDTOExport.getFunctionCode());
                    cell2.setCellStyle(cs);
                    // ten chuc nang
                    Cell cell3 = xRow.createCell(3);
                    cell3.setCellValue(functionObjectDTOExport.getFunctionName());
                    cell3.setCellStyle(cs);
                    // loai chuc nang
                    Cell cell4 = xRow.createCell(4);
                    cell4.setCellValue(functionTypeMap.get(DataUtil.safeToString(functionObjectDTOExport.getFunctionType())));
                    cell4.setCellStyle(cs);
                    // duong dan
                    Cell cell5 = xRow.createCell(5);
                    cell5.setCellValue(functionObjectDTOExport.getFunctionPath());
                    cell5.setCellStyle(cs);
                    // mo ta
                    Cell cell6 = xRow.createCell(6);
                    cell6.setCellValue(functionObjectDTOExport.getDescription());
                    cell6.setCellStyle(cs);
                    // loai quyen
                    Cell cell7 = xRow.createCell(7);
                    cell7.setCellValue(permissionTypeMap.get(DataUtil.safeToString(functionObjectDTOExport.getPermissionType())));
                    cell7.setCellStyle(cs);
                    // trang thai
                    Cell cell8 = xRow.createCell(8);
                    cell8.setCellValue(functionStatusMap.get(functionObjectDTOExport.getStatus().toString()));
                    cell8.setCellStyle(cs);
                }

            }
            fileExport = File.createTempFile("exportFile", suffix);
            FileOutputStream fileOut = new FileOutputStream(fileExport);
            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        } catch (Exception ex) {
            reportError("", "", "common.error.happened.againAction");
            logger.error(ex.getMessage(), ex);
        }
    }


    public void setInputLazyFunctionSearch(InputLazyFunctionSearch inputLazyFunctionSearch) {
        this.inputLazyFunctionSearch = inputLazyFunctionSearch;
    }

    public LazyDataModel<FunctionObjectDTO> getFunctionObjectDTOList() {
        return functionObjectDTOList;
    }

    public void setFunctionObjectDTOList(LazyDataModel<FunctionObjectDTO> functionObjectDTOList) {
        this.functionObjectDTOList = functionObjectDTOList;
    }

    public List<FunctionObjectDTO> getSelectedFunctionObjectDTOList() {
        return selectedFunctionObjectDTOList;
    }

    public void setSelectedFunctionObjectDTOList(List<FunctionObjectDTO> selectedFunctionObjectDTOList) {
        this.selectedFunctionObjectDTOList = selectedFunctionObjectDTOList;
    }

    public FunctionObjectDTO getFunctionObjectDTO() {
        return functionObjectDTO;
    }

    public void setFunctionObjectDTO(FunctionObjectDTO functionObjectDTO) {
        this.functionObjectDTO = functionObjectDTO;
    }

    public List<ApDomainDTO> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<ApDomainDTO> statusList) {
        this.statusList = statusList;
    }

    public FunctionObjectDTO getFunctionObjectDTOSelected() {
        return functionObjectDTOSelected;
    }

    public void setFunctionObjectDTOSelected(FunctionObjectDTO functionObjectDTOSelected) {
        this.functionObjectDTOSelected = functionObjectDTOSelected;
    }

    public List<FunctionObjectDTO> getFunctionObjectDTOForTreeList() {
        return functionObjectDTOForTreeList;
    }

    public void setFunctionObjectDTOForTreeList(List<FunctionObjectDTO> functionObjectDTOForTreeList) {
        this.functionObjectDTOForTreeList = functionObjectDTOForTreeList;
    }

    public TreeNode getRootTreeMenu() {
        return rootTreeMenu;
    }

    public void setRootTreeMenu(TreeNode rootTreeMenu) {
        this.rootTreeMenu = rootTreeMenu;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public String getSearchFunctionObject() {
        return searchFunctionObject;
    }

    public void setSearchFunctionObject(String searchFunctionObject) {
        this.searchFunctionObject = searchFunctionObject;
    }

    public String getDlgHeader() {
        return dlgHeader;
    }

    public void setDlgHeader(String dlgHeader) {
        this.dlgHeader = dlgHeader;
    }

    public List<ApDomainDTO> getPermissionTypeList() {
        return permissionTypeList;
    }

    public void setPermissionTypeList(List<ApDomainDTO> permissionTypeList) {
        this.permissionTypeList = permissionTypeList;
    }

    public Boolean getAdminOnly() {
        return adminOnly;
    }

    public void setAdminOnly(Boolean adminOnly) {
        this.adminOnly = adminOnly;
    }

    public File getFileExport() {
        return fileExport;
    }

    public void setFileExport(File fileExport) {
        this.fileExport = fileExport;
    }

    public List<FunctionObjectDTO> getFunctionObjectDTOExportList() {
        return functionObjectDTOExportList;
    }

    public void setFunctionObjectDTOExportList(List<FunctionObjectDTO> functionObjectDTOExportList) {
        this.functionObjectDTOExportList = functionObjectDTOExportList;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }


    public String getDlgConfirmHeader() {
        return dlgConfirmHeader;
    }

    public void setDlgConfirmHeader(String dlgConfirmHeader) {
        this.dlgConfirmHeader = dlgConfirmHeader;
    }

    public String getDlgConfirmContent() {
        return dlgConfirmContent;
    }

    public void setDlgConfirmContent(String dlgConfirmContent) {
        this.dlgConfirmContent = dlgConfirmContent;
    }

    public InputLazyFunctionSearch getInputLazyFunctionSearch() {
        return inputLazyFunctionSearch;
    }

    public List<ApDomainDTO> getFunctionTypeList() {
        return functionTypeList;
    }

    public void setFunctionTypeList(List<ApDomainDTO> functionTypeList) {
        this.functionTypeList = functionTypeList;
    }

    public Map<String, String> getPermissionTypeMap() {
        return permissionTypeMap;
    }

    public void setPermissionTypeMap(Map<String, String> permissionTypeMap) {
        this.permissionTypeMap = permissionTypeMap;
    }
}
