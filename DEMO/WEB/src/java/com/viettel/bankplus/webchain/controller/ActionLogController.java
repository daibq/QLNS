package com.viettel.bankplus.webchain.controller;

import com.google.common.collect.Lists;
import com.viettel.bankplus.webchain.dto.ActionDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDetailDTO;
import com.viettel.bankplus.webchain.input.InputLazyActionLogSearch;
import com.viettel.bankplus.webchain.service.ActionLogDetailService;
import com.viettel.bankplus.webchain.service.ActionLogService;
import com.viettel.bankplus.webchain.service.ActionService;
import com.viettel.bankplus.webchain.service.ApDomainService;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.web.common.controller.BaseController;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Dong on 8/14/2017.
 */
@Component
@ManagedBean
@Scope("view")
public class ActionLogController extends BaseController {
    private static final long serialVersionUID = 1;
    @Autowired
    private transient ActionLogService actionLogService;

    @Autowired
    private transient ActionLogDetailService actionLogDetailService;

    @Autowired
    private transient ApDomainService apDomainService;

    @Autowired
    private transient ActionService actionService;

    private transient InputLazyActionLogSearch inputLazyActionLogSearch;
    private LazyDataModel<ActionLogDTO> actionLogDTOS;
    private LazyDataModel<ActionLogDetailDTO> actionLogDetailDTOS;
    private ActionLogDTO actionLogDTO;
    private List<ActionDTO> listActionName = new ArrayList<>();

    @PostConstruct
    public void init() {
        inputLazyActionLogSearch = new InputLazyActionLogSearch();
        actionLogDTO = new ActionLogDTO();
        try {
            listActionName = actionService.getListAction();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Secured("@")
    public void doSearch() {
        try {
            actionLogDTOS = findLazy();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private LazyDataModel<ActionLogDTO> findLazy() {
        return new LazyDataModel<ActionLogDTO>() {
            @Override
            public List<ActionLogDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    if (sortField != null && sortOrder != null) {
                        if (SortOrder.ASCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.ASC, sortField);
                        } else if (SortOrder.DESCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.DESC, sortField);
                        }
                    }
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(actionLogService.countList(inputLazyActionLogSearch));
                    return actionLogService.findLazingPaging(inputLazyActionLogSearch, page.getPageSize(), page.getPageNumber());
                } catch (Exception e) {
                    logger.error("findAllLazy", e);
                    return Lists.newArrayList();
                }
            }

            @Override
            public ActionLogDTO getRowData(String rowKey) {
                List<ActionLogDTO> tmp = (List<ActionLogDTO>) getWrappedData();
                Optional<ActionLogDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getActionId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(ActionLogDTO object) {
                return object != null ? object.getActionId() : null;
            }
        };
    }

    @Secured("@")
    public void doViewDetail(ActionLogDTO dto) {
        try {

            actionLogDTO = new ActionLogDTO();
            BeanUtils.copyProperties(dto, actionLogDTO);
            actionLogDetailDTOS = viewDetailLazy();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private LazyDataModel<ActionLogDetailDTO> viewDetailLazy() {
        return new LazyDataModel<ActionLogDetailDTO>() {
            @Override
            public List<ActionLogDetailDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    if (sortField != null && sortOrder != null) {
                        if (SortOrder.ASCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.ASC, sortField);
                        } else if (SortOrder.DESCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.DESC, sortField);
                        }
                    }
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(actionLogDetailService.countList(actionLogDTO));
                    return actionLogDetailService.findLazingPaging(actionLogDTO, page.getPageSize(), page.getPageNumber());
                } catch (Exception e) {
                    logger.error("findAllLazy", e);
                    return Lists.newArrayList();
                }
            }

            @Override
            public ActionLogDetailDTO getRowData(String rowKey) {
                List<ActionLogDetailDTO> tmp = (List<ActionLogDetailDTO>) getWrappedData();
                Optional<ActionLogDetailDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getActionDetailId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(ActionLogDetailDTO object) {
                return object != null ? object.getActionDetailId() : null;
            }
        };
    }



    /* =============================SETTER GETTER======================================*/

    public InputLazyActionLogSearch getInputLazyActionLogSearch() {
        return inputLazyActionLogSearch;
    }

    public void setInputLazyActionLogSearch(InputLazyActionLogSearch inputLazyActionLogSearch) {
        this.inputLazyActionLogSearch = inputLazyActionLogSearch;
    }

    public LazyDataModel<ActionLogDTO> getActionLogDTOS() {
        return actionLogDTOS;
    }

    public void setActionLogDTOS(LazyDataModel<ActionLogDTO> actionLogDTOS) {
        this.actionLogDTOS = actionLogDTOS;
    }

    public LazyDataModel<ActionLogDetailDTO> getActionLogDetailDTOS() {
        return actionLogDetailDTOS;
    }

    public void setActionLogDetailDTOS(LazyDataModel<ActionLogDetailDTO> actionLogDetailDTOS) {
        this.actionLogDetailDTOS = actionLogDetailDTOS;
    }

    public ActionLogDTO getActionLogDTO() {
        return actionLogDTO;
    }

    public void setActionLogDTO(ActionLogDTO actionLogDTO) {
        this.actionLogDTO = actionLogDTO;
    }

    public List<ActionDTO> getListActionName() {
        return listActionName;
    }

    public void setListActionName(List<ActionDTO> listActionName) {
        this.listActionName = listActionName;
    }
}
