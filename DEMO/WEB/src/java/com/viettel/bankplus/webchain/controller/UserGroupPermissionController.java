package com.viettel.bankplus.webchain.controller;

import com.google.common.collect.Lists;
import com.viettel.bankplus.webchain.dto.*;
import com.viettel.bankplus.webchain.input.InputLazyUserPermissionGroup;
import com.viettel.bankplus.webchain.input.InputLazyUserPermissionGroupCreate;
import com.viettel.bankplus.webchain.service.*;
import com.viettel.bankplus.webchain.utils.ActionUtil;
import com.viettel.bccs.fw.common.BccsLoginSuccessHandler;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.dto.BaseMessage;
import com.viettel.utils.ExcelUtil;
import com.viettel.web.common.controller.BaseController;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * @author hungnq on 9/5/2017
 */
@ManagedBean
@Component
@Scope("view")
public class UserGroupPermissionController extends BaseController {
    private static final long serialVersionUID = 1L;
    @Autowired
    private transient ChainUserService chainUserService;
    @Autowired
    private transient PermissionGroupService permissionGroupService;
    @Autowired
    private transient PermissionManagerService permissionManagerService;
    @Autowired
    private transient ApDomainService apDomainService;
    @Autowired
    private transient ActionService actionService;

    private List<ActionDTO> actionDTOList;

    private LazyDataModel<PermissionGroupUserDTO> permissionGroupUserDTOList;
    private List<ChainUserDTO> chainUserDTOList;
    private Long[] chainUserIdSelected = {};
    private Long[] permissionGroupIdSelected = {};
    private Boolean onChange = false;
    private Map<String, String> labelSelectCheckBoxs = new HashMap<>();
    private transient InputLazyUserPermissionGroup inputSearch;
    private transient InputLazyUserPermissionGroupCreate inputLazyUserPermissionGroup;
    /*dai*/


    private List<ChainUserDTO> chainUserDTOSearchList;
    private List<ChainUserDTO> chainUserDTOUpdateList;
    private List<ChainUserDTO> selectChainUserDTOLists;
    private List<PermissionGroupDTO> selectPermissionGroupDTOList;
    private PermissionGroupUserDTO permissionGroupUserDTO;
    private List<PermissionGroupDTO> permissionGroupDTOList;
    private List<Long> chainUserIdCreateSelected;
    private List<Long> permissionGroupIdCreateSelected;
    private Map<String, String> labelSelectCreateCheckBoxs = new HashMap<>();
    private Long chainShopId;
    private Long chainShopIdSearch;
    private boolean create;
    private boolean viewUpdate;
    /*dai end*/
    private List<ApDomainDTO> statusList = new ArrayList<>();

    //xu ly ve file
    private List<ChainUserDTO> chainUserDTOImportList;
    private List<PermissionGroupDTO> permissionGroupDTOImportList;
    private Map<String, Long> chainUserIdMap;
    private Map<String, Long> permissionIdMap;
    private Map<String, String> checkDuplicateImport;
    private String fileData;
    private transient UploadedFile uploadedFile;
    private String name;
    private byte[] contentInBytes;
    private boolean renderTemplateFile;
    private boolean renderResultFileError;
    private List<PermissionGroupUserImportDTO> importExcelPermissionGroupUserDTO1List;
    private List<PermissionGroupUserImportDTO> importExcelSuccessList;
    private static final String[] ALLOW_EXTENSION_TEMPLATE = {"xls", "xlsx"};
    private boolean isExceptionHyperLink;
    private List<PermissionGroupUserImportDTO> importExcelPermissionGroupUserDTOList;
    private boolean importMaxRecord = false;

    @PostConstruct
    public void init() {
        try {
            actionDTOList = actionService.getListActionDTOByType(Const.ACTION_LOG.PERMISSION_GROUP_USER_TYPE);
            permissionGroupUserDTO = new PermissionGroupUserDTO();
            labelSelectCheckBoxs.put(getText("web.chain.user"), getText("value.selected"));
            labelSelectCheckBoxs.put(getText("web.chain.permission.group"), getText("value.selected"));
            inputSearch = new InputLazyUserPermissionGroup();
            inputSearch.setChainId(BccsLoginSuccessHandler.getChainId());
            inputLazyUserPermissionGroup = new InputLazyUserPermissionGroupCreate();
                /*dai*/
            viewUpdate = false;
            labelSelectCreateCheckBoxs.put(getText("web.chain.user"), getText("value.selected"));
            labelSelectCreateCheckBoxs.put(getText("web.chain.permission.group"), getText("value.selected"));
            chainUserDTOList = new ArrayList<>();
            chainUserDTOList = chainUserService.getChainUserByShopId(BccsLoginSuccessHandler.getShopId());
            chainUserDTOSearchList = new ArrayList<>();
            chainUserDTOSearchList = chainUserService.getChainUserByShopId(BccsLoginSuccessHandler.getShopId());
            chainUserDTOUpdateList = new ArrayList<>();
            chainUserDTOUpdateList = chainUserService.getChainUserByShopId(BccsLoginSuccessHandler.getShopId());
            chainUserDTOImportList = new ArrayList<>();
            chainUserDTOImportList = chainUserService.getChainUserByParentShopId(BccsLoginSuccessHandler.getShopId());
            permissionGroupDTOImportList = new ArrayList<>();
            permissionGroupDTOImportList = permissionGroupService.getPermissionGroupDTOListByShopId(BccsLoginSuccessHandler.getShopId());
            getNameforExcel();
                /*dai-end*/
            permissionGroupDTOList = permissionGroupService.getPermissionGroupDTOListByShopId(BccsLoginSuccessHandler.getShopId());
            statusList = apDomainService.getValueApDomainByTypeOrderByCode(Const.AP_DOMAIN.WC_STATUS);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
    /**
     * dai
     *
     * @param key
     * @param value
     */
    public void updateLabelSelectCheckBox(String key, int value) {
        if (!onChange) {
            return;
        }
        labelSelectCheckBoxs.put(key, (value != 0 ? (value + " " + getText("common.value.selected")) : getText("value.selected")));
        try {
            if (key.equalsIgnoreCase(getText("web.chain.user"))) {
                if (value > 0) {
                    inputSearch.setChainUserIdList(Lists.newArrayList());
                    for (int i = 0; i < value; i++) {
                        inputSearch.getChainUserIdList().add(chainUserIdSelected[i]);
                    }
                } else {
                    chainUserIdSelected = new Long[]{};
                    inputSearch.setChainUserIdList(Lists.newArrayList());
                }
            } else if (key.equalsIgnoreCase(getText("web.chain.permission.group"))) {
                if (value > 0) {
                    inputSearch.setPermissionGroupIdList(Lists.newArrayList());
                    for (int j = 0; j < value; j++) {
                        inputSearch.getPermissionGroupIdList().add(permissionGroupIdSelected[j]);
                    }
                } else {
                    permissionGroupIdSelected = new Long[]{};
                    inputSearch.setPermissionGroupIdList(Lists.newArrayList());
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("csp.service.error")));
        } finally {
            onChange = false;
        }
    }

    /*dai*/
    public void updateLabelSelectCreateCheckBoxChainUser(String key, int value) {
        if (!onChange) {
            return;
        }
        labelSelectCreateCheckBoxs.put(key, (value != 0 ? (value + " " +
                getText("common.value.selected")) : getText("value.selected")));
        try {
            selectChainUserDTOLists = new ArrayList<>();
            if (key.equalsIgnoreCase(getText("web.chain.user"))) {
                if (value > 0) {
                    inputLazyUserPermissionGroup.setChainUserIdListCreate(Lists.newArrayList());
                    for (int i = 0; i < value; i++) {
                        inputLazyUserPermissionGroup.getChainUserIdListCreate().add(chainUserIdCreateSelected.get(i));
                    }
                } else {
                    chainUserIdCreateSelected = new ArrayList<>();
                    inputLazyUserPermissionGroup.setChainUserIdListCreate(Lists.newArrayList());
                }
                if (!DataUtil.isNullOrEmpty(chainUserDTOList)) {
                    for (Object id : chainUserIdCreateSelected) {
                        String a = String.valueOf(id);
                        for (ChainUserDTO dto : chainUserDTOList) {
                            if (dto.getChainUserId().toString().equals(a)) {
                                selectChainUserDTOLists.add(dto);
                            }
                        }
                    }
                }


            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage("messageserror",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            null, getText("csp.service.error")));
        } finally {
            onChange = false;
        }
    }

    /*dai*/
    public void updateLabelSelectCreateCheckBoxPermissionGroup(String key, int value) {
        if (!onChange) {
            return;
        }
        labelSelectCreateCheckBoxs.put(key, (value != 0 ? (value + " " + getText("common.value.selected")) : getText("value.selected")));
        try {
            selectPermissionGroupDTOList = new ArrayList<>();
            if (key.equalsIgnoreCase(getText("web.chain.permission.group"))) {
                if (value > 0) {
                    inputLazyUserPermissionGroup.setPermissionGroupIdListCreate(Lists.newArrayList());
                    for (int j = 0; j < value; j++) {
                        inputLazyUserPermissionGroup.getPermissionGroupIdListCreate().add(permissionGroupIdCreateSelected.get(j));
                    }
                } else {
                    permissionGroupIdCreateSelected = new ArrayList<>();
                    inputLazyUserPermissionGroup.setPermissionGroupIdListCreate(Lists.newArrayList());
                }
                if (!DataUtil.isNullOrEmpty(permissionGroupDTOList)) {
                    for (Object id : permissionGroupIdCreateSelected) {
                        String a = String.valueOf(id);
                        for (PermissionGroupDTO dto : permissionGroupDTOList) {
                            if (dto.getPermissionGroupId().toString().equals(a)) {
                                selectPermissionGroupDTOList.add(dto);
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage("messageserror", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("csp.service.error")));
        } finally {
            onChange = false;
        }
    }

    /**
     * dai chuan bi du lieu cho cap nhat chuc nang
     */
    public void prePermissionGroupUserEdit(PermissionGroupUserDTO permissionGroupUserDTOEdit) {
        try {
            inputLazyUserPermissionGroup = new InputLazyUserPermissionGroupCreate();
            chainUserDTOUpdateList = new ArrayList<>();
            selectPermissionGroupDTOList = new ArrayList<>();
            List<ChainUserDTO> list = permissionManagerService.getNameByChainUserIdByShopId(permissionGroupUserDTOEdit.getChainUserId());
            //gán tên mã theo chainUserId
            chainShopId = list.get(0).getChainShopId();
            //update ten theo mã shop
            getChainUserByChainShopIdForUpdate();
            RequestContext.getCurrentInstance().update("addNewForm:sbUserDlgupdate");
            viewUpdate = true;
            permissionGroupUserDTO = new PermissionGroupUserDTO();
            BeanUtils.copyProperties(permissionGroupUserDTOEdit, permissionGroupUserDTO);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                    getText("common.error.happened.againAction")));
        }
    }

    /*
     *dai
     * làm mới form trước khi thêm mới
     * */
    public void prepareToShowAdd() {
        try {
            chainShopId = null;
            viewUpdate = false;
            chainUserDTOList = new ArrayList<>();
            permissionGroupUserDTO = new PermissionGroupUserDTO();
            chainUserIdCreateSelected = new ArrayList<>();
            inputLazyUserPermissionGroup.setChainUserIdListCreate(Lists.newArrayList());
            permissionGroupIdCreateSelected = new ArrayList<>();
            inputLazyUserPermissionGroup.setPermissionGroupIdListCreate(Lists.newArrayList());
            selectPermissionGroupDTOList = new ArrayList<>();
            selectChainUserDTOLists = new ArrayList<>();
            labelSelectCreateCheckBoxs.put(getText("web.chain.user"), getText("value.selected"));
            labelSelectCreateCheckBoxs.put(getText("web.chain.permission.group"), getText("value.selected"));

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage("messageserror",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                            getText("common.msg.error")));
        }

    }



    /*dai null cac truong co trong form */
    public boolean validateCreatAddNew() {
        try {
            if (selectPermissionGroupDTOList.isEmpty()) {
                FacesContext.getCurrentInstance().addMessage("messageserror",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                getText("permission.group.empty")));

                return false;
            }
            if (selectChainUserDTOLists.isEmpty()) {
                FacesContext.getCurrentInstance().addMessage("messageserror",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                getText("permission.group.user.empty")));
                return false;
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage("messageserror",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                            getText("common.error.happened.againAction")));
        }
        return true;
    }

    /*dai null khi chon nguoi dung chuoi khi update*/
    public boolean checkUpdateForm() {
        try {

            if (DataUtil.isNullObject(permissionGroupUserDTO.getChainUserId())) {
                FacesContext.getCurrentInstance().addMessage("messageserror",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                getText("permission.group.user.empty")));
                return false;
            }
            if (DataUtil.isNullObject(permissionGroupUserDTO.getStatus())) {
                FacesContext.getCurrentInstance().addMessage("messageserror",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                getText("permission.group.user.status.empty")));
                return false;
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage("messageserror", new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                    getText("common.error.happened.againAction")));
        }
        return true;

    }

    /*
   * check chainUserId của quan ly nhom chuc nang nguoi dung
   */
    public boolean validateChainUserIdBeforeSave() {

        try {

            boolean isDuplicateChainUserId = permissionManagerService.checkchainUserId(permissionGroupUserDTO.getChainUserId(),
                    permissionGroupUserDTO.getPermissionGroupId());
            if (!isDuplicateChainUserId) {
                FacesContext.getCurrentInstance().addMessage("messageserror", new FacesMessage(FacesMessage.SEVERITY_WARN, null,
                        getText("webchain.functionGroup.duplicate.chainUserId")));
                return false;
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("messageserror",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                            getText("webchain.catalog.err")));
            logger.error(e.getMessage(), e);
        }
        return true;
    }

    /*dai
    * check ban ghi có hiêu lực hay không*/
    public boolean checkRecordExist(PermissionGroupUserDTO dto) {
        try {
            boolean isRecordExist = permissionManagerService.checkIdPermissionGroupUser(dto.getPermissionGroupUserId());
            if (isRecordExist) {
                RequestContext.getCurrentInstance().execute("PF('dlgUserGroupPermission').hide()");
                FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_WARN, null,
                                getText("webchain.cataog.exist")));
                return false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return true;
    }

    /*dai*/
    public void getChainUserByChainShopId() {
        try {
            if (null != chainShopId) {
                chainUserDTOList = chainUserService.getChainUserByShopId(chainShopId);
            } else {
                chainUserDTOList = new ArrayList<>();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    /*dai*/
    public void getChainUserByChainShopIdForSearch() {
        try {
            if (null != chainShopIdSearch) {
                chainUserDTOSearchList = chainUserService.getChainUserByShopId(chainShopIdSearch);
            } else {
                chainUserDTOSearchList = new ArrayList<>();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage("messagese", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    /*dai*/
    public void getChainUserByChainShopIdForUpdate() {
        try {
            if (null != chainShopId) {
                chainUserDTOUpdateList = chainUserService.getChainUserByShopId(chainShopId);
            } else {
                chainUserDTOUpdateList = new ArrayList<>();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().validationFailed();
            FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    /*dai*/
    public LazyDataModel<PermissionGroupUserDTO> findLazyFunction() {
        return new LazyDataModel<PermissionGroupUserDTO>() {
            @Override
            public List<PermissionGroupUserDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(permissionManagerService.countLazyUserPermissionGroup(inputSearch));
                    return permissionManagerService.findLazyUserPermissionGroup(inputSearch, page.getPageSize(), page.getPageNumber());
                } catch (Exception e) {
                    logger.error("findAllLazy", e);
                    return Lists.newArrayList();
                }
            }

            @Override
            public PermissionGroupUserDTO getRowData(String rowKey) {
                List<PermissionGroupUserDTO> tmp = (List<PermissionGroupUserDTO>) getWrappedData();
                Optional<PermissionGroupUserDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getPermissionGroupUserId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(PermissionGroupUserDTO object) {
                return object != null ? object.getPermissionGroupUserId() : null;
            }
        };
    }

    /**
     * dai tim kiêm theo nhóm chức năng và ngươif dùng chuỗi theo của hàng chuỗi lựa chọn
     */
    public void doSearch() {
        try {
            permissionGroupUserDTOList = findLazyFunction();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /*dai
    * thêm moi người dùng theo nhóm chức nang*/
    public void doCreateOrUpdate() {
        try {

            create = permissionGroupUserDTO.getPermissionGroupUserId() == null ? true : false;
            permissionGroupUserDTO.setCreateUser(BccsLoginSuccessHandler.getUserName());
            if (create) {
                if (validateCreatAddNew()) {
                    ActionLogDTO actionLogDTO = new ActionLogDTO();
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList,
                            Const.ACTION_LOG.PERMISSION_GROUP_USER_TYPE_ADD_NEW));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_USER_TYPE_ADD_NEW));
                    actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                    permissionManagerService.savePermissionGroupUser(permissionGroupUserDTO,
                            selectPermissionGroupDTOList, selectChainUserDTOLists, actionLogDTO);
                    RequestContext.getCurrentInstance().execute("PF('dlgUserGroupPermission').hide()");
                    FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO,
                            null, getText("webchain.catalog.create.success")));
                }
            } else {
                if (checkUpdateForm()) {
                    doUpdate();
                }

            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage("messageserror", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.msg.error")));
        }

    }

    //sua nguoi dung theo nhom chuc nang
    public void doUpdate() {
        try {
            Short statusValue = DataUtil.isNullObject(permissionGroupUserDTO.getStatus()) ? 1 : permissionGroupUserDTO.getStatus();
            PermissionGroupUserDTO oldPermissionGroupUserDTO = permissionManagerService.findOne(permissionGroupUserDTO.getPermissionGroupUserId());
            Long oldChainUserId = oldPermissionGroupUserDTO.getChainUserId();

            ActionLogDTO actionLogDTO = new ActionLogDTO();
            actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.PERMISSION_GROUP_USER_TYPE_UPDATE));
            actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_USER_TYPE_UPDATE));
            actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());

            if (!DataUtil.safeEqual(oldChainUserId, permissionGroupUserDTO.getChainUserId())) {
                if (validateChainUserIdBeforeSave()) {
                    permissionGroupUserDTO.setStatus(statusValue);
                    permissionManagerService.updatePermissionGroupUser(permissionGroupUserDTO, actionLogDTO);
                    RequestContext.getCurrentInstance().execute("PF('dlgUserGroupPermission').hide()");
                    FacesContext.getCurrentInstance().addMessage("messages",
                            new FacesMessage(FacesMessage.SEVERITY_INFO, null,
                                    getText("webchain.catalog.update.success")));
                }
            } else {
                permissionGroupUserDTO.setStatus(statusValue);
                permissionManagerService.updatePermissionGroupUser(permissionGroupUserDTO, actionLogDTO);
                RequestContext.getCurrentInstance().execute("PF('dlgUserGroupPermission').hide()");
                FacesContext.getCurrentInstance().addMessage("messages",
                        new FacesMessage(FacesMessage.SEVERITY_INFO, null,
                                getText("webchain.catalog.update.success")));

            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage("messageserror",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                            getText("common.msg.error")));
        }


    }

    /**
     * dai xóa(chuyển trạng thái) của bản ghi
     */
    public void doDeletePermissiomGroupUser(PermissionGroupUserDTO dto) {
        try {
            ActionLogDTO actionLogDTO = new ActionLogDTO();
            actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList,
                    Const.ACTION_LOG.PERMISSION_GROUP_USER_TYPE_DELETE));
            actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_USER_TYPE_DELETE));
            actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
            if (checkRecordExist(dto)) {
                permissionManagerService.deletePermissionGroupUser(dto.getPermissionGroupUserId(), actionLogDTO);
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO,
                        null, getText("webchain.catalog.delete.success")));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage("messages",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                            getText("webchain.catalog.err")));
        }

    }

    /*
* Import file - Start
*/
    // lấy id theo ten va ma
    public void getNameforExcel() {
        chainUserIdMap = new HashMap<>();
        permissionIdMap = new HashMap<>();
        if (!chainUserDTOImportList.isEmpty()) {
            for (ChainUserDTO chainUserDTO : chainUserDTOImportList) {
                chainUserIdMap.put(chainUserDTO.getUserName().trim().toLowerCase(), chainUserDTO.getChainUserId());
            }
        }
        if (!permissionGroupDTOImportList.isEmpty()) {
            for (PermissionGroupDTO permissionGroupDTO : permissionGroupDTOImportList) {
                permissionIdMap.put(permissionGroupDTO.getPermissionGroupCode().trim().toLowerCase(),
                        permissionGroupDTO.getPermissionGroupId());
            }
        }

    }

    //import file
    public void importDataByExcel() {
        try {
            importMaxRecord = false;
            importExcelPermissionGroupUserDTO1List = new ArrayList<>();
            importExcelSuccessList = new ArrayList<>();
            checkDuplicateImport = new ConcurrentHashMap<>();
            BaseMessage message = validateFileUploadWhiteList(uploadedFile,
                    ALLOW_EXTENSION_TEMPLATE, Const.WEB_CHAIN.MAX_SIZE_5M);
            if (!message.isSuccess()) {
                fileData = null;
                contentInBytes = null;
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null, message.getDescription()));
                return;
            }
            // check file mau template
            if (!checkFileTemplate(contentInBytes)) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                getText("common.import.file.error.template")));
                return;
            }
            if (contentInBytes == null || DataUtil.isNullOrEmpty(fileData)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                        getText("common.import.file.error.empty")));
                return;
            }
            ActionLogDTO actionLogDTO = new ActionLogDTO();
            actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
            actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList,
                    Const.ACTION_LOG.PERMISSION_GROUP_USER_IMPORT));
            importExcelSuccessList = readDataInFile(contentInBytes);
            if (isExceptionHyperLink) {
                return;
            }

            if (!DataUtil.isNullOrEmpty(importExcelSuccessList)) {
                List<PermissionGroupUserImportDTO> listResult =
                        permissionManagerService.importPermissionGroupUserDTOTemplate
                                (importExcelSuccessList, actionLogDTO);
                renderResultFileError = true;
                renderTemplateFile = false;
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, null,
                                getText("webchain.functionGroup.import.successful")));
                importExcelPermissionGroupUserDTO1List.addAll(0, listResult);
                importExcelSuccessList.clear();
                fileData = null;
                uploadedFile = null;
            } else if (DataUtil.isNullOrEmpty(importExcelPermissionGroupUserDTO1List)) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                getText("common.import.file.error.empty")));
            } else {
                fileData = null;
                uploadedFile = null;
                if (importMaxRecord) {
                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                    getText("common.import.file.error.file.max.record")));
                } else {
                    renderResultFileError = true;
                    renderTemplateFile = false;
                    FacesContext.getCurrentInstance().addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO, null,
                                    getText("webchain.functionGroup.import.successful")));
                }
                importMaxRecord = false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                            getText("common.error.happened.againAction")));
        }
    }

    public void preImport() {
        renderResultFileError = false;
        renderTemplateFile = true;
        fileData = null;
        uploadedFile = null;
        contentInBytes = null;
        importExcelSuccessList = new ArrayList<>();
        importExcelPermissionGroupUserDTO1List = new ArrayList<>();
    }

    @Secured("@")
    public void fileUploadAction(FileUploadEvent event) {
        try {
            uploadedFile = event.getFile();
            BaseMessage message = validateFileUploadWhiteList(uploadedFile, ALLOW_EXTENSION_TEMPLATE, Const.WEB_CHAIN.MAX_SIZE_10M);
            if (!message.isSuccess()) {
                fileData = null;
                focusElementByRawCSSSlector(".outputAttachFiledata");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, message.getDescription()));
                return;
            }
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
            name = fmt.format(new Date())
                    + uploadedFile.getFileName().substring(uploadedFile.getFileName().lastIndexOf('.'));
            contentInBytes = uploadedFile.getContents();
            fileData = uploadedFile.getFileName();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                    getText("common.error.happened.againAction")));
        }
    }

    //tai file mau
    @Secured("@")
    public void downloadFileTemplate() {
        try {
            InputStream createStream = Faces.getResourceAsStream(Const.RESOURCE_TEMPLATE.RESOURCE_TEMPLATE_PATH +
                    Const.PERMISSION_GROUP_USER.FILE_NAME);
            XSSFWorkbook workbook = new XSSFWorkbook(createStream);
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" +
                    Const.PERMISSION_GROUP_USER.FILE_NAME + "\"");
            externalContext.setResponseContentType("application/excel");
            workbook.write(externalContext.getResponseOutputStream());
            facesContext.responseComplete();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    null, getText("common.error.happened.againAction")));
        }
    }

    //tai file ket qua
    @Secured("@")
    public void downloadFileResult() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            InputStream createStream = Faces.getResourceAsStream(Const.RESOURCE_TEMPLATE.RESOURCE_TEMPLATE_PATH +
                    Const.PERMISSION_GROUP_USER.FILE_NAME_RESULT + ".xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook(createStream);
            XSSFSheet sheet = workbook.getSheetAt(0);


            XSSFCellStyle styleError = workbook.createCellStyle();
            styleError.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleError.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleError.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleError.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            Font fontError = workbook.createFont();
            /*fontError.setColor(IndexedColors.RED.getIndex());*/
            styleError.setFont(fontError);
            styleError.setWrapText(true);

            XSSFCellStyle styleSuccess = workbook.createCellStyle();
            styleSuccess.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleSuccess.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleSuccess.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleSuccess.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            Font fontSuccess = workbook.createFont();
            fontSuccess.setColor(IndexedColors.GREEN.getIndex());
            styleSuccess.setFont(fontSuccess);
            styleSuccess.setWrapText(true);
            int row = 0;

            if (!DataUtil.isNullOrEmpty(importExcelPermissionGroupUserDTO1List)) {
                for (PermissionGroupUserImportDTO dtoExt : importExcelPermissionGroupUserDTO1List) {
                    row++;
                    String result = dtoExt.getImportExcelError();
                    XSSFCellStyle style;
                    style = "1".equals(result) ? styleSuccess : styleError;
                    Row xRow = sheet.createRow(row);
                    Cell cell0 = xRow.createCell(0);
                    cell0.setCellValue(row);
                    cell0.setCellStyle(style);

                    //id ma nhom chuc nang
                    Cell cell01 = xRow.createCell(1);
                    cell01.setCellValue(dtoExt.getPermissionGroupCode());
                    cell01.setCellStyle(style);

                    //// id nguoi dung chuoi
                    Cell cell02 = xRow.createCell(2);
                    cell02.setCellValue(dtoExt.getUserName());
                    cell02.setCellStyle(style);

                    // ketqua
                    Cell cell03 = xRow.createCell(3);
                    cell03.setCellStyle(style);
                    if ("0".equals(result)) {
                        cell03.setCellValue(getText("webchain.shop.import.notOK"));
                    } else if ("1".equals(result)) {
                        cell03.setCellValue(getText("webchain.shop.import.OK"));
                    } else {
                        cell03.setCellValue(result);
                    }
                }
            }
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" +
                    Const.PERMISSION_GROUP_USER.FILE_NAME_RESULT + sdf.format(new Date()) + ".xlsx");
            externalContext.setResponseContentType("application/excel");
            workbook.write(externalContext.getResponseOutputStream());
            facesContext.responseComplete();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                            getText("common.error.happened.againAction")));
        }
    }

    //doc file
    private List<PermissionGroupUserImportDTO> readDataInFile(byte[] contentInBytes) {
        List<PermissionGroupUserImportDTO> importList = new ArrayList<>();
        PermissionGroupUserImportDTO dto;
        importMaxRecord = false;
        importExcelPermissionGroupUserDTOList = new ArrayList<>();
        isExceptionHyperLink = false;
        ExcelUtil ex = null;
        try {
            ex = new ExcelUtil(uploadedFile, contentInBytes);
            Sheet sheet = ex.getWorkbook().getSheetAt(0);
            Row xRow = sheet.getRow(2);
            int totalCol = xRow.getLastCellNum();
            if (totalCol < 3) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                getText("common.import.file.error.template")));
                return new ArrayList();
            }
            List<Integer> rowIgnore = new ArrayList<>();
            int totalRow = 1;
            //validate 100 bản ghi
            for (int row = 3; row <= sheet.getLastRowNum(); row++) {
                xRow = sheet.getRow(row);
                if (totalRow > 100) {
                    importMaxRecord = true;
                    importExcelPermissionGroupUserDTOList.addAll(0, importList);
                    return new ArrayList();
                }
                if (xRow != null && xRow.getFirstCellNum() < 3) {
                    dto = new PermissionGroupUserImportDTO();
                    String code = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(1)))) {
                        code = ExcelUtil.getStringValue(xRow.getCell(1)).trim();
                    }
                    String name = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(2)))) {
                        name = ExcelUtil.getStringValue(xRow.getCell(2)).trim();
                    }
                    if (DataUtil.isNullOrEmpty(code) && DataUtil.isNullOrEmpty(name)) {
                        rowIgnore.add(row);
                        continue;
                    }
                    totalRow++;
                    String result = validate(dto, code, name);
                    dto.setUserName(name);
                    dto.setPermissionGroupCode(code);
                    if (!DataUtil.isNullOrEmpty(result)) {
                        dto.setImportExcelError(Const.WEB_CHAIN.IMPORT_ERROR + "! " + result);
                        importExcelPermissionGroupUserDTO1List.add(dto);
                    } else {
                        dto.setCreateUser(BccsLoginSuccessHandler.getUserName());
                        dto.setImportExcelError(null);
                        importList.add(dto);
                    }

                }
            }
        } catch (Exception e) {
            if (ex == null) {
                FacesContext context = FacesContext.getCurrentInstance();
                Iterator<FacesMessage> it = context.getMessages();
                while (it.hasNext()) {
                    it.next();
                    it.remove();
                }
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                getText("common.import.file.error.file.hyperLink")));
                isExceptionHyperLink = true;
                return new ArrayList();
            }
            dto = new PermissionGroupUserImportDTO();
            dto.setImportExcelError(Const.WEB_CHAIN.IMPORT_ERROR + getText("common.error.happened.againAction"));
            importExcelPermissionGroupUserDTO1List.add(dto);
            logger.error(e.getMessage(), e);

        }
        return importList;
    }

    private String validate(PermissionGroupUserImportDTO dto, String code, String name) throws Exception {
        StringBuilder result = new java.lang.StringBuilder();
        if (DataUtil.isNullOrEmpty(code)) {
            result.append(getText("webchain.functionGroup.permissionGroupCodeEmpty") + ". ");
        }
        if (DataUtil.isNullOrEmpty(name)) {
            result.append(getText("action.chain.user1.import") + ". ");
        }
        if (!DataUtil.isNullOrEmpty(name) && !DataUtil.isNullOrEmpty(code)) {
            dto.setChainUserId(chainUserIdMap.get(name.trim().toLowerCase()));
            dto.setPermissionGroupId(permissionIdMap.get(code.trim().toLowerCase()));

            if (code.length() > Const.PERMISSION_GROUP_USER.MAXLENGTH_CODE) {
                result.append(getText("webchain.functionGroup.NotImport.maxLengh.permissionGroupCode") + ". ");
            }
            if (name.length() > Const.PERMISSION_GROUP_USER.MAXLENGTH_NAME) {
                result.append(getText("webchain.functionGroup.NotImport.maxLengh.chain.user") + ". ");
            }
            /*if (!DataUtil.isCheckSpecialCharacter(code)) {
                result.append(getText("webchain.functionGroup.NotImportWord.permissionGroupCode") + ". ");
            }*/
            Pattern docCode = Pattern.compile("[\\d\\w_]+");
            if (!docCode.matcher(code).matches()) {
                result.append(getText("webchain.functionGroup.NotImportWord.permissionGroupCode") + ". ");
            } else {
                if (DataUtil.isNullObject(dto.getPermissionGroupId())) {
                    result.append(getText("action.permission.group.user1.import") + ". ");
                }
            }
            if (DataUtil.isNullObject(dto.getChainUserId())) {
                result.append(getText("action.permission.group.user2.import") + ". ");
            }
            if (!DataUtil.isNullObject(dto.getChainUserId()) && !DataUtil.isNullObject(dto.getPermissionGroupId())) {
                boolean isDuplicateChainUserId =
                        permissionManagerService.checkchainUserId(chainUserIdMap.get(name.trim().toLowerCase()),
                                permissionIdMap.get(code.trim().toLowerCase()));
                if (!isDuplicateChainUserId) {
                    result.append(getTextParam
                            ("webchain.functionGroup.duplicate.PermissionGroupId",
                                    name + "", code + ""));

                } else {
                    dto.setPermissionGroupId(permissionIdMap.get(code.trim().toLowerCase()));
                    dto.setChainUserId(chainUserIdMap.get(name.trim().toLowerCase()));
                }

            }
        }
        // Check trung ten, ma tren file
        if (!DataUtil.isNullOrEmpty(code) && !DataUtil.isNullOrEmpty(name)) {
            String a = code + name;
            if (DataUtil.isNullOrEmpty(checkDuplicateImport.get(a.trim().toLowerCase()))) {
                checkDuplicateImport.put(a.trim().toLowerCase(), a.trim().toLowerCase());
            } else {
                result.append(getTextParam
                        ("validate.webchain.functionGroup.code.name.file.exist",
                                code + "", name + ""));
            }

        }

        return result.toString();

    }


    private boolean checkFileTemplate(byte[] contentByte) {
        try {
            ExcelUtil ex = new ExcelUtil(uploadedFile, contentByte);
            Sheet sheet = ex.getWorkbook().getSheetAt(0);
            Row row2 = sheet.getRow(1);
            if (null == row2)
                return false;
            int totalCol = row2.getLastCellNum();
            if (totalCol < 3)
                return false;
            // ma nhom chuc nang
            String functionGroupCode = ExcelUtil.getStringValue(row2.getCell(1)).trim();
            if (!getText("webchain.functionGroup.permissionGroupCode").equalsIgnoreCase(functionGroupCode.trim())) {
                return false;
            }
            // ten người dung chuoi
            String functionGroupName = ExcelUtil.getStringValue(row2.getCell(2)).trim();
            if (!getText("webchain.chainUser.username").equalsIgnoreCase(functionGroupName.trim())) {
                return false;
            }

            return true;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                            getText("common.error.happened.againAction")));
        }
        return true;
    }
    /*
    * Import file - END
    */



    /*=============================Getter//Setter=========================*/

    public List<ChainUserDTO> getChainUserDTOList() {
        return chainUserDTOList;
    }

    public void setChainUserDTOList(List<ChainUserDTO> chainUserDTOList) {
        this.chainUserDTOList = chainUserDTOList;
    }

    public List<PermissionGroupDTO> getPermissionGroupDTOList() {
        return permissionGroupDTOList;
    }

    public void setPermissionGroupDTOList(List<PermissionGroupDTO> permissionGroupDTOList) {
        this.permissionGroupDTOList = permissionGroupDTOList;
    }

    public Long[] getChainUserIdSelected() {
        return chainUserIdSelected;
    }

    public void setChainUserIdSelected(Long[] chainUserIdSelected) {
        this.chainUserIdSelected = chainUserIdSelected;
    }

    public Boolean getOnChange() {
        return onChange;
    }

    public void setOnChange(Boolean onChange) {
        this.onChange = onChange;
    }

    public Map<String, String> getLabelSelectCheckBoxs() {
        return labelSelectCheckBoxs;
    }

    public void setLabelSelectCheckBoxs(Map<String, String> labelSelectCheckboxs) {
        this.labelSelectCheckBoxs = labelSelectCheckboxs;
    }

    public Long[] getPermissionGroupIdSelected() {
        return permissionGroupIdSelected;
    }

    public void setPermissionGroupIdSelected(Long[] permissionGroupIdSelected) {
        this.permissionGroupIdSelected = permissionGroupIdSelected;
    }

    public InputLazyUserPermissionGroup getInputSearch() {
        return inputSearch;
    }

    public void setInputSearch(InputLazyUserPermissionGroup inputSearch) {
        this.inputSearch = inputSearch;
    }

    public LazyDataModel<PermissionGroupUserDTO> getPermissionGroupUserDTOList() {
        return permissionGroupUserDTOList;
    }

    public void setPermissionGroupUserDTOList(LazyDataModel<PermissionGroupUserDTO> permissionGroupUserDTOList) {
        this.permissionGroupUserDTOList = permissionGroupUserDTOList;
    }

    public PermissionGroupService getPermissionGroupService() {
        return permissionGroupService;
    }

    public void setPermissionGroupService(PermissionGroupService permissionGroupService) {
        this.permissionGroupService = permissionGroupService;
    }

    public PermissionManagerService getPermissionManagerService() {
        return permissionManagerService;
    }

    public void setPermissionManagerService(PermissionManagerService permissionManagerService) {
        this.permissionManagerService = permissionManagerService;
    }


    public List<PermissionGroupDTO> getSelectPermissionGroupDTOList() {
        return selectPermissionGroupDTOList;
    }

    public void setSelectPermissionGroupDTOList(List<PermissionGroupDTO> selectPermissionGroupDTOList) {
        this.selectPermissionGroupDTOList = selectPermissionGroupDTOList;
    }


    public boolean isCreate() {
        return create;
    }

    public void setCreate(boolean create) {
        this.create = create;
    }

    public List<ChainUserDTO> getSelectChainUserDTOLists() {
        return selectChainUserDTOLists;
    }

    public void setSelectChainUserDTOLists(List<ChainUserDTO> selectChainUserDTOLists) {
        this.selectChainUserDTOLists = selectChainUserDTOLists;
    }


    public PermissionGroupUserDTO getPermissionGroupUserDTO() {
        return permissionGroupUserDTO;
    }

    public void setPermissionGroupUserDTO(PermissionGroupUserDTO permissionGroupUserDTO) {
        this.permissionGroupUserDTO = permissionGroupUserDTO;
    }

    public List<Long> getChainUserIdCreateSelected() {
        return chainUserIdCreateSelected;
    }

    public void setChainUserIdCreateSelected(List<Long> chainUserIdCreateSelected) {
        this.chainUserIdCreateSelected = chainUserIdCreateSelected;
    }

    public List<Long> getPermissionGroupIdCreateSelected() {
        return permissionGroupIdCreateSelected;
    }

    public void setPermissionGroupIdCreateSelected(List<Long> permissionGroupIdCreateSelected) {
        this.permissionGroupIdCreateSelected = permissionGroupIdCreateSelected;
    }

    public Map<String, String> getLabelSelectCreateCheckBoxs() {
        return labelSelectCreateCheckBoxs;
    }

    public void setLabelSelectCreateCheckBoxs(Map<String, String> labelSelectCreateCheckBoxs) {
        this.labelSelectCreateCheckBoxs = labelSelectCreateCheckBoxs;
    }

    public Long getChainShopId() {
        return chainShopId;
    }

    public void setChainShopId(Long chainShopId) {
        this.chainShopId = chainShopId;
    }

    public List<ChainUserDTO> getChainUserDTOSearchList() {
        return chainUserDTOSearchList;
    }

    public void setChainUserDTOSearchList(List<ChainUserDTO> chainUserDTOSearchList) {
        this.chainUserDTOSearchList = chainUserDTOSearchList;
    }

    public InputLazyUserPermissionGroupCreate getInputLazyUserPermissionGroup() {
        return inputLazyUserPermissionGroup;
    }

    public void setInputLazyUserPermissionGroup(InputLazyUserPermissionGroupCreate inputLazyUserPermissionGroup) {
        this.inputLazyUserPermissionGroup = inputLazyUserPermissionGroup;
    }

    public List<ActionDTO> getActionDTOList() {
        return actionDTOList;
    }

    public void setActionDTOList(List<ActionDTO> actionDTOList) {
        this.actionDTOList = actionDTOList;
    }

    public boolean isViewUpdate() {
        return viewUpdate;
    }

    public void setViewUpdate(boolean viewUpdate) {
        this.viewUpdate = viewUpdate;
    }

    public List<ChainUserDTO> getChainUserDTOUpdateList() {
        return chainUserDTOUpdateList;
    }

    public void setChainUserDTOUpdateList(List<ChainUserDTO> chainUserDTOUpdateList) {
        this.chainUserDTOUpdateList = chainUserDTOUpdateList;
    }

    public List<ApDomainDTO> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<ApDomainDTO> statusList) {
        this.statusList = statusList;
    }

    public Long getChainShopIdSearch() {
        return chainShopIdSearch;
    }

    public void setChainShopIdSearch(Long chainShopIdSearch) {
        this.chainShopIdSearch = chainShopIdSearch;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getContentInBytes() {
        return contentInBytes;
    }

    public void setContentInBytes(byte[] contentInBytes) {
        this.contentInBytes = contentInBytes;
    }

    public boolean isRenderTemplateFile() {
        return renderTemplateFile;
    }

    public void setRenderTemplateFile(boolean renderTemplateFile) {
        this.renderTemplateFile = renderTemplateFile;
    }

    public boolean isRenderResultFileError() {
        return renderResultFileError;
    }

    public void setRenderResultFileError(boolean renderResultFileError) {
        this.renderResultFileError = renderResultFileError;
    }


    public static String[] getAllowExtensionTemplate() {
        return ALLOW_EXTENSION_TEMPLATE;
    }

    public boolean isExceptionHyperLink() {
        return isExceptionHyperLink;
    }

    public void setExceptionHyperLink(boolean exceptionHyperLink) {
        isExceptionHyperLink = exceptionHyperLink;
    }


    public boolean isImportMaxRecord() {
        return importMaxRecord;
    }

    public void setImportMaxRecord(boolean importMaxRecord) {
        this.importMaxRecord = importMaxRecord;

    }

    public List<ChainUserDTO> getChainUserDTOImportList() {
        return chainUserDTOImportList;
    }

    public void setChainUserDTOImportList(List<ChainUserDTO> chainUserDTOImportList) {
        this.chainUserDTOImportList = chainUserDTOImportList;
    }

    public List<PermissionGroupDTO> getPermissionGroupDTOImportList() {
        return permissionGroupDTOImportList;
    }

    public void setPermissionGroupDTOImportList(List<PermissionGroupDTO> permissionGroupDTOImportList) {
        this.permissionGroupDTOImportList = permissionGroupDTOImportList;
    }


    public List<PermissionGroupUserImportDTO> getImportExcelPermissionGroupUserDTO1List() {
        return importExcelPermissionGroupUserDTO1List;
    }

    public void setImportExcelPermissionGroupUserDTO1List(List<PermissionGroupUserImportDTO> importExcelPermissionGroupUserDTO1List) {
        this.importExcelPermissionGroupUserDTO1List = importExcelPermissionGroupUserDTO1List;
    }

    public ChainUserService getChainUserService() {
        return chainUserService;
    }

    public void setChainUserService(ChainUserService chainUserService) {
        this.chainUserService = chainUserService;
    }

    public List<PermissionGroupUserImportDTO> getImportExcelSuccessList() {
        return importExcelSuccessList;
    }

    public void setImportExcelSuccessList(List<PermissionGroupUserImportDTO> importExcelSuccessList) {
        this.importExcelSuccessList = importExcelSuccessList;
    }

    public List<PermissionGroupUserImportDTO> getImportExcelPermissionGroupUserDTOList() {
        return importExcelPermissionGroupUserDTOList;
    }

    public void setImportExcelPermissionGroupUserDTOList(List<PermissionGroupUserImportDTO> importExcelPermissionGroupUserDTOList) {
        this.importExcelPermissionGroupUserDTOList = importExcelPermissionGroupUserDTOList;
    }

    public Map<String, Long> getChainUserIdMap() {
        return chainUserIdMap;
    }

    public void setChainUserIdMap(Map<String, Long> chainUserIdMap) {
        this.chainUserIdMap = chainUserIdMap;
    }

    public Map<String, Long> getPermissionIdMap() {
        return permissionIdMap;
    }

    public void setPermissionIdMap(Map<String, Long> permissionIdMap) {
        this.permissionIdMap = permissionIdMap;
    }


}
