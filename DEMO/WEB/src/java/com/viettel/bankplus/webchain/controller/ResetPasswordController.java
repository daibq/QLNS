package com.viettel.bankplus.webchain.controller;

import com.viettel.bankplus.webchain.dto.ActionDTO;
import com.viettel.bankplus.webchain.dto.ActionLogDTO;
import com.viettel.bankplus.webchain.dto.ChainUserDTO;
import com.viettel.bankplus.webchain.service.ActionService;
import com.viettel.bankplus.webchain.service.ChainUserService;
import com.viettel.bankplus.webchain.utils.ActionUtil;
import com.viettel.bccs.fw.common.BccsLoginSuccessHandler;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.dto.BaseMessage;
import com.viettel.web.common.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import vn.com.viettel.bccs.api.SendSmsWebRequest;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import static com.viettel.fw.common.util.DataUtil.isNullOrEmpty;

/**
 * Created by Truongxp on 09/07/17.
 */
@Component
@Scope("view")
@ManagedBean
public class ResetPasswordController extends BaseController {
    private static final long serialVersionUID = 1;
    @Autowired
    private transient ChainUserService chainUserService;
    @Autowired
    private transient ActionService actionService;
    private String imageUrl;
    private String userName;
    // Capt for form 1
    private String captcha = "";
    // Capt for form 2
    private String captchaReset = "";
    private String newPassword;
    private String typeAgainPassword;
    private String securityCode;
    private String otpView = "";
    private ChainUserDTO cUser;
    private boolean viewReset;
    private String header;

    private List<ActionDTO> actionDTOList;

    public String getImageUrl() {
        return imageUrl + "?a=" + new SecureRandom().nextInt();
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @PostConstruct
    public void init() {
        //Graphic Text
        imageUrl = "captcha.jpg";
        viewReset = true;
        header = getText("reset.pass.get.code.security");
        try {
            actionDTOList = new ArrayList<>();
            actionDTOList = actionService.getListActionDTOByType(Const.ACTION_LOG.CHAIN_USER_TYPE);
            securityCode = null;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private String generateOTP() {

        int otpRandom = (int) ((new SecureRandom().nextInt()) * 90000 + 10000);
        String otp = String.valueOf(otpRandom).substring(1, 5);
        return otp;
    }


    private SendSmsWebRequest createSendSMSCustomer(ChainUserDTO chainUserDTOSMS) {
        SendSmsWebRequest sendSmsWebRequest = new SendSmsWebRequest();
        sendSmsWebRequest.setRequestDate(new Date());
        sendSmsWebRequest.setMsisdn(chainUserDTOSMS.getPhoneNumber());
        sendSmsWebRequest.setStaffUsername(userName);
        sendSmsWebRequest.setProcessCode(Const.WEB_CHAIN.SMS_OTP_RESET_PASSWORD);
        sendSmsWebRequest.setChainName(chainUserDTOSMS.getParentShopName());
        sendSmsWebRequest.setShopName(chainUserDTOSMS.getChainShopName());
        sendSmsWebRequest.setShopCode(BccsLoginSuccessHandler.getShopCode());
        sendSmsWebRequest.setObjectStaffUsername(userName);
//        sendSmsWebRequest.setObjectShopCode(chainUserDTOSMS.getChainShopCode());
//        sendSmsWebRequest.setObjectShopName(chainUserDTOSMS.getChainShopName());
        sendSmsWebRequest.setNewPIN(otpView);
        sendSmsWebRequest.setPartnerCode(chainUserDTOSMS.getChainShopCode());
        return sendSmsWebRequest;
    }

    public void checkUserName() {
        if (!validateResetPass()) {
            return;
        } else {
            header = getText("reset.pass.header");
        }
        try {
            securityCode = null;
            cUser = chainUserService.getValueByUserName(userName);
            if (DataUtil.isNullObject(cUser)) {
                viewReset = true;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.account.not.exist")));
                this.captcha = null;
                return;
            }
            String captchaStr = DataUtil.safeToString(getSession().getAttribute("captcha"));
            if (captcha.equals(captchaStr)) {
                viewReset = false;
                otpView = generateOTP();
                this.captcha = null;

/*//HungNQ
                SendSmsWebRequest request = createSendSMSCustomer(cUser);
                BaseMessage baseMessage = customerSmsService.sendSms(request);
                if (baseMessage.isSuccess()) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("reset.pass.system.send.code.minute")));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.system.send.code.error")));
                    viewReset = true;
                    this.captcha = null;
                }*/
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
            logger.error(e.getMessage(), e);
        }
    }

    public void closeForm() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            context.redirect(context.getRequestContextPath() + "/home");
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    //Form 1
    public boolean validateResetPass() {
        if (isNullOrEmpty(userName)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.accoount.not.blank")));
            return false;
        }
        if (isNullOrEmpty(captcha)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.code.confirm.not.blank")));
            this.captcha = null;
            return false;
        }
        String captchaStr = DataUtil.safeToString(getSession().getAttribute("captcha"));
        if (!captcha.equals(captchaStr)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.code.confirm.enter.fail")));
            this.captcha = null;
            return false;
        }
        return true;
    }

    // Form 2
    public void checkResetPass() {
        if (!validateChangePass())
            return;
        ActionLogDTO actionLogDTO = new ActionLogDTO();
        actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.CHAIN_USER_TYPE_UPDATE));
        actionLogDTO.setDescription(getText(Const.ACTION_LOG.CHAIN_USER_TYPE_UPDATE));
        String takeSalt = null;
        try {
            String captchaStr = DataUtil.safeToString(getSession().getAttribute("captcha"));
            takeSalt = chainUserService.takeSaltWithUser(userName);
            takeSalt = takeSalt == null ? "" : takeSalt.trim();
            if (!newPassword.equals(typeAgainPassword)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.change.not.math")));
                this.captchaReset = null;
            } else if (!isNullOrEmpty(newPassword) && !isNullOrEmpty(typeAgainPassword) && newPassword.equals(typeAgainPassword) && captchaReset.equals(captchaStr)) {
                // Get id to userName
                long id = chainUserService.findId(userName);
                BaseMessage baseMessage = chainUserService.changePassword(newPassword, userName, takeSalt, id, actionLogDTO);
                if (baseMessage.isSuccess()) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("pw.change.successful")));
                    this.captchaReset = null;
                }
            }
        } catch (Exception e) {
            logger.error(e);
        }
    }

    // Form 2
    public boolean validateChangePass() {
//        boolean isOk = true, checkCode = true;
        if (isNullOrEmpty(newPassword)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.new.not.blank")));
            this.captchaReset = null;
            return false;
        }
        if (isNullOrEmpty(typeAgainPassword)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.repeat.not.blank")));
            this.captchaReset = null;
            return false;
        }
        Pattern checkChar = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})");
        if (!isNullOrEmpty(newPassword) && !checkChar.matcher(newPassword).matches()) {
            FacesContext.getCurrentInstance().addMessage("frmChangePw:pwNew", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.char.special.and.MaxLengh")));
            this.captchaReset = null;
            return false;
        }
        if (!isNullOrEmpty(typeAgainPassword) && !checkChar.matcher(typeAgainPassword).matches()) {
            FacesContext.getCurrentInstance().addMessage("frmChangePw:pwRepeat", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("pw.char.special.and.MaxLengh")));
            this.captchaReset = null;
            return false;
        }
        if (isNullOrEmpty(securityCode)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.code.security.not.blank")));
            this.captchaReset = null;
            return false;
        }
        if (!DataUtil.isNullOrEmpty(securityCode) && securityCode.length() > 4) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.code.security.lengh")));
            this.captchaReset = null;
            return false;
        }
        Pattern code = Pattern.compile("[\\d]+");
        if (!code.matcher(securityCode).matches()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.code.security.must.number")));
            this.captchaReset = null;
            return false;
        }

        if (!securityCode.equalsIgnoreCase(otpView)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.code.security.not.right")));
            this.captchaReset = null;
            return false;
        }
        if (isNullOrEmpty(captchaReset)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.code.confirm.not.blank")));
            return false;
        }
        String captchaStr = DataUtil.safeToString(getSession().getAttribute("captcha"));
        if (!captchaReset.equals(captchaStr)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("reset.pass.not.right")));
            this.captchaReset = null;
            return false;
        }
        return true;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public boolean isViewReset() {
        return viewReset;
    }

    public void setViewReset(boolean viewReset) {
        this.viewReset = viewReset;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getTypeAgainPassword() {
        return typeAgainPassword;
    }

    public void setTypeAgainPassword(String typeAgainPassword) {
        this.typeAgainPassword = typeAgainPassword;
    }

    public String getCaptchaReset() {
        return captchaReset;
    }

    public void setCaptchaReset(String captchaReset) {
        this.captchaReset = captchaReset;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }
}
