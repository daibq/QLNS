package com.viettel.bankplus.webchain.controller;

import com.google.common.collect.Lists;
import com.viettel.bankplus.webchain.dto.*;
import com.viettel.bankplus.webchain.input.InputLazyFunctionGroupSearch;
import com.viettel.bankplus.webchain.service.ActionService;
import com.viettel.bankplus.webchain.service.ApDomainService;
import com.viettel.bankplus.webchain.service.ChainUserService;
import com.viettel.bankplus.webchain.service.PermissionGroupService;
import com.viettel.bankplus.webchain.utils.ActionUtil;
import com.viettel.bccs.fw.common.BccsLoginSuccessHandler;
import com.viettel.common.Const;
import com.viettel.fw.common.util.DataUtil;
import com.viettel.fw.dto.BaseMessage;
import com.viettel.utils.ExcelUtil;
import com.viettel.web.common.controller.BaseController;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

@ManagedBean
@Component
@Scope("view")
public class FunctionGroupController extends BaseController {
    private static final long serialVersionUID = 1;
    @Autowired
    private transient ApDomainService apDomainService;
    @Autowired
    private transient PermissionGroupService permissionGroupService;
    @Autowired
    private transient ActionService actionService;
    @Autowired
    private transient ChainUserService chainUserService;


    private List<ActionDTO> actionDTOList;
    private transient InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch;
    private List<ApDomainDTO> listStatus = new ArrayList<>();
    private List<PermissionGroupDTO> listPermissionCode = new ArrayList<>();
    private List<ApDomainDTO> listUserObjectType = new ArrayList<>();
    private LazyDataModel<PermissionGroupDTO> permissionGroupDTOS;
    private PermissionGroupDTO permissionGroupDTO;
    private boolean isCreateState;
    private LazyDataModel<FunctionObjectDTO> functionObjectDTOS;
    private PermissionGroupDTO dtoDEtail;
    private List<PermissionGroupDTO> permissionGroupDTOExportList;
    private File fileExport;
    private String suffix;
    private String templatePath;
    private Map<String, String> functionGruopStatusMap;
    private Map<String, String> permissionTypeMap;
    private ChainUserDTO chainUserDTO;

    //xu ly ve file
    private String fileData;
    private transient UploadedFile uploadedFile;
    private String name;
    private byte[] contentInBytes;
    private boolean renderTemplateFile;
    private static final String FILE_NAME = "FUNCTION_GROUP.xlsx";
    private boolean renderResultFileError;
    private List<PermissionGroupDTO> importExcelFuntionGroupDTOList;
    private List<PermissionGroupDTO> importExcelSuccessList;
    private static final String[] ALLOW_EXTENSION_TEMPLATE = {"xls", "xlsx"};
    private boolean isExceptionHyperLink;
    private List<PermissionGroupDTO> importExcelChainShopDTOList;
    private boolean importMaxRecord = false;

    @PostConstruct
    public void init() {
        permissionGroupDTO = new PermissionGroupDTO();

        inputLazyFunctionGroupSearch = new InputLazyFunctionGroupSearch();
        inputLazyFunctionGroupSearch.setChainShopId(BccsLoginSuccessHandler.getShopId());
        templatePath = Const.WEB_CHAIN.FUNCTION_GROUP_EXCEL_TEMPLATE_PATH;
        try {
            chainUserDTO = chainUserService.findOne(BccsLoginSuccessHandler.getChainUserType());
            listStatus = apDomainService.getValueApDomainByType(Const.AP_DOMAIN.WC_STATUS);
            listUserObjectType = apDomainService.getValueApDomainByType(Const.AP_DOMAIN.WC_CHAIN_USER);
            listPermissionCode = permissionGroupService.getPermissionCodeListByShopId(BccsLoginSuccessHandler.getShopId());
            actionDTOList = actionService.getListActionDTOByType(Const.ACTION_LOG.PERMISSION_GROUP_TYPE);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        /*permissionGroupDTOS = findLazy();*/
        getNameforExcel();
    }

    // lấy tên theo giá trị đưa vào excel
    public void getNameforExcel() {
        functionGruopStatusMap = new HashMap<>();
        permissionTypeMap = new HashMap<>();
        if (!listStatus.isEmpty()) {
            for (ApDomainDTO apDomainDTO : listStatus) {
                functionGruopStatusMap.put(apDomainDTO.getValue().trim().toLowerCase(), apDomainDTO.getName());
            }
        }
        if (!listUserObjectType.isEmpty()) {
            for (ApDomainDTO apDomainDTO : listUserObjectType) {
                permissionTypeMap.put(apDomainDTO.getValue().trim().toLowerCase(), apDomainDTO.getName());
            }
        }

    }

    public void prepareToShowAdd() {
        permissionGroupDTO = new PermissionGroupDTO();
    }

    public void prepareToShowEdit(PermissionGroupDTO permissionGroupDtoOld) {
        permissionGroupDTO = new PermissionGroupDTO();
        BeanUtils.copyProperties(permissionGroupDtoOld, permissionGroupDTO);

    }

    private LazyDataModel<PermissionGroupDTO> findLazy() {
        return new LazyDataModel<PermissionGroupDTO>() {
            @Override
            public List<PermissionGroupDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    if (sortField != null && sortOrder != null) {
                        if (SortOrder.ASCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.ASC, sortField);
                        } else if (SortOrder.DESCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.DESC, sortField);
                        }
                    }
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(permissionGroupService.countLazyFunctionGroup(inputLazyFunctionGroupSearch));
                    return permissionGroupService.findLazingPaging(inputLazyFunctionGroupSearch, page.getPageSize(), page.getPageNumber());
                } catch (Exception e) {
                    logger.error("findLazy", e);
                    return Lists.newArrayList();
                }
            }


            @Override
            public PermissionGroupDTO getRowData(String rowKey) {
                List<PermissionGroupDTO> tmp = (List<PermissionGroupDTO>) getWrappedData();
                Optional<PermissionGroupDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getPermissionGroupId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(PermissionGroupDTO object) {
                return object != null ? object.getPermissionGroupId() : null;
            }
        };

    }

    /*
   * check null các trường có trong form của danh mục tham số
   */
    private boolean validateAddNew() {
        Pattern docCode;
        docCode = Pattern.compile("[\\d\\w_]+");
        Pattern pattern;
        pattern = Pattern.compile("[\\d\\w_ ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+");
        if (DataUtil.isNullObject(permissionGroupDTO.getPermissionGroupCode())) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.permissionGroupCodeEmpty")));
            return false;
        }
        if (permissionGroupDTO.getPermissionGroupCode().length() > 50) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.NotImport.maxLengh.permissionGroupCode")));
            return false;
        }
        if (!docCode.matcher(permissionGroupDTO.getPermissionGroupCode()).matches()) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.NotImportWord.permissionGroupCode")));
            return false;
        }

        if (DataUtil.isNullObject(permissionGroupDTO.getPermissionGroupName())) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.permissionGroupNameEmpty")));
            return false;
        }

        if (permissionGroupDTO.getPermissionGroupName().length() > 200) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.NotImport.maxLengh.permissionGroupName")));
            return false;
        }
        if (permissionGroupDTO.getDescription().length() > 500) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.NotImport.maxLengh.permissionGroupDescription")));
            return false;
        }
        if (!pattern.matcher(permissionGroupDTO.getPermissionGroupName()).matches()) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.NotImportWord.permissionGroupName")));
            return false;
        }

        if (DataUtil.isNullObject(permissionGroupDTO.getUserObjectType())) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.permissionGroupTypeEmpty")));
            return false;
        }
        if (DataUtil.isNullObject(permissionGroupDTO.getStatus())) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.permissionGroupStatusEmpty")));
            return false;
        }


        return true;
    }

    /*
   * check code của nhóm chức năng
   */
    public boolean validateCodeBeforeSave() {

        try {
            boolean isDuplicateCode = permissionGroupService.checkCodeOrId(permissionGroupDTO.getPermissionGroupCode(), null);
            if (!isDuplicateCode) {
                FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                        getText("webchain.functionGroup.duplicate.permissionGroupCode")));
                return false;
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.err")));
            logger.error(e.getMessage(), e);
        }
        return true;
    }

    public boolean validateNameBeforeSave() {

        try {
            boolean isDuplicateName = permissionGroupService.checkName(BccsLoginSuccessHandler.getShopId(), permissionGroupDTO.getPermissionGroupName());
            if (!isDuplicateName) {
                FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.duplicate.permissionGroupName")));
                return false;

            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.err")));
            logger.error(e.getMessage(), e);
        }
        return true;
    }

      /*
    * check bản ghi còn tồn tại hay không của nhóm chức năng
    */

    public boolean checkRecordExist(PermissionGroupDTO dto) {
        try {
            boolean isRecordExist = permissionGroupService.checkCodeOrId(null, dto.getPermissionGroupId());
            if (isRecordExist) {
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.exist")));
                RequestContext.getCurrentInstance().execute("PF('dlgFunctionGroup').hide()");
                return false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return true;
    }

    /*
* check va update bản ghi của danh mục tham số
*/
    public boolean checkValidateUpdateAll() {

        try {
            if (chainUserDTO.getChainUserType() == 1) {
                permissionGroupDTO.setPermissionGroupType(2l);

            } else if (chainUserDTO.getChainUserType() == 2) {
                permissionGroupDTO.setPermissionGroupType(3l);
            } else {
                permissionGroupDTO.setPermissionGroupType(3l);
            }
            ActionLogDTO actionLogDTO = new ActionLogDTO();
            actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.PERMISSION_GROUP_TYPE_UPDATE));
            actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_TYPE_UPDATE));
            Long statusValue = DataUtil.isNullObject(permissionGroupDTO.getStatus()) ? 1 : permissionGroupDTO.getStatus();
            PermissionGroupDTO oldPermissionGroupDTO = permissionGroupService.findOne(permissionGroupDTO.getPermissionGroupId());
            String oldCode = oldPermissionGroupDTO.getPermissionGroupCode();
            String oldName = oldPermissionGroupDTO.getPermissionGroupName();
            if (!DataUtil.safeEqual(oldCode.toUpperCase(), permissionGroupDTO.getPermissionGroupCode().toUpperCase())) {
                if (validateCodeBeforeSave()) {
//                    apDomainDTO.setStatus(1L);
                    permissionGroupDTO.setStatus(statusValue);
                    actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                    permissionGroupDTO.setChainShopId(BccsLoginSuccessHandler.getShopId());
                    permissionGroupDTO.setUpdateUser(BccsLoginSuccessHandler.getUserName());
                    permissionGroupService.saveorUodate(permissionGroupDTO, actionLogDTO);
                    RequestContext.getCurrentInstance().execute("PF('dlgFunctionGroup').hide()");
                    FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.functionGroup.update.success")));
                }
            } else if (!DataUtil.safeEqual(oldName, permissionGroupDTO.getPermissionGroupName())) {
                if (validateNameBeforeSave()) {
                    permissionGroupDTO.setStatus(statusValue);
                    actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                    permissionGroupDTO.setChainShopId(BccsLoginSuccessHandler.getShopId());
                    permissionGroupDTO.setUpdateUser(BccsLoginSuccessHandler.getUserName());
                    permissionGroupService.saveorUodate(permissionGroupDTO, actionLogDTO);
                    RequestContext.getCurrentInstance().execute("PF('dlgFunctionGroup').hide()");
                    FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.functionGroup.update.success")));
                }
            } else {
//            apDomainDTO.setStatus(1L);
                permissionGroupDTO.setStatus(statusValue);
                actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                permissionGroupDTO.setChainShopId(BccsLoginSuccessHandler.getShopId());
                permissionGroupDTO.setUpdateUser(BccsLoginSuccessHandler.getUserName());
                permissionGroupService.saveorUodate(permissionGroupDTO, actionLogDTO);
                RequestContext.getCurrentInstance().execute("PF('dlgFunctionGroup').hide()");
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.functionGroup.update.success")));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.err")));
            logger.error(e.getMessage(), e);
        }


        return true;
    }

    @Secured("@")
    public void doSearch() {
        try {
            permissionGroupDTOS = findLazy();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void doSaveorUpdate() {
        ActionLogDTO actionLogDTO = new ActionLogDTO();
        try {
            if (!(validateAddNew()))
                return;
            Long statusValue = DataUtil.isNullObject(permissionGroupDTO.getStatus()) ? 1 : permissionGroupDTO.getStatus();
            isCreateState = permissionGroupDTO.getPermissionGroupId() == null ? true : false;
            if (isCreateState) {
                if (validateCodeBeforeSave() && validateNameBeforeSave()) {
                    if (chainUserDTO.getChainUserType() == 1) {
                        permissionGroupDTO.setPermissionGroupType(2l);

                    } else if (chainUserDTO.getChainUserType() == 2) {
                        permissionGroupDTO.setPermissionGroupType(3l);
                    } else {
                        permissionGroupDTO.setPermissionGroupType(3l);
                    }

                    permissionGroupDTO.setStatus(statusValue);
                    actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
                    actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.PERMISSION_GROUP_TYPE_ADD_NEW));
                    actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_TYPE_ADD_NEW));
                    permissionGroupDTO.setChainShopId(BccsLoginSuccessHandler.getShopId());
                    permissionGroupDTO.setCreateUser(BccsLoginSuccessHandler.getUserName());
                    permissionGroupService.saveorUodate(permissionGroupDTO, actionLogDTO);
                    RequestContext.getCurrentInstance().execute("PF('dlgFunctionGroup').hide()");
                    FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.functionGroup.create.success")));

                }
            } else {
                checkValidateUpdateAll();
            }
            listPermissionCode = permissionGroupService.getPermissionCodeListByShopId(BccsLoginSuccessHandler.getShopId());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.functionGroup.err")));
        }
    }

    public void doDelete(PermissionGroupDTO dto) {
        ActionLogDTO actionLogDTO = new ActionLogDTO();
        actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.PERMISSION_GROUP_TYPE_DELETE));
        actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_TYPE_DELETE));
        actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
        if (chainUserDTO.getChainUserType() == 1) {
            dto.setPermissionGroupType(2l);

        } else if (chainUserDTO.getChainUserType() == 2) {
            dto.setPermissionGroupType(3l);
        } else {
            dto.setPermissionGroupType(3l);
        }
        try {
            if (checkRecordExist(dto)) {
                permissionGroupService.deleteList(dto, actionLogDTO, BccsLoginSuccessHandler.getShopId());
                FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_INFO,
                        null, getText("webchain.functionGroup.delete.success")));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage("messages", new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("webchain.catalog.err")));
        }

    }

    public void doViewDetail(PermissionGroupDTO dto) {
        try {
            dtoDEtail = new PermissionGroupDTO();
            BeanUtils.copyProperties(dto, dtoDEtail);
            functionObjectDTOS = lazyViewDetail();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }

    /*
    * Import file - Start
    */
    public void importDataByExcel() {
        try {
            importMaxRecord = false;
            importExcelFuntionGroupDTOList = new ArrayList<>();
            importExcelSuccessList = new ArrayList<>();
            BaseMessage message = validateFileUploadWhiteList(uploadedFile, ALLOW_EXTENSION_TEMPLATE, Const.WEB_CHAIN.MAX_SIZE_5M);
            if (!message.isSuccess()) {
                fileData = null;
                contentInBytes = null;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, message.getDescription()));
                return;
            }
            // check file mau template
            if (!checkFileTemplate(contentInBytes)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.import.file.error.template")));
                return;
            }
            if (contentInBytes == null || DataUtil.isNullOrEmpty(fileData)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.import.file.error.empty")));
                return;
            }
            ActionLogDTO actionLogDTO = new ActionLogDTO();
            actionLogDTO.setUserName(BccsLoginSuccessHandler.getUserName());
            actionLogDTO.setActionId(ActionUtil.getActionIdByCode(actionDTOList, Const.ACTION_LOG.PERMISSION_GROUP_TYPE_IMPORT));
            actionLogDTO.setDescription(getText(Const.ACTION_LOG.PERMISSION_GROUP_TYPE_IMPORT));
            importExcelSuccessList = readDataInFile(contentInBytes);
            if (isExceptionHyperLink) {
                return;
            }

            if (!DataUtil.isNullOrEmpty(importExcelSuccessList)) {
                List<PermissionGroupDTO> listResult = permissionGroupService.importChainShopTemplate(importExcelSuccessList, actionLogDTO);
                renderResultFileError = true;
                renderTemplateFile = false;
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.functionGroup.import.successful")));
                importExcelFuntionGroupDTOList.addAll(0, listResult);
                importExcelSuccessList.clear();
                fileData = null;
                uploadedFile = null;
            } else if (DataUtil.isNullOrEmpty(importExcelFuntionGroupDTOList)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.import.file.error.empty")));
            } else {
                fileData = null;
                uploadedFile = null;
                if (importMaxRecord) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.import.file.error.file.max.record")));
                } else {
                    renderResultFileError = true;
                    renderTemplateFile = false;
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, getText("webchain.functionGroup.import.successful")));
                }
                importMaxRecord = false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    public void preImport() {
        renderResultFileError = false;
        renderTemplateFile = true;
        fileData = null;
        uploadedFile = null;
        contentInBytes = null;
        importExcelSuccessList = new ArrayList<>();
        importExcelFuntionGroupDTOList = new ArrayList<>();
    }

    @Secured("@")
    public void fileUploadAction(FileUploadEvent event) {
        try {
            uploadedFile = event.getFile();
            BaseMessage message = validateFileUploadWhiteList(uploadedFile, ALLOW_EXTENSION_TEMPLATE, Const.WEB_CHAIN.MAX_SIZE_10M);
            if (!message.isSuccess()) {
                fileData = null;
                focusElementByRawCSSSlector(".outputAttachFiledata");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, message.getDescription()));
                return;
            }
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
            name = fmt.format(new Date())
                    + uploadedFile.getFileName().substring(uploadedFile.getFileName().lastIndexOf('.'));
            contentInBytes = uploadedFile.getContents();
            fileData = uploadedFile.getFileName();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    @Secured("@")
    public void downloadFileTemplate() {
        try {
            InputStream createStream = Faces.getResourceAsStream(Const.RESOURCE_TEMPLATE.RESOURCE_TEMPLATE_PATH + Const.FUNCTION_GROUP.FILE_NAME);
            XSSFWorkbook workbook = new XSSFWorkbook(createStream);
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + FILE_NAME + "\"");
            externalContext.setResponseContentType("application/excel");
            workbook.write(externalContext.getResponseOutputStream());
            facesContext.responseComplete();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    @Secured("@")
    public void downloadFileResult() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            InputStream createStream = Faces.getResourceAsStream(Const.RESOURCE_TEMPLATE.RESOURCE_TEMPLATE_PATH + Const.FUNCTION_GROUP.FILE_NAME_RESULT + ".xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook(createStream);
            XSSFSheet sheet = workbook.getSheetAt(0);


            XSSFCellStyle styleError = workbook.createCellStyle();
            styleError.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleError.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleError.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleError.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            Font fontError = workbook.createFont();
            fontError.setColor(IndexedColors.RED.getIndex());
            styleError.setFont(fontError);
            styleError.setWrapText(true);

            XSSFCellStyle styleSuccess = workbook.createCellStyle();
            styleSuccess.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleSuccess.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleSuccess.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleSuccess.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            Font fontSuccess = workbook.createFont();
            fontSuccess.setColor(IndexedColors.GREEN.getIndex());
            styleSuccess.setFont(fontSuccess);
            styleSuccess.setWrapText(true);

            for (int i = 0; i < importExcelFuntionGroupDTOList.size(); i++) {
                PermissionGroupDTO dtoExt = importExcelFuntionGroupDTOList.get(i);
                String result = dtoExt.getImportExcelError();
                XSSFCellStyle style;
                style = "1".equals(result) ? styleSuccess : styleError;

                sheet.setAutobreaks(true);
                XSSFRow row = sheet.createRow(i + 1);
                //STT
                XSSFCell cell00 = row.createCell(0);
                cell00.setCellStyle(style);
                cell00.setCellValue((i + 1));
                // ma nhom chuc nang
                XSSFCell cell01 = row.createCell(1);
                cell01.setCellStyle(style);
                cell01.setCellValue(dtoExt.getPermissionGroupCode());
                // ten nhom chuc nang
                XSSFCell cell02 = row.createCell(2);
                cell02.setCellStyle(style);
                cell02.setCellValue(dtoExt.getPermissionGroupName());
                //  doi tuong su dung
                XSSFCell cell03 = row.createCell(3);
                cell03.setCellStyle(style);
                if (DataUtil.isNullOrEmpty(dtoExt.getStrPermissionGroupType())) {
                    cell03.setCellValue(DataUtil.safeToString(dtoExt.getPermissionGroupType()));
                } else {
                    cell03.setCellValue(DataUtil.safeToString(dtoExt.getStrPermissionGroupType()));
                }
                // mo ta
                XSSFCell cell04 = row.createCell(4);
                cell04.setCellStyle(style);
                cell04.setCellValue(dtoExt.getDescription());
                // trang thai
                XSSFCell cell05 = row.createCell(5);
                cell05.setCellStyle(style);
                if (DataUtil.isNullOrEmpty(dtoExt.getStrStatus())) {
                    cell05.setCellValue(DataUtil.safeToString(dtoExt.getStatus()));
                } else {
                    cell05.setCellValue(DataUtil.safeToString(dtoExt.getStrStatus()));
                }
                // ketqua
                XSSFCell cell06 = row.createCell(6);
                cell06.setCellStyle(style);
                if ("0".equals(result)) {
                    cell06.setCellValue(getText("webchain.shop.import.notOK"));
                } else if ("1".equals(result)) {
                    cell06.setCellValue(getText("webchain.shop.import.OK"));
                } else {
                    cell06.setCellValue(result);
                }

            }
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"" + Const.FUNCTION_GROUP.FILE_NAME_RESULT + sdf.format(new Date()) + ".xlsx");
            externalContext.setResponseContentType("application/excel");
            workbook.write(externalContext.getResponseOutputStream());
            facesContext.responseComplete();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
    }

    private List<PermissionGroupDTO> readDataInFile(byte[] contentInBytes) {
        List<PermissionGroupDTO> importList = new ArrayList<>();
        PermissionGroupDTO dto;
        importMaxRecord = false;
        importExcelChainShopDTOList = new ArrayList<>();
        isExceptionHyperLink = false;
        ExcelUtil ex = null;
        try {
            ex = new ExcelUtil(uploadedFile, contentInBytes);
            Sheet sheet = ex.getWorkbook().getSheetAt(0);
            Row xRow = sheet.getRow(0);

            int totalCol = xRow.getLastCellNum();
            if (totalCol < 6) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.import.file.error.template")));
                return new ArrayList();
            }
            List<Integer> rowIgnore = new ArrayList<>();
            Map<String, String> duplicateCodeMap = new ConcurrentHashMap<>();
            Map<String, String> duplicateNameMap = new ConcurrentHashMap<>();
            int totalRow = 1;
            //validate 100 bản ghi
            for (int row = 1; row <= sheet.getLastRowNum(); row++) {
                xRow = sheet.getRow(row);
                if (totalRow > 100) {
                    importMaxRecord = true;
                    importExcelChainShopDTOList.addAll(0, importList);
                    return new ArrayList();
                }
                if (xRow != null && xRow.getFirstCellNum() < 6) {
                    dto = new PermissionGroupDTO();
                    String code = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(1)))) {
                        code = ExcelUtil.getStringValue(xRow.getCell(1)).trim();
                    }
                    String name = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(2)))) {
                        name = ExcelUtil.getStringValue(xRow.getCell(2)).trim();
                    }
                    String userObjectType = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(3)))) {
                        userObjectType = ExcelUtil.getStringValue(xRow.getCell(3)).trim().toUpperCase();
                    }
                    String description = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(4)))) {
                        description = ExcelUtil.getStringValue(xRow.getCell(4)).trim().toUpperCase();
                    }
                    String status = null;
                    if (!DataUtil.isNullObject(ExcelUtil.getStringValue(xRow.getCell(5)))) {
                        status = ExcelUtil.getStringValue(xRow.getCell(5)).trim();
                    }
                    if (DataUtil.isNullOrEmpty(code) && DataUtil.isNullOrEmpty(name) &&
                            DataUtil.isNullOrEmpty(userObjectType) && DataUtil.isNullOrEmpty(description)) {
                        rowIgnore.add(row);
                        continue;
                    }
                    totalRow++;
                    String result = validate(dto, duplicateCodeMap, duplicateNameMap, code, name, userObjectType, description, status);
                    if (!DataUtil.isNullOrEmpty(result)) {
                        dto.setImportExcelError(Const.WEB_CHAIN.IMPORT_ERROR + "! " + result);
                        importExcelFuntionGroupDTOList.add(dto);
                    } else {
                        if (chainUserDTO.getChainUserType() == 1) {
                            dto.setPermissionGroupType(2l);

                        } else if (chainUserDTO.getChainUserType() == 2) {
                            dto.setPermissionGroupType(3l);
                        } else {
                            dto.setPermissionGroupType(3l);
                        }
                        dto.setChainShopId(BccsLoginSuccessHandler.getShopId());
                        dto.setCreateUser(BccsLoginSuccessHandler.getUserName());
                        dto.setImportExcelError(null);
                        importList.add(dto);
                    }

                }
            }
        } catch (Exception e) {
            if (ex == null) {
                FacesContext context = FacesContext.getCurrentInstance();
                Iterator<FacesMessage> it = context.getMessages();
                while (it.hasNext()) {
                    it.next();
                    it.remove();
                }
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, null,
                                getText("common.import.file.error.file.hyperLink")));
                isExceptionHyperLink = true;
                return new ArrayList();
            }
            dto = new PermissionGroupDTO();
            dto.setImportExcelError(Const.WEB_CHAIN.IMPORT_ERROR + getText("common.error.happened.againAction"));
            importExcelFuntionGroupDTOList.add(dto);
            logger.error(e.getMessage(), e);

        }
        return importList;
    }

    private String validate(PermissionGroupDTO dto, Map<String, String> duplicateCodeMap, Map<String, String> duplicateNameMap, String code, String name, String
            userObjectType, String description, String status) throws Exception {
        StringBuilder result = new java.lang.StringBuilder();
        // check shop code
        if (DataUtil.isNullOrEmpty(code)) {
            result.append(getText("webchain.functionGroup.permissionGroupCodeEmpty") + ". ");
        } else {
            dto.setPermissionGroupCode(code);
            if (code.length() > Const.FUNCTION_GROUP.MAXLENGTH_CODE) {
                result.append(getText("webchain.functionGroup.NotImport.maxLengh.permissionGroupCode") + ". ");
            } else if (!DataUtil.isCheckSpecialCharacter(code)) {
                result.append(getText("webchain.functionGroup.NotImportWord.permissionGroupCode") + ". ");
            } else {
                if (permissionGroupService.checkShopCodeOrNameExist(BccsLoginSuccessHandler.getShopId(), code, null)) {
                    result.append(getText("webchain.functionGroup.duplicate.permissionGroupCode") + ". ");
                } else {
                    dto.setPermissionGroupCode(code);
                }
            }
        }

        // check shop name
        if (DataUtil.isNullOrEmpty(name)) {
            result.append(getText("webchain.functionGroup.permissionGroupNameEmpty") + ". ");
        } else {
            dto.setPermissionGroupName(name);
            if (name.length() > Const.FUNCTION_GROUP.MAXLENGTH_NAME) {
                result.append(getText("webchain.functionGroup.NotImport.maxLengh.permissionGroupName") + ". ");
            } else if (permissionGroupService.checkShopCodeOrNameExist(BccsLoginSuccessHandler.getShopId(), null, name)) {
                result.append(getText("webchain.functionGroup.duplicate.permissionGroupName") + ". ");
            } else {
                dto.setPermissionGroupName(name);
            }
        }
        // check userObjectType
        if (DataUtil.isNullOrEmpty(userObjectType)) {
            result.append(getText("validate.webchain.functionGroup.permissionGroupType.empty") + ". ");
        } else {
            if ("1".equals(userObjectType.trim()) || "2".equals(userObjectType.trim()) || "3".equals(userObjectType.trim())) {
                dto.setUserObjectType(Long.valueOf(userObjectType));
            } else {
                dto.setStrPermissionGroupType(userObjectType);
                result.append(getText("validate.webchain.functionGroup.permissionGroupType.format") + ". ");
            }
        }

        // check description
        if (DataUtil.isNullOrEmpty(description)) {
            dto.setDescription(description);
        } else if (description.length() > Const.FUNCTION_GROUP.MAXLENGTH_DESCRIPTION) {
            result.append(getText("validate.webchain.shop.address.len.500") + ". ");
        } else {
            dto.setDescription(description);
        }

        // check status
        if (DataUtil.isNullOrEmpty(status)) {
            dto.setStatus(DataUtil.safeToLong(Const.WEB_CHAIN.STATUS_ACTIVE));
        } else {
            if (Const.WEB_CHAIN.STATUS_IN_ACTIVE.equals(status.trim()) || Const.WEB_CHAIN.STATUS_ACTIVE.equals(status.trim())) {
                dto.setStatus(Long.valueOf(status));
            } else {
                dto.setStrStatus(status);
                result.append(getText("validate.webchain.functionGroup.status.format") + ". ");
            }

        }
        // Check trung ten, ma
        if (DataUtil.isNullOrEmpty(result.toString())) {
            String tempName = duplicateNameMap.get(name.toLowerCase());
            if (null != tempName) {
                result.append(getText("validate.webchain.functionGroup.name.file.exist") + ". ");
            } else {
                duplicateNameMap.put(name.toLowerCase(), name.toLowerCase());
            }
        }
        if (DataUtil.isNullOrEmpty(result.toString())) {
            String tempCode = duplicateCodeMap.get(code.toLowerCase());
            if (null != tempCode) {
                result.append(getText("validate.webchain.functionGroup.code.file.exist") + ". ");
            } else {
                duplicateCodeMap.put(code.toLowerCase(), code.toLowerCase());
            }
        }

        return result.toString();
    }

    private boolean checkFileTemplate(byte[] contentByte) {
        try {
            ExcelUtil ex = new ExcelUtil(uploadedFile, contentByte);
            Sheet sheet = ex.getWorkbook().getSheetAt(0);
            Row row2 = sheet.getRow(0);
            if (null == row2)
                return false;
            int totalCol = row2.getLastCellNum();
            if (totalCol < 6)
                return false;
            // ma nhom chuc nang
            String functionGroupCode = ExcelUtil.getStringValue(row2.getCell(1)).trim();
            if (!getText("webchain.functionGroup.permissionGroupCode").equalsIgnoreCase(functionGroupCode.trim())) {
                return false;
            }
            // ten nhom chuc nang
            String functionGroupName = ExcelUtil.getStringValue(row2.getCell(2)).trim();
            if (!getText("webchain.functionGroup.permissionGroupName").equalsIgnoreCase(functionGroupName)) {
                return false;
            }
            // doi tuong
            String userObjectType = ExcelUtil.getStringValue(row2.getCell(3)).trim();
            if (!getText("webchain.functionGroup.permissionGroupType").equalsIgnoreCase(userObjectType)) {
                return false;
            }
            // mo ta
            String description = ExcelUtil.getStringValue(row2.getCell(4)).trim();
            if (!getText("webchain.functionGroup.permissionGroupDescription").equalsIgnoreCase(description)) {
                return false;
            }
            // trang thai
            String status = ExcelUtil.getStringValue(row2.getCell(5)).trim();
            if (!getText("webchain.functionGroup.status").equalsIgnoreCase(status)) {
                return false;
            }

            return true;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, null, getText("common.error.happened.againAction")));
        }
        return true;
    }
    /*
    * Import file - END
    */

    /**
     * @throws Exception
     */
    public void prepareDownloadFileExport() throws Exception {

        permissionGroupDTOExportList = permissionGroupService.findLazingPaging(inputLazyFunctionGroupSearch, 0, 0);
    }

    /**
     * download excel file
     *
     * @return
     */
    public StreamedContent downloadFile() {
        createFile();
        InputStream stream;
        suffix = templatePath.substring(templatePath.lastIndexOf(".") + 1, templatePath.length());
        try {
            stream = new FileInputStream(fileExport);
            return new DefaultStreamedContent(stream, "application/excel", getText("webchain.functionGroup.export.name") + "." + suffix);
        } catch (FileNotFoundException ex) {
            logger.error(ex.toString(), ex);
            reportError("", "", "common.error.happened.againAction");
        }
        return null;
    }

    /**
     * create excel file
     */
    public void createFile() {
        try {
            Workbook workbook = WorkbookFactory.create(Faces.getResourceAsStream(templatePath));
            Sheet sheet = workbook.getSheetAt(0);
            CellStyle cs = workbook.createCellStyle();
            cs.setBorderRight(CellStyle.BORDER_THIN);
            cs.setRightBorderColor(IndexedColors.BLACK.getIndex());
            cs.setBorderBottom(CellStyle.BORDER_THIN);
            cs.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            cs.setBorderLeft(CellStyle.BORDER_THIN);
            cs.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            cs.setBorderTop(CellStyle.BORDER_THIN);
            cs.setTopBorderColor(IndexedColors.BLACK.getIndex());
            cs.setWrapText(true);

            Font font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            CellStyle csHeader = workbook.createCellStyle();
            csHeader.setBorderRight(CellStyle.BORDER_THIN);
            csHeader.setRightBorderColor(IndexedColors.BLACK.getIndex());
            csHeader.setBorderBottom(CellStyle.BORDER_THIN);
            csHeader.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            csHeader.setBorderLeft(CellStyle.BORDER_THIN);
            csHeader.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            csHeader.setBorderTop(CellStyle.BORDER_THIN);
            csHeader.setTopBorderColor(IndexedColors.BLACK.getIndex());
            csHeader.setFont(font);
            csHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            csHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
            int row = 2;
            if (!DataUtil.isNullOrEmpty(permissionGroupDTOExportList)) {
                for (PermissionGroupDTO permissionGroupDTOExport : permissionGroupDTOExportList) {
                    ++row;
                    Row xRow = sheet.createRow(row);
                    Cell cell0 = xRow.createCell(0);
                    cell0.setCellValue((row - 2));
                    cell0.setCellStyle(cs);
                    // Mã CN
                    Cell cell1 = xRow.createCell(1);
                    cell1.setCellValue(permissionGroupDTOExport.getPermissionGroupCode());
                    cell1.setCellStyle(cs);
                    // tên chức năng
                    Cell cell2 = xRow.createCell(2);
                    cell2.setCellValue(permissionGroupDTOExport.getPermissionGroupName());
                    cell2.setCellStyle(cs);
                    // đối tượng sử dụng
                    Cell cell3 = xRow.createCell(3);
                    cell3.setCellValue(permissionTypeMap.get(permissionGroupDTOExport.getUserObjectType().toString()));
                    cell3.setCellStyle(cs);
                    // mô tả
                    Cell cell4 = xRow.createCell(4);
                    cell4.setCellValue(permissionGroupDTOExport.getDescription());
                    cell4.setCellStyle(cs);
                    // trạng thái
                    Cell cell5 = xRow.createCell(5);
                    cell5.setCellValue(functionGruopStatusMap.get(permissionGroupDTOExport.getStatus().toString()));
                    cell5.setCellStyle(cs);
                    // Ngày tạo
                    SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    String date = ft.format(permissionGroupDTOExport.getCreateDate());
                    Cell cell6 = xRow.createCell(6);
                    cell6.setCellValue(date);
                    cell6.setCellStyle(cs);
                    // người tạo
                    Cell cell7 = xRow.createCell(7);
                    cell7.setCellValue(permissionGroupDTOExport.getCreateUser());
                    cell7.setCellStyle(cs);
                }

            }
            fileExport = File.createTempFile("exportFile", suffix);
            FileOutputStream fileOut = new FileOutputStream(fileExport);
            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        } catch (Exception ex) {
            reportError("", "", "common.error.happened.againAction");
            logger.error(ex.getMessage(), ex);
        }
    }

    private LazyDataModel<FunctionObjectDTO> lazyViewDetail() {
        return new LazyDataModel<FunctionObjectDTO>() {
            @Override
            public List<FunctionObjectDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                try {
                    Sort order = null;
                    if (sortField != null && sortOrder != null) {
                        if (SortOrder.ASCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.ASC, sortField);
                        } else if (SortOrder.DESCENDING == sortOrder) {
                            order = new Sort(Sort.Direction.DESC, sortField);
                        }
                    }
                    Pageable page = new PageRequest(first / pageSize, pageSize, order);
                    setRowCount(permissionGroupService.listDetailCount(dtoDEtail));
                    return permissionGroupService.listDetail(dtoDEtail, page.getPageSize(), page.getPageNumber());
                } catch (Exception e) {
                    logger.error("findLazy", e);
                    return Lists.newArrayList();
                }
            }


            @Override
            public FunctionObjectDTO getRowData(String rowKey) {
                List<FunctionObjectDTO> tmp = (List<FunctionObjectDTO>) getWrappedData();
                Optional<FunctionObjectDTO> selected = tmp.stream()
                        .filter(x -> DataUtil.safeEqual(String.valueOf(x.getFunctionObjectId()), rowKey))
                        .findFirst();
                return selected.orElse(null);
            }

            @Override
            public Object getRowKey(FunctionObjectDTO object) {
                return object != null ? object.getFunctionObjectId() : null;
            }
        };

    }


    //    getter,setter


    public LazyDataModel<FunctionObjectDTO> getFunctionObjectDTOS() {
        return functionObjectDTOS;
    }

    public void setFunctionObjectDTOS(LazyDataModel<FunctionObjectDTO> functionObjectDTOS) {
        this.functionObjectDTOS = functionObjectDTOS;
    }

    public InputLazyFunctionGroupSearch getInputLazyFunctionGroupSearch() {
        return inputLazyFunctionGroupSearch;
    }

    public void setInputLazyFunctionGroupSearch(InputLazyFunctionGroupSearch inputLazyFunctionGroupSearch) {
        this.inputLazyFunctionGroupSearch = inputLazyFunctionGroupSearch;
    }

    public List<ApDomainDTO> getListStatus() {
        return listStatus;
    }

    public void setListStatus(List<ApDomainDTO> listStatus) {
        this.listStatus = listStatus;
    }

    public List<ApDomainDTO> getListUserObjectType() {
        return listUserObjectType;
    }

    public void setListUserObjectType(List<ApDomainDTO> listUserObjectType) {
        this.listUserObjectType = listUserObjectType;
    }

    public LazyDataModel<PermissionGroupDTO> getPermissionGroupDTOS() {
        return permissionGroupDTOS;
    }

    public void setPermissionGroupDTOS(LazyDataModel<PermissionGroupDTO> permissionGroupDTOS) {
        this.permissionGroupDTOS = permissionGroupDTOS;
    }

    public PermissionGroupDTO getPermissionGroupDTO() {
        return permissionGroupDTO;
    }

    public void setPermissionGroupDTO(PermissionGroupDTO permissionGroupDTO) {
        this.permissionGroupDTO = permissionGroupDTO;
    }

    public boolean isCreateState() {
        return isCreateState;
    }

    public void setCreateState(boolean createState) {
        isCreateState = createState;
    }

    public PermissionGroupDTO getDtoDEtail() {
        return dtoDEtail;
    }

    public void setDtoDEtail(PermissionGroupDTO dtoDEtail) {
        this.dtoDEtail = dtoDEtail;
    }

    public List<PermissionGroupDTO> getPermissionGroupDTOExportList() {
        return permissionGroupDTOExportList;
    }

    public void setPermissionGroupDTOExportList(List<PermissionGroupDTO> permissionGroupDTOExportList) {
        this.permissionGroupDTOExportList = permissionGroupDTOExportList;
    }

    public ApDomainService getApDomainService() {
        return apDomainService;
    }

    public void setApDomainService(ApDomainService apDomainService) {
        this.apDomainService = apDomainService;
    }

    public File getFileExport() {
        return fileExport;
    }

    public void setFileExport(File fileExport) {
        this.fileExport = fileExport;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public Map<String, String> getFunctionGruopStatusMap() {
        return functionGruopStatusMap;
    }

    public void setFunctionGruopStatusMap(Map<String, String> functionGruopStatusMap) {
        this.functionGruopStatusMap = functionGruopStatusMap;
    }

    public Map<String, String> getPermissionTypeMap() {
        return permissionTypeMap;
    }

    public void setPermissionTypeMap(Map<String, String> permissionTypeMap) {
        this.permissionTypeMap = permissionTypeMap;
    }

    public List<ActionDTO> getActionDTOList() {
        return actionDTOList;
    }

    public void setActionDTOList(List<ActionDTO> actionDTOList) {
        this.actionDTOList = actionDTOList;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRenderTemplateFile() {
        return renderTemplateFile;
    }

    public void setRenderTemplateFile(boolean renderTemplateFile) {
        this.renderTemplateFile = renderTemplateFile;
    }

    public boolean isRenderResultFileError() {
        return renderResultFileError;
    }

    public void setRenderResultFileError(boolean renderResultFileError) {
        this.renderResultFileError = renderResultFileError;
    }

    public List<PermissionGroupDTO> getImportExcelFuntionGroupDTOList() {
        return importExcelFuntionGroupDTOList;
    }

    public void setImportExcelFuntionGroupDTOList(List<PermissionGroupDTO> importExcelFuntionGroupDTOList) {
        this.importExcelFuntionGroupDTOList = importExcelFuntionGroupDTOList;
    }

    public byte[] getContentInBytes() {
        return contentInBytes;
    }

    public void setContentInBytes(byte[] contentInBytes) {
        this.contentInBytes = contentInBytes;
    }

    public ChainUserDTO getChainUserDTO() {
        return chainUserDTO;
    }

    public void setChainUserDTO(ChainUserDTO chainUserDTO) {
        this.chainUserDTO = chainUserDTO;
    }

    public List<PermissionGroupDTO> getListPermissionCode() {
        return listPermissionCode;
    }

    public void setListPermissionCode(List<PermissionGroupDTO> listPermissionCode) {
        this.listPermissionCode = listPermissionCode;
    }


}
